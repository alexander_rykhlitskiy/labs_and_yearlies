﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace laba_1
{
    abstract class Shape
    {
        public int xInitial, yInitial, xFinal, yFinal;
        public Color chosenColor;
        abstract public void drawShape(Bitmap someBitmap);
    }
}
