﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace laba_1
{
    class BuilderEllipse : Builder
    {
        public override Shape createShape(MouseEventArgs e, Color colorOfShape)
        {
            ShapeEllipse creatingShape = new ShapeEllipse();
            creatingShape.xInitial = e.X;
            creatingShape.yInitial = e.Y;
            creatingShape.chosenColor = colorOfShape;
            return creatingShape;
        }
    }
}
