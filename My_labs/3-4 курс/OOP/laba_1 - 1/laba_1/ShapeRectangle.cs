﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace laba_1
{
    class ShapeRectangle : Shape
    {
        public void transposition(ref int x, ref int y)
        {
            int temp;
            temp = x;
            x = y;
            y = temp;
        }
        public override void drawShape(Bitmap someBitmap)
        {
            int xInitialToDraw = xInitial, yInitialToDraw = yInitial,
                xFinalToDraw = xFinal, yFinalToDraw = yFinal;

            if (xInitialToDraw > xFinalToDraw)
                transposition(ref xInitialToDraw, ref xFinalToDraw);
            if (yInitialToDraw > yFinalToDraw)
                transposition(ref yInitialToDraw, ref yFinalToDraw);

            Graphics formGraphics = Graphics.FromImage(someBitmap);
            System.Drawing.Pen myPen;
            myPen = new Pen(chosenColor);
            formGraphics.DrawRectangle(myPen, new Rectangle(xInitialToDraw, yInitialToDraw, 
                xFinalToDraw - xInitialToDraw, yFinalToDraw - yInitialToDraw));
            myPen.Dispose();
            formGraphics.Dispose();
        }
    }
}
