﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace laba_1
{
    class ShapeTriangle : Shape
    {
        public override void drawShape(Bitmap someBitmap)
        {
            Graphics formGraphics = Graphics.FromImage(someBitmap);
            System.Drawing.Pen myPen;
            myPen = new Pen(chosenColor);
            PointF[] vertexes = new PointF[3];
            vertexes[0] = new PointF((xInitial + xFinal) / 2, yInitial);
            vertexes[1] = new PointF(xInitial, yFinal);
            vertexes[2] = new PointF(xFinal, yFinal);
            formGraphics.DrawPolygon(myPen, vertexes);
            myPen.Dispose();
            formGraphics.Dispose();
        }
    }
}
