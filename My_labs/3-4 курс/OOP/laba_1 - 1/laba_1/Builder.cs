﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace laba_1
{
    abstract class Builder
    {
        abstract public Shape createShape(MouseEventArgs e, Color colorOfShape);
    }
}
