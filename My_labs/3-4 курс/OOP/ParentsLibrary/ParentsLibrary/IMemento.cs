﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParentsLibrary
{
    public interface IMemento
    {
        void RestoreState();
    }
}
