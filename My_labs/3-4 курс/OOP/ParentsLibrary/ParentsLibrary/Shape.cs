﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

namespace ParentsLibrary
{
    [Serializable]
    public abstract class Shape //: IOriginator
    {
        public Shape(int xInitial, int yInitial, Color chosenColor, int thickness)
        {
            this.chosenColor = chosenColor;
            this.thickness = thickness;
            this.xInitial = xInitial;
            this.yInitial = yInitial;
            this.xFinal = xInitial;
            this.yFinal = yInitial;
        }
        public int xInitial, yInitial, xFinal, yFinal;
        public Color chosenColor;
        public int thickness = 0;
        abstract public void DrawShape(Bitmap someBitmap, UserShapeContextToDrawShape data = null);
        public void DrawShape(Bitmap someBitmap)
        {
            UserShapeContextToDrawShape data = new UserShapeContextToDrawShape(1, 1, 0, 0);
            DrawShape(someBitmap, data);
        }
        abstract public bool IsPointInOrOut(int xPoint, int yPoint, UserShapeContextToDrawShape data = null);
        public bool IsPointInOrOut(int xPoint, int yPoint)
        {
            UserShapeContextToDrawShape data = new UserShapeContextToDrawShape(1, 1, 0, 0);
            return IsPointInOrOut(xPoint, yPoint, data);

        }

        public MementoShape GetState()
        {
            return new MementoShape(this);
        }
    }
}
