﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace laba_1
{
    class MementoShape : IMemento
    {
        private readonly Color chosenColor;
        private readonly int thickness;
        private readonly int xInitial;
        private readonly int yInitial;
        private readonly int xFinal;
        private readonly int yFinal;
        private readonly int _numberOfShape;
        public int numberOfShape
        {
            get { return _numberOfShape; }
        }
        private readonly Shape originator;
        public MementoShape(Shape originator)
        {
            this.originator = originator;
            xInitial = originator.xInitial;
            yInitial = originator.yInitial;
            xFinal = originator.xFinal;
            yFinal = originator.yFinal;
            thickness = originator.thickness;
            chosenColor = originator.chosenColor;
        }
        public void RestoreState()
        {
            originator.xInitial = xInitial;
            originator.yInitial = yInitial;
            originator.xFinal = xFinal;
            originator.yFinal = yFinal;
            originator.thickness = thickness;
            originator.chosenColor = chosenColor;
        }
    }
}
