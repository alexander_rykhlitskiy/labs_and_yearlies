﻿namespace laba_1
{
    partial class OOP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chooseShape = new System.Windows.Forms.ComboBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.workArea = new System.Windows.Forms.PictureBox();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.colorButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.loadButton = new System.Windows.Forms.Button();
            this.thicknessNumeric = new System.Windows.Forms.NumericUpDown();
            this.undoButton = new System.Windows.Forms.Button();
            this.redoButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.workArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.thicknessNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // chooseShape
            // 
            this.chooseShape.FormattingEnabled = true;
            this.chooseShape.Location = new System.Drawing.Point(50, 5);
            this.chooseShape.Name = "chooseShape";
            this.chooseShape.Size = new System.Drawing.Size(106, 21);
            this.chooseShape.TabIndex = 0;
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(475, 6);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(55, 21);
            this.buttonClear.TabIndex = 1;
            this.buttonClear.Text = "clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // workArea
            // 
            this.workArea.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.workArea.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.workArea.Cursor = System.Windows.Forms.Cursors.Cross;
            this.workArea.Location = new System.Drawing.Point(3, 33);
            this.workArea.Name = "workArea";
            this.workArea.Size = new System.Drawing.Size(658, 356);
            this.workArea.TabIndex = 2;
            this.workArea.TabStop = false;
            this.workArea.MouseDown += new System.Windows.Forms.MouseEventHandler(this.workArea_MouseDown);
            this.workArea.MouseMove += new System.Windows.Forms.MouseEventHandler(this.workArea_MouseMove);
            this.workArea.MouseUp += new System.Windows.Forms.MouseEventHandler(this.workArea_MouseUp);
            this.workArea.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.workArea_PreviewKeyDown);
            // 
            // colorButton
            // 
            this.colorButton.Location = new System.Drawing.Point(162, 5);
            this.colorButton.Name = "colorButton";
            this.colorButton.Size = new System.Drawing.Size(31, 19);
            this.colorButton.TabIndex = 3;
            this.colorButton.UseVisualStyleBackColor = true;
            this.colorButton.Click += new System.EventHandler(this.colorButton_Click);
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(3, 4);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(42, 21);
            this.addButton.TabIndex = 4;
            this.addButton.Text = "add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(536, 6);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(55, 20);
            this.saveButton.TabIndex = 5;
            this.saveButton.Text = "save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // loadButton
            // 
            this.loadButton.Location = new System.Drawing.Point(597, 6);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(55, 20);
            this.loadButton.TabIndex = 6;
            this.loadButton.Text = "load";
            this.loadButton.UseVisualStyleBackColor = true;
            this.loadButton.Click += new System.EventHandler(this.recoverButton_Click);
            // 
            // thicknessNumeric
            // 
            this.thicknessNumeric.Location = new System.Drawing.Point(199, 5);
            this.thicknessNumeric.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.thicknessNumeric.Name = "thicknessNumeric";
            this.thicknessNumeric.Size = new System.Drawing.Size(53, 20);
            this.thicknessNumeric.TabIndex = 7;
            this.thicknessNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // undoButton
            // 
            this.undoButton.Location = new System.Drawing.Point(335, 5);
            this.undoButton.Name = "undoButton";
            this.undoButton.Size = new System.Drawing.Size(39, 19);
            this.undoButton.TabIndex = 8;
            this.undoButton.Text = "undo";
            this.undoButton.UseVisualStyleBackColor = true;
            this.undoButton.Click += new System.EventHandler(this.undoButton_Click);
            // 
            // redoButton
            // 
            this.redoButton.Location = new System.Drawing.Point(380, 5);
            this.redoButton.Name = "redoButton";
            this.redoButton.Size = new System.Drawing.Size(39, 19);
            this.redoButton.TabIndex = 9;
            this.redoButton.Text = "redo";
            this.redoButton.UseVisualStyleBackColor = true;
            this.redoButton.Click += new System.EventHandler(this.redoButton_Click);
            // 
            // OOP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(664, 392);
            this.Controls.Add(this.redoButton);
            this.Controls.Add(this.undoButton);
            this.Controls.Add(this.thicknessNumeric);
            this.Controls.Add(this.loadButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.colorButton);
            this.Controls.Add(this.workArea);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.chooseShape);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Name = "OOP";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "laba_1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OOP_FormClosing);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OOP_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.workArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.thicknessNumeric)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox chooseShape;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.PictureBox workArea;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Button colorButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.NumericUpDown thicknessNumeric;
        private System.Windows.Forms.Button undoButton;
        private System.Windows.Forms.Button redoButton;
    }
}

