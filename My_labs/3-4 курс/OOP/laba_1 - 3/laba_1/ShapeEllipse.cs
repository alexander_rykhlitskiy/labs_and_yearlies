﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace laba_1
{
    [Serializable]
    class ShapeEllipse : Shape
    {
        public ShapeEllipse(int xInitial, int yInitial, Color chosenColor, int thickness) : 
                       base(xInitial, yInitial, chosenColor, thickness)
        {
        }
//        private int vxInitial, vyInitial, vxFinal, vyFinal;
        //private void SetVertexes(UserShapeContextToDrawShape data)
        //{
        //    this.vxInitial = (int)(xInitial * data.scaleX) + data.shiftX;
        //    this.vyInitial = (int)(yInitial * data.scaleY) + data.shiftY;
        //    this.vxFinal = (int)(xFinal * data.scaleX) + data.shiftX;
        //    this.vyFinal = (int)(yFinal * data.scaleY) + data.shiftY;
        //}
        public override void DrawShape(Bitmap someBitmap, UserShapeContextToDrawShape data = null)
        {
        //    SetVertexes(data);
            Graphics formGraphics = Graphics.FromImage(someBitmap);
            System.Drawing.Pen myPen;
            myPen = new Pen(chosenColor, thickness);
            formGraphics.DrawEllipse(myPen, new Rectangle((int)(xInitial * data.scaleX + data.shiftX), 
                                                            (int)(yInitial * data.scaleY + data.shiftY), 
                                                            (int)((xFinal - xInitial) * data.scaleX), 
                                                            (int)((yFinal - yInitial) * data.scaleY)));
            myPen.Dispose();
            formGraphics.Dispose();
        }
        public override bool IsPointInOrOut(int xPoint, int yPoint, UserShapeContextToDrawShape data = null)
        {
            //(x^2 / a^2) + (y^2 / b^2) = 1
            int vxInitial = (int)(xInitial * data.scaleX) + data.shiftX;
            int vyInitial = (int)(yInitial * data.scaleY) + data.shiftY;
            int vxFinal = (int)(xFinal * data.scaleX) + data.shiftX;
            int vyFinal = (int)(yFinal * data.scaleY) + data.shiftY;
            const int widthOfBorder = 5;
            bool result = false;
            int xCentre = (vxInitial + vxFinal) / 2;
            int yCentre = (vyInitial + vyFinal) / 2;
            int x = Math.Abs(xPoint - xCentre);
            int y = Math.Abs(yPoint - yCentre);
            double a = Math.Abs(vxFinal - vxInitial) / 2;
            double b = Math.Abs(vyFinal - vyInitial) / 2;

            a = a + widthOfBorder;
            b = b + widthOfBorder;
            double valueMax = (x * x) / (double)(a * a) + (y * y) / (double)(b * b);
            a = a - 2 * widthOfBorder;
            b = b - 2 * widthOfBorder;
            double valueMin = (x * x) / (double)(a * a) + (y * y) / (double)(b * b);
            if ((valueMax < 1) & (valueMin > 1))
            {
                result = true;
            }
            else result = false;
            return result;
        }
    }
}