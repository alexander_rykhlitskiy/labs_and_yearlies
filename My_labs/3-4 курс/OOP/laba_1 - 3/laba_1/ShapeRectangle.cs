﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace laba_1
{
    [Serializable]
    class ShapeRectangle : Shape
    {
        public ShapeRectangle(int xInitial, int yInitial, Color chosenColor, int thickness) : 
                         base(xInitial, yInitial, chosenColor, thickness)
        {
        }
        public void transposition(ref int x, ref int y)
        {
            int temp;
            temp = x;
            x = y;
            y = temp;
        }
      //  int vxInitial, vyInitial, vxFinal, vyFinal;
        //private void SetVertexes(UserShapeContextToDrawShape data)
        //{
        //    vxInitial = (int)(xInitial * data.scaleX) + data.shiftX;
        //    vyInitial = (int)(yInitial * data.scaleY) + data.shiftY;
        //    vxFinal = (int)(xFinal * data.scaleX) + data.shiftX;
        //    vyFinal = (int)(yFinal * data.scaleY) + data.shiftY;
        //}
        public override void DrawShape(Bitmap someBitmap, UserShapeContextToDrawShape data = null)
        {
            int xInitialToDraw = xInitial, yInitialToDraw = yInitial,
                xFinalToDraw = xFinal, yFinalToDraw = yFinal;

            if (xInitialToDraw > xFinalToDraw)
                transposition(ref xInitialToDraw, ref xFinalToDraw);
            if (yInitialToDraw > yFinalToDraw)
                transposition(ref yInitialToDraw, ref yFinalToDraw);
            
            Graphics formGraphics = Graphics.FromImage(someBitmap);
            System.Drawing.Pen myPen;
            myPen = new Pen(chosenColor, thickness);
            if (data.scaleX < 0)
                transposition(ref xInitialToDraw, ref xFinalToDraw);
            if (data.scaleY < 0)
                transposition(ref yInitialToDraw, ref yFinalToDraw);
            formGraphics.DrawRectangle(myPen, new Rectangle((int)(xInitialToDraw * data.scaleX) + data.shiftX,
                                                            (int)(yInitialToDraw * data.scaleY) + data.shiftY,
                                                            (int)((xFinalToDraw - xInitialToDraw) * data.scaleX),
                                                            (int)((yFinalToDraw - yInitialToDraw) * data.scaleY)));
            
            myPen.Dispose();
            formGraphics.Dispose();
        }
        public override bool IsPointInOrOut(int xPoint, int yPoint, UserShapeContextToDrawShape data = null)
        {
            int vxInitial = (int)(xInitial * data.scaleX) + data.shiftX;
            int vyInitial = (int)(yInitial * data.scaleY) + data.shiftY;
            int vxFinal = (int)(xFinal * data.scaleX) + data.shiftX;
            int vyFinal = (int)(yFinal * data.scaleY) + data.shiftY;
            bool result = false;
            const int halfWidthOfBorder = 10;
            int xCentre = (vxInitial + vxFinal) / 2;
            int yCentre = (vyInitial + vyFinal) / 2;
            int a = Math.Abs(vxInitial - vxFinal) / 2;
            int b = Math.Abs(vyInitial - vyFinal) / 2;
            if (((Math.Abs(xPoint - xCentre) < a + halfWidthOfBorder) & (Math.Abs(xPoint - xCentre) > a - halfWidthOfBorder)
                & (Math.Abs(yPoint - yCentre) < b + halfWidthOfBorder)) |
                ((Math.Abs(yPoint - yCentre) < b + halfWidthOfBorder) & (Math.Abs(yPoint - yCentre) > b - halfWidthOfBorder)
                & (Math.Abs(xPoint - xCentre) < a + halfWidthOfBorder)))
                result = true;
            else
                result = false;
            return result;
        }
    }
}
