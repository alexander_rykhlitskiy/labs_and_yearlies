﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

namespace laba_1
{
    public partial class OOP : Form
    {
        public const int numberOfBaseShapes = 3;


        List<Shape> shapeList = new List<Shape>(),
                    reserveShapeList = new List<Shape>();
        List<Builder> builderList = new List<Builder>();
        Bitmap mainBitmap;
        Color colorOfShape;
        bool addShapeBool = false;
        ActionContext actionContext = new ActionContext();
        List<IMemento> stateList = new List<IMemento>();
        int undoPointer = 0;

        public OOP()
        {
            InitializeComponent();
            builderList.Add(new BuilderTriangle("Triangle"));
            builderList.Add(new BuilderRectangle("Rectangle"));
            builderList.Add(new BuilderEllipse("Ellipse"));
            chooseShape.Items.Add(builderList[0].name);
            chooseShape.Items.Add(builderList[1].name);
            chooseShape.Items.Add(builderList[2].name);
            chooseShape.SelectedIndex = 0;
            mainBitmap = new Bitmap(workArea.Width, workArea.Height);
            colorOfShape = Color.Black;
            colorButton.BackColor = colorOfShape;
            SaveLoad.ReadBuilderFromFile(builderList, chooseShape.Items);
        }

        private void ClearScreen()
        {
            mainBitmap = new Bitmap(workArea.Width, workArea.Height);
            workArea.Image = mainBitmap;
        }
        private void FullClear()
        {
            stateList = new List<IMemento>();
            undoPointer = 0;
            ClearScreen();
            shapeList.Clear();
        }
        private void DrawShapeList()
        {
            for (int i = 0; i < shapeList.Count; i++)
            {
                shapeList[i].DrawShape(mainBitmap);
            }
            workArea.Image = mainBitmap;
        }
        private void buttonClear_Click(object sender, EventArgs e)
        {
            FullClear();
        }
        private void workArea_MouseDown(object sender, MouseEventArgs e)
        {
            actionContext.ready = true;
            Cursor.Current = actionContext.action.GetCursor();
            shapeList.Add(builderList[chooseShape.SelectedIndex].createShape(e, colorOfShape, (int)thicknessNumeric.Value));
        }
        private void workArea_MouseMove(object sender, MouseEventArgs e)
        {
            Cursor.Current = actionContext.action.GetCursor();
            ClearScreen();
            if (actionContext.ready)
                actionContext.action.ChangeShape(e, shapeList);
            else
            {
                actionContext.SetAction(e, shapeList);
                actionContext.action.MakePreparations(Cursor, shapeList, mainBitmap);
            }
            DrawShapeList();
        }

        private void workArea_MouseUp(object sender, MouseEventArgs e)
        {
            if (shapeList.Count != 0)
            {
                if ((shapeList[shapeList.Count - 1].xInitial == shapeList[shapeList.Count - 1].xFinal) &
                    (shapeList[shapeList.Count - 1].yInitial == shapeList[shapeList.Count - 1].yFinal))
                    shapeList.RemoveAt(shapeList.Count - 1);
                stateList.RemoveRange(undoPointer, stateList.Count - undoPointer);
                stateList.Add(shapeList[shapeList.Count - 1].GetState());
                undoPointer = stateList.Count;
            }

            actionContext.ready = false;
        }

        private void colorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                colorOfShape = colorDialog.Color;
                colorButton.BackColor = colorOfShape;
            }
        }
        private void addButton_Click(object sender, EventArgs e)
        {
            addShapeBool = !addShapeBool;
            if (addShapeBool)
            {
                this.Text = "Create your shape";
                addButton.Text = "ready";
                mainBitmap = new Bitmap(workArea.Width, workArea.Height);
                reserveShapeList = shapeList;
                shapeList = new List<Shape>();
                ClearScreen();
            }
            else
            {
                BuilderUserShape currentBuilder = new BuilderUserShape(chooseShape.Text, shapeList);
                builderList.Add(currentBuilder);
                this.Text = "laba_1";
                addButton.Text = "add";
                chooseShape.SelectedIndex = chooseShape.Items.Add(chooseShape.Text);
                shapeList = reserveShapeList;
                ClearScreen();
                DrawShapeList();
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            SaveLoad.SaveScreen(shapeList);
        }

        private void recoverButton_Click(object sender, EventArgs e)
        {
            FullClear();
            SaveLoad.LoadScreen(ref shapeList);
            DrawShapeList();
        }

        private void OOP_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveLoad.WriteBuilderInFile(numberOfBaseShapes, builderList);
        }
        private List<IMemento> SaveStates(List<Shape> shapeList)
        {
            List<IMemento> states = new List<IMemento>();
            foreach (Shape shape in shapeList)
            {
                states.Add(shape.GetState());
            }
            return states;
        }
        private void RestoreStates(List<IMemento> states)
        {
            foreach (IMemento state in states)
            {
                state.RestoreState();
            }
        }
        private void DoUndo()
        {
            if (undoPointer >= 2)
            {
                undoPointer--;
                stateList[undoPointer - 1].RestoreState();
                ClearScreen();
                DrawShapeList();
            }
        }
        private void undoButton_Click(object sender, EventArgs e)
        {
            DoUndo();
        }

        private void redoButton_Click(object sender, EventArgs e)
        {
            if ((undoPointer >= 0) & (undoPointer < stateList.Count))
            {
                undoPointer++;
                stateList[undoPointer - 1].RestoreState();
                ClearScreen();
                DrawShapeList();
            }
        }

        private void workArea_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Z)// & e.Control)
                DoUndo();
        }

        private void OOP_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 'z')// & e.Control)
                DoUndo();
        }
    }
}
