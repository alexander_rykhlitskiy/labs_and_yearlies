﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using ParentsLibrary;

namespace Trapezium
{
    delegate int delAbc();
    interface IAbcd
    {
     //   event delAbc eventAbc;
      //  int a;
    }
    [Serializable]
    class ShapeTrapezium : Shape
    {
        private event EventHandler ev;

        public ShapeTrapezium(int xInitial, int yInitial, Color chosenColor, int thickness) :
            base(xInitial, yInitial, chosenColor, thickness)
        {
        }
        const int numberOfVertexes = 4;
        private void CalculateVertexe(PointF[] vertexe, UserShapeContextToDrawShape data)
        {
            vertexe[0] = new PointF((int)((xInitial + (xFinal - xInitial) / 4) * data.scaleX) + data.shiftX, (int)(yInitial * data.scaleY) + data.shiftY);
            vertexe[1] = new PointF((int)((xFinal - (xFinal - xInitial) / 4) * data.scaleX) + data.shiftX, (int)(yInitial * data.scaleY) + data.shiftY);
            vertexe[2] = new PointF((int)(xFinal * data.scaleX) + data.shiftX, (int)(yFinal * data.scaleY) + data.shiftY);
            vertexe[3] = new PointF((int)(xInitial * data.scaleX) + data.shiftX, (int)(yFinal * data.scaleY) + data.shiftY);
        }
        public override void DrawShape(Bitmap someBitmap, UserShapeContextToDrawShape data = null)
        {
            Graphics formGraphics = Graphics.FromImage(someBitmap);
            System.Drawing.Pen myPen;
            myPen = new Pen(chosenColor, thickness);
            PointF[] vertexe = new PointF[numberOfVertexes];
            CalculateVertexe(vertexe, data);
            formGraphics.DrawPolygon(myPen, vertexe);
            myPen.Dispose();
            formGraphics.Dispose();
        }

        private void Swap(PointF[,] a)
        {
            for (int i = 0; i < a.Length / 2; i++)
            {
                PointF temp = a[0, i];
                a[0, i] = a[1, i];
                a[1, i] = temp;
            }
        }
        private void SetVertexes(PointF[,] tempVertexe, PointF[] vertexe)
        {
            const int widthOfBorder = 10;
            tempVertexe[0, 0].X = vertexe[0].X + widthOfBorder;
            tempVertexe[0, 0].Y = vertexe[0].Y + widthOfBorder;
            tempVertexe[0, 1].X = vertexe[1].X - widthOfBorder;
            tempVertexe[0, 1].Y = vertexe[1].Y + widthOfBorder;
            tempVertexe[0, 2].X = vertexe[2].X - widthOfBorder;
            tempVertexe[0, 2].Y = vertexe[2].Y - widthOfBorder;
            tempVertexe[0, 3].X = vertexe[3].X + widthOfBorder;
            tempVertexe[0, 3].Y = vertexe[3].Y - widthOfBorder;

            tempVertexe[1, 0].X = vertexe[0].X - widthOfBorder;
            tempVertexe[1, 0].Y = vertexe[0].Y - widthOfBorder;
            tempVertexe[1, 1].X = vertexe[1].X + widthOfBorder;
            tempVertexe[1, 1].Y = vertexe[1].Y - widthOfBorder;
            tempVertexe[1, 2].X = vertexe[2].X + widthOfBorder;
            tempVertexe[1, 2].Y = vertexe[2].Y + widthOfBorder;
            tempVertexe[1, 3].X = vertexe[3].X - widthOfBorder;
            tempVertexe[1, 3].Y = vertexe[3].Y + widthOfBorder;
            if (vertexe[2].Y < vertexe[0].Y)
            {
                Swap(tempVertexe);
            }
        }
        private void Swap(ref PointF a, ref PointF b)
        {
            PointF temp = a;
            a = b;
            b = temp;
        }
        private void SetTriangleVertexesOrder(PointF[] tempVertexe)
        {
            if (tempVertexe[1].X < tempVertexe[0].X)
            {
                Swap(ref tempVertexe[0], ref tempVertexe[1]);
                Swap(ref tempVertexe[2], ref tempVertexe[3]);
            }
            if (tempVertexe[3].Y < tempVertexe[0].Y)
            {
                Swap(ref tempVertexe[0], ref tempVertexe[3]);
                Swap(ref tempVertexe[1], ref tempVertexe[2]);
            }
        }
        private bool ScalarProduct(PointF[,] tempVertexe, int xPoint, int yPoint, int index)
        {
            int[] directrix = new int[2];
            int[] normal = new int[2];
            int dXn = 0;
            bool result = true;
            for (int i = 0; i < numberOfVertexes; i++)
            {
                directrix[0] = (int)(tempVertexe[index, i].X - xPoint);
                directrix[1] = (int)(tempVertexe[index, i].Y - yPoint);
                normal[0] = (int)(tempVertexe[index, (i + 1) % numberOfVertexes].Y - tempVertexe[index, i].Y);
                normal[1] = (int)(tempVertexe[index, i].X - tempVertexe[index, (i + 1) % numberOfVertexes].X);
                dXn = directrix[0] * normal[0] + directrix[1] * normal[1];
                if (dXn < 0)
                {
                    result = false;
                    break;
                }
            }
            return result;
        }
        public override bool IsPointInOrOut(int xPoint, int yPoint, UserShapeContextToDrawShape data = null)
        {
            bool result = true;
            PointF[] vertexe = new PointF[numberOfVertexes];
            CalculateVertexe(vertexe, data);
            SetTriangleVertexesOrder(vertexe);
            PointF[,] tempVertexe = new PointF[2, numberOfVertexes];
            SetVertexes(tempVertexe, vertexe);
            //for (int i = 0; i < numberOfVertexes; i++)
            //    tempVertexe[1, i] = vertexe[i];
            result = !ScalarProduct(tempVertexe, xPoint, yPoint, 0)
                & ScalarProduct(tempVertexe, xPoint, yPoint, 1);
            return result;
        }
    }
}
