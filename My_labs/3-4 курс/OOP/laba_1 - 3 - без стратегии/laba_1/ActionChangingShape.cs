﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;
namespace laba_1
{
    abstract class ActionChangingShape
    {
        private int propNumberOfShape;
        public int numberOfShape
        {
            get { return propNumberOfShape; }
            set { propNumberOfShape = value; }
        }
        public abstract void ChangeShape(MouseEventArgs e, List<Shape> shapeList);
        public abstract bool AboveArea(MouseEventArgs e, List<Shape> shapeList);
    }
}
