﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace laba_1
{
    [Serializable]
    class ShapeEllipse : Shape
    {
        public ShapeEllipse(int xInitial, int yInitial, Color chosenColor, int thickness) : 
                       base(xInitial, yInitial, chosenColor, thickness)
        {
        }
        public override void DrawShape(Bitmap someBitmap, UserShapeContextToDrawShape data = null)
        {
            Graphics formGraphics = Graphics.FromImage(someBitmap);
            System.Drawing.Pen myPen;
            myPen = new Pen(chosenColor, thickness);
            formGraphics.DrawEllipse(myPen, new Rectangle((int)(xInitial * data.scaleX) + data.shiftX, 
                                                          (int)(yInitial * data.scaleY) + data.shiftY, 
                                                          (int)((xFinal - xInitial) * data.scaleX), 
                                                          (int)((yFinal - yInitial) * data.scaleY)));
            myPen.Dispose();
            formGraphics.Dispose();
        }
        public override bool IsPointInOrOut(int xPoint, int yPoint)
        {
            //(x^2 / a^2) + (y^2 / b^2) = 1
            const int widthOfBorder = 5;
            bool result = false;
            int xCentre = (xInitial + xFinal) / 2;
            int yCentre = (yInitial + yFinal) / 2;
            int x = Math.Abs(xPoint - xCentre);
            int y = Math.Abs(yPoint - yCentre);
            double a = Math.Abs(xFinal - xInitial) / 2;
            double b = Math.Abs(yFinal - yInitial) / 2;

            a = a + widthOfBorder;
            b = b + widthOfBorder;
            double valueMax = (x * x) / (double)(a * a) + (y * y) / (double)(b * b);
            a = a - 2 * widthOfBorder;
            b = b - 2 * widthOfBorder;
            double valueMin = (x * x) / (double)(a * a) + (y * y) / (double)(b * b);
            if ((valueMax < 1) & (valueMin > 1))
            {
                result = true;
            }
            else result = false;
            return result;
        }
    }
}