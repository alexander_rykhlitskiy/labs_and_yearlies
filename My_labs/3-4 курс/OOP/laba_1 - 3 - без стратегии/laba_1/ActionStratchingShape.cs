﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace laba_1
{
    class ActionStratchingShape : ActionChangingShape
    {
        //private int propNumberOfShape;
        //public int numberOfShape
        //{
        //    get { return propNumberOfShape; }
        //    set { propNumberOfShape = value; }
        //}
        public override bool AboveArea(MouseEventArgs e, List<Shape> shapeList)
        {
            bool result = false;
            const int halfCornerWidth = 10;
            for (int i = 0; i < shapeList.Count; i++)
            {
                if ((e.X < shapeList[i].xFinal + halfCornerWidth) & (e.X > shapeList[i].xFinal - halfCornerWidth) &
                    (e.Y < shapeList[i].yFinal + halfCornerWidth) & (e.Y > shapeList[i].yFinal - halfCornerWidth))
                {
                    result = true;
                    if (e.Button != MouseButtons.Left)
                        numberOfShape = i;
                    break;
                }
                else
                    result = false;
            }
            return result;
        }
        public override void ChangeShape(MouseEventArgs e, List<Shape> shapeList)
        {
            shapeList[numberOfShape].xFinal = e.X;
            shapeList[numberOfShape].yFinal = e.Y;
        }
        public void DrawMarker(Shape shape, Bitmap someBitmap, Color color)
        {
            const int sideOfMarker = 5;
            Graphics formGraphics = Graphics.FromImage(someBitmap);
            System.Drawing.Pen myPen;
            myPen = new Pen(color);
            formGraphics.DrawRectangle(myPen, new Rectangle(shape.xFinal - sideOfMarker,
                                                            shape.yFinal - sideOfMarker,
                                                            sideOfMarker * 2, sideOfMarker * 2));
            myPen.Dispose();
            formGraphics.Dispose();
        }
    }
}
