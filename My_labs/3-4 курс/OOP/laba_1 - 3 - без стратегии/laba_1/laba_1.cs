﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

namespace laba_1
{
    public partial class OOP : Form
    {
        public const int numberOfBaseShapes = 3;
        bool drawOrNot = false;
        bool pullOrNot = false;
        bool stratchOrNot = false;

        List<Shape> shapeList = new List<Shape>(),
                    reserveShapeList = new List<Shape>();
        List<Builder> builderList = new List<Builder>();
        Bitmap mainBitmap;
        Color colorOfShape;
        bool addShapeBool = false;

        ActionPullingShape pullingShape = new ActionPullingShape();
        ActionStratchingShape stratchingShape = new ActionStratchingShape();
        ActionDrawingShape drawingShape = new ActionDrawingShape();
        List<MementoShape> undoMementoShape;
        List<MementoShape> redoMementoShape;
        CaretakerShapeList caretakerShapeList = new CaretakerShapeList();
        public OOP()
        {
            InitializeComponent();
            builderList.Add(new BuilderTriangle("Triangle"));
            builderList.Add(new BuilderRectangle("Rectangle"));
            builderList.Add(new BuilderEllipse("Ellipse"));
            chooseShape.Items.Add(builderList[0].name);
            chooseShape.Items.Add(builderList[1].name);
            chooseShape.Items.Add(builderList[2].name);
            chooseShape.SelectedIndex = 0;
            mainBitmap = new Bitmap(workArea.Width, workArea.Height);
            colorOfShape = Color.Black;
            colorButton.BackColor = colorOfShape;
            SaveLoad.ReadBuilderFromFile(builderList, chooseShape.Items);

            undoMementoShape = new List<MementoShape>();
            redoMementoShape = new List<MementoShape>();
        }

        private void ClearScreen()
        {
          //  Graphics formGraphics = Graphics.FromImage(mainBitmap);
         //   formGraphics.Clear(Color.White);
            mainBitmap = new Bitmap(workArea.Width, workArea.Height);
            workArea.Image = mainBitmap;
        }
        private void FullClear()
        {
            pullingShape = new ActionPullingShape();
            stratchingShape = new ActionStratchingShape();
            undoMementoShape = new List<MementoShape>();
            ClearScreen();
            shapeList.Clear();
        }
        private void DrawShapeList()
        {
            for (int i = 0; i < shapeList.Count; i++)
            {
                shapeList[i].DrawShape(mainBitmap);
            }
            workArea.Image = mainBitmap;
        }
        private void buttonClear_Click(object sender, EventArgs e)
        {
            FullClear();
        }

        private void workArea_MouseDown(object sender, MouseEventArgs e)
        {
            if (stratchingShape.AboveArea(e, shapeList))
                stratchOrNot = true;
            else
                if (pullingShape.AboveArea(e, shapeList))
                {
                    pullingShape.pastStateCursor = e;
                    pullOrNot = true;
                }
                else
                //    if ((!pullingShape.Above(e, shapeList)) & (!stratchingShape.AboveCorner(e, shapeList)))
                {
                    shapeList.Add(builderList[chooseShape.SelectedIndex].createShape(e, colorOfShape, (int)thicknessNumeric.Value));
                    drawOrNot = true;
                }
        }
        private void workArea_MouseMove(object sender, MouseEventArgs e)
        {
            if (drawOrNot)
            {
                ClearScreen();
                drawingShape.ChangeShape(e, shapeList);
                DrawShapeList();
            }
            else
            if (stratchOrNot)
            {
                ClearScreen();
                stratchingShape.ChangeShape(e, shapeList);
                DrawShapeList();
            }
            else
                if (pullOrNot)
                {
                    ClearScreen();    
                    pullingShape.ChangeShape(e, shapeList);
                    DrawShapeList();
                }
                else

            if (stratchingShape.AboveArea(e, shapeList))
            {
             //   ClearScreen();
                Cursor = Cursors.SizeNWSE;
                stratchingShape.DrawMarker(shapeList[stratchingShape.numberOfShape], mainBitmap, Color.Red);
                workArea.Image = mainBitmap;
           //     DrawShapeList();
            }
            else
                if (pullingShape.AboveArea(e, shapeList))
                {
                //    ClearScreen();
                    Cursor = Cursors.SizeAll;
                    stratchingShape.DrawMarker(shapeList[pullingShape.numberOfShape], mainBitmap, Color.Red);
                    workArea.Image = mainBitmap;
              //      DrawShapeList();
                }
                else
                {
                    ClearScreen();
                    Cursor = Cursors.Default;
                    DrawShapeList();
                }

           // pullingShape.pastStateCursor = e;
        }

        private void workArea_MouseUp(object sender, MouseEventArgs e)
        {
            if (pullOrNot | drawOrNot | stratchOrNot)
                undoMementoShape.Add(shapeList[0].CreateMemento());//0 заменить на number
            drawOrNot = false;
            pullOrNot = false;
            stratchOrNot = false;
        }

        private void colorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                colorOfShape = colorDialog.Color;
                colorButton.BackColor = colorOfShape;
            }
        }
        private void addButton_Click(object sender, EventArgs e)
        {
            addShapeBool = !addShapeBool;
            if (addShapeBool)
            {
                this.Text = "Create your shape";
                addButton.Text = "ready";
               // formGraphics.Clear(Color.White);
                mainBitmap = new Bitmap(workArea.Width, workArea.Height);

                reserveShapeList = shapeList;
                shapeList = new List<Shape>();
                ClearScreen();
            }
            else
            {
                BuilderUserShape currentBuilder = new BuilderUserShape(chooseShape.Text, shapeList);
                builderList.Add(currentBuilder);
                this.Text = "laba_1";
                addButton.Text = "add";
                chooseShape.SelectedIndex = chooseShape.Items.Add(chooseShape.Text);
                shapeList = reserveShapeList;
                ClearScreen();
                DrawShapeList();
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            SaveLoad.SaveScreen(shapeList);
        }

        private void recoverButton_Click(object sender, EventArgs e)
        {
            FullClear();
            SaveLoad.LoadScreen(ref shapeList);
            DrawShapeList();
        }

        private void OOP_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveLoad.WriteBuilderInFile(numberOfBaseShapes, builderList);
        }

        private void undoButton_Click(object sender, EventArgs e)
        {
            if (undoMementoShape.Count >= 2)
            {
                int c = undoMementoShape.Count;
                shapeList[undoMementoShape[c - 2].numberOfShape].SetMemento(undoMementoShape[c - 2]);
                redoMementoShape.Add(undoMementoShape[c - 1]);
                undoMementoShape.RemoveAt(c - 1);
                //if (undoMementoShape.Count == 1)
                //{
                //    undoMementoShape.RemoveAt(0);
                //    undoMementoShape.Add(shapeList[0].CreateMemento());
                //}
                ClearScreen();
                DrawShapeList();
            }
        }

        private void redoButton_Click(object sender, EventArgs e)
        {
            if (redoMementoShape.Count >= 1)
            {
                int c = redoMementoShape.Count;
                undoMementoShape.Add(redoMementoShape[c - 1]);
                shapeList[redoMementoShape[c - 1].numberOfShape].SetMemento(redoMementoShape[c - 1]);
                redoMementoShape.RemoveAt(c - 1);
                ClearScreen();
                DrawShapeList();
            }
        }
    }
}
