﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace laba_1
{
    class ActionPullingShape : ActionChangingShape
    {
        private MouseEventArgs propPastStateCursor;
        public MouseEventArgs pastStateCursor
        {
         //   get { return propPastStateCursor; }
            set { propPastStateCursor = value; }
        }
        //private int propNumberOfShape;
        //public int numberOfShape
        //{
        //    get { return propNumberOfShape; }
        //    set { propNumberOfShape = value; }
        //}

        public override bool AboveArea(MouseEventArgs e, List<Shape> shapeList)
        {
            bool result = false;
            if (shapeList.Count != 0)//crutch
            {
                for (int i = 0; i < shapeList.Count; i++)
                {
                    result = shapeList[i].IsPointInOrOut(e.X, e.Y);
                    if (result == true)
                    {
                        if (e.Button != MouseButtons.Left)
                            numberOfShape = i;
                        break;
                    }
                }
            }
            return result;
        }
        public override void ChangeShape(MouseEventArgs e, List<Shape> shapeList)
        {
            shapeList[numberOfShape].xInitial += (e.X - propPastStateCursor.X);
            shapeList[numberOfShape].xFinal += (e.X - propPastStateCursor.X);
            shapeList[numberOfShape].yInitial += (e.Y - propPastStateCursor.Y);
            shapeList[numberOfShape].yFinal += (e.Y - propPastStateCursor.Y);
            pastStateCursor = e;
        }
    }
}
