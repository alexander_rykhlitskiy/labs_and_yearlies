﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

namespace laba_1
{
    [Serializable]
    abstract class Shape
    {
        public Shape(int xInitial, int yInitial, Color chosenColor, int thickness)
        {
            this.chosenColor = chosenColor;
            this.thickness = thickness;
            this.xInitial = xInitial;
            this.yInitial = yInitial;
            this.xFinal = xInitial;
            this.yFinal = yInitial;
        }
        public int xInitial, yInitial, xFinal, yFinal;
        public Color chosenColor;
        public int thickness = 0;
        abstract public void DrawShape(Bitmap someBitmap, UserShapeContextToDrawShape data = null);
        public void DrawShape(Bitmap someBitmap)
        {
            UserShapeContextToDrawShape data = new UserShapeContextToDrawShape(1, 1, 0, 0);
            DrawShape(someBitmap, data);
        }
        abstract public bool IsPointInOrOut(int xPoint, int yPoint);
        public MementoShape CreateMemento()
        {
            return (new MementoShape(xInitial, yInitial, xFinal, yFinal, chosenColor, thickness, 0));
        }                                                               //изменить 0 на numberOfShape
        public void SetMemento(MementoShape memento)
        {
            this.xInitial = memento.xInitial;
            this.yInitial = memento.yInitial;
            this.xFinal = memento.xFinal;
            this.yFinal = memento.yFinal;
            this.chosenColor = memento.chosenColor;
            this.thickness = memento.thickness;
        }

    }
}
