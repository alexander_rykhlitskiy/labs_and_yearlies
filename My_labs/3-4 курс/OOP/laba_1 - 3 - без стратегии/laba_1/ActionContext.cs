﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;
namespace laba_1
{
    class ActionContext
    {
        private ActionChangingShape _action;
        ActionChangingShape action
        {
            set { _action = value; }
            get { return _action; }
        }
        public bool ChooseAction(MouseEventArgs e, List<Shape> shapeList)
        {
            bool result = false;

            return result;
        }
        public override bool PullingAbove(MouseEventArgs e, List<Shape> shapeList)
        {
            bool result = false;
            if (shapeList.Count != 0)//crutch
            {
                for (int i = 0; i < shapeList.Count; i++)
                {
                    result = shapeList[i].IsPointInOrOut(e.X, e.Y);
                    if (result == true)
                    {
                        if (e.Button != MouseButtons.Left)
                            action.numberOfShape = i;
                        break;
                    }
                }
            }
            return result;
        }
        public override bool StratchingAbove(MouseEventArgs e, List<Shape> shapeList)
        {
            bool result = false;
            const int halfCornerWidth = 10;
            for (int i = 0; i < shapeList.Count; i++)
            {
                if ((e.X < shapeList[i].xFinal + halfCornerWidth) & (e.X > shapeList[i].xFinal - halfCornerWidth) &
                    (e.Y < shapeList[i].yFinal + halfCornerWidth) & (e.Y > shapeList[i].yFinal - halfCornerWidth))
                {
                    result = true;
                    if (e.Button != MouseButtons.Left)
                        action.numberOfShape = i;
                    break;
                }
                else
                    result = false;
            }
            return result;
        }
    }
}
