﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;
using System.Windows.Forms;
namespace laba_1
{
    abstract class ActionChangingShape
    {
        private int propNumberOfShape;
        public int numberOfShape
        {
            get { return propNumberOfShape; }
            set { propNumberOfShape = value; }
        }
        public ActionChangingShape(int numberOfShape)
        {
            this.numberOfShape = numberOfShape;
        }
        public ActionChangingShape()
        {
        }
        public abstract void ChangeShape(MouseEventArgs e, List<Shape> shapeList);
        public abstract Cursor MakePreparations(Cursor cursor, List<Shape> shapeList, Bitmap mainBitmap);
        public abstract Cursor GetCursor();
        public void DrawMarker(Shape shape, Bitmap someBitmap, Color color)
        {
            const int sideOfMarker = 5;
            Graphics formGraphics = Graphics.FromImage(someBitmap);
            System.Drawing.Pen myPen;
            myPen = new Pen(color);
            formGraphics.DrawRectangle(myPen, new Rectangle(shape.xFinal - sideOfMarker,
                                                            shape.yFinal - sideOfMarker,
                                                            sideOfMarker * 2, sideOfMarker * 2));
            myPen.Dispose();
            formGraphics.Dispose();
        }
    }
}
