﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace laba_1
{
    class MementoShape
    {
        public int xInitial, yInitial, xFinal, yFinal;
        public Color chosenColor;
        public int thickness;
        public int numberOfShape;
        public MementoShape(int xInitial, int yInitial, int xFinal, int yFinal,
                            Color chosenColor, int thickness, int numberOfShape)
        {
            this.xInitial = xInitial;
            this.yInitial = yInitial;
            this.xFinal = xFinal;
            this.yFinal = yFinal;
            this.chosenColor = chosenColor;
            this.thickness = thickness;
            this.numberOfShape = numberOfShape;
        }
    }
}
