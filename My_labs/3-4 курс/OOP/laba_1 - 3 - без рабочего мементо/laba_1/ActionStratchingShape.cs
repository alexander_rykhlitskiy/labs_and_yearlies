﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace laba_1
{
    class ActionStratchingShape : ActionChangingShape
    {
        //private int propNumberOfShape;
        //public int numberOfShape
        //{
        //    get { return propNumberOfShape; }
        //    set { propNumberOfShape = value; }
        //}
        public ActionStratchingShape(int numberOfShape)
            : base(numberOfShape)
        {
            this.numberOfShape = numberOfShape;
        }
        public override void ChangeShape(MouseEventArgs e, List<Shape> shapeList)
        {
            shapeList[numberOfShape].xFinal = e.X;
            shapeList[numberOfShape].yFinal = e.Y;
        }
        public override Cursor MakePreparations(Cursor cursor, List<Shape> shapeList, System.Drawing.Bitmap mainBitmap)
        {
            DrawMarker(shapeList[numberOfShape], mainBitmap, Color.Red);
            return GetCursor();
        }
        public override Cursor GetCursor()
        {
            return Cursors.SizeNWSE;
        }
    }
}
