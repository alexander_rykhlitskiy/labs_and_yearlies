﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace laba_1
{
    [Serializable]
    class ShapeRectangle : Shape
    {
        public ShapeRectangle(int xInitial, int yInitial, Color chosenColor, int thickness) : 
                         base(xInitial, yInitial, chosenColor, thickness)
        {
        }
        public void transposition(ref int x, ref int y)
        {
            int temp;
            temp = x;
            x = y;
            y = temp;
        }
        public override void DrawShape(Bitmap someBitmap, UserShapeContextToDrawShape data = null)
        {
            int xInitialToDraw = xInitial, yInitialToDraw = yInitial,
                xFinalToDraw = xFinal, yFinalToDraw = yFinal;

            if (xInitialToDraw > xFinalToDraw)
                transposition(ref xInitialToDraw, ref xFinalToDraw);
            if (yInitialToDraw > yFinalToDraw)
                transposition(ref yInitialToDraw, ref yFinalToDraw);
            
            Graphics formGraphics = Graphics.FromImage(someBitmap);
            System.Drawing.Pen myPen;
            myPen = new Pen(chosenColor, thickness);
            if (data.scaleX < 0)
                transposition(ref xInitialToDraw, ref xFinalToDraw);
            if (data.scaleY < 0)
                transposition(ref yInitialToDraw, ref yFinalToDraw);
            formGraphics.DrawRectangle(myPen, new Rectangle((int)(xInitialToDraw * data.scaleX) + data.shiftX,
                                                            (int)(yInitialToDraw * data.scaleY) + data.shiftY,
                                                            (int)((xFinalToDraw - xInitialToDraw) * data.scaleX),
                                                            (int)((yFinalToDraw - yInitialToDraw) * data.scaleY)));
            
            myPen.Dispose();
            formGraphics.Dispose();
        }
        public override bool IsPointInOrOut(int xPoint, int yPoint)
        {
            bool result = false;
            const int halfWidthOfBorder = 10;
            int xCentre = (xInitial + xFinal) / 2;
            int yCentre = (yInitial + yFinal) / 2;
            int a = Math.Abs(xInitial - xFinal) / 2;
            int b = Math.Abs(yInitial - yFinal) / 2;
            if (((Math.Abs(xPoint - xCentre) < a + halfWidthOfBorder) & (Math.Abs(xPoint - xCentre) > a - halfWidthOfBorder)
                & (Math.Abs(yPoint - yCentre) < b + halfWidthOfBorder)) |
                ((Math.Abs(yPoint - yCentre) < b + halfWidthOfBorder) & (Math.Abs(yPoint - yCentre) > b - halfWidthOfBorder)
                & (Math.Abs(xPoint - xCentre) < a + halfWidthOfBorder)))
                result = true;
            else
                result = false;
            return result;
        }
    }
}
