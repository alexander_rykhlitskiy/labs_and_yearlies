﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace laba_5
{
    class IntervalDay : Interval
    {
        public IntervalDay(int multiplier)
            : base(multiplier)
        {}
        public override DateTime GetDateTime(int count)
        {
            DateTime result = DateTime.Now;
            result = result.AddDays(-count * this.multiplier);
            return result;
        }
        public override int GetCount(DateTime dateTime)
        {
            int result = 0;
            DateTime dateTimeNow = DateTime.Now;
            while (dateTime.CompareTo(dateTimeNow) < 0)
            {
                dateTime = dateTime.AddDays(this.multiplier);
                result++;
            }
            return result;
        }
    }
}
