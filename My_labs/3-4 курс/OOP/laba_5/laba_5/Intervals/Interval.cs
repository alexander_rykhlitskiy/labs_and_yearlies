﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace laba_5
{
    abstract class Interval
    {
        protected int multiplier { get; set; }
        public Interval(int multiplier)
        {
            this.multiplier = multiplier;
        }
        public abstract DateTime GetDateTime(int count);
        public abstract int GetCount(DateTime dateTime);
    }
}
