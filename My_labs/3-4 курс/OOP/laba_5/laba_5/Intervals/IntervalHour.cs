﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace laba_5
{
    class IntervalHour : Interval
    {
        public IntervalHour(int multiplier)
            : base(multiplier)
        {}
        public override DateTime GetDateTime(int count)
        {
            DateTime result = DateTime.Now;
            result = result.AddHours(-count * this.multiplier);
            return result;
        }
        public override int GetCount(DateTime dateTime)
        {
            int result = 0;
            DateTime dateTimeNow = DateTime.Now;
            while (dateTime.CompareTo(dateTimeNow) < 0)
            {
                dateTime = dateTime.AddHours(this.multiplier);
                result++;
            }
            return result;
        }
    }
}
