﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace laba_5
{
    abstract class Builder
    {
        public abstract Interval CreateInterval(int multiplier);
    }
}
