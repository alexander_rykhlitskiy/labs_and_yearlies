﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace laba_5
{
    class BuilderYear : Builder
    {
        public override Interval CreateInterval(int multiplier)
        {
            return new IntervalYear(multiplier);
        }
    }
}
