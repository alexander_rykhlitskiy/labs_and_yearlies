﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace laba_5
{
    public partial class MainForm : Form
    {
        private void InitializeComboBox(ComboBox comboBox)
        {
            comboBox.Items.Add("Second");
            comboBox.Items.Add("Hour");
            comboBox.Items.Add("Day");
            comboBox.Items.Add("Week");
            comboBox.Items.Add("Month");
            comboBox.Items.Add("Year");
        }
        public MainForm()
        {
            InitializeComponent();
            InitializeComboBox(initialIntervalType);
            InitializeComboBox(finalIntervalType);
            InitializeComboBox(finalToAnotherIntervalType);
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            if ((initialIntervalType.SelectedIndex != -1) & (finalIntervalType.SelectedIndex != -1)
                & (finalToAnotherIntervalType.SelectedIndex != -1))
            {
                DateRange from = new DateRange((int)initialIntervalMultiplier.Value,
                    initialIntervalType.SelectedIndex, (int)initialDateRangeCount.Value);
                DateRange to = from.ConvertTo((int)finalIntervalMultiplier.Value, finalIntervalType.SelectedIndex);
                finalDateRangeCount.Value = to.count;
                DateRange fromTo = to.ConvertTo((int)finalToAnotherIntervalMultiplier.Value, finalToAnotherIntervalType.SelectedIndex);
                finalToAnotherDateRangeCount.Value = fromTo.count;
            }
            else
                MessageBox.Show("Incorrect interval name");
        }
    }
}
