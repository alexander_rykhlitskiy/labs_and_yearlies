﻿namespace laba_5
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OKButton = new System.Windows.Forms.Button();
            this.initialIntervalType = new System.Windows.Forms.ComboBox();
            this.initialIntervalMultiplier = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.initialDateRangeCount = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.finalIntervalMultiplier = new System.Windows.Forms.NumericUpDown();
            this.finalDateRangeCount = new System.Windows.Forms.NumericUpDown();
            this.finalIntervalType = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.finalToAnotherIntervalMultiplier = new System.Windows.Forms.NumericUpDown();
            this.finalToAnotherDateRangeCount = new System.Windows.Forms.NumericUpDown();
            this.finalToAnotherIntervalType = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.initialIntervalMultiplier)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.initialDateRangeCount)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.finalIntervalMultiplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.finalDateRangeCount)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.finalToAnotherIntervalMultiplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.finalToAnotherDateRangeCount)).BeginInit();
            this.SuspendLayout();
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(6, 56);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(61, 21);
            this.OKButton.TabIndex = 0;
            this.OKButton.Text = "calculate";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // initialIntervalType
            // 
            this.initialIntervalType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.initialIntervalType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.initialIntervalType.DisplayMember = "Day";
            this.initialIntervalType.Location = new System.Drawing.Point(6, 19);
            this.initialIntervalType.Name = "initialIntervalType";
            this.initialIntervalType.Size = new System.Drawing.Size(106, 21);
            this.initialIntervalType.TabIndex = 1;
            this.initialIntervalType.Text = "Hour";
            // 
            // initialIntervalMultiplier
            // 
            this.initialIntervalMultiplier.Location = new System.Drawing.Point(180, 20);
            this.initialIntervalMultiplier.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.initialIntervalMultiplier.Minimum = new decimal(new int[] {
            2147483647,
            0,
            0,
            -2147483648});
            this.initialIntervalMultiplier.Name = "initialIntervalMultiplier";
            this.initialIntervalMultiplier.Size = new System.Drawing.Size(65, 20);
            this.initialIntervalMultiplier.TabIndex = 2;
            this.initialIntervalMultiplier.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.initialIntervalMultiplier);
            this.groupBox1.Controls.Add(this.initialDateRangeCount);
            this.groupBox1.Controls.Add(this.initialIntervalType);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(260, 96);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Initial daterange";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(118, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Multiplier:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(118, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Count:";
            // 
            // initialDateRangeCount
            // 
            this.initialDateRangeCount.Location = new System.Drawing.Point(180, 57);
            this.initialDateRangeCount.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.initialDateRangeCount.Minimum = new decimal(new int[] {
            2147483647,
            0,
            0,
            -2147483648});
            this.initialDateRangeCount.Name = "initialDateRangeCount";
            this.initialDateRangeCount.Size = new System.Drawing.Size(65, 20);
            this.initialDateRangeCount.TabIndex = 4;
            this.initialDateRangeCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.OKButton);
            this.groupBox2.Controls.Add(this.finalIntervalMultiplier);
            this.groupBox2.Controls.Add(this.finalDateRangeCount);
            this.groupBox2.Controls.Add(this.finalIntervalType);
            this.groupBox2.Location = new System.Drawing.Point(12, 114);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(260, 96);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Final daterange";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(118, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Multiplier:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(118, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Count:";
            // 
            // finalIntervalMultiplier
            // 
            this.finalIntervalMultiplier.Location = new System.Drawing.Point(180, 20);
            this.finalIntervalMultiplier.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.finalIntervalMultiplier.Minimum = new decimal(new int[] {
            2147483647,
            0,
            0,
            -2147483648});
            this.finalIntervalMultiplier.Name = "finalIntervalMultiplier";
            this.finalIntervalMultiplier.Size = new System.Drawing.Size(65, 20);
            this.finalIntervalMultiplier.TabIndex = 2;
            this.finalIntervalMultiplier.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // finalDateRangeCount
            // 
            this.finalDateRangeCount.Location = new System.Drawing.Point(180, 57);
            this.finalDateRangeCount.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.finalDateRangeCount.Minimum = new decimal(new int[] {
            2147483647,
            0,
            0,
            -2147483648});
            this.finalDateRangeCount.Name = "finalDateRangeCount";
            this.finalDateRangeCount.ReadOnly = true;
            this.finalDateRangeCount.Size = new System.Drawing.Size(65, 20);
            this.finalDateRangeCount.TabIndex = 4;
            // 
            // finalIntervalType
            // 
            this.finalIntervalType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.finalIntervalType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.finalIntervalType.FormattingEnabled = true;
            this.finalIntervalType.Location = new System.Drawing.Point(6, 19);
            this.finalIntervalType.Name = "finalIntervalType";
            this.finalIntervalType.Size = new System.Drawing.Size(106, 21);
            this.finalIntervalType.TabIndex = 1;
            this.finalIntervalType.Text = "Day";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.finalToAnotherIntervalMultiplier);
            this.groupBox3.Controls.Add(this.finalToAnotherDateRangeCount);
            this.groupBox3.Controls.Add(this.finalToAnotherIntervalType);
            this.groupBox3.Location = new System.Drawing.Point(10, 216);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(260, 96);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Final daterange";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(118, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Multiplier:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(118, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Count:";
            // 
            // finalToAnotherIntervalMultiplier
            // 
            this.finalToAnotherIntervalMultiplier.Location = new System.Drawing.Point(180, 20);
            this.finalToAnotherIntervalMultiplier.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.finalToAnotherIntervalMultiplier.Minimum = new decimal(new int[] {
            2147483647,
            0,
            0,
            -2147483648});
            this.finalToAnotherIntervalMultiplier.Name = "finalToAnotherIntervalMultiplier";
            this.finalToAnotherIntervalMultiplier.Size = new System.Drawing.Size(65, 20);
            this.finalToAnotherIntervalMultiplier.TabIndex = 2;
            this.finalToAnotherIntervalMultiplier.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // finalToAnotherDateRangeCount
            // 
            this.finalToAnotherDateRangeCount.Location = new System.Drawing.Point(180, 57);
            this.finalToAnotherDateRangeCount.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.finalToAnotherDateRangeCount.Minimum = new decimal(new int[] {
            2147483647,
            0,
            0,
            -2147483648});
            this.finalToAnotherDateRangeCount.Name = "finalToAnotherDateRangeCount";
            this.finalToAnotherDateRangeCount.ReadOnly = true;
            this.finalToAnotherDateRangeCount.Size = new System.Drawing.Size(65, 20);
            this.finalToAnotherDateRangeCount.TabIndex = 4;
            // 
            // finalToAnotherIntervalType
            // 
            this.finalToAnotherIntervalType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.finalToAnotherIntervalType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.finalToAnotherIntervalType.FormattingEnabled = true;
            this.finalToAnotherIntervalType.Location = new System.Drawing.Point(6, 19);
            this.finalToAnotherIntervalType.Name = "finalToAnotherIntervalType";
            this.finalToAnotherIntervalType.Size = new System.Drawing.Size(106, 21);
            this.finalToAnotherIntervalType.TabIndex = 1;
            this.finalToAnotherIntervalType.Text = "Hour";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 328);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OOP_5";
            ((System.ComponentModel.ISupportInitialize)(this.initialIntervalMultiplier)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.initialDateRangeCount)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.finalIntervalMultiplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.finalDateRangeCount)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.finalToAnotherIntervalMultiplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.finalToAnotherDateRangeCount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.ComboBox initialIntervalType;
        private System.Windows.Forms.NumericUpDown initialIntervalMultiplier;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown initialDateRangeCount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown finalIntervalMultiplier;
        private System.Windows.Forms.NumericUpDown finalDateRangeCount;
        private System.Windows.Forms.ComboBox finalIntervalType;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown finalToAnotherIntervalMultiplier;
        private System.Windows.Forms.NumericUpDown finalToAnotherDateRangeCount;
        private System.Windows.Forms.ComboBox finalToAnotherIntervalType;
    }
}

