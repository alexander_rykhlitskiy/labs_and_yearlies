﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace laba_5
{
    class DateRange
    {
        private Interval _interval;
        private Interval _baseInterval;
        private int _count;
        public int count { get { return _count; } }
        private int _baseCount;
        private Builder[] builderList = { new BuilderSecond(), new BuilderHour(), new BuilderDay(), 
                                        new BuilderWeek(), new BuilderMonth(), new BuilderYear() };
        public DateRange(int multiplier, int indexInterval, int count = 0, int baseCount = 0, Interval baseInterval = null)
        {
            this._interval = this.builderList[indexInterval].CreateInterval(multiplier);
            this._count = count;
            this._baseInterval = baseInterval;
            this._baseCount = baseCount;
        }
        public DateRange ConvertTo(int multiplier, int indexInterval)
        {
            //DateTime dateTime = _interval.GetDateTime(_count);
            //int count1 = result._interval.GetCount(dateTime);
            DateRange result;
            if (this._baseCount != 0 & this._baseInterval != null)
            {
                result = new DateRange(multiplier, indexInterval, 0, this._baseCount, this._baseInterval);
                result._count = result._interval.GetCount(this._baseInterval.GetDateTime(this._baseCount));
            }
            else
            {
                result = new DateRange(multiplier, indexInterval, 0, this._count, this._interval);
                result._count = result._interval.GetCount(this._interval.GetDateTime(this._count));
            }
            return result;
        }
    }
}
