﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using TrapeziumLibrary;
using ParentsLibrary;
using System.IO;
using System.Reflection;

namespace laba_1
{
    public partial class laba_1 : Form
    {
        Boolean drawOrNot = false;
        List<Shape> shapeList = new List<Shape>();
        List<Builder> builderList = new List<Builder>();
        Bitmap someBitmap;
        Color colorOfShape;
        public laba_1()
        {
            InitializeComponent();
            chooseShape.Items.Add("Triangle");
            chooseShape.Items.Add("Rectangle");
            chooseShape.Items.Add("Ellipse");
            chooseShape.SelectedIndex = 0;
            builderList.Add(new BuilderTriangle());
            builderList.Add(new BuilderRectangle());
            builderList.Add(new BuilderEllipse());
            someBitmap = new Bitmap(workArea.Width, workArea.Height);
            colorOfShape = Color.Black;
            colorButton.BackColor = colorOfShape;
        }

        private void buttonClear_Click_1(object sender, EventArgs e)
        {
            System.Drawing.Graphics formGraphics = workArea.CreateGraphics();
            formGraphics.Clear(Color.White);
            shapeList.Clear();
        }

        private void workArea_MouseDown(object sender, MouseEventArgs e)
        {
            shapeList.Add(builderList[chooseShape.SelectedIndex].createShape(e, colorOfShape));
            drawOrNot = true;

        }

        private void workArea_MouseMove(object sender, MouseEventArgs e)
        {
            if (drawOrNot)
            {
                someBitmap = new Bitmap(workArea.Width, workArea.Height);
                for (int i = 0; i < shapeList.Count - 1; i++)
                {
                    shapeList[i].drawShape(someBitmap);
                }
                shapeList[shapeList.Count - 1].xFinal = e.X;
                shapeList[shapeList.Count - 1].yFinal = e.Y;
                shapeList[shapeList.Count - 1].drawShape(someBitmap);
                workArea.Image = someBitmap;
            }
        }

        private void workArea_MouseUp(object sender, MouseEventArgs e)
        {
            shapeList[shapeList.Count - 1].drawShape(someBitmap);
            drawOrNot = false;
        }

        private void colorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                colorOfShape = colorDialog.Color;
                colorButton.BackColor = colorOfShape;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            {
              //  bool foundShapeIn = false;
                Assembly newShapeAssembly = null;
                try
                {
                    newShapeAssembly = Assembly.LoadFrom("TrapeziumLibrary.dll");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                  //  return foundShapeIn;
                }
                //Type builder = typeof(Shape);
                //foreach (Type t in assemblyTypes)
                //{
                //    if (t.BaseType != null)
                //    {
                //        if (t.BaseType == typeof(ShapeCreator))
                //        {
                //            Object shapeBuilder = Activator.CreateInstance(t, new object[] { "" });
                //            ShapeCreator.Add((ShapeCreator)shapeBuilder);
                //        }
                //    }
                //}
                foreach (Type t in newShapeAssembly.GetTypes())
                {
                    if (t.IsSubclassOf(typeof(Builder)))
                    {
                        Object shapeBuilder = Activator.CreateInstance(t);
                        builderList.Add((Builder)shapeBuilder);
                        chooseShape.Items.Add("Trapezium");
                    }
                }
            }
        }
    }
}
