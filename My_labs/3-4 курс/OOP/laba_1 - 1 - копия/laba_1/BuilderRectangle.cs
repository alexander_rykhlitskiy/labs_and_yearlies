﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using ParentsLibrary;
namespace laba_1
{
    class BuilderRectangle : Builder
    {
        public override Shape createShape(MouseEventArgs e, Color colorOfShape)
        {
            ShapeRectangle creatingShape = new ShapeRectangle();
            creatingShape.xInitial = e.X;
            creatingShape.yInitial = e.Y;
            creatingShape.chosenColor = colorOfShape;
            return creatingShape;
        }
    }
}
