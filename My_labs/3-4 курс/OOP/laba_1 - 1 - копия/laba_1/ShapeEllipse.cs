﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using ParentsLibrary;
namespace laba_1
{
    class ShapeEllipse : Shape
    {
        public override void drawShape(Bitmap someBitmap)
        {
            Graphics formGraphics = Graphics.FromImage(someBitmap);
            System.Drawing.Pen myPen;
            myPen = new Pen(chosenColor);
            formGraphics.DrawEllipse(myPen, new Rectangle(xInitial, yInitial, xFinal - xInitial, yFinal - yInitial));
            myPen.Dispose();
            formGraphics.Dispose();
        }

    }
}
