﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using ParentsLibrary;

namespace laba_1
{
    [Serializable]
    class BuilderUserShape : Builder
    {
        public BuilderUserShape(string strName, List<Shape> partsOfUserShape)
        {
            name = strName;
            this.partsOfBuilderUserShape = partsOfUserShape;
        }
        public BuilderUserShape(string strName)
        {
            name = strName;
            partsOfBuilderUserShape = new List<Shape>();
        }
        private List<Shape> partsOfBuilderUserShape;
        public override Shape createShape(MouseEventArgs e, Color colorOfShape, int thicknessOut)
        {
            UserShape creatingShape = new UserShape(e.X, e.Y, colorOfShape, thicknessOut, this.partsOfBuilderUserShape);
            return creatingShape;
        }
    }
}
