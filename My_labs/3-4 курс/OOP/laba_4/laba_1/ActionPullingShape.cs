﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using ParentsLibrary;

namespace laba_1
{
    class ActionPullingShape : ActionChangingShape
    {
        private MouseEventArgs propPastStateCursor;
        public MouseEventArgs pastStateCursor
        {
         //   get { return propPastStateCursor; }
            set { propPastStateCursor = value; }
        }

        public ActionPullingShape(MouseEventArgs pastStateCursor, int numberOfShape)
            : base(numberOfShape)
        {
            this.pastStateCursor = pastStateCursor;
        }
        public ActionPullingShape()
        {
        }
        public override void ChangeShape(MouseEventArgs e, List<Shape> shapeList)
        {
            shapeList[numberOfShape].xInitial += (e.X - propPastStateCursor.X);
            shapeList[numberOfShape].xFinal += (e.X - propPastStateCursor.X);
            shapeList[numberOfShape].yInitial += (e.Y - propPastStateCursor.Y);
            shapeList[numberOfShape].yFinal += (e.Y - propPastStateCursor.Y);
            pastStateCursor = e;
        }
        public override Cursor MakePreparations(Cursor cursor, List<Shape> shapeList, System.Drawing.Bitmap mainBitmap)
        {
            DrawMarker(shapeList[numberOfShape], mainBitmap);
            return GetCursor();
        }
        public override Cursor GetCursor()
        {
            return Cursors.SizeAll;
        }

    }
}
