﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using System.Windows.Forms;
using ParentsLibrary;

namespace laba_1
{
    class Plugin
    {
        public static void IncludePlugin(List<Builder> builderList, ComboBox chooseShape, ref int numberOfBaseShapes)
        {
            string directory = "Plugins";
            string[] fileEntries = Directory.GetFiles(directory);
            foreach (string fileName in fileEntries)
            {
                try
                {
                    Assembly newShapeAssembly = null;
                    try { newShapeAssembly = Assembly.LoadFrom(fileName); }
                    catch (Exception ex)
                    { MessageBox.Show(ex.Message); }


                    foreach (Type t in newShapeAssembly.GetTypes())
                    {
                        if (t.IsSubclassOf(typeof(Builder)))
                        {
                            String shapeName = fileName.Remove(0, directory.Length + 1);
                            shapeName = shapeName.Remove(shapeName.Length - 4, 4);
                            Object shapeBuilder = Activator.CreateInstance(t, shapeName);
                            builderList.Add((Builder)shapeBuilder);
                            chooseShape.Items.Add(shapeName);
                            numberOfBaseShapes++;
                        }
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
        }
    }
}
