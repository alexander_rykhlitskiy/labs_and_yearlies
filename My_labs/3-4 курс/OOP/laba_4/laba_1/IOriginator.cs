﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ParentsLibrary;

namespace laba_1
{
    interface IOriginator
    {
        IMemento GetState();
    }
}
