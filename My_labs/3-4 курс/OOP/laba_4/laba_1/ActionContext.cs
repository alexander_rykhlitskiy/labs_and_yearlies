﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;
using System.Windows.Forms;
using ParentsLibrary;
namespace laba_1
{
    class ActionContext
    {
        private ActionChangingShape _action = new ActionDrawingShape();
        public ActionChangingShape action
        {
            set { _action = value; }
            get { return _action; }
        }
        private int _numberOfShape;
        public int numberOfShape
        {
            set { _numberOfShape = value; }
            get { return _numberOfShape; }
        }
        private bool _ready = false;
        public bool ready
        {
            set { _ready = value; }
            get { return _ready; }
        }
        public void SetAction(MouseEventArgs e, List<Shape> shapeList)
        {
            if (StratchingAbove(e, shapeList))
                action = new ActionStratchingShape(numberOfShape);
            else
                if (PullingAbove(e, shapeList))
                    action = new ActionPullingShape(e, numberOfShape);
                else
                    if (DrawingAbove(e, shapeList))
                        action = new ActionDrawingShape(shapeList.Count - 1);
        }
        public bool PullingAbove(MouseEventArgs e, List<Shape> shapeList)
        {
            bool result = false;
            for (int i = 0; i < shapeList.Count; i++)
            {
                result = shapeList[i].IsPointInOrOut(e.X, e.Y);
                if (result == true)
                {
                    this.numberOfShape = i;
                    break;
                }
            }
            return result;
        }
        public bool StratchingAbove(MouseEventArgs e, List<Shape> shapeList)
        {
            bool result = false;
            const int halfCornerWidth = 10;
            for (int i = 0; i < shapeList.Count; i++)
            {
                if ((e.X < shapeList[i].xFinal + halfCornerWidth) & (e.X > shapeList[i].xFinal - halfCornerWidth) &
                    (e.Y < shapeList[i].yFinal + halfCornerWidth) & (e.Y > shapeList[i].yFinal - halfCornerWidth))
                {
                    result = true;
                    this.numberOfShape = i;
                    break;
                }
            }
            return result;
        }
        public bool DrawingAbove(MouseEventArgs e, List<Shape> shapeList)
        {
            return true;
        }
    }
}
