﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;
using System.Windows.Forms;
using ParentsLibrary;

namespace laba_1
{
    abstract class ActionChangingShape
    {
        private int propNumberOfShape;
        public int numberOfShape
        {
            get { return propNumberOfShape; }
            set { propNumberOfShape = value; }
        }
        public ActionChangingShape(int numberOfShape)
        {
            this.numberOfShape = numberOfShape;
        }
        public ActionChangingShape()
        {
        }
        public abstract void ChangeShape(MouseEventArgs e, List<Shape> shapeList);
        public abstract Cursor MakePreparations(Cursor cursor, List<Shape> shapeList, Bitmap mainBitmap);
        public abstract Cursor GetCursor();
        private void transposition(ref int x, ref int y)
        {
            int temp;
            temp = x;
            x = y;
            y = temp;
        }
        public void DrawMarker(Shape shape, Bitmap someBitmap)
        {
            const int sideOfMarker = 5;
            Graphics formGraphics = Graphics.FromImage(someBitmap);
            System.Drawing.Pen myPen;
            myPen = new Pen(Color.Red);

            int xInitialToDraw = shape.xInitial, yInitialToDraw = shape.yInitial,
    xFinalToDraw = shape.xFinal, yFinalToDraw = shape.yFinal;

            if (xInitialToDraw > xFinalToDraw)
                transposition(ref xInitialToDraw, ref xFinalToDraw);
            if (yInitialToDraw > yFinalToDraw)
                transposition(ref yInitialToDraw, ref yFinalToDraw);
            xInitialToDraw -= sideOfMarker;
            yInitialToDraw -= sideOfMarker;
            xFinalToDraw += sideOfMarker;
            yFinalToDraw += sideOfMarker;
            //с этим невнушительно смотриться определение границ
            //formGraphics.DrawRectangle(myPen, new Rectangle(xInitialToDraw, yInitialToDraw, xFinalToDraw - xInitialToDraw,
            //                                                yFinalToDraw - yInitialToDraw));
            formGraphics.DrawRectangle(myPen, new Rectangle(shape.xFinal - sideOfMarker,
                                                shape.yFinal - sideOfMarker,
                                                sideOfMarker * 2, sideOfMarker * 2));
            myPen.Dispose();
            formGraphics.Dispose();
        }
    }
}