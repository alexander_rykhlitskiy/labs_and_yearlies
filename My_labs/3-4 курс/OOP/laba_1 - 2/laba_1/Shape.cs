﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

namespace laba_1
{
    [Serializable]
    abstract class Shape
    {
        public int xInitial, yInitial, xFinal, yFinal;
        public Color chosenColor;
        public int thickness = 0;
        abstract public void DrawShape(Bitmap someBitmap, Context data = null);
        public void DrawShape(Bitmap someBitmap)
        {
            Context data = new Context(1, 1, 0, 0);
            DrawShape(someBitmap, data);
        }
    }
}
