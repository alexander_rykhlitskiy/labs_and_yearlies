﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace laba_1
{
    [Serializable]
    class BuilderRectangle : Builder
    {
        public override Shape createShape(MouseEventArgs e, Color colorOfShape, int thicknessOut)
        {
            ShapeRectangle creatingShape = new ShapeRectangle();
            creatingShape.xInitial = e.X;
            creatingShape.yInitial = e.Y;
            creatingShape.chosenColor = colorOfShape;
            creatingShape.thickness = thicknessOut;
            return creatingShape;
        }
        public BuilderRectangle(string strName)
        {
            name = strName;
        }
    }
}
