﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace laba_1
{
    [Serializable]
    class BuilderUserShape : Builder
    {
        public List<Shape> PartsOfUserShape;
        public override Shape createShape(MouseEventArgs e, Color colorOfShape, int thicknessOut)
        {
            UserShape creatingShape = new UserShape();
            creatingShape.PartsOfUserShape = this.PartsOfUserShape;
            creatingShape.xInitial = e.X;
            creatingShape.yInitial = e.Y;
            creatingShape.chosenColor = colorOfShape;
            creatingShape.thickness = thicknessOut;
            return creatingShape;
        }
        public BuilderUserShape(string strName)
        {
            name = strName;
        }
    }
}
