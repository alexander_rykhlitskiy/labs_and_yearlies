﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace laba_1
{
    [Serializable]
    class UserShape : Shape
    {
        public List<Shape> PartsOfUserShape;
        public double scaleX, scaleY;
        public override void DrawShape(Bitmap someBitmap, Context data = null)
        {
            this.scaleX = (this.xFinal - this.xInitial) / (double)someBitmap.Width;
            this.scaleY = (this.yFinal - this.yInitial) / (double)someBitmap.Height;
            foreach (Shape drawingShape in PartsOfUserShape)
            {
          //      drawingShape.chosenColor = this.chosenColor; //if it'll be necessary to change parameters of userShape drawing it
                data = new Context(this.scaleX, this.scaleY, this.xInitial, this.yInitial);
                drawingShape.DrawShape(someBitmap, data);
            }
        }
    }
}
