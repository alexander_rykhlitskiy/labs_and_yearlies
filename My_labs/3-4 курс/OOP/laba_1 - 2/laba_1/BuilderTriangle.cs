﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace laba_1
{
    [Serializable]
    class BuilderTriangle : Builder
    {
        public override Shape createShape(MouseEventArgs e, Color colorOfShape, int thicknessOut)
        {
            ShapeTriangle creatingShape = new ShapeTriangle();
            creatingShape.xInitial = e.X;
            creatingShape.yInitial = e.Y;
            creatingShape.chosenColor = colorOfShape;
            creatingShape.thickness = thicknessOut;
            return creatingShape;
        }
        public BuilderTriangle(string strName)
        {
            name = strName;
        }
    }
}
