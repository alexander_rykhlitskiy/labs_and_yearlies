﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace laba_1
{
    [Serializable]
    class ShapeRectangle : Shape
    {
        public void transposition(ref int x, ref int y)
        {
            int temp;
            temp = x;
            x = y;
            y = temp;
        }
        public override void DrawShape(Bitmap someBitmap, Context data = null)
        {
            int xInitialToDraw = xInitial, yInitialToDraw = yInitial,
                xFinalToDraw = xFinal, yFinalToDraw = yFinal;

            if (xInitialToDraw > xFinalToDraw)
                transposition(ref xInitialToDraw, ref xFinalToDraw);
            if (yInitialToDraw > yFinalToDraw)
                transposition(ref yInitialToDraw, ref yFinalToDraw);
            
            Graphics formGraphics = Graphics.FromImage(someBitmap);
            System.Drawing.Pen myPen;
            myPen = new Pen(chosenColor, thickness);
            if (data.scaleX < 0)
                transposition(ref xInitialToDraw, ref xFinalToDraw);
            if (data.scaleY < 0)
                transposition(ref yInitialToDraw, ref yFinalToDraw);
            formGraphics.DrawRectangle(myPen, new Rectangle((int)(xInitialToDraw * data.scaleX) + data.shiftX,
                                                            (int)(yInitialToDraw * data.scaleY) + data.shiftY,
                                                            (int)((xFinalToDraw - xInitialToDraw) * data.scaleX),
                                                            (int)((yFinalToDraw - yInitialToDraw) * data.scaleY)));
            
          //  }
            myPen.Dispose();
            formGraphics.Dispose();
        }
    }
}
