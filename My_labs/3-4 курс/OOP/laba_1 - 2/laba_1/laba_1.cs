﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

namespace laba_1
{
    public partial class OOP : Form
    {
        public const int numberOfBaseShapes = 3;
        Boolean drawOrNot = false;
        List<Shape> shapeList = new List<Shape>(),
                    addShapeList = new List<Shape>();
        List<Builder> builderList = new List<Builder>();
        Bitmap someBitmap, addSomeBitmap;
        Color colorOfShape;
        bool addShapeBool = false;
        public OOP()
        {
            InitializeComponent();
            builderList.Add(new BuilderTriangle("Triangle"));
            builderList.Add(new BuilderRectangle("Rectangle"));
            builderList.Add(new BuilderEllipse("Ellipse"));
            chooseShape.Items.Add(builderList[0].name);
            chooseShape.Items.Add(builderList[1].name);
            chooseShape.Items.Add(builderList[2].name);
            chooseShape.SelectedIndex = 0;
            someBitmap = new Bitmap(workArea.Width, workArea.Height);
            colorOfShape = Color.Black;
            colorButton.BackColor = colorOfShape;
            ReadBuilderFromFile();
        }

        private void ClearScreenWithoutShapes()
        {
            Graphics formGraphics = Graphics.FromImage(someBitmap);
            formGraphics.Clear(Color.White);
            workArea.Image = someBitmap;
        }
        private void FullClearScreen()
        {
            System.Drawing.Graphics formGraphics = Graphics.FromImage(someBitmap);
            formGraphics.Clear(Color.White);
            shapeList.Clear();
            workArea.Image = someBitmap;
        }
        private void WriteBuilderInFile()
        {
            for (int i = numberOfBaseShapes; i < builderList.Count; i++)
            {
                FileStream fs = new FileStream(Environment.CurrentDirectory + "\\" + "filesForBuilders" + "\\" 
                                               + builderList[i].name, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, builderList[i]);
                fs.Close();
             }
        }
        private void ReadBuilderFromFile()
        {
            string[] fileEntries = Directory.GetFiles("filesForBuilders");
            int i = numberOfBaseShapes;
            BuilderUserShape tempBuilderUserShape;
            foreach (string fileName in fileEntries)
            {
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                BinaryFormatter bf = new BinaryFormatter();
                tempBuilderUserShape = new BuilderUserShape(fileName);
                tempBuilderUserShape = (BuilderUserShape)bf.Deserialize(fs);
                builderList.Add(tempBuilderUserShape);
                chooseShape.Items.Add(builderList[builderList.Count - 1].name);
                fs.Close();
            }
        }
        private void DrawShapeList()
        {
            for (int i = 0; i < shapeList.Count; i++)
            {
                shapeList[i].DrawShape(someBitmap);
            }
            workArea.Image = someBitmap;
        }
        private void buttonClear_Click_1(object sender, EventArgs e)
        {
            FullClearScreen();
        }

        private void workArea_MouseDown(object sender, MouseEventArgs e)
        {
            shapeList.Add(builderList[chooseShape.SelectedIndex].createShape(e, colorOfShape, (int)thicknessNumeric.Value));
            drawOrNot = true;
        }

        private void workArea_MouseMove(object sender, MouseEventArgs e)
        {
            if (drawOrNot)
            {
                ClearScreenWithoutShapes();
                shapeList[shapeList.Count - 1].xFinal = e.X;
                shapeList[shapeList.Count - 1].yFinal = e.Y;
                DrawShapeList();
            }
        }

        private void workArea_MouseUp(object sender, MouseEventArgs e)
        {
            drawOrNot = false;
        }

        private void colorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                colorOfShape = colorDialog.Color;
                colorButton.BackColor = colorOfShape;
            }
        }
        private void addButton_Click(object sender, EventArgs e)
        {
            addShapeBool = !addShapeBool;
            if (addShapeBool)
            {
                this.Text = "Create your shape";
                addButton.Text = "ready";
                addSomeBitmap = someBitmap;
                someBitmap = new Bitmap(workArea.Width, workArea.Height);

                addShapeList = shapeList;
                shapeList = new List<Shape>();
                ClearScreenWithoutShapes();
            }
            else
            {
                BuilderUserShape currentBuilder = new BuilderUserShape(chooseShape.Text);
                currentBuilder.PartsOfUserShape = shapeList;
                builderList.Add(currentBuilder);
                this.Text = "laba_1";
                addButton.Text = "add";
                chooseShape.SelectedIndex = chooseShape.Items.Add(chooseShape.Text);
                someBitmap = addSomeBitmap;
                shapeList = addShapeList;
                DrawShapeList();
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            FileStream fs = new FileStream("screen.s", FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, shapeList);
            fs.Close();
        }

        private void recoverButton_Click(object sender, EventArgs e)
        {
            FullClearScreen();
            FileStream fs = new FileStream("screen.s", FileMode.Open, FileAccess.Read, FileShare.Read);
            BinaryFormatter bf = new BinaryFormatter();
            shapeList = (List<Shape>)bf.Deserialize(fs);
            fs.Close();
            DrawShapeList();
        }

        private void OOP_FormClosing(object sender, FormClosingEventArgs e)
        {
            WriteBuilderInFile();
        }


    }
}
