﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace laba_1
{
    [Serializable]
    class ShapeEllipse : Shape
    {
        public override void DrawShape(Bitmap someBitmap, Context data = null)
        {
            Graphics formGraphics = Graphics.FromImage(someBitmap);
            System.Drawing.Pen myPen;
            myPen = new Pen(chosenColor, thickness);
            formGraphics.DrawEllipse(myPen, new Rectangle((int)(xInitial * data.scaleX) + data.shiftX, 
                                                          (int)(yInitial * data.scaleY) + data.shiftY, 
                                                          (int)((xFinal - xInitial) * data.scaleX), 
                                                          (int)((yFinal - yInitial) * data.scaleY)));
            myPen.Dispose();
            formGraphics.Dispose();
        }

    }
}
