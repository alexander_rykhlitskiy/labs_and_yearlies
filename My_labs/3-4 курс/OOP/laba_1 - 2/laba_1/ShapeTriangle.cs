﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace laba_1
{
    [Serializable]
    class ShapeTriangle : Shape
    {
        public override void DrawShape(Bitmap someBitmap, Context data = null)
        {
            Graphics formGraphics = Graphics.FromImage(someBitmap);
            System.Drawing.Pen myPen;
            myPen = new Pen(chosenColor, thickness);
            PointF[] vertexes = new PointF[3];
            vertexes[0] = new PointF((int)(((xInitial + xFinal) / 2) * data.scaleX) + data.shiftX, (int)(yInitial * data.scaleY) + data.shiftY);
            vertexes[1] = new PointF((int)(xInitial * data.scaleX) + data.shiftX , (int)(yFinal * data.scaleY) + data.shiftY);
            vertexes[2] = new PointF((int)(xFinal * data.scaleX) + data.shiftX, (int)(yFinal * data.scaleY) + data.shiftY);
            formGraphics.DrawPolygon(myPen, vertexes);
            myPen.Dispose();
            formGraphics.Dispose();
        }
    }
}
