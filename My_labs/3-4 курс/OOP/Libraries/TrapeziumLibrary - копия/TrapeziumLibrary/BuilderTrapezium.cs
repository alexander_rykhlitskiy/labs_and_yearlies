﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using ParentsLibrary;
//using laba_1;

namespace TrapeziumLibrary
{
    public class BuilderTrapezium : Builder
    {
        public override Shape createShape(MouseEventArgs e, Color colorOfShape)
        {
            ShapeTrapezium creatingShape = new ShapeTrapezium();
            creatingShape.xInitial = e.X;
            creatingShape.yInitial = e.Y;
            creatingShape.chosenColor = colorOfShape;
            return creatingShape;
        }
    }
}
