﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using ParentsLibrary;
//using laba_1;

namespace TrapeziumLibrary
{
    public class ShapeTrapezium : Shape
    {
        public override void drawShape(Bitmap someBitmap)
        {
            Graphics formGraphics = Graphics.FromImage(someBitmap);
            System.Drawing.Pen myPen;
            myPen = new Pen(chosenColor);
            PointF[] vertexes = new PointF[4];
            vertexes[0] = new PointF((xInitial + xFinal) / 2 - 10, yInitial);
            vertexes[1] = new PointF(xInitial, yFinal);
            vertexes[2] = new PointF(xFinal, yFinal);
            vertexes[3] = new PointF((xInitial + xFinal) / 2 + 10, yInitial);
            formGraphics.DrawPolygon(myPen, vertexes);
            myPen.Dispose();
            formGraphics.Dispose();
        }
    }
}
