﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using ParentsLibrary;
//using laba_1;

namespace Trapezium
{
    public class ShapeTrapezium : Shape
    {
        public ShapeTrapezium(int xInitial, int yInitial, Color chosenColor, int thickness) :
            base(xInitial, yInitial, chosenColor, thickness)
        {
        }
        const int numberOfVertexes = 4;
        private void CalculateVertexe(PointF[] vertexe, UserShapeContextToDrawShape data)
        {
            vertexe[0] = new PointF((int)((xInitial + (xFinal - xInitial) / 4) * data.scaleX) + data.shiftX, (int)(yInitial * data.scaleY) + data.shiftY);
            vertexe[1] = new PointF((int)(xFinal * data.scaleX) + data.shiftX, (int)(yFinal * data.scaleY) + data.shiftY);
            vertexe[2] = new PointF((int)(xInitial * data.scaleX) + data.shiftX, (int)(yFinal * data.scaleY) + data.shiftY);
            vertexe[3] = new PointF((int)((xFinal - (xFinal - xInitial) / 4) * data.scaleX) + data.shiftX, (int)(yInitial * data.scaleY) + data.shiftY);
        }
        public override void DrawShape(Bitmap someBitmap, UserShapeContextToDrawShape data = null)
        {
            Graphics formGraphics = Graphics.FromImage(someBitmap);
            System.Drawing.Pen myPen;
            myPen = new Pen(chosenColor, thickness);
            PointF[] vertexe = new PointF[numberOfVertexes];
            CalculateVertexe(vertexe, data);
            formGraphics.DrawPolygon(myPen, vertexe);
            myPen.Dispose();
            formGraphics.Dispose();
        }
        public override bool IsPointInOrOut(int xPoint, int yPoint, UserShapeContextToDrawShape data = null)
        {
            bool result = true;
            result = ((xPoint < xFinal) & (xPoint > xInitial) & (yPoint < yFinal) & (yPoint > yInitial));
            return result;
        }
    }
}
