﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using ParentsLibrary;

namespace Trapezium
{
    public class BuilderTrapezium : Builder
    {
        public override Shape createShape(MouseEventArgs e, Color colorOfShape, int thicknessOut)
        {
            ShapeTrapezium creatingShape = new ShapeTrapezium(e.X, e.Y, colorOfShape, thicknessOut);
            return creatingShape;
        }
        public BuilderTrapezium(string strName)
        {
            name = strName;
        }
    }
}
