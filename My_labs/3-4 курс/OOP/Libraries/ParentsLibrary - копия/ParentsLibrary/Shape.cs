﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

namespace ParentsLibrary
{
    public abstract class Shape
    {
        public int xInitial, yInitial, xFinal, yFinal;
        public Color chosenColor;
        abstract public void drawShape(Bitmap someBitmap);
    }
}
