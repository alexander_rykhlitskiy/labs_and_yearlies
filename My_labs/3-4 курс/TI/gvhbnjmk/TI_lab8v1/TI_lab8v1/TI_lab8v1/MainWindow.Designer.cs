﻿namespace TI_lab8v1
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TextBox_M = new System.Windows.Forms.TextBox();
            this.Label_M = new System.Windows.Forms.Label();
            this.Label_a = new System.Windows.Forms.Label();
            this.Label_b = new System.Windows.Forms.Label();
            this.TextBox_a = new System.Windows.Forms.TextBox();
            this.TextBox_b = new System.Windows.Forms.TextBox();
            this.DataGridView_EllipticGroup = new System.Windows.Forms.DataGridView();
            this.Index = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Group = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Label_EllipticGroup = new System.Windows.Forms.Label();
            this.button_GenerateOpenKeys = new System.Windows.Forms.Button();
            this.groupBox_KeyExchange = new System.Windows.Forms.GroupBox();
            this.textBox_Pb = new System.Windows.Forms.TextBox();
            this.label_Pb = new System.Windows.Forms.Label();
            this.textBox_Pa = new System.Windows.Forms.TextBox();
            this.label_Pa = new System.Windows.Forms.Label();
            this.groupBox_SourceData = new System.Windows.Forms.GroupBox();
            this.groupBox_DigitalSignature = new System.Windows.Forms.GroupBox();
            this.button_VerifyDigitalSignature = new System.Windows.Forms.Button();
            this.button_GenerateDigitalSignature = new System.Windows.Forms.Button();
            this.label_Na = new System.Windows.Forms.Label();
            this.textBox_Na = new System.Windows.Forms.TextBox();
            this.label_G = new System.Windows.Forms.Label();
            this.textBox_G = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_EllipticGroup)).BeginInit();
            this.groupBox_KeyExchange.SuspendLayout();
            this.groupBox_SourceData.SuspendLayout();
            this.groupBox_DigitalSignature.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(726, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.openToolStripMenuItem.Text = "Open...";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // TextBox_M
            // 
            this.TextBox_M.Enabled = false;
            this.TextBox_M.Location = new System.Drawing.Point(9, 32);
            this.TextBox_M.MaxLength = 2;
            this.TextBox_M.Name = "TextBox_M";
            this.TextBox_M.Size = new System.Drawing.Size(25, 20);
            this.TextBox_M.TabIndex = 1;
            this.TextBox_M.Text = "47";
            // 
            // Label_M
            // 
            this.Label_M.AutoSize = true;
            this.Label_M.Location = new System.Drawing.Point(6, 16);
            this.Label_M.Name = "Label_M";
            this.Label_M.Size = new System.Drawing.Size(16, 13);
            this.Label_M.TabIndex = 2;
            this.Label_M.Text = "M";
            // 
            // Label_a
            // 
            this.Label_a.AutoSize = true;
            this.Label_a.Location = new System.Drawing.Point(62, 16);
            this.Label_a.Name = "Label_a";
            this.Label_a.Size = new System.Drawing.Size(13, 13);
            this.Label_a.TabIndex = 3;
            this.Label_a.Text = "a";
            // 
            // Label_b
            // 
            this.Label_b.AutoSize = true;
            this.Label_b.Location = new System.Drawing.Point(116, 16);
            this.Label_b.Name = "Label_b";
            this.Label_b.Size = new System.Drawing.Size(13, 13);
            this.Label_b.TabIndex = 4;
            this.Label_b.Text = "b";
            // 
            // TextBox_a
            // 
            this.TextBox_a.Enabled = false;
            this.TextBox_a.Location = new System.Drawing.Point(65, 32);
            this.TextBox_a.MaxLength = 2;
            this.TextBox_a.Name = "TextBox_a";
            this.TextBox_a.Size = new System.Drawing.Size(25, 20);
            this.TextBox_a.TabIndex = 5;
            // 
            // TextBox_b
            // 
            this.TextBox_b.Enabled = false;
            this.TextBox_b.Location = new System.Drawing.Point(119, 32);
            this.TextBox_b.MaxLength = 2;
            this.TextBox_b.Name = "TextBox_b";
            this.TextBox_b.Size = new System.Drawing.Size(25, 20);
            this.TextBox_b.TabIndex = 6;
            // 
            // DataGridView_EllipticGroup
            // 
            this.DataGridView_EllipticGroup.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridView_EllipticGroup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView_EllipticGroup.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Index,
            this.Group});
            this.DataGridView_EllipticGroup.Location = new System.Drawing.Point(9, 86);
            this.DataGridView_EllipticGroup.Name = "DataGridView_EllipticGroup";
            this.DataGridView_EllipticGroup.Size = new System.Drawing.Size(171, 221);
            this.DataGridView_EllipticGroup.TabIndex = 7;
            // 
            // Index
            // 
            this.Index.FillWeight = 20F;
            this.Index.HeaderText = "№";
            this.Index.Name = "Index";
            this.Index.ReadOnly = true;
            // 
            // Group
            // 
            this.Group.FillWeight = 80F;
            this.Group.HeaderText = "Member";
            this.Group.Name = "Group";
            this.Group.ReadOnly = true;
            // 
            // Label_EllipticGroup
            // 
            this.Label_EllipticGroup.AutoSize = true;
            this.Label_EllipticGroup.Location = new System.Drawing.Point(6, 70);
            this.Label_EllipticGroup.Name = "Label_EllipticGroup";
            this.Label_EllipticGroup.Size = new System.Drawing.Size(69, 13);
            this.Label_EllipticGroup.TabIndex = 8;
            this.Label_EllipticGroup.Text = "Elliptic Group";
            // 
            // button_GenerateOpenKeys
            // 
            this.button_GenerateOpenKeys.Location = new System.Drawing.Point(38, 19);
            this.button_GenerateOpenKeys.Name = "button_GenerateOpenKeys";
            this.button_GenerateOpenKeys.Size = new System.Drawing.Size(114, 23);
            this.button_GenerateOpenKeys.TabIndex = 9;
            this.button_GenerateOpenKeys.Text = "Generate Open Keys";
            this.button_GenerateOpenKeys.UseVisualStyleBackColor = true;
            this.button_GenerateOpenKeys.Click += new System.EventHandler(this.button_GenerateOpenKeys_Click);
            // 
            // groupBox_KeyExchange
            // 
            this.groupBox_KeyExchange.Controls.Add(this.textBox_G);
            this.groupBox_KeyExchange.Controls.Add(this.label_G);
            this.groupBox_KeyExchange.Controls.Add(this.textBox_Na);
            this.groupBox_KeyExchange.Controls.Add(this.label_Na);
            this.groupBox_KeyExchange.Controls.Add(this.textBox_Pb);
            this.groupBox_KeyExchange.Controls.Add(this.label_Pb);
            this.groupBox_KeyExchange.Controls.Add(this.textBox_Pa);
            this.groupBox_KeyExchange.Controls.Add(this.label_Pa);
            this.groupBox_KeyExchange.Controls.Add(this.button_GenerateOpenKeys);
            this.groupBox_KeyExchange.Location = new System.Drawing.Point(240, 27);
            this.groupBox_KeyExchange.Name = "groupBox_KeyExchange";
            this.groupBox_KeyExchange.Size = new System.Drawing.Size(200, 158);
            this.groupBox_KeyExchange.TabIndex = 10;
            this.groupBox_KeyExchange.TabStop = false;
            this.groupBox_KeyExchange.Text = "Key Exchange";
            // 
            // textBox_Pb
            // 
            this.textBox_Pb.Enabled = false;
            this.textBox_Pb.Location = new System.Drawing.Point(126, 64);
            this.textBox_Pb.Name = "textBox_Pb";
            this.textBox_Pb.Size = new System.Drawing.Size(55, 20);
            this.textBox_Pb.TabIndex = 13;
            // 
            // label_Pb
            // 
            this.label_Pb.AutoSize = true;
            this.label_Pb.Location = new System.Drawing.Point(123, 47);
            this.label_Pb.Name = "label_Pb";
            this.label_Pb.Size = new System.Drawing.Size(20, 13);
            this.label_Pb.TabIndex = 12;
            this.label_Pb.Text = "Pb";
            // 
            // textBox_Pa
            // 
            this.textBox_Pa.Enabled = false;
            this.textBox_Pa.Location = new System.Drawing.Point(6, 64);
            this.textBox_Pa.Name = "textBox_Pa";
            this.textBox_Pa.Size = new System.Drawing.Size(55, 20);
            this.textBox_Pa.TabIndex = 11;
            // 
            // label_Pa
            // 
            this.label_Pa.AutoSize = true;
            this.label_Pa.Location = new System.Drawing.Point(7, 47);
            this.label_Pa.Name = "label_Pa";
            this.label_Pa.Size = new System.Drawing.Size(20, 13);
            this.label_Pa.TabIndex = 10;
            this.label_Pa.Text = "Pa";
            // 
            // groupBox_SourceData
            // 
            this.groupBox_SourceData.Controls.Add(this.DataGridView_EllipticGroup);
            this.groupBox_SourceData.Controls.Add(this.Label_EllipticGroup);
            this.groupBox_SourceData.Controls.Add(this.Label_M);
            this.groupBox_SourceData.Controls.Add(this.TextBox_b);
            this.groupBox_SourceData.Controls.Add(this.TextBox_M);
            this.groupBox_SourceData.Controls.Add(this.TextBox_a);
            this.groupBox_SourceData.Controls.Add(this.Label_a);
            this.groupBox_SourceData.Controls.Add(this.Label_b);
            this.groupBox_SourceData.Location = new System.Drawing.Point(12, 27);
            this.groupBox_SourceData.Name = "groupBox_SourceData";
            this.groupBox_SourceData.Size = new System.Drawing.Size(211, 313);
            this.groupBox_SourceData.TabIndex = 11;
            this.groupBox_SourceData.TabStop = false;
            this.groupBox_SourceData.Text = "Source Data";
            // 
            // groupBox_DigitalSignature
            // 
            this.groupBox_DigitalSignature.Controls.Add(this.button_VerifyDigitalSignature);
            this.groupBox_DigitalSignature.Controls.Add(this.button_GenerateDigitalSignature);
            this.groupBox_DigitalSignature.Location = new System.Drawing.Point(240, 191);
            this.groupBox_DigitalSignature.Name = "groupBox_DigitalSignature";
            this.groupBox_DigitalSignature.Size = new System.Drawing.Size(200, 142);
            this.groupBox_DigitalSignature.TabIndex = 12;
            this.groupBox_DigitalSignature.TabStop = false;
            this.groupBox_DigitalSignature.Text = "Digital Signature";
            // 
            // button_VerifyDigitalSignature
            // 
            this.button_VerifyDigitalSignature.Location = new System.Drawing.Point(30, 78);
            this.button_VerifyDigitalSignature.Name = "button_VerifyDigitalSignature";
            this.button_VerifyDigitalSignature.Size = new System.Drawing.Size(151, 23);
            this.button_VerifyDigitalSignature.TabIndex = 1;
            this.button_VerifyDigitalSignature.Text = "Verify Digital Signature";
            this.button_VerifyDigitalSignature.UseVisualStyleBackColor = true;
            this.button_VerifyDigitalSignature.Click += new System.EventHandler(this.button_VerifyDigitalSignature_Click);
            // 
            // button_GenerateDigitalSignature
            // 
            this.button_GenerateDigitalSignature.Location = new System.Drawing.Point(30, 34);
            this.button_GenerateDigitalSignature.Name = "button_GenerateDigitalSignature";
            this.button_GenerateDigitalSignature.Size = new System.Drawing.Size(151, 23);
            this.button_GenerateDigitalSignature.TabIndex = 0;
            this.button_GenerateDigitalSignature.Text = "Generate Digital Signature";
            this.button_GenerateDigitalSignature.UseVisualStyleBackColor = true;
            this.button_GenerateDigitalSignature.Click += new System.EventHandler(this.button_GenerateDigitalSignature_Click);
            // 
            // label_Na
            // 
            this.label_Na.AutoSize = true;
            this.label_Na.Location = new System.Drawing.Point(7, 94);
            this.label_Na.Name = "label_Na";
            this.label_Na.Size = new System.Drawing.Size(21, 13);
            this.label_Na.TabIndex = 14;
            this.label_Na.Text = "Na";
            // 
            // textBox_Na
            // 
            this.textBox_Na.Enabled = false;
            this.textBox_Na.Location = new System.Drawing.Point(6, 110);
            this.textBox_Na.Name = "textBox_Na";
            this.textBox_Na.Size = new System.Drawing.Size(55, 20);
            this.textBox_Na.TabIndex = 15;
            // 
            // label_G
            // 
            this.label_G.AutoSize = true;
            this.label_G.Location = new System.Drawing.Point(123, 94);
            this.label_G.Name = "label_G";
            this.label_G.Size = new System.Drawing.Size(15, 13);
            this.label_G.TabIndex = 16;
            this.label_G.Text = "G";
            // 
            // textBox_G
            // 
            this.textBox_G.Enabled = false;
            this.textBox_G.Location = new System.Drawing.Point(126, 110);
            this.textBox_G.Name = "textBox_G";
            this.textBox_G.Size = new System.Drawing.Size(55, 20);
            this.textBox_G.TabIndex = 17;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(726, 352);
            this.Controls.Add(this.groupBox_DigitalSignature);
            this.Controls.Add(this.groupBox_SourceData);
            this.Controls.Add(this.groupBox_KeyExchange);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainWindow";
            this.Text = "TI_lab8v1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_EllipticGroup)).EndInit();
            this.groupBox_KeyExchange.ResumeLayout(false);
            this.groupBox_KeyExchange.PerformLayout();
            this.groupBox_SourceData.ResumeLayout(false);
            this.groupBox_SourceData.PerformLayout();
            this.groupBox_DigitalSignature.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.TextBox TextBox_M;
        private System.Windows.Forms.Label Label_M;
        private System.Windows.Forms.Label Label_a;
        private System.Windows.Forms.Label Label_b;
        private System.Windows.Forms.TextBox TextBox_a;
        private System.Windows.Forms.TextBox TextBox_b;
        private System.Windows.Forms.DataGridView DataGridView_EllipticGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn Index;
        private System.Windows.Forms.DataGridViewTextBoxColumn Group;
        private System.Windows.Forms.Label Label_EllipticGroup;
        private System.Windows.Forms.Button button_GenerateOpenKeys;
        private System.Windows.Forms.GroupBox groupBox_KeyExchange;
        private System.Windows.Forms.TextBox textBox_Pa;
        private System.Windows.Forms.Label label_Pa;
        private System.Windows.Forms.GroupBox groupBox_SourceData;
        private System.Windows.Forms.TextBox textBox_Pb;
        private System.Windows.Forms.Label label_Pb;
        private System.Windows.Forms.GroupBox groupBox_DigitalSignature;
        private System.Windows.Forms.Button button_GenerateDigitalSignature;
        private System.Windows.Forms.Button button_VerifyDigitalSignature;
        private System.Windows.Forms.Label label_G;
        private System.Windows.Forms.TextBox textBox_Na;
        private System.Windows.Forms.Label label_Na;
        private System.Windows.Forms.TextBox textBox_G;
    }
}

