﻿using System;
using System.Windows.Forms;

namespace TI_lab8v1
{
    class Ecdsa
    {
        private readonly string _fileName = "";
        private const string KeyFile = "key.txt";
        private IoDataOperations hash;
        private int hashPart;
        private readonly int _na;


        public Ecdsa(string fileName, int na)
        {
            _fileName = fileName;
            _na = na;
            hash = new IoDataOperations(_fileName);
            
        }

        private int CalculateHashPart()
        {
            int result = 0;
            string hashStr = hash.GetSHA1HashFunction();

            for (int shift = 0; shift < 8; ++shift)
            {
                byte temp = (byte)hashStr[shift];
                if (temp >= '0' && temp <= '9')
                    temp -= (byte)'0';
                else if (temp >= 'A' && temp <= 'F')
                    temp -= (byte)('A' - 10);
                result |= (byte)(temp << ((7 - shift) * 4));
            }
            return result;
        }

        private int FermatsLittleTheorem(int a, int b, int MOD)
        {
            long x = 1, y = a;

            while (b > 0)
            {
                if (b % 2 == 1)
                {
                    x = (x * y);
                    if (x > MOD) x %= MOD;
                }
                y = (y * y);
                if (y > MOD) y %= MOD;
                b /= 2;
            }
            return (int)x;
        }

        public void GenerateDigitalSignature()
        {
            hashPart = CalculateHashPart();
            int q;
            EllipticCurve gPoint = EllipticCurve.DetermineGeneratingPoint(out q);

            if (q == 0)
            {
                MessageBox.Show("Fail!", "Error!", MessageBoxButtons.OK);
                return;
            }

            Random rnd = new Random();
            int k = 0;
            int r = 0;
            int s = 0;

            do
            {
                do
                {
                    k = rnd.Next(1, q);
                    EllipticCurve m = gPoint*k;
                    r = m._point.X%q;
                } while (r == 0);
                int kInv = FermatsLittleTheorem(k, q - 2, q);
                s = hashPart + _na*r;
                s = ((s%q)*(kInv%q))%q;
            } while (s == 0);

            string buffer = r.ToString() + " " + s.ToString();
            hash.WriteString(KeyFile, buffer);
        }
        public bool VerifyDigitalSignature()
        {
            hashPart = CalculateHashPart();
            int q;
            EllipticCurve gPoint = EllipticCurve.DetermineGeneratingPoint(out q);
            EllipticCurve Pa = gPoint*_na;
            if (q == 0)
            {
                MessageBox.Show("Fail!", "Error!", MessageBoxButtons.OK);
                return false;
            }

            string signatureStr = hash.ReadString(KeyFile);
            string[] rsValues = signatureStr.Split(' ');
            int r = 0;
            int s = 0;
            try
            {
                r = Int32.Parse(rsValues[0]);
                s = Int32.Parse(rsValues[1]);
            }
            catch (Exception e)
            {
                throw (e);
            }

            int sInv = FermatsLittleTheorem(s, q - 2, q);
            int w = sInv%q;
            int u1 = ((hashPart%q)*(w%q))%q;
            int u2 = ((r%q)*(w%q))%q;
            EllipticCurve m = gPoint*u1 + Pa*u2;
            int rN = m._point.X%q;

            if (rN == r)return true;
            return false;
        }
    }
}
