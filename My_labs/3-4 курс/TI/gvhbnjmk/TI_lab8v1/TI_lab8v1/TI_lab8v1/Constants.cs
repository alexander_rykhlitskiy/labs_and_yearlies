﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TI_lab8v1
{
    internal static class Constants
    {
        internal const int LimitAB = 10000;
        internal const int Characteristic = 200;
        //SHA-1
        internal const int BlockSize = 64;
        internal const int Limit = 56;

        internal const uint H0 = 0x67452301;
        internal const uint H1 = 0xEFCDAB89;
        internal const uint H2 = 0x98BADCFE;
        internal const uint H3 = 0x10325476;
        internal const uint H4 = 0xC3D2E1F0;

        internal const int NumberOfParts = 16;
        internal const int ExtendedNumberOfParts = 80;

        internal const uint K0 = 0x5A827999;
        internal const uint K1 = 0x6ED9EBA1;
        internal const uint K2 = 0x8F1BBCDC;
        internal const uint K3 = 0xCA62C1D6;

        internal const ulong Modulus = 0x100000000;
    }
}
