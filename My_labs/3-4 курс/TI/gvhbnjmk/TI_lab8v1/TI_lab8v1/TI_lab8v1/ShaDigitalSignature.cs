﻿using System;
using System.Linq;

namespace TI_lab8v1
{
    class ShaDigitalSignature
    {
        private readonly ulong _messageSize;

        private uint _a;
        private uint _b;
        private uint _c;
        private uint _d;
        private uint _e;

        public ShaDigitalSignature(ulong messageSize)
        {
            _a = Constants.H0;
            _b = Constants.H1;
            _c = Constants.H2;
            _d = Constants.H3;
            _e = Constants.H4;
            _messageSize = messageSize * 8;
        }
        //Make the last 8 bytes equal to the length of the input data
        private byte[] DataBlockComplement(byte[] data)
        {
            ulong umask = 0x00000000000000FF;
            byte shift = 0;

            for (int i = Constants.BlockSize - 1; i >= Constants.Limit; --i, ++shift)
            {
                ulong temp = _messageSize & umask;
                temp >>= (shift*8);
                data[i] = (byte) (temp & 0x00000000000000FF);
                umask <<= 8;
            }
            return data;
        }
        //return the 64 bytes representation of the data block
        private byte[] ExtendBlock(byte[] data)
        {
            byte[] resultBlock = new byte[Constants.BlockSize];
            for (int i = 0; i < data.Length; ++i)
            {
                resultBlock[i] = data[i];
            }
            return resultBlock;
        }
        //Correct the data block if its size is lower than 64 bytes
        private byte[][] DataCorrection(byte[] dataBlock)
        {
            int blockBorder = dataBlock.Length;
            byte[][] result = new byte[1][];
            if (blockBorder == Constants.BlockSize)
            {
                result[0] = dataBlock.ToArray();
            }
            else
            {
                dataBlock = ExtendBlock(dataBlock);
                dataBlock[blockBorder] = 0x80;
                ++blockBorder;
                if (blockBorder < Constants.Limit)
                    result[0] = DataBlockComplement(dataBlock);
                else
                {
                    result = new byte[2][];
                    result[0] = dataBlock;
                    byte[] temp = new byte[Constants.BlockSize];
                    result[1] = DataBlockComplement(temp);
                }
            }
            return result;
        }
        //cyclical shift
        private uint LeftCyclicalShift(uint number, int n)
        {
            uint mask = 0x80000000;
            for (int i = 0; i < n; ++i)
            {
                uint temp = number & mask;
                temp >>= 31;
                number <<= 1;
                number |= temp;
            }
            return number;
        }
        //divide the source data block into 80 parts
        private uint[] DataBlockDivision(byte[] dataBlock)
        {
            uint[] resultParts = new uint[Constants.ExtendedNumberOfParts];

            for (int i = 0; i < Constants.NumberOfParts; ++i)
            {
                uint temp = 0;
                for (int j = 0; j < 4; ++j)
                {
                    temp |= ((uint)dataBlock[i*4 + j]) << (24 - j*8);    
                }
                resultParts[i] = temp;
            }
            for (int i = Constants.NumberOfParts; i < Constants.ExtendedNumberOfParts; ++i)
            {
                resultParts[i] = (resultParts[i - 3] ^ resultParts[i - 8] ^
                                  resultParts[i - 14] ^ resultParts[i - 16]);
                resultParts[i] = LeftCyclicalShift(resultParts[i], 1);
            }
            return resultParts;
        }

        public override string ToString()
        {
            string result = Convert.ToString(_a, 16) + Convert.ToString(_b, 16) +
                            Convert.ToString(_c, 16) + Convert.ToString(_d, 16) +
                            Convert.ToString(_e, 16);
            return result.ToUpper();
        }

        public void ProcessDataBlock(byte[] inputDataBlock)
        {
            byte[][] dataBlocks = DataCorrection(inputDataBlock);

            int numberOfBlocks = (dataBlocks.Length > Constants.BlockSize) ? 2 : 1;

            for (int i = 0; i < numberOfBlocks; ++i)
            {
                byte[] dataBlock = dataBlocks[i];
                uint a = _a;
                uint b = _b;
                uint c = _c;
                uint d = _d;
                uint e = _e;

                uint[] w = DataBlockDivision(dataBlock);
                uint f = 0;
                uint k = 0;
                for (int j = 0; j < Constants.ExtendedNumberOfParts; ++j)
                {
                    if (j <= 19)
                    {
                        f = (b & c) | ((~b) & d);
                        k = Constants.K0;
                    } else if ((19 < j) && (j <= 39))
                    {
                        f = b ^ c ^ d;
                        k = Constants.K1;
                    }else if ((39 < j) && (j <= 59))
                    {
                        f = (b & c) | (b & d) | (c & d);
                        k = Constants.K2;
                    } else if ((59 < j) && (j <= 79))
                    {
                        f = b ^ c ^ d;
                        k = Constants.K3;
                    }

                    uint temp = LeftCyclicalShift(a, 5);
                    temp = (uint)(((ulong)temp + (ulong)f) % Constants.Modulus);
                    temp = (uint)(((ulong)temp + (ulong)e) % Constants.Modulus);
                    temp = (uint)(((ulong)temp + (ulong)k) % Constants.Modulus);
                    temp = (uint)(((ulong)temp + (ulong)w[j]) % Constants.Modulus);
                    e = d;
                    d = c;
                    c = LeftCyclicalShift(b, 30);
                    b = a;
                    a = temp;
                }
                _a += a;
                _b += b;
                _c += c;
                _d += d;
                _e += e;
            }
        }
    }
}
