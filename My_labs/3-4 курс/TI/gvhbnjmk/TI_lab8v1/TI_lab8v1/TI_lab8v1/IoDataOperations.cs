﻿using System;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace TI_lab8v1
{
    class IoDataOperations
    {
        private readonly string _fileName;
        private ShaDigitalSignature shaHash;

        public IoDataOperations(string fileName)
        {
            _fileName = fileName;
        }

        public string GetSHA1HashFunction()
        {
            byte[] data;
            using (FileStream fReader = new FileStream(_fileName, FileMode.Open))
            {
                BinaryReader fBinReader = new BinaryReader(fReader, Encoding.ASCII);
                try
                {
                    shaHash = new ShaDigitalSignature((ulong)fReader.Length);
                    do
                    {
                        data = fBinReader.ReadBytes(Constants.BlockSize);
                        if (data.Length == 0)
                            data = new byte[1];
                        shaHash.ProcessDataBlock(data);
                    } while (fBinReader.PeekChar() != -1);
                }
                catch (IOException e)
                {
                    throw e;
                }
                finally
                {
                    fBinReader.Close();
                }
            }
            return shaHash.ToString();
        }

        public void GetKey(string keyFile, out string rStr, out string keyStr)
        {
            keyStr = "";
            rStr = "";
            StreamReader keyReader = new StreamReader(keyFile);
            try
            {
                string temp = keyReader.ReadToEnd();
                string[] keysAr = temp.Split('\n');
                rStr = keysAr[0];
                keyStr = keysAr[1];
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error!", MessageBoxButtons.OK);
            }
            finally
            {
                keyReader.Close();
            }
        }

        public void WriteString(string fileName, string message)
        {
            StreamWriter writer = new StreamWriter(fileName);
            try
            {
                writer.Write(message);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error!", MessageBoxButtons.OK);
            }
            finally
            {
                writer.Close();
            }
        }

        public string ReadString(string fileName)
        {
            StreamReader reader = new StreamReader(fileName);
            string temp = "";
            try
            {
                temp = reader.ReadLine();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error!", MessageBoxButtons.OK);
            }
            finally
            {
                reader.Close();
            }
            return temp;
        }
    }
}
