﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;

namespace TI_lab8v1
{
    class EllipticCurve
    {
        //fields
        private static int _m;
        private static int _a;
        private static int _b;
        static private List<EllipticCurve> _ellipticGroup;

        public Point _point;


        //properties
        public int GetA
        {
            get { return _a; }
        }

        public int GetB
        {
            get { return _b; }
        }
        public string GetCoordinates
        {
            get
            {
                string temp = "";
                temp += "(" + _point.X.ToString() + "," +
                        _point.Y.ToString() + ")";
                return temp;
            }
        }
        //constructors
        public EllipticCurve(int M)
        {
            _m = M;
            bool flag = CalculateAB(out _a, out _b);
            //    _m = 5;
            //    _a = 0;
            //    _b = 1;
            _point = new Point();
            _ellipticGroup = new List<EllipticCurve>();
            CalculateEllipticGroup();
            //    EllipticCurve m1 = new EllipticCurve();
            //    EllipticCurve m2 = new EllipticCurve(0, 4);
            //    EllipticCurve m3 = m1 + m2;
        }
        public EllipticCurve()
        {
            _point = new Point();
            /*
            _m = 211;
            _a = 0;
            _b = -4;
            CalculateEllipticGroup();
             */
        }
        public EllipticCurve(int x, int y)
        {
            _point = new Point(x, y);

        }
        //overloaded operators
        public static bool operator ==(EllipticCurve c1, EllipticCurve c2)
        {
            if (c1._point == c2._point) return true;
            return false;
        }
        public static bool operator !=(EllipticCurve c1, EllipticCurve c2)
        {
            if (c1._point == c2._point) return false;
            return true;
        }
        public static EllipticCurve operator -(EllipticCurve c1)
        {
            EllipticCurve temp = new EllipticCurve();
            temp._point.X = c1._point.X;
            temp._point.Y = c1._point.Y;
            temp._point.Y = _m - temp._point.Y;
            return temp;
        }
        public static EllipticCurve operator +(EllipticCurve c1, EllipticCurve c2)
        {
            EllipticCurve result = new EllipticCurve();
            int numerator = 0;
            int denominator = 0;

            if (c1._point.X == 0 && c1._point.Y == 0)
                return c2;
            if (c2._point.X == 0 && c2._point.Y == 0)
                return c1;

            if (c1 != c2)
            {
                if (c1 == -c2)
                    return new EllipticCurve();
                numerator = c2._point.Y - c1._point.Y;
                denominator = c2._point.X - c1._point.X;
            }
            else
            {
                numerator = 3 * c1._point.X * c1._point.X + _a;
                denominator = 2 * c1._point.Y;
            }
            int t1 = (numerator % _m >= 0) ? (numerator % _m) : (numerator % _m + _m);
            int t2 = ModPow(denominator, _m - 2, _m);
            t2 = (t2 >= 0) ? t2 : (t2 + _m);
            int lambda = (t1 * t2) % _m;
            lambda = (lambda >= 0) ? lambda : (lambda + _m);

            int X3 = (lambda * lambda - c1._point.X - c2._point.X) % _m;
            X3 = (X3 >= 0) ? X3 : (X3 + _m);
            int Y3 = (-c1._point.Y + lambda * (c1._point.X - X3)) % _m;
            Y3 = (Y3 >= 0) ? Y3 : (Y3 + _m);
            result._point.X = X3;
            result._point.Y = Y3;
            return result;
        }

        public static EllipticCurve operator *(EllipticCurve c1, int n)
        {
            EllipticCurve result = c1;
            for (int i = 0; i < n - 1; ++i)
                result += c1;
            return result;
        }

        //methods
        
        public static EllipticCurve DetermineGeneratingPoint(out int op)
        {
            op = 0;
            for (int num = 0; num < _ellipticGroup.Count(); ++num)
            {
                for (int i = 0; (i < Constants.Characteristic); ++i)
                {
                    for (int j = i + 1; j < 8 * Constants.Characteristic; ++j)
                    {
                        EllipticCurve p1 = _ellipticGroup[num] * i;
                        EllipticCurve p2 = _ellipticGroup[num] * j;
                        if (p1 == p2)
                        {
                            if (IsPrime(j - i))
                            {
                                op = j - i;
                                return _ellipticGroup[num];
                            }
                            break;
                        }
                    }
                }
            }
            Random rnd = new Random();
            return _ellipticGroup[rnd.Next(0, _ellipticGroup.Count)];
        }
        
        /*
        public static EllipticCurve DetermineGeneratingPoint(out int op)
        {
            op = 0;
            for (int num = 1; num < _ellipticGroup.Count(); ++num)
            {
                for (int i = 0; (i < Constants.Characteristic); ++i)
                {
                    for (int j = i + 1; j < Constants.Characteristic; ++j)
                    {
                        if (IsPrime(j - i))
                        {
                            EllipticCurve p1 = _ellipticGroup[num]*i;
                            EllipticCurve p2 = _ellipticGroup[num]*j;
                            if (p1 == p2)
                            {
                                op = j - i;
                                return _ellipticGroup[num];
                            }
                       //     break;
                        }
                    }
                }
            }
            Random rnd = new Random();
            return _ellipticGroup[rnd.Next(0, _ellipticGroup.Count)];
        }
         */
        public static string[][] GetEllipticGroup()
        {
            string[][] result = new string[_ellipticGroup.Count][];
            for (int i = 0; i < _ellipticGroup.Count; ++i)
            {
                result[i] = new string[2];
                result[i][0] = i.ToString();
                result[i][1] = "(" + _ellipticGroup[i]._point.X.ToString() +
                               "," + _ellipticGroup[i]._point.Y.ToString() +
                               ");";
            }
            return result;
        }
        private void CalculateEllipticGroup()
        {
            _ellipticGroup.Add(new EllipticCurve());
            for (int x = 1; x < _m; ++x)
            {
                long tempX = (x * x * x + _a * x + _b) % _m;
                for (int y = 1; y < _m; ++y)
                {
                    long tempY = (y * y) % _m;
                    if (tempX == tempY)
                        _ellipticGroup.Add(new EllipticCurve(x, y));
                }
            }
        }
        private static bool IsPrime(int number)
        {
            if (number == 2 || number == 1)
                return false;
            for (int i = 2; i < (number >> 1); ++i)
            {
                if (number % i == 0)
                    return false;
            }
            return true;
        }
        private bool CalculateAB(out int a, out int b)
        {
            int iterations = 0;
            /*
            for (int i = 1; i < _m; ++i)
            {
                for (int j = 2; j < _m; ++j)
                {
                    long temp = (4*i*i + 27*j*j)%_m;
                    if (temp != 0)
                    {
                        a = i;
                        b = j;
                        return true;
                    }
                }
            }
             * */

            Random r = new Random();
            do
            {
                int tempA = r.Next(1, _m);
                int tempB = r.Next(1, _m);
                long temp = 4 * tempA * tempA * tempA + 27 * tempB * tempB;
                if (temp % _m != 0)
                {
                    a = tempA;
                    b = tempB;
                  //  a = 34;
                  //  b = 18;
                   
                    return true;
                }
                ++iterations;
            } while (iterations < Constants.LimitAB);

            a = 0;
            b = 0;
            return false;
        }
        private static int ModPow(int x, int exp, int mod)
        {
            int b = 1;
            while (exp != 0)
            {
                if (exp % 2 == 0)
                {
                    exp >>= 1;
                    x = (x * x) % mod;
                }
                else
                {
                    --exp;
                    b = (b * x) % mod;
                }
            }
            return b;
        }

    }
}
