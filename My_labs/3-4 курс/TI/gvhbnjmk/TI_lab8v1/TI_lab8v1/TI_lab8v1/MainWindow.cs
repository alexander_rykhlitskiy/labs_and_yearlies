﻿using System;
using System.Windows.Forms;

namespace TI_lab8v1
{
    public partial class MainWindow : Form
    {
        private EllipticCurve ellipticCurve;
        private string _fileName;
        private int characteristic;
        private int _na;
        private int _nb;

        public MainWindow()
        {
            InitializeComponent();
            GenerateSourceData();
        }

        private void GenerateSourceData()
        {
            ellipticCurve = new EllipticCurve(Int32.Parse(TextBox_M.Text));
            string[][] ellipticGroup = EllipticCurve.GetEllipticGroup();
            for (int i = 0; i < ellipticGroup.Length; ++i)
            {
                DataGridView_EllipticGroup.Rows.Add(ellipticGroup[i]);
            }
            TextBox_a.Text = ellipticCurve.GetA.ToString();
            TextBox_b.Text = ellipticCurve.GetB.ToString();
            button_GenerateDigitalSignature.Enabled = false;
            button_VerifyDigitalSignature.Enabled = false;
        }

        private void button_GenerateOpenKeys_Click(object sender, EventArgs e)
        {
            EllipticCurve generatingPoint = 
                EllipticCurve.DetermineGeneratingPoint(out characteristic);
            Random rnd = new Random();
            _na = rnd.Next(0, Int32.Parse(TextBox_M.Text));
            _nb = rnd.Next(0, Int32.Parse(TextBox_M.Text));

            textBox_Na.Text = _na.ToString();
            textBox_G.Text = generatingPoint.GetCoordinates;

            EllipticCurve pa = generatingPoint*_na;
            EllipticCurve pb = generatingPoint*_nb;

            textBox_Pa.Text = pa.GetCoordinates;
            textBox_Pb.Text = pb.GetCoordinates;
            button_GenerateDigitalSignature.Enabled = true;
            /*
            EllipticCurve k1 = pa*nb;
            EllipticCurve k2 = pb*na;

            textBox1.Text = k1.GetCoordinates;
            textBox2.Text = k2.GetCoordinates;
             * */
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openDlg = new OpenFileDialog())
            {
                openDlg.InitialDirectory = ".";
                openDlg.Filter = "txt files (*.txt)|*.txt";
                openDlg.RestoreDirectory = true;
                openDlg.FileName = "numbers.txt";
                if (openDlg.ShowDialog() == DialogResult.OK)
                {
                    _fileName = openDlg.FileName;
                }
                if (_fileName == "")
                    MessageBox.Show("Choose the file!", "Attention!", MessageBoxButtons.OK);
            }
        }

        private void button_GenerateDigitalSignature_Click(object sender, EventArgs e)
        {
            if (_fileName != "")
            {
                Ecdsa sigSender = new Ecdsa(_fileName, _na);
                sigSender.GenerateDigitalSignature();
                MessageBox.Show("Digital signature has been generated successfully!",
                                "Success!", MessageBoxButtons.OK);
                button_GenerateDigitalSignature.Enabled = false;
                button_VerifyDigitalSignature.Enabled = true;
            }
        }

        private void button_VerifyDigitalSignature_Click(object sender, EventArgs e)
        {
            if (_fileName != "")
            {
                try
                {
                    Ecdsa sigReceiver = new Ecdsa(_fileName, _na);
                    bool flag = sigReceiver.VerifyDigitalSignature();
                    if (flag)
                        MessageBox.Show("Digital signature is correct!",
                                        "Success!", MessageBoxButtons.OK);
                    else
                        MessageBox.Show("Digital signature is broken!",
                                        "Failure!", MessageBoxButtons.OK);
                    button_GenerateDigitalSignature.Enabled = false;
                    button_VerifyDigitalSignature.Enabled = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK);
                }
            }
        }
    }
}
