To be sure, the broad river now cut them off from this beautiful land. But the raft was nearly done, 
and after the Tin Woodman had cut a few more logs and fastened them together with wooden pins, they 
were ready to start. Dorothy sat down in the middle of the raft and held Toto in her arms. When the 
Cowardly Lion stepped upon the raft it tipped badly, for he was big and heavy; but the Scarecrow and
 the Tin Woodman stood upon the other end to steady it, and they had long poles in their hands to
 push the raft through the water.