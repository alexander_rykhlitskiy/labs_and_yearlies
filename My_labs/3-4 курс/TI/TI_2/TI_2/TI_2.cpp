// TI_2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int GetBit(int number, int serialNumber)
{
	int result = 0;
	int temp = 0;
	temp = 1 << serialNumber;
	result = temp & number;
	result >>= serialNumber;
	return result;
}
int ShiftToLeft(int number, int quantity, int maxDegree)
{
	int result = 0;
	int temp = (int)pow(2.0, maxDegree) - 1;
	number <<= quantity;
	result = number & temp;
	return result;
}
void OutputInBinaryFormat(int number)
{
	char binaryNumber[100];
	int i = 0;
	while (number >= 1)
	{
		if ((number % 2) == 1) binaryNumber[i] = '1';
			else binaryNumber[i] = '0';
		number /= 2;
		i++;
	}
	for(i = i - 1; i >= 0; i--)
		printf("%c", binaryNumber[i]);
	printf("\n");
}
int GetTempKey(unsigned long long *b, int a, int degree)
{
	int tempKey = 0;
	for(int i = 0; i < 8; i++)
	{
		tempKey <<= 1;
		tempKey |= GetBit(*b, degree - 1);
		int firstBit = GetBit(a, 0) & GetBit(*b, 0);
		for (int j = 1; j < degree; j++)
		{
			firstBit = firstBit ^ (GetBit(a, j) & GetBit(*b, j));
		}
		*b = ShiftToLeft(*b, 1, degree);
		*b |= firstBit;
	}
	return tempKey;
}
int WriteFileInString(FILE *f, char** result)
{
	int i = 0;
	while (feof(f) == 0)
	{
		char c;
		c = fgetc(f);
		i++;
	}
	*result = (char *) calloc(i, sizeof(char));
	fseek(f, 0, SEEK_SET);
	fread(*result, i, 1, f);
	return i;
}
int LFSRCyphering(char* MString, int lengthOfMString)
{
	const int degree = 27;
	unsigned long long a, b;
	a = (1 << 26) + (1 << 7) + (1 << 6) + (1 << 0);
	b = pow(2.0, degree) - 1;
	for (int i = 0; i < lengthOfMString; i++)
	{
		MString[i] = MString[i] ^ GetTempKey(&b, a, degree);
	}
	return 0;
}
int GeffeCyphering(char* MString, int lengthOfMString)
{
	const int degree[3] = {27, 35, 25};
	unsigned long long a[3] = {}, b[3] = {};
	int tempKey = 0;
	a[0] = (1 << 26) + (1 << 7) + (1 << 6) + (1 << 0);
	a[1] = (1 << 34) + (1 << 1);
	a[2] = (1 << 24) + (1 << 2);
	for (int j = 0; j < 3; j++)
	{
		b[j] = pow(2.0, degree[j]) - 1;
	}
	int xG = 0;
	char x[3] = {};
	for (int i = 0; i < lengthOfMString; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			x[j] = GetTempKey(&b[j], a[j], degree[j]);
		}
		xG = (x[0] & x[1]) | (~x[0] & x[2]);
		MString[i] = MString[i] ^ xG;
	}
	return 0;
}
void Swap(unsigned char *a, unsigned char *b)
{
	char temp;
	temp = *a;
	*a = *b;
	*b = temp;
}
int RC4Cyphering(char* MString, int lengthOfMString)
{
	char userKeyFileString[100];
	int input;
	puts("Do you want to enter path to file with key? (1 - yes, 0 - no)");
	scanf("%d", &input);
	if (input == 0)
	{
		strcpy(userKeyFileString, "userKeyFile.txt");
	}
	else
	{
		flushall();
		puts("Name of readFile:");
		gets(userKeyFileString);
	}
	FILE *userKeyFile;
	userKeyFile = fopen(userKeyFileString, "r");
	if (userKeyFile == 0)
	{
		printf("There is no file \"userKeyFile.txt\" in folder \"Debug\"");
		getchar();
		return 1;
	}
	char* userKey;
	WriteFileInString(userKeyFile, &userKey);
	int j = 0;
	unsigned char sBox[256];
	for (int i = 0; i <= 255; i++)
	{
		sBox[i] = i;
	}
	for (int i = 0; i <= 255; i++)
	{
		j = (j + sBox[i] + userKey[i % strlen(userKey)]) % 256;
		int temp;
		temp = sBox[i];
		sBox[i] = sBox[j];
		sBox[j] = temp;
		Swap(&sBox[i], &sBox[j]);
	}
	j = 0;
	unsigned char key = 0;
	int i = 0;
	for (int k = 0; k < lengthOfMString; k++)
	{
		i = (i + 1) % 256;
		j = (j + sBox[i]) % 256;
		Swap(&sBox[i], &sBox[j]);
		key = sBox[(sBox[i] + sBox[j]) % 256];
		MString[k] = MString[k] ^ key;
	}
	return 0;
}
int _tmain(int argc, _TCHAR* argv[])
{
		//FILE *userKeyFile;
		//char string[10];
		//strcpy(string, "mouse");
		//string[2] = '\n';
		//puts(string);
		//getchar();
	int input = 0;
	char readFileString[100], writeFileString[100], finalFileString[100];
	puts("Do you want to enter paths to files? (1 - yes, 0 - no)");
	scanf("%d", &input);
	if (input == 0)
	{
		strcpy(readFileString, "readFile.txt");
		strcpy(writeFileString, "writeFile.txt");
		strcpy(finalFileString, "finalFile.txt");
	}
	else
	{
		flushall();
		puts("Name of readFile:");
		gets(readFileString);
		puts("Name of writeFile:");
		gets(writeFileString);
		puts("Name of finalFile:");
		gets(finalFileString);
	}
	FILE *readFile;
	readFile = fopen(readFileString, "r");
	if (readFile == 0)
	{
		printf("There is no file \"readFile.txt\" in folder \"Debug\"");
		getchar();
		return 1;
	}
	char *MString;
	int lengthOfMString = WriteFileInString(readFile, &MString);
	FILE *writeFile = fopen(writeFileString, "w+");
	FILE *finalFile = fopen(finalFileString, "w+");

	while (input != 4)
	{
		puts("Choose type of cyphering:");
		puts("1 - LFSR");
		puts("2 - Geffe");
		puts("3 - RC4");
		puts("4 - Exit");
		scanf("%d", &input);
		int (*Cyphering) (char *, int);
		switch (input)
		{
			case 1:
				Cyphering = LFSRCyphering;
				break;
			case 2:
				Cyphering = GeffeCyphering;
				break;
			case 3:
				Cyphering = RC4Cyphering;
				break;
		}
		if ((input == 1) || (input == 2) || (input == 3))
		{
			FILE *writeFile = fopen(writeFileString, "w+");
			FILE *finalFile = fopen(finalFileString, "w+");
			puts(MString);
			Cyphering(MString, lengthOfMString);
			fwrite(MString, lengthOfMString, 1, writeFile); 
			puts(MString);
			Cyphering(MString, lengthOfMString);
			puts(MString);
			fwrite(MString, lengthOfMString - 1, 1, finalFile); 
			getchar();
		}
	}

	fclose(readFile);
	fclose(writeFile);
	fclose(finalFile);
	return 0;
}

