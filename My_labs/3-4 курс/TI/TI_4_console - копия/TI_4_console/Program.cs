﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TI_4_console
{
    class Program
    {
        static string currentCypheringString;
        const string readFile = "readFile.txt";
        const string writeFile = "writeFile.txt";
        const string finalFile = "finalFile.txt";
        static ushort[] constKey = { 32768, 2, 38000, 4, 5, 6, 7, 8 };
        static ushort[] key = { 32768, 2, 38000, 4, 5, 6, 7, 8 };
        static List<Subunit> Z = new List<Subunit>();
        static List<Subunit> U = new List<Subunit>();
        static void Swap(ref Subunit a, ref Subunit b)
        {
            Subunit temp = a;
            a = b;
            b = temp;
        }
        static void CycleIDEA(List<Subunit> X, List<Subunit> U)
        {
            List<Subunit> I = new List<Subunit>();
            for (int i = 0; i < 4; i++)
                I.Add(new Subunit());
            List<Subunit> F = new List<Subunit>();
            for (int i = 0; i < 2; i++)
                F.Add(new Subunit());

            for (int i = 0; i < 8; i++)
            {
                I[0] = X[0] * U[i * 6];
                I[1] = X[1] + U[i * 6 + 1];
                I[2] = X[2] + U[i * 6 + 2];
                I[3] = X[3] * U[i * 6 + 3];

                F[0] = I[0] ^ I[2];
                F[1] = I[1] ^ I[3];

                F[0] = F[0] * U[i * 6 + 4];
                F[1] = F[0] + F[1];
                F[1] = F[1] * U[i * 6 + 5];
                F[0] = F[0] + F[1];

                X[0] = I[0] ^ F[1];
                X[2] = I[1] ^ F[0];
                X[1] = I[2] ^ F[1];
                X[3] = I[3] ^ F[0];

            }
            X[0] = X[0] * U[48];
            X[2] = X[2] + U[49];
            X[1] = X[1] + U[50]; 
            X[3] = X[3] * U[51];
            Subunit temp = X[2];
            X[2] = X[1];
            X[1] = temp;
         }
        public static void GetNewKey(List<Subunit> Z, List<Subunit> U)
        {
            for (int i = 0; i < 8; i++)
            {
                U.Add(--Z[48 - i * 6]);
 //               U[i * 6] = --Z[48 - i * 6];
                if (i == 0)
                {
                    U.Add(-Z[49]);
                    U.Add(-Z[50]);
                    //U[1] = -Z[49];
                    //U[2] = -Z[50];
                }
                else
                {
                    U.Add(-Z[50 - i * 6]);
                    U.Add(-Z[49 - i * 6]);
                    //U[i * 6 + 1] = -Z[50 - i * 6];
                    //U[i * 6 + 2] = -Z[49 - i * 6];
                }
                U.Add(--Z[51 - i * 6]);
                U.Add(Z[46 - i * 6]);
                U.Add(Z[47 - i * 6]);
                //U[i * 6 + 3] = --Z[51 - i * 6];
                //U[i * 6 + 4] = Z[46 - i * 6];
                //U[i * 6 + 5] = Z[47 - i * 6];
            }
            U.Add(--Z[0]);
            U.Add(-Z[1]);
            U.Add(-Z[2]);
            U.Add(--Z[3]);
            //U[48] = --Z[0];
            //U[49] = -Z[1];
            //U[50] = -Z[2];
            //U[51] = --Z[3];
        }
        static ushort GetBit(ushort a)
        {
            ushort result = (ushort)(a & (1 << 15));
            result >>= 15;
            return result;
        }
        static void ShiftArrayLeft(ushort[] key)
        {
            const ushort shiftQuantity = 25;
            ushort[] bits = new ushort[8];
            ushort previousBit = 0, currentBit = 0;
            for (int j = 0; j < shiftQuantity; j++)
            {
                previousBit = GetBit(key[0]);
                for (int i = key.Length - 1; i >= 0; i--)
                {
                    currentBit = GetBit(key[i]);
                    key[i] <<= 1;
                    key[i] |= previousBit;
                    previousBit = currentBit;
                }
            }
        }
        static void GenerateKey(List<Subunit> Z)
        {
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Z.Add(key[j]);
                }
                ShiftArrayLeft(key);
            }
            for (int j = 0; j < 4; j++)
            {
                Z.Add(key[j]);
            }
        }
        static void WriteSymbolInBinaryFile(String symbol, string file)
        {
            FileStream fs = File.Create(file);
            UTF8Encoding utf8 = new UTF8Encoding();
            BinaryWriter bw = new BinaryWriter(fs, utf8);
            bw.Write(symbol);
            fs.Close();
        }
        static void ReadSymbolFromBinaryFile(ref char symbol, string file)
        {
            FileStream fs = File.Open(file, FileMode.Open);
            UTF8Encoding utf8 = new UTF8Encoding();
            BinaryReader br = new BinaryReader(fs, utf8);
            symbol = br.ReadChar();
            fs.Close();
        }
        static void CypheringIDEA(string text, bool cyphering)
        {
            StringBuilder tempText = new StringBuilder(text);
            List<Subunit> X = new List<Subunit>();
            for (int j = 0; j < 4; j++)
            {
                X.Add(new Subunit());
            }
            for (int i = 0; i < text.Length; i += 4)
            {
                for (int j = i; j < i + 4; j++)
                {
                    if (j >= text.Length)
                    {
                        X[2] = 0;
                        X[3] = 0;
                        Console.WriteLine("{0}  {1}  {2}  {3}", (char)X[0], (char)X[1], (char)X[2], (char)X[3]);
                        break;
                    }
                    X[j - i] = (uint)text[j];
                }


                Z = new List<Subunit>();
                GenerateKey(Z);
                if (cyphering)
                    CycleIDEA(X, Z);
                else
                {
                    U = new List<Subunit>();
                    GetNewKey(Z, U);
                    CycleIDEA(X, U);
                }
                for (int j = i; j < i + 4; j++)
                {
                    if (j >= text.Length)
                        break;
                    //if (cyphering)
                    //    WriteSymbolInBinaryFile((char)X[j - i], writeFile);
                    //else
                    //    WriteSymbolInBinaryFile((char)X[j - i], finalFile);
                    tempText[j] = (char)X[j - i];//записывать сразу в файл
                }
            }
            

            //for (int j = i; j < text.Length; j++)
            //{
            //    X[j - i] = (uint)text[j];
            //}
            //Z = new List<Subunit>();
            //GenerateKey(Z);
            //if (cyphering)
            //    CycleIDEA(X, Z);
            //else
            //{
            //    U = new List<Subunit>();
            //    GetNewKey(Z, U);
            //    CycleIDEA(X, U);
            //}
            //for (int j = i; j < text.Length; j++)
            //{
            //    tempText[j] = (char)X[j - i];//записывать сразу в файл
            //}



            if (cyphering)
            {
                WriteSymbolInBinaryFile(tempText.ToString(), writeFile);
                //System.IO.File.WriteAllText(writeFile, tempText.ToString());
            }
            else
            {
                System.IO.File.WriteAllText(finalFile, tempText.ToString());
            }
            currentCypheringString = tempText.ToString();
        }
        static void Main(string[] args)
        {
            String text = System.IO.File.ReadAllText(readFile);
            Console.WriteLine(text);
            CypheringIDEA(text, true);

            key = constKey;

            text = currentCypheringString;
            Console.WriteLine(text);
            CypheringIDEA(text, false);
            text = currentCypheringString;
            Console.WriteLine(text);

            Console.ReadKey();
        }
    }
}
//{ 0x1, 0x2, 0x3, 0x4, 0x5, 0x6,
//                                        0x0007,	0x0008,	0x0400,	0x0600,	0x0800,	0x0a00,
//                                        0x0c00,	0x0e00,	0x1000,	0x0200,	0x0010,	0x0014,
//                                        0x0018,	0x001c,	0x0020,	0x0004,	0x0008,	0x000c,
//                                        0x2800,	0x3000,	0x3800,	0x4000,	0x0800,	0x1000,
//                                        0x1800,	0x2000,	0x0070,	0x0080,	0x0010,	0x0020,
//                                        0x0030,	0x0040,	0x0050,	0x0060,	0x0000,	0x2000,
//                                        0x4000,	0x6000,	0x8000,	0xa000,	0xc000,	0xe001,
//                                        0x0080,	0x00c0,	0x0100,	0x0140};
//for (int i = 0; i < 52; i++)
//    Z.Add(new Subunit((uint)i));
//{--Z[48], -Z[49], -Z[50], --Z[51], Z[46], Z[47],
//                                       --Z[42], -Z[44], -Z[43], --Z[45], Z[40], Z[41],
//                                       --Z[36], -Z[38], -Z[37], --Z[39], Z[34], Z[35],
//                                       --Z[30], -Z[32], -Z[31], --Z[33], Z[28], Z[29],
//                                       --Z[24], -Z[26], -Z[25], --Z[27], Z[22], Z[23],
//                                       --Z[18], -Z[20], -Z[19], --Z[21], Z[16], Z[17],
//                                       --Z[12], -Z[14], -Z[13], --Z[15], Z[10], Z[11],
//                                       --Z[6], -Z[8], -Z[7], --Z[9], Z[4], Z[5],
//                                       --Z[0], -Z[1], -Z[2], --Z[3]};
