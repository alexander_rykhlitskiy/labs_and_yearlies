﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace TI_6
{
    class Program
    {
                public const int mod = 65537;
        public static int binPow(long a, int n)
        {
            long r = 1;
            while (n != 0)
            {
                if ((n & 1) != 0)
                    r = (r * a) % mod;
                n >>= 1;
                a = (a * a) % mod;
            }
            return (int)r;
        }
        static void Main(string[] args)
        {
            String readFile = "readFile.txt";
            String writeFile = "writeFile.txt";
            String finalFile = "finalFile.txt";
            String crackFile = "crackFile.txt";
            byte[] text;
            try
            {
                text = System.IO.File.ReadAllBytes(readFile);
                File.WriteAllBytes(writeFile, RSA.Cyphering(text));
                text = System.IO.File.ReadAllBytes(writeFile);
                File.WriteAllBytes(finalFile, RSA.Decyphering(text));
                try
                {
                    File.WriteAllBytes(crackFile, RSA.Crack(text, 2419, 157));
                }
                catch (FailInitialData)
                {
                    Console.WriteLine("Fail Initial Data");
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("File " + readFile + " not found");
            }
            Console.ReadKey();
        }
    }
}