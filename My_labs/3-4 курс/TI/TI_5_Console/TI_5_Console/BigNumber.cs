﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TI_5_Console
{
    class BigNumber
    {
        public List<byte> row;
        public bool signed = false;
        public BigNumber()
        {
            row = new List<byte>();
        }
        public BigNumber(int count)
        {
            row = new List<byte>();
            for (int i = 0; i < count; i++)
            {
                row.Add(0);
            }
        }
        public BigNumber(BigNumber a)
        {
            this.row = new List<byte>(a.row);
            this.signed = a.signed;
        }
        public BigNumber(List<byte> row)
        {
            this.row = new List<byte>(row);
            this.signed = false;
        }
        public BigNumber(String numberString)
        {
            BigNumber a = new BigNumber();
            byte temp = 0;
            for (int i = numberString.Length - 1; i >= 0; i -= 2)//222
            {
                for (int k = i; (k >= i - 1) & (k >= 0); k--)//222
                {
                    temp |= (byte)(byte.Parse(numberString[k].ToString(), System.Globalization.NumberStyles.HexNumber) << (4 * (i - k)));
                }
                a.row.Add(temp);
                temp = 0;
            }
            row = a.row;
        }
        public static BigNumber operator +(BigNumber u, BigNumber v)
        {

            BigNumber result = new BigNumber();
            byte carry = 0;
            for (int i = 0; i < (u.row.Count > v.row.Count ? u.row.Count : v.row.Count); i++)
            {
                byte temp = 0;
                try
                {
                    temp = checked((byte)((i < u.row.Count ? u.row[i] : 0) + (i < v.row.Count ? v.row[i] : 0) + carry));
                    carry = 0;
                }
                catch (System.OverflowException)
                {
                    temp = (byte)((i < u.row.Count ? u.row[i] : 0) + (i < v.row.Count ? v.row[i] : 0) + carry);
                    carry = 1;
                }
                result.row.Add(temp);
            }
            if (carry != 0)
                result.row.Add(carry);
            return result;
        }
        public static BigNumber operator -(BigNumber u, BigNumber v)
        {

            BigNumber result = new BigNumber();
            byte carry = 0;
            for (int i = 0; i < (u.row.Count > v.row.Count ? u.row.Count : v.row.Count); i++)
            {
                byte temp = 0;
                try
                {
                    temp = checked((byte)((i < u.row.Count ? u.row[i] : 0) - (i < v.row.Count ? v.row[i] : 0) - carry));
                    carry = 0;
                }
                catch (System.OverflowException)
                {
                    temp = (byte)((i < u.row.Count ? u.row[i] : 0) - (i < v.row.Count ? v.row[i] : 0) - carry);
                    carry = 1;
                }
                result.row.Add(temp);
            }
            if (carry != 0)
                result.signed = true;
            for (int i = result.row.Count - 1; (i > 0) & (result.row[i] == 0); i--)
            {
                result.row.RemoveAt(i);
            }
            return result;
        }
        public static BigNumber operator *(BigNumber u, BigNumber v)
        {
            int m = u.row.Count;
            int n = v.row.Count;
            BigNumber result = new BigNumber(m + n);
            for (int j = 0; j < n; j++)
            {
                if (v.row[j] > 0)
                {
                    byte k = 0;
                    for (int i = 0; i < m; i++)
                    {
                        UInt64 temp = (UInt64)(u.row[i] * v.row[j] + result.row[i + j] + k);
                        result.row[i + j] = (byte)(temp % (byte.MaxValue + 1));
                        k = (byte)(temp / (byte.MaxValue + 1));
                    }
                    if (k != 0)
                        try
                        {
                            result.row[j + m] = k;
                        }
                        catch(ArgumentOutOfRangeException)
                        {
                            result.row.Add(0);
                            result.row[j + m] = k;
                        }
                }
                //else
                //    result.row[j + m] = 0;
            }
            return result;
        }
        public static BigNumber operator /(BigNumber u, BigNumber v)
        {
            int n = v.row.Count;
            int m = u.row.Count - n;
            int b = 256;
            BigNumber result = new BigNumber(m + n - 1);
            int d = b / (v.row[n - 1] + 1);
            u = u * (new BigNumber(String.Format("{0:X}", d)));
            v = v * (new BigNumber(String.Format("{0:X}", d)));
            int[] q = new int[m + 1];
            for (int j = m; j >= 0; j--)
            {
                int qq = (u.row[j + n] * b + u.row[j + n - 1]) / v.row[n - 1];
                int r = (u.row[j + n] * b + u.row[j + n - 1]) % v.row[n - 1];
                if ((qq == b) | (qq * v.row[n - 2] > b * r + u.row[j + n - 2]))
                {
                    qq--;
                    r += v.row[n - 1];

                    //if ((r < b) | (qq * v.row[n - 2] > b * r + u.row[j + n - 2]))
                    //{
                    //    qq--;
                    //    r += v.row[n - 1];
                    //}
                }
                BigNumber temp = new BigNumber(u.row.GetRange(j, n + 1));
                BigNumber temp1 = new BigNumber(v);
                String str = String.Format("{0:X}", qq);
                BigNumber temp2 = new BigNumber(str);
                temp1 = temp1 * temp2;
                temp = temp - temp1;
                bool NegFlag;
                if (temp.signed)
                {
                    NegFlag = true;
                    temp1 = new BigNumber(1.ToString());
                    for (int i = 1; i <= n + 1; i++)
                        temp1 = temp1 * (new BigNumber(String.Format("{0:X}", b)));
                    temp = temp + temp1;
                }
                else
                {
                    NegFlag = false;
                }
                q[j] = qq;
                if (NegFlag)
                {
                    q[j] = q[j] - 1;
                    temp1 = new BigNumber(v.row.GetRange(0, n));
                    temp1.row.Add(0);
                    temp = temp + temp1;
                }
                temp.row.Add(0);
                for (int i = j; i <= j + n; i++)
                    u.row[i] = temp.row[i - j];
            }
            for (int i = 0; i < m + 1; i++)
            {
                result.row[i] = (byte)q[i];
            }
            return result;
        }
        public BigNumber Karatsuba(BigNumber a, BigNumber b)
        {
            BigNumber result = new BigNumber();//a.row.Count + b.row.Count);
            if ((a.row.Count <= 3) | //(a.row.Count != b.row.Count) |
                (b.row.Count <= 3))
                result = a * b;
            else
            {
                BigNumber aPart1 = new BigNumber(a);
                aPart1.row.RemoveRange((a.row.Count + 1) / 2, a.row.Count / 2);
                BigNumber aPart2 = new BigNumber(a);
                aPart2.row.RemoveRange(0, (a.row.Count + 1) / 2);

                BigNumber bPart1 = new BigNumber(b);
                bPart1.row.RemoveRange((b.row.Count + 1) / 2, b.row.Count / 2);
                BigNumber bPart2 = new BigNumber(b);
                bPart2.row.RemoveRange(0, (b.row.Count + 1) / 2);

                BigNumber sumOfAParts = aPart1 + aPart2;
                BigNumber sumOfBParts = bPart1 + bPart2;
                BigNumber resultOfSumsOfParts = Karatsuba(sumOfAParts, sumOfBParts);

                BigNumber resultOfFirstParts = Karatsuba(aPart1, bPart1);
                BigNumber resultOfSecondParts = Karatsuba(aPart2, bPart2);
                BigNumber sumOfMiddleTerms = resultOfSumsOfParts - resultOfFirstParts;
                sumOfMiddleTerms = sumOfMiddleTerms - resultOfSecondParts;
                result.row = result.row.Concat(resultOfFirstParts.row).ToList();
                for (int i = result.row.Count - 1; i >= 0; i--)
                {
                    if (result.row[i] != 0)
                        break;
                    else
                        result.row.RemoveAt(i);
                }

                result.row = result.row.Concat(resultOfSecondParts.row).ToList();
                //BigNumber temp = new BigNumber(result);
               // result.row.RemoveRange(aPart1.row.Count
                //  result.row.InsertRange(k
              //  temp.row.RemoveRange(0, aPart1.row.Count);
              //  temp = temp + sumOfMiddleTerms;
              //  temp
                for (int i = result.row.Count - 1; i >= 0; i--)
                {
                    if (result.row[i] != 0)
                        break;
                    else
                        result.row.RemoveAt(i);
                }
                for (int i = 0; i < sumOfMiddleTerms.row.Count; i++)
                {
                    try
                    {
                        result.row[aPart1.row.Count + i] = checked((byte)(result.row[aPart1.row.Count + i] + sumOfMiddleTerms.row[i]));
                    }
                    catch(OverflowException)
                    {
                        result.row[aPart1.row.Count + i] = ((byte)(result.row[aPart1.row.Count + i] + sumOfMiddleTerms.row[i]));
                        result.row.Add(0);
                        result.row[aPart1.row.Count + i + 1]++;
                    }
                }
            }
            for (int i = result.row.Count - 1; i >= 0; i--)
            {
                if (result.row[i] != 0)
                    break;
                else
                    result.row.RemoveAt(i);
            }
            return result;
        }
        public static explicit operator String(BigNumber number)
        {
            String result = new String('a', 0);
            for (int i = 0; i < number.row.Count; i++)
            {
                if ((i % 2 == 0) & (i != 0))
                    result = String.Concat(" ", result);
                result = String.Concat(String.Format("{0:X}", number.row[i]), result);
                if ((number.row[i] < 0x10) & (i != number.row.Count - 1))
                    result = String.Concat("0", result);
            }
            result = String.Concat(number.signed.ToString() + " ", result);
            return result;
        }
    }
}