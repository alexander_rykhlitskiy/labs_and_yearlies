﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TI_5_Console
{
    class Program
    {
        static void Print(byte a)
        {
            Console.WriteLine(a);
        }
        static void Main(string[] args)
        {
            //List<byte> lis = new List<byte> { 0, 1, 2, 3 };
            //lis.RemoveRange(0, 2);
            //lis.ForEach(Print);
            //List<byte> lis1 = new List<byte> { 1, 2, 4 };
            //lis = lis.Concat(lis1).ToList().Concat(lis).ToList();
            //lis.ForEach(Print);

            BigNumber a = new BigNumber("845e485b84e9ac3b4c154a41d7e45c45b");
            BigNumber b = new BigNumber("4eb58b45e45a5a4598a8bf544a451b1c4");

            //BigNumber a = new BigNumber("1234213431243");
            //BigNumber b = new BigNumber("3214423124312");
            BigNumber c = new BigNumber();
            c = c.Karatsuba(a, b);
            BigNumber d = new BigNumber();
            d = a * b;
            Console.WriteLine("           " + (string)a);
            Console.WriteLine("           " + (string)b);
            Console.WriteLine("");
            Console.WriteLine("*:         " + (string)d);
            Console.WriteLine("karatsuba: " + (string)c);
            int i = 5;
            Console.WriteLine((byte)(i - 6));
            Console.ReadKey();
        }
    }
}