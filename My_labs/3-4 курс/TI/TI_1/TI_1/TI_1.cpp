// TI_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
//#include "string.h"
#include "stdlib.h"
#include "time.h"
#include <string>
#define lettersInSubstring 9
#define key "mouse"
struct letter
{
	int quantity;
	char sign;
};
int sorting(struct letter a[25])
{
	int i, j;
	struct letter temp;
	for (i = 0; i < 25; i++)
		for (j = 0; j < 25 - i; j++)
			if (a[j].quantity < a[j+1].quantity)
			{
				temp = a[j];
				a[j] = a[j+1];
				a[j+1] = temp;
			}
	return 0;
}
int encoding(FILE *readF, FILE *writeF, int cipher)
{
	fseek(readF, 0, SEEK_SET);
	unsigned char ch;
	while (feof(readF) == 0)
	{
		ch = fgetc(readF);
		if ((ch >= 'a') && (ch <= 'z'))
		{
			ch = ch + cipher;
			if (ch > 'z')
				ch = ch - 26;
			if (ch < 'a')
				ch = ch + 26;
		}

		if (feof(readF) == 0)
			fputc(ch, writeF);
	}
	return 0;
}
int encoding_vigenere(FILE *readF, FILE *writeF, char key_word[10])
{
	fseek(writeF, 0, SEEK_SET);
	int length = strlen(key_word);
	int cipher_array[10], i = 0;
	unsigned char ch;
	while (feof(readF) == 0)
	{
		ch = fgetc(readF);
		if ((ch >= 'a') && (ch <= 'z'))
			ch = 'a' + (ch + key_word[i] - 'a' -'a')%26;
		if ((ch >= 'A') && (ch <= 'Z'))
			ch = 'A' + (ch + key_word[i] - 'a' -'A')%26;
		i = (i + 1) % length;
		if (feof(readF) == 0)
			fputc(ch, writeF);
	}
	return 0;
}
int simple_multiplier(int number, int *multipliers)
{
	int a = number, prime_number = 2, quant_of_multipliers = 0;
	while (prime_number <= a)
	{
		if ((a % prime_number) == 0)
		{
			a = a/prime_number;
			multipliers[quant_of_multipliers] = prime_number;
			quant_of_multipliers++;
			prime_number = 2;
		}
		else
			prime_number++;
	}
	return(quant_of_multipliers);
}
int common_multipliers(int number1, int number2)
{
	int i, j, k;
	int array1[30], size_array1, array2[30], size_array2;
	size_array1 = simple_multiplier(number1, array1);
	size_array2 = simple_multiplier(number2, array2);
	bool there_is_common = false;
	i = 0;
	while (i < size_array1)
	{
		there_is_common = false;
		for (j = 0; j < size_array2; j++)
		{
			if (array1[i] == array2[j])
			{
				size_array2--;
				for (k = j; k < size_array2; k++)
					array2[k] = array2[k + 1];
				there_is_common = true;
				break;
			}
		}
		if (!there_is_common)
		{
			size_array1--;
			for (k = i; k < size_array1; k++)
				array1[k] = array1[k + 1];
			i--;
		}
		i++;
	}
	int sum = 1;
	for (k = 0; k < size_array1; k++)
		sum = sum * array1[k];
	return sum;
}
int BinaryAlgorithm(int a, int b)
{
	int GCD = 1, temp = 0;
	do
	{
		if (a == 0){
			temp = b;
			break;
		}
		if (b == 0){
			temp =  a;
			break;
		}
		if (a == b){
			temp = b;
			break;
		}
		if ((a == 1) || (b == 1)){
			temp = 1;
			break;
		}
		if ((a%2 == 0) && (b%2 == 0)){
			a = a/2;
			b = b/2;
			GCD = GCD*2;
			continue;
		}
		if (a%2 == 0){
			a = a/2;
			continue;
		}
		if (b%2 == 0){
			b = b/2;
			continue;
		}
		if (a > b){
			a = (a - b)/2;
			continue;
		}
		if (a < b){
			b = (b - a)/2;
		}
	}
	while (a != b);
	if (a == b) temp = b;
	GCD = GCD*temp;
	return GCD;
}
int ReturnCipher(FILE *writeF, int KeyLength, int Start)
{
	struct letter letter_array[26] = {0};
	int cipher, i;
	for (i = 0; i < 26; i++)
		letter_array[i].sign = (char)(i+97);
	char ch;
	fseek(writeF, Start, SEEK_SET);
	while (feof(writeF) == 0)
	{
		ch = fgetc(writeF);
		if ((ch >= 'a') && (ch <= 'z'))
			letter_array[ch - 97].quantity++;
		for (i = 0; i < KeyLength - 1; i++)
		{
			ch = fgetc(writeF);
			if (feof(writeF) != 0)
				break;
		}
	}
	sorting(letter_array);
	cipher = (int)'e' - (int)letter_array[0].sign;
	//for (i = 0; i < 26; i++)
	//	printf("\n%d %c", letter_array[i].quantity, letter_array[i].sign);
	if (cipher > 0)
		cipher = 26 - cipher;
	return abs(cipher);
}
int _tmain(int argc, _TCHAR* argv[])
{
	int cipher = 10, i;
	char ch;
	FILE *readF, *writeF, *finalF;
	readF = fopen("d:\\Project\\file_read_TI_1.txt", "r+");
	writeF = fopen("d:\\Project\\file_write_TI_1.txt", "w");
	finalF = fopen("d:\\Project\\file_final_TI_1.txt", "w+");
	puts("1 - Caesar");
	puts("2 - Vigenere");
	scanf("%d", &i); 
	switch (i)
	{
		case 1:
		{
			encoding(readF, writeF, cipher);
			fclose(writeF);	
			writeF = fopen("d:\\Project\\file_write_TI_1.txt", "r");
			struct letter letter_array[26] = {0};
			for (i = 0; i < 26; i++)
				letter_array[i].sign = (char)(i+97);
			while (feof(writeF) == 0)
			{
				ch = fgetc(writeF);
				if ((ch >= 'a') && (ch <= 'z'))
					letter_array[ch - 97].quantity++;
			}
			fseek(writeF, 0, 0);
			sorting(letter_array);
			cipher = (int)'e' - (int)letter_array[0].sign;
			encoding(writeF, finalF, cipher);
		}; break;
		case 2:
		{
			encoding_vigenere(readF, writeF, key);
			fclose(writeF);
			i = 0;
			writeF = fopen("d:\\Project\\file_write_TI_1.txt", "r");
			while (feof(writeF) == 0)
			{
				i++;
				ch = fgetc(writeF);
			}
			i--;
			int NumberArray[2000][10], PoleIndex = 0, LineIndex = -1, PositionOfText = 0;
			char SmallString[lettersInSubstring], *BigString, *TempString1, *TempString2;
			SmallString[lettersInSubstring - 1] = '\0';
			fseek(writeF, 0, SEEK_SET);
			BigString = (char *) calloc(i, sizeof(char));
			fread(BigString, sizeof(char)*i, 1, writeF);
			BigString[i] = '\0';
			for (PositionOfText = 0; PositionOfText < strlen(BigString) - lettersInSubstring; PositionOfText++)
			{
				PoleIndex = 0;
				TempString1 = BigString + PositionOfText;
				for (i = 0; i < lettersInSubstring - 1; i++)
					SmallString[i] = TempString1[i];
				TempString2 = strstr(TempString1 + 1, SmallString);
				if (TempString2 != NULL)
				{
					LineIndex++;
					while(TempString2 != NULL)
					{
						NumberArray[LineIndex][PoleIndex++] = TempString2 - TempString1;
						TempString1 = TempString2;
						TempString2 = strstr(TempString1 + 1, SmallString);
					}
					for (i = 1; i < PoleIndex; i++)						
						NumberArray[LineIndex][i] = BinaryAlgorithm(NumberArray[LineIndex][i], NumberArray[LineIndex][i - 1]);
					NumberArray[LineIndex][0] = NumberArray[LineIndex][i-1];
				}
			}
			for (i = 1; i < LineIndex; i++)
			{
		//		printf("%d %d\n", i, NumberArray[i][0]);
				NumberArray[i][0] = BinaryAlgorithm(NumberArray[i - 1][0], NumberArray[i][0]);
			}
			int KeyLength;
			KeyLength = NumberArray[i-1][0];
			printf("keyLength = %d", KeyLength);
			char KeyWord[10];
			for (i = 0; i < KeyLength; i++)
				KeyWord[i] = ReturnCipher(writeF, KeyLength, i) + 97;
			KeyWord[i] = '\0';
			printf("\nKeyWord = %s\n", KeyWord);
			for (i = 0; i < KeyLength; i++)
				KeyWord[i] = 97 + 26 - (KeyWord[i]-97);
			fclose(writeF);
			writeF = fopen("d:\\Project\\file_write_TI_1.txt", "r");
			encoding_vigenere(writeF, finalF, KeyWord);
		}; break;
	}
	flushall();
	getchar();
	fclose(readF);
	fclose(writeF);
	fclose(finalF);
	return 0;
}

