﻿namespace TI_6_form
{
    partial class TI_6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pCypheringNumeric = new System.Windows.Forms.NumericUpDown();
            this.qCypheringNumeric = new System.Windows.Forms.NumericUpDown();
            this.openKCypheringNumeric = new System.Windows.Forms.NumericUpDown();
            this.filePathCipheringTextBox = new System.Windows.Forms.TextBox();
            this.cypheringButton = new System.Windows.Forms.Button();
            this.cypheringGroupBox = new System.Windows.Forms.GroupBox();
            this.decipheringGroupBox = new System.Windows.Forms.GroupBox();
            this.decipheringButton = new System.Windows.Forms.Button();
            this.filePathDecipheringTextBox = new System.Windows.Forms.TextBox();
            this.closeKDecipheringNumeric = new System.Windows.Forms.NumericUpDown();
            this.rDecypheringNumeric = new System.Windows.Forms.NumericUpDown();
            this.crackGroupBox = new System.Windows.Forms.GroupBox();
            this.crackButton = new System.Windows.Forms.Button();
            this.filePathCrackTextBox = new System.Windows.Forms.TextBox();
            this.openKCrackNumeric = new System.Windows.Forms.NumericUpDown();
            this.rCrackNumeric = new System.Windows.Forms.NumericUpDown();
            this.pCypheringLlabel = new System.Windows.Forms.Label();
            this.qCypheringLabel = new System.Windows.Forms.Label();
            this.openKCypheringLabel = new System.Windows.Forms.Label();
            this.filePathCypheringLabel = new System.Windows.Forms.Label();
            this.filePathDecipheringLabel = new System.Windows.Forms.Label();
            this.closeKDecipheringLabel = new System.Windows.Forms.Label();
            this.rDecipheringLabel = new System.Windows.Forms.Label();
            this.filePathCrackLabel = new System.Windows.Forms.Label();
            this.openKCrackLabel = new System.Windows.Forms.Label();
            this.rCrackLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pCypheringNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qCypheringNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.openKCypheringNumeric)).BeginInit();
            this.cypheringGroupBox.SuspendLayout();
            this.decipheringGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.closeKDecipheringNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rDecypheringNumeric)).BeginInit();
            this.crackGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.openKCrackNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rCrackNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // pCypheringNumeric
            // 
            this.pCypheringNumeric.Location = new System.Drawing.Point(83, 20);
            this.pCypheringNumeric.Name = "pCypheringNumeric";
            this.pCypheringNumeric.Size = new System.Drawing.Size(73, 20);
            this.pCypheringNumeric.TabIndex = 1;
            this.pCypheringNumeric.Value = new decimal(new int[] {
            41,
            0,
            0,
            0});
            // 
            // qCypheringNumeric
            // 
            this.qCypheringNumeric.Location = new System.Drawing.Point(83, 46);
            this.qCypheringNumeric.Name = "qCypheringNumeric";
            this.qCypheringNumeric.Size = new System.Drawing.Size(73, 20);
            this.qCypheringNumeric.TabIndex = 2;
            this.qCypheringNumeric.Value = new decimal(new int[] {
            59,
            0,
            0,
            0});
            // 
            // openKCypheringNumeric
            // 
            this.openKCypheringNumeric.Location = new System.Drawing.Point(83, 72);
            this.openKCypheringNumeric.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.openKCypheringNumeric.Name = "openKCypheringNumeric";
            this.openKCypheringNumeric.Size = new System.Drawing.Size(73, 20);
            this.openKCypheringNumeric.TabIndex = 3;
            this.openKCypheringNumeric.Value = new decimal(new int[] {
            157,
            0,
            0,
            0});
            // 
            // filePathCipheringTextBox
            // 
            this.filePathCipheringTextBox.Location = new System.Drawing.Point(83, 98);
            this.filePathCipheringTextBox.Name = "filePathCipheringTextBox";
            this.filePathCipheringTextBox.Size = new System.Drawing.Size(153, 20);
            this.filePathCipheringTextBox.TabIndex = 4;
            this.filePathCipheringTextBox.Text = "readFile.txt";
            // 
            // cypheringButton
            // 
            this.cypheringButton.Location = new System.Drawing.Point(83, 122);
            this.cypheringButton.Name = "cypheringButton";
            this.cypheringButton.Size = new System.Drawing.Size(54, 21);
            this.cypheringButton.TabIndex = 5;
            this.cypheringButton.Text = "Start";
            this.cypheringButton.UseVisualStyleBackColor = true;
            this.cypheringButton.Click += new System.EventHandler(this.cypheringButton_Click);
            // 
            // cypheringGroupBox
            // 
            this.cypheringGroupBox.Controls.Add(this.filePathCypheringLabel);
            this.cypheringGroupBox.Controls.Add(this.openKCypheringLabel);
            this.cypheringGroupBox.Controls.Add(this.qCypheringLabel);
            this.cypheringGroupBox.Controls.Add(this.pCypheringLlabel);
            this.cypheringGroupBox.Controls.Add(this.cypheringButton);
            this.cypheringGroupBox.Controls.Add(this.filePathCipheringTextBox);
            this.cypheringGroupBox.Controls.Add(this.openKCypheringNumeric);
            this.cypheringGroupBox.Controls.Add(this.qCypheringNumeric);
            this.cypheringGroupBox.Controls.Add(this.pCypheringNumeric);
            this.cypheringGroupBox.Location = new System.Drawing.Point(12, 12);
            this.cypheringGroupBox.Name = "cypheringGroupBox";
            this.cypheringGroupBox.Size = new System.Drawing.Size(260, 149);
            this.cypheringGroupBox.TabIndex = 6;
            this.cypheringGroupBox.TabStop = false;
            this.cypheringGroupBox.Text = "Ciphering";
            // 
            // decipheringGroupBox
            // 
            this.decipheringGroupBox.Controls.Add(this.filePathDecipheringLabel);
            this.decipheringGroupBox.Controls.Add(this.closeKDecipheringLabel);
            this.decipheringGroupBox.Controls.Add(this.rDecipheringLabel);
            this.decipheringGroupBox.Controls.Add(this.decipheringButton);
            this.decipheringGroupBox.Controls.Add(this.filePathDecipheringTextBox);
            this.decipheringGroupBox.Controls.Add(this.closeKDecipheringNumeric);
            this.decipheringGroupBox.Controls.Add(this.rDecypheringNumeric);
            this.decipheringGroupBox.Location = new System.Drawing.Point(12, 167);
            this.decipheringGroupBox.Name = "decipheringGroupBox";
            this.decipheringGroupBox.Size = new System.Drawing.Size(260, 126);
            this.decipheringGroupBox.TabIndex = 7;
            this.decipheringGroupBox.TabStop = false;
            this.decipheringGroupBox.Text = "Deciphering";
            // 
            // decipheringButton
            // 
            this.decipheringButton.Location = new System.Drawing.Point(83, 96);
            this.decipheringButton.Name = "decipheringButton";
            this.decipheringButton.Size = new System.Drawing.Size(54, 21);
            this.decipheringButton.TabIndex = 5;
            this.decipheringButton.Text = "Start";
            this.decipheringButton.UseVisualStyleBackColor = true;
            this.decipheringButton.Click += new System.EventHandler(this.decipheringButton_Click);
            // 
            // filePathDecipheringTextBox
            // 
            this.filePathDecipheringTextBox.Location = new System.Drawing.Point(83, 70);
            this.filePathDecipheringTextBox.Name = "filePathDecipheringTextBox";
            this.filePathDecipheringTextBox.Size = new System.Drawing.Size(153, 20);
            this.filePathDecipheringTextBox.TabIndex = 4;
            this.filePathDecipheringTextBox.Text = "writeFile.txt";
            // 
            // closeKDecipheringNumeric
            // 
            this.closeKDecipheringNumeric.Location = new System.Drawing.Point(83, 44);
            this.closeKDecipheringNumeric.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.closeKDecipheringNumeric.Name = "closeKDecipheringNumeric";
            this.closeKDecipheringNumeric.Size = new System.Drawing.Size(73, 20);
            this.closeKDecipheringNumeric.TabIndex = 2;
            this.closeKDecipheringNumeric.Value = new decimal(new int[] {
            133,
            0,
            0,
            0});
            // 
            // rDecypheringNumeric
            // 
            this.rDecypheringNumeric.Location = new System.Drawing.Point(83, 18);
            this.rDecypheringNumeric.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.rDecypheringNumeric.Name = "rDecypheringNumeric";
            this.rDecypheringNumeric.Size = new System.Drawing.Size(73, 20);
            this.rDecypheringNumeric.TabIndex = 1;
            this.rDecypheringNumeric.Value = new decimal(new int[] {
            2419,
            0,
            0,
            0});
            // 
            // crackGroupBox
            // 
            this.crackGroupBox.Controls.Add(this.filePathCrackLabel);
            this.crackGroupBox.Controls.Add(this.openKCrackLabel);
            this.crackGroupBox.Controls.Add(this.rCrackLabel);
            this.crackGroupBox.Controls.Add(this.crackButton);
            this.crackGroupBox.Controls.Add(this.filePathCrackTextBox);
            this.crackGroupBox.Controls.Add(this.openKCrackNumeric);
            this.crackGroupBox.Controls.Add(this.rCrackNumeric);
            this.crackGroupBox.Location = new System.Drawing.Point(12, 299);
            this.crackGroupBox.Name = "crackGroupBox";
            this.crackGroupBox.Size = new System.Drawing.Size(260, 127);
            this.crackGroupBox.TabIndex = 8;
            this.crackGroupBox.TabStop = false;
            this.crackGroupBox.Text = "Crack";
            // 
            // crackButton
            // 
            this.crackButton.Location = new System.Drawing.Point(83, 97);
            this.crackButton.Name = "crackButton";
            this.crackButton.Size = new System.Drawing.Size(54, 21);
            this.crackButton.TabIndex = 5;
            this.crackButton.Text = "Start";
            this.crackButton.UseVisualStyleBackColor = true;
            this.crackButton.Click += new System.EventHandler(this.crackButton_Click);
            // 
            // filePathCrackTextBox
            // 
            this.filePathCrackTextBox.Location = new System.Drawing.Point(83, 71);
            this.filePathCrackTextBox.Name = "filePathCrackTextBox";
            this.filePathCrackTextBox.Size = new System.Drawing.Size(153, 20);
            this.filePathCrackTextBox.TabIndex = 4;
            this.filePathCrackTextBox.Text = "writeFile.txt";
            // 
            // openKCrackNumeric
            // 
            this.openKCrackNumeric.Location = new System.Drawing.Point(83, 45);
            this.openKCrackNumeric.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.openKCrackNumeric.Name = "openKCrackNumeric";
            this.openKCrackNumeric.Size = new System.Drawing.Size(73, 20);
            this.openKCrackNumeric.TabIndex = 2;
            this.openKCrackNumeric.Value = new decimal(new int[] {
            157,
            0,
            0,
            0});
            // 
            // rCrackNumeric
            // 
            this.rCrackNumeric.Location = new System.Drawing.Point(83, 19);
            this.rCrackNumeric.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.rCrackNumeric.Name = "rCrackNumeric";
            this.rCrackNumeric.Size = new System.Drawing.Size(73, 20);
            this.rCrackNumeric.TabIndex = 1;
            this.rCrackNumeric.Value = new decimal(new int[] {
            2419,
            0,
            0,
            0});
            // 
            // pCypheringLlabel
            // 
            this.pCypheringLlabel.AutoSize = true;
            this.pCypheringLlabel.Location = new System.Drawing.Point(21, 22);
            this.pCypheringLlabel.Name = "pCypheringLlabel";
            this.pCypheringLlabel.Size = new System.Drawing.Size(16, 13);
            this.pCypheringLlabel.TabIndex = 6;
            this.pCypheringLlabel.Text = "p:";
            // 
            // qCypheringLabel
            // 
            this.qCypheringLabel.AutoSize = true;
            this.qCypheringLabel.Location = new System.Drawing.Point(21, 48);
            this.qCypheringLabel.Name = "qCypheringLabel";
            this.qCypheringLabel.Size = new System.Drawing.Size(16, 13);
            this.qCypheringLabel.TabIndex = 7;
            this.qCypheringLabel.Text = "q:";
            // 
            // openKCypheringLabel
            // 
            this.openKCypheringLabel.AutoSize = true;
            this.openKCypheringLabel.Location = new System.Drawing.Point(21, 74);
            this.openKCypheringLabel.Name = "openKCypheringLabel";
            this.openKCypheringLabel.Size = new System.Drawing.Size(44, 13);
            this.openKCypheringLabel.TabIndex = 8;
            this.openKCypheringLabel.Text = "K open:";
            this.openKCypheringLabel.Click += new System.EventHandler(this.label3_Click);
            // 
            // filePathCypheringLabel
            // 
            this.filePathCypheringLabel.AutoSize = true;
            this.filePathCypheringLabel.Location = new System.Drawing.Point(21, 101);
            this.filePathCypheringLabel.Name = "filePathCypheringLabel";
            this.filePathCypheringLabel.Size = new System.Drawing.Size(50, 13);
            this.filePathCypheringLabel.TabIndex = 9;
            this.filePathCypheringLabel.Text = "File path:";
            // 
            // filePathDecipheringLabel
            // 
            this.filePathDecipheringLabel.AutoSize = true;
            this.filePathDecipheringLabel.Location = new System.Drawing.Point(21, 74);
            this.filePathDecipheringLabel.Name = "filePathDecipheringLabel";
            this.filePathDecipheringLabel.Size = new System.Drawing.Size(50, 13);
            this.filePathDecipheringLabel.TabIndex = 13;
            this.filePathDecipheringLabel.Text = "File path:";
            // 
            // closeKDecipheringLabel
            // 
            this.closeKDecipheringLabel.AutoSize = true;
            this.closeKDecipheringLabel.Location = new System.Drawing.Point(21, 46);
            this.closeKDecipheringLabel.Name = "closeKDecipheringLabel";
            this.closeKDecipheringLabel.Size = new System.Drawing.Size(45, 13);
            this.closeKDecipheringLabel.TabIndex = 11;
            this.closeKDecipheringLabel.Text = "K close:";
            // 
            // rDecipheringLabel
            // 
            this.rDecipheringLabel.AutoSize = true;
            this.rDecipheringLabel.Location = new System.Drawing.Point(21, 20);
            this.rDecipheringLabel.Name = "rDecipheringLabel";
            this.rDecipheringLabel.Size = new System.Drawing.Size(13, 13);
            this.rDecipheringLabel.TabIndex = 10;
            this.rDecipheringLabel.Text = "r:";
            // 
            // filePathCrackLabel
            // 
            this.filePathCrackLabel.AutoSize = true;
            this.filePathCrackLabel.Location = new System.Drawing.Point(21, 75);
            this.filePathCrackLabel.Name = "filePathCrackLabel";
            this.filePathCrackLabel.Size = new System.Drawing.Size(50, 13);
            this.filePathCrackLabel.TabIndex = 13;
            this.filePathCrackLabel.Text = "File path:";
            // 
            // openKCrackLabel
            // 
            this.openKCrackLabel.AutoSize = true;
            this.openKCrackLabel.Location = new System.Drawing.Point(21, 47);
            this.openKCrackLabel.Name = "openKCrackLabel";
            this.openKCrackLabel.Size = new System.Drawing.Size(44, 13);
            this.openKCrackLabel.TabIndex = 11;
            this.openKCrackLabel.Text = "K open:";
            // 
            // rCrackLabel
            // 
            this.rCrackLabel.AutoSize = true;
            this.rCrackLabel.Location = new System.Drawing.Point(21, 21);
            this.rCrackLabel.Name = "rCrackLabel";
            this.rCrackLabel.Size = new System.Drawing.Size(13, 13);
            this.rCrackLabel.TabIndex = 10;
            this.rCrackLabel.Text = "r:";
            // 
            // TI_6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 439);
            this.Controls.Add(this.crackGroupBox);
            this.Controls.Add(this.decipheringGroupBox);
            this.Controls.Add(this.cypheringGroupBox);
            this.Name = "TI_6";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RSA";
            ((System.ComponentModel.ISupportInitialize)(this.pCypheringNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qCypheringNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.openKCypheringNumeric)).EndInit();
            this.cypheringGroupBox.ResumeLayout(false);
            this.cypheringGroupBox.PerformLayout();
            this.decipheringGroupBox.ResumeLayout(false);
            this.decipheringGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.closeKDecipheringNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rDecypheringNumeric)).EndInit();
            this.crackGroupBox.ResumeLayout(false);
            this.crackGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.openKCrackNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rCrackNumeric)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NumericUpDown pCypheringNumeric;
        private System.Windows.Forms.NumericUpDown qCypheringNumeric;
        private System.Windows.Forms.NumericUpDown openKCypheringNumeric;
        private System.Windows.Forms.TextBox filePathCipheringTextBox;
        private System.Windows.Forms.Button cypheringButton;
        private System.Windows.Forms.GroupBox cypheringGroupBox;
        private System.Windows.Forms.Label filePathCypheringLabel;
        private System.Windows.Forms.Label openKCypheringLabel;
        private System.Windows.Forms.Label qCypheringLabel;
        private System.Windows.Forms.Label pCypheringLlabel;
        private System.Windows.Forms.GroupBox decipheringGroupBox;
        private System.Windows.Forms.Label filePathDecipheringLabel;
        private System.Windows.Forms.Label closeKDecipheringLabel;
        private System.Windows.Forms.Label rDecipheringLabel;
        private System.Windows.Forms.Button decipheringButton;
        private System.Windows.Forms.TextBox filePathDecipheringTextBox;
        private System.Windows.Forms.NumericUpDown closeKDecipheringNumeric;
        private System.Windows.Forms.NumericUpDown rDecypheringNumeric;
        private System.Windows.Forms.GroupBox crackGroupBox;
        private System.Windows.Forms.Label filePathCrackLabel;
        private System.Windows.Forms.Label openKCrackLabel;
        private System.Windows.Forms.Label rCrackLabel;
        private System.Windows.Forms.Button crackButton;
        private System.Windows.Forms.TextBox filePathCrackTextBox;
        private System.Windows.Forms.NumericUpDown openKCrackNumeric;
        private System.Windows.Forms.NumericUpDown rCrackNumeric;
    }
}

