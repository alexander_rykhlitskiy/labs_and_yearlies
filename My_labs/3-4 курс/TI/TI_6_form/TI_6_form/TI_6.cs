﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Numerics;

namespace TI_6_form
{
    public partial class TI_6 : Form
    {
        public TI_6()
        {
            InitializeComponent();
        }

        private void startButton_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void cypheringButton_Click(object sender, EventArgs e)
        {
            String readFile = filePathCipheringTextBox.Text;
            String writeFile = "writeFile.txt";
            try
            {
                byte[] text;
                text = System.IO.File.ReadAllBytes(readFile);
                try
                {
                    File.WriteAllBytes(writeFile, RSA.Ciphering(text, (BigInteger)pCypheringNumeric.Value,
                        (BigInteger)qCypheringNumeric.Value, (BigInteger)openKCypheringNumeric.Value));
                }
                catch (FailInitialData)
                {
                    MessageBox.Show("Initial Data is wrong");
                }
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("File " + readFile + " not found");
            }
        }

        private void decipheringButton_Click(object sender, EventArgs e)
        {
            String readFile = filePathDecipheringTextBox.Text;
            String writeFile = "finalFile.txt";
            try
            {
                byte[] text;
                text = System.IO.File.ReadAllBytes(readFile);
                try
                {
                    File.WriteAllBytes(writeFile, RSA.Deciphering(text, (BigInteger)closeKDecipheringNumeric.Value, 
                        (BigInteger)rDecypheringNumeric.Value));
                }
                catch (FailInitialData)
                {
                    MessageBox.Show("Initial Data is wrong");
                }
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("File " + readFile + " not found");
            }
        }

        private void crackButton_Click(object sender, EventArgs e)
        {
            String readFile = filePathCrackTextBox.Text;
            String writeFile = "crackFile.txt";
            try
            {
                byte[] text;
                text = System.IO.File.ReadAllBytes(readFile);
                try
                {
                    File.WriteAllBytes(writeFile, RSA.Crack(text, (BigInteger)rCrackNumeric.Value,
                        (BigInteger)openKCrackNumeric.Value));
                }
                catch (FailInitialData)
                {
                    MessageBox.Show("Initial Data is wrong");
                }
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("File " + readFile + " not found");
            }
        }
    }
}
