﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;

namespace TI_6_form
{
    class RSA
    {
        struct EuclidResult
        {
            public BigInteger x1, y1, d1;
            public EuclidResult(BigInteger x, BigInteger y, BigInteger d)
            {
                x1 = x;
                y1 = y;
                d1 = d;
            }
        }
        static EuclidResult Euclidex(BigInteger a, BigInteger b)
        {
            BigInteger[] d = new BigInteger[3],
                  x = new BigInteger[3],
                  y = new BigInteger[3];
            d[0] = a; d[1] = b;
            x[0] = 1; x[1] = 0;
            y[0] = 0; y[1] = 1;
            while (d[1] > 1)
            {
                BigInteger q = d[0] / d[1];
                d[2] = d[0] % d[1];
                x[2] = x[0] - q * x[1];
                y[2] = y[0] - q * y[1];
                d[0] = d[1];
                d[1] = d[2];
                x[0] = x[1];
                x[1] = x[2];
                y[0] = y[1];
                y[1] = y[2];
            }
            return (new EuclidResult(x[1], (y[1] < 0 ? y[1] + a : y[1]), d[1]));
        }
        static BigInteger FastExp(int a, BigInteger z, BigInteger n)
        {
            BigInteger result = 1, a1 = a, z1 = z;
            while (z1 != 0)
            {
                while ((z1 % 2) == 0)
                {
                    z1 = z1 / 2;
                    a1 = (a1 * a1) % n;
                }
                z1--;
                result = (result * a1) % n;
            }
            return result;
        }
        //static void GetPrimaryNumbers(ref BigInteger p, ref BigInteger q)
        //{
        //    p = 41;
        //    q = 59;
        //}
        //static BigInteger GetOpenK(BigInteger f)
        //{
        //    BigInteger result = new BigInteger();
        //    // result = f - 1;
        //    result = 157;
        //    return result;
        //}
        public static byte[] Ciphering(byte[] text, BigInteger p, BigInteger q, BigInteger openK)
        {
            byte[] result = new byte[text.Length * 2];
            if (IsPrimary(p) & IsPrimary(q))
            {
                BigInteger r = new BigInteger();
                r = p * q;
                for (int i = 0; i < text.Length; i++)
                {
                    result[2 * i] = (byte)(FastExp(text[i], openK, r) >> 8);
                    result[2 * i + 1] = (byte)(FastExp(text[i], openK, r) & 0xFF);
                }
            }
            else
                throw new FailInitialData();
            return result;
        }
        public static byte[] Deciphering(byte[] byteText, BigInteger closeK, BigInteger r)
        {
            if (IsPrimary(r))
                throw new FailInitialData();
            short[] shortText = new short[byteText.Length / 2];
            for (int i = 0; i < shortText.Length; i++)
            {
                shortText[i] = (short)((byteText[2 * i] << 8) | (byteText[2 * i + 1]));
            }
            byte[] result = new byte[byteText.Length / 2];
            for (int i = 0; i < shortText.Length; i++)
            {
                result[i] = (byte)FastExp(shortText[i], closeK, r);
            }
            return result;
        }
        private static bool IsPrimary(BigInteger number)
        {
            bool result = true;
            for (int i = 2; i < Math.Sqrt((double)number); i++)
                if (number % i == 0)
                {
                    result = false;
                    break;
                }
            return result;
        }
        private static int GetPrimaryNumbersFromRToCrack(ref BigInteger p, ref BigInteger q, BigInteger r)
        {
            int result = -1;
            for (int i = 2; i < Math.Sqrt((double)r); i++)
            {
                if (r % i == 0)
                {
                    p = i;
                    q = r / i;
                    result = 0;
                }
            }
            return result;
        }
        public static byte[] Crack(byte[] byteText, BigInteger r, BigInteger openK)
        {
            short[] shortText = new short[byteText.Length / 2];
            for (int i = 0; i < shortText.Length; i++)
            {
                shortText[i] = (short)((byteText[2 * i] << 8) | (byteText[2 * i + 1]));
            }
            byte[] result = new byte[byteText.Length / 2];
            BigInteger p = new BigInteger(),
                       q = new BigInteger(),
                       f = new BigInteger(),
                       closeK = new BigInteger();
            if (GetPrimaryNumbersFromRToCrack(ref p, ref q, r) != -1)
            {
                f = (p - 1) * (q - 1);
                EuclidResult euclidResult = Euclidex(f, openK);
                closeK = euclidResult.y1;
                for (int i = 0; i < shortText.Length; i++)
                {
                    result[i] = (byte)FastExp(shortText[i], closeK, r);
                }
            }
            else
                throw new FailInitialData();
            return result;
        }
    }
}
