﻿namespace TI_5_form
{
    partial class TI_5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.firstNumberTextBox = new System.Windows.Forms.TextBox();
            this.secondNumberTextBox = new System.Windows.Forms.TextBox();
            this.resultNumberTextBox = new System.Windows.Forms.TextBox();
            this.sumButton = new System.Windows.Forms.Button();
            this.sumLabel = new System.Windows.Forms.Label();
            this.subLabel = new System.Windows.Forms.Label();
            this.subButton = new System.Windows.Forms.Button();
            this.multLabel = new System.Windows.Forms.Label();
            this.multButton = new System.Windows.Forms.Button();
            this.karatsubaLabel = new System.Windows.Forms.Label();
            this.karatsubaButton = new System.Windows.Forms.Button();
            this.divLabel = new System.Windows.Forms.Label();
            this.divButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // firstNumberTextBox
            // 
            this.firstNumberTextBox.Location = new System.Drawing.Point(12, 12);
            this.firstNumberTextBox.Name = "firstNumberTextBox";
            this.firstNumberTextBox.Size = new System.Drawing.Size(465, 20);
            this.firstNumberTextBox.TabIndex = 0;
            this.firstNumberTextBox.Text = "845e485b84e4c154a41d7e45c45b874512451";
            // 
            // secondNumberTextBox
            // 
            this.secondNumberTextBox.Location = new System.Drawing.Point(12, 38);
            this.secondNumberTextBox.Name = "secondNumberTextBox";
            this.secondNumberTextBox.Size = new System.Drawing.Size(465, 20);
            this.secondNumberTextBox.TabIndex = 1;
            this.secondNumberTextBox.Text = "48b45e45a4598a8bf544a451b1c48451845157";
            // 
            // resultNumberTextBox
            // 
            this.resultNumberTextBox.Location = new System.Drawing.Point(12, 132);
            this.resultNumberTextBox.Name = "resultNumberTextBox";
            this.resultNumberTextBox.Size = new System.Drawing.Size(465, 20);
            this.resultNumberTextBox.TabIndex = 2;
            // 
            // sumButton
            // 
            this.sumButton.Location = new System.Drawing.Point(12, 64);
            this.sumButton.Name = "sumButton";
            this.sumButton.Size = new System.Drawing.Size(35, 22);
            this.sumButton.TabIndex = 3;
            this.sumButton.Text = "+";
            this.sumButton.UseVisualStyleBackColor = true;
            this.sumButton.Click += new System.EventHandler(this.sumButton_Click);
            // 
            // sumLabel
            // 
            this.sumLabel.AutoSize = true;
            this.sumLabel.Location = new System.Drawing.Point(12, 97);
            this.sumLabel.Name = "sumLabel";
            this.sumLabel.Size = new System.Drawing.Size(37, 13);
            this.sumLabel.TabIndex = 4;
            this.sumLabel.Text = "00000";
            // 
            // subLabel
            // 
            this.subLabel.AutoSize = true;
            this.subLabel.Location = new System.Drawing.Point(89, 97);
            this.subLabel.Name = "subLabel";
            this.subLabel.Size = new System.Drawing.Size(37, 13);
            this.subLabel.TabIndex = 6;
            this.subLabel.Text = "00000";
            // 
            // subButton
            // 
            this.subButton.Location = new System.Drawing.Point(89, 64);
            this.subButton.Name = "subButton";
            this.subButton.Size = new System.Drawing.Size(35, 22);
            this.subButton.TabIndex = 5;
            this.subButton.Text = "-";
            this.subButton.UseVisualStyleBackColor = true;
            this.subButton.Click += new System.EventHandler(this.subButton_Click);
            // 
            // multLabel
            // 
            this.multLabel.AutoSize = true;
            this.multLabel.Location = new System.Drawing.Point(178, 97);
            this.multLabel.Name = "multLabel";
            this.multLabel.Size = new System.Drawing.Size(37, 13);
            this.multLabel.TabIndex = 8;
            this.multLabel.Text = "00000";
            // 
            // multButton
            // 
            this.multButton.Location = new System.Drawing.Point(178, 64);
            this.multButton.Name = "multButton";
            this.multButton.Size = new System.Drawing.Size(35, 22);
            this.multButton.TabIndex = 7;
            this.multButton.Text = "*";
            this.multButton.UseVisualStyleBackColor = true;
            this.multButton.Click += new System.EventHandler(this.multButton_Click);
            // 
            // karatsubaLabel
            // 
            this.karatsubaLabel.AutoSize = true;
            this.karatsubaLabel.Location = new System.Drawing.Point(363, 97);
            this.karatsubaLabel.Name = "karatsubaLabel";
            this.karatsubaLabel.Size = new System.Drawing.Size(37, 13);
            this.karatsubaLabel.TabIndex = 10;
            this.karatsubaLabel.Text = "00000";
            // 
            // karatsubaButton
            // 
            this.karatsubaButton.Location = new System.Drawing.Point(363, 64);
            this.karatsubaButton.Name = "karatsubaButton";
            this.karatsubaButton.Size = new System.Drawing.Size(67, 22);
            this.karatsubaButton.TabIndex = 9;
            this.karatsubaButton.Text = "Karatsuba";
            this.karatsubaButton.UseVisualStyleBackColor = true;
            this.karatsubaButton.Click += new System.EventHandler(this.karatsubaButton_Click);
            // 
            // divLabel
            // 
            this.divLabel.AutoSize = true;
            this.divLabel.Location = new System.Drawing.Point(266, 97);
            this.divLabel.Name = "divLabel";
            this.divLabel.Size = new System.Drawing.Size(37, 13);
            this.divLabel.TabIndex = 12;
            this.divLabel.Text = "00000";
            // 
            // divButton
            // 
            this.divButton.Location = new System.Drawing.Point(266, 64);
            this.divButton.Name = "divButton";
            this.divButton.Size = new System.Drawing.Size(35, 22);
            this.divButton.TabIndex = 11;
            this.divButton.Text = "/";
            this.divButton.UseVisualStyleBackColor = true;
            this.divButton.Click += new System.EventHandler(this.divButton_Click);
            // 
            // TI_5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(489, 177);
            this.Controls.Add(this.divLabel);
            this.Controls.Add(this.divButton);
            this.Controls.Add(this.karatsubaLabel);
            this.Controls.Add(this.karatsubaButton);
            this.Controls.Add(this.multLabel);
            this.Controls.Add(this.multButton);
            this.Controls.Add(this.subLabel);
            this.Controls.Add(this.subButton);
            this.Controls.Add(this.sumLabel);
            this.Controls.Add(this.sumButton);
            this.Controls.Add(this.resultNumberTextBox);
            this.Controls.Add(this.secondNumberTextBox);
            this.Controls.Add(this.firstNumberTextBox);
            this.Name = "TI_5";
            this.Text = "TI_5";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox firstNumberTextBox;
        private System.Windows.Forms.TextBox secondNumberTextBox;
        private System.Windows.Forms.TextBox resultNumberTextBox;
        private System.Windows.Forms.Button sumButton;
        private System.Windows.Forms.Label sumLabel;
        private System.Windows.Forms.Label subLabel;
        private System.Windows.Forms.Button subButton;
        private System.Windows.Forms.Label multLabel;
        private System.Windows.Forms.Button multButton;
        private System.Windows.Forms.Label karatsubaLabel;
        private System.Windows.Forms.Button karatsubaButton;
        private System.Windows.Forms.Label divLabel;
        private System.Windows.Forms.Button divButton;
    }
}

