﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace TI_5_form
{
    public partial class TI_5 : Form
    {
        public TI_5()
        {
            InitializeComponent();
        }
        private void sumButton_Click(object sender, EventArgs e)
        {
            BigNumber a = new BigNumber(firstNumberTextBox.Text);
            BigNumber b = new BigNumber(secondNumberTextBox.Text);
            BigNumber d = new BigNumber();
            Stopwatch timePerParse;
            long ticksThisTime = 0;
            timePerParse = Stopwatch.StartNew();

            d = a + b;

            timePerParse.Stop();
            ticksThisTime = timePerParse.ElapsedTicks;
            sumLabel.Text = ticksThisTime.ToString();
            resultNumberTextBox.Text = (string)d;      
        }

        private void subButton_Click(object sender, EventArgs e)
        {
            BigNumber a = new BigNumber(firstNumberTextBox.Text);
            BigNumber b = new BigNumber(secondNumberTextBox.Text);
            BigNumber d = new BigNumber();
            Stopwatch timePerParse;
            long ticksThisTime = 0;
            timePerParse = Stopwatch.StartNew();

            d = a - b;

            timePerParse.Stop();
            ticksThisTime = timePerParse.ElapsedTicks;
            subLabel.Text = ticksThisTime.ToString();
            resultNumberTextBox.Text = (string)d;      
        }

        private void multButton_Click(object sender, EventArgs e)
        {
            BigNumber a = new BigNumber(firstNumberTextBox.Text);
            BigNumber b = new BigNumber(secondNumberTextBox.Text);
            BigNumber d = new BigNumber();
            Stopwatch timePerParse;
            long ticksThisTime = 0;
            timePerParse = Stopwatch.StartNew();

            d = a * b;

            timePerParse.Stop();
            ticksThisTime = timePerParse.ElapsedTicks;
            multLabel.Text = ticksThisTime.ToString();
            resultNumberTextBox.Text = (string)d;        
        }

        private void divButton_Click(object sender, EventArgs e)
        {
            BigNumber a = new BigNumber(firstNumberTextBox.Text);
            BigNumber b = new BigNumber(secondNumberTextBox.Text);
            BigNumber d = new BigNumber();
            Stopwatch timePerParse;
            long ticksThisTime = 0;
            timePerParse = Stopwatch.StartNew();

            d = a / b;

            timePerParse.Stop();
            ticksThisTime = timePerParse.ElapsedTicks;
            divLabel.Text = ticksThisTime.ToString();
            resultNumberTextBox.Text = (string)d;          
        }

        private void karatsubaButton_Click(object sender, EventArgs e)
        {
                                                                                                                                                                                              int c = 100;
            BigNumber a = new BigNumber(firstNumberTextBox.Text);
            BigNumber b = new BigNumber(secondNumberTextBox.Text);
            BigNumber d = new BigNumber();
            Stopwatch timePerParse;
            long ticksThisTime = 0;
            timePerParse = Stopwatch.StartNew();

            d = d.Karatsuba(a, b);

            timePerParse.Stop();
            ticksThisTime = timePerParse.ElapsedTicks/c;
            karatsubaLabel.Text = ticksThisTime.ToString();
            resultNumberTextBox.Text = (string)d;
        }
    }
}
