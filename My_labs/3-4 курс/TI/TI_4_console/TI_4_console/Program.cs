﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TI_4_console
{
    class Program
    {
        static ushort[] constKey = { 32768, 2, 38000, 4, 5, 6, 7, 8 };
        static ushort[] key = { 32768, 2, 38000, 4, 5, 6, 7, 8 };
        static List<Subunit> Z = new List<Subunit>();
        static List<Subunit> U = new List<Subunit>();
        static void Swap(ref Subunit a, ref Subunit b)
        {
            Subunit temp = a;
            a = b;
            b = temp;
        }
        static void CycleIDEA(List<Subunit> X, List<Subunit> U)
        {
            List<Subunit> I = new List<Subunit>();
            for (int i = 0; i < 4; i++)
                I.Add(new Subunit());
            List<Subunit> F = new List<Subunit>();
            for (int i = 0; i < 2; i++)
                F.Add(new Subunit());

            for (int i = 0; i < 8; i++)
            {
                I[0] = X[0] * U[i * 6];
                I[1] = X[1] + U[i * 6 + 1];
                I[2] = X[2] + U[i * 6 + 2];
                I[3] = X[3] * U[i * 6 + 3];

                F[0] = I[0] ^ I[2];
                F[1] = I[1] ^ I[3];

                F[0] = F[0] * U[i * 6 + 4];
                F[1] = F[0] + F[1];
                F[1] = F[1] * U[i * 6 + 5];
                F[0] = F[0] + F[1];

                X[0] = I[0] ^ F[1];
                X[2] = I[1] ^ F[0];
                X[1] = I[2] ^ F[1];
                X[3] = I[3] ^ F[0];

            }
            X[0] = X[0] * U[48];
            X[2] = X[2] + U[49];
            X[1] = X[1] + U[50]; 
            X[3] = X[3] * U[51];
            Subunit temp = X[2];
            X[2] = X[1];
            X[1] = temp;
         }
        public static void GetNewKey(List<Subunit> Z, List<Subunit> U)
        {
            for (int i = 0; i < 8; i++)
            {
                U.Add(--Z[48 - i * 6]);
                if (i == 0)
                {
                    U.Add(-Z[49]);
                    U.Add(-Z[50]);
                }
                else
                {
                    U.Add(-Z[50 - i * 6]);
                    U.Add(-Z[49 - i * 6]);
                }
                U.Add(--Z[51 - i * 6]);
                U.Add(Z[46 - i * 6]);
                U.Add(Z[47 - i * 6]);
            }
            U.Add(--Z[0]);
            U.Add(-Z[1]);
            U.Add(-Z[2]);
            U.Add(--Z[3]);
        }
        static ushort GetBit(ushort a)
        {
            ushort result = (ushort)(a & (1 << 15));
            result >>= 15;
            return result;
        }
        static void ShiftArrayLeft(ushort[] key)
        {
            const ushort shiftQuantity = 25;
            ushort[] bits = new ushort[8];
            ushort previousBit = 0, currentBit = 0;
            for (int j = 0; j < shiftQuantity; j++)
            {
                previousBit = GetBit(key[0]);
                for (int i = key.Length - 1; i >= 0; i--)
                {
                    currentBit = GetBit(key[i]);
                    key[i] <<= 1;
                    key[i] |= previousBit;
                    previousBit = currentBit;
                }
            }
        }
        static void GenerateKey(List<Subunit> Z)
        {
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Z.Add(key[j]);
                }
                ShiftArrayLeft(key);
            }
            for (int j = 0; j < 4; j++)
            {
                Z.Add(key[j]);
            }
        }
        static byte[] CypheringIDEA(byte[] text, bool cyphering)
        {
            byte[] tempText = new byte[text.Length + (text.Length % 8)];
            List<Subunit> X = new List<Subunit>();
            for (int j = 0; j < 4; j++)
            {
                X.Add(new Subunit());
            }
            for (int i = 0; i < text.Length; i += 8)
            {
                for (int j = i; j < i + 8; j += 2)
                {
                    if (j >= text.Length)
                    {
                        break;
                    }
                    X[(int)((j - i) / 2)] = (uint)((text[j] << 8) | text[j + 1]);
                }

                Z = new List<Subunit>();
                GenerateKey(Z);
                if (cyphering)
                    CycleIDEA(X, Z);
                else
                {
                    U = new List<Subunit>();
                    GetNewKey(Z, U);
                    CycleIDEA(X, U);
                }
                for (int j = i; j < i + 8; j += 2)
                {
                    if (j >= text.Length)
                        break;
                    tempText[j] = (byte)((X[(int)((j - i) / 2)] & 0xFF00) >> 8);
                    tempText[j + 1] = (byte)(X[(int)((j - i) / 2)] & 0x00FF);
                }
            }
            return tempText;
        }
        static void ReadKey(String file, ushort[] key)
        {
            byte[] tempKey = File.ReadAllBytes(file);
            for (int i = 0; i < 8; i++)
                key[i] = tempKey[i];
        }
        static void Main(string[] args)
        {
            //const String readFile = "readFile.txt";
            //const String writeFile = "writeFile.txt";
            //const String finalFile = "finalFile.txt";
            //const String keyFile = "keyFile.txt";
            //ReadKey(keyFile, key);

            //byte[] text = System.IO.File.ReadAllBytes(readFile);
            //text = CypheringIDEA(text, true);
            //File.WriteAllBytes(writeFile, text);
            //ReadKey(keyFile, key);
            //text = File.ReadAllBytes(writeFile);
            //text = CypheringIDEA(text, false);
            //File.WriteAllBytes(finalFile, text);
            Subunit a = new Subunit(5433);
            Console.WriteLine(--a);
            Console.ReadKey();
        }
    }
}