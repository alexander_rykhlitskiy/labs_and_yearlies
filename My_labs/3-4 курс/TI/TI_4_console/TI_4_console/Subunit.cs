﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TI_4_console
{
    class Subunit
    {
        public uint value;
        public Subunit()
        {
            value = 0;
        }
        public Subunit(uint a)
        {
            value = a;
            value %= (1 << 16);
        }
        public static Subunit operator ^(Subunit a, Subunit b)
        {
            Subunit result = new Subunit();
            result.value = a.value ^ b.value;
            return result;
        }
        public static Subunit operator +(Subunit a, Subunit b)
        {
            Subunit result = new Subunit();
            result.value = a.value + b.value;
            result.value %= (1 << 16);
            return result;
        }
        public static Subunit operator *(Subunit a, Subunit b)
        {
            Subunit result = new Subunit();
            uint aValue = a.value, bValue = b.value;
            if (aValue == 0)
                aValue = (1 << 16);
            if (bValue == 0)
                bValue = (1 << 16);
            result.value = aValue * bValue;
            result.value %= ((1 << 16) + 1);
            result.value %= (1 << 16);
            return result;
        }
        public static Subunit operator -(Subunit a)
        {
            Subunit result = new Subunit();
            result.value = (1 << 16) - a.value;
            return result;
        }
        const int mod = 65537;
        public static int binPow(long a, int n)
        {
            long r = 1;
            while (n != 0)
            {
                if ((n & 1) != 0)
                    r = (r * a) % mod;
                n >>= 1;
                a = (a * a) % mod;
            }
            return (int)r;
        }
        public static Subunit operator --(Subunit a)
        {
            Subunit result = new Subunit();
            result.value = (uint)binPow(a.value, mod - 2);
            return result;
        }
        public static implicit operator Subunit(uint a)
        {
            Subunit result = new Subunit();
            result.value = a % (1 << 16);
            return result;
        }
        public static implicit operator uint(Subunit a)
        {
            return a.value;
        }

    }
}
