﻿So he sprang into the water, and the Tin Woodman caught fast hold of
 his tail. Then the Lion began to swim with all his might toward the
 shore. It was hard work, although he was so big; but by and by they
 were drawn out of the current, and then Dorothy took the Tin Woodman’s
 long pole and helped push the raft to the land.