﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace TI_4_form
{
    public partial class IDEA : Form
    {
        public IDEA()
        {
            InitializeComponent();
            openFileDialog.InitialDirectory = Environment.CurrentDirectory;
            readFileComboBox.Text = readFileComboBox.Items[0].ToString();
            FillRichTextBox();
        }
        public static ushort[] key = new ushort[8];
        public static List<Subunit> Z = new List<Subunit>();
        public static List<Subunit> U = new List<Subunit>();
        void FillRichTextBox()
        {
            try
            {
                richTextBox.Text = File.ReadAllText(readFileComboBox.Text);
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show(readFileComboBox.Text + "is not found");
            }
        }
        static void Swap(ref Subunit a, ref Subunit b)
        {
            Subunit temp = a;
            a = b;
            b = temp;
        }
        static void CycleIDEA(List<Subunit> X, List<Subunit> U)
        {
            List<Subunit> I = new List<Subunit>();
            for (int i = 0; i < 4; i++)
                I.Add(new Subunit());
            List<Subunit> F = new List<Subunit>();
            for (int i = 0; i < 2; i++)
                F.Add(new Subunit());

            for (int i = 0; i < 8; i++)
            {
                I[0] = X[0] * U[i * 6];
                I[1] = X[1] + U[i * 6 + 1];
                I[2] = X[2] + U[i * 6 + 2];
                I[3] = X[3] * U[i * 6 + 3];

                F[0] = I[0] ^ I[2];
                F[1] = I[1] ^ I[3];

                F[0] = F[0] * U[i * 6 + 4];
                F[1] = F[0] + F[1];
                F[1] = F[1] * U[i * 6 + 5];
                F[0] = F[0] + F[1];

                X[0] = I[0] ^ F[1];
                X[2] = I[1] ^ F[0];
                X[1] = I[2] ^ F[1];
                X[3] = I[3] ^ F[0];

            }
            X[0] = X[0] * U[48];
            X[2] = X[2] + U[49];
            X[1] = X[1] + U[50];
            X[3] = X[3] * U[51];
            Subunit temp = X[2];
            X[2] = X[1];
            X[1] = temp;
        }
        public static void GetNewKey(List<Subunit> Z, List<Subunit> U)
        {
            for (int i = 0; i < 8; i++)
            {
                U.Add(--Z[48 - i * 6]);
                if (i == 0)
                {
                    U.Add(-Z[49]);
                    U.Add(-Z[50]);
                }
                else
                {
                    U.Add(-Z[50 - i * 6]);
                    U.Add(-Z[49 - i * 6]);
                }
                U.Add(--Z[51 - i * 6]);
                U.Add(Z[46 - i * 6]);
                U.Add(Z[47 - i * 6]);
            }
            U.Add(--Z[0]);
            U.Add(-Z[1]);
            U.Add(-Z[2]);
            U.Add(--Z[3]);
        }
        static ushort GetBit(ushort a)
        {
            ushort result = (ushort)(a & (1 << 15));
            result >>= 15;
            return result;
        }
        static void ShiftArrayLeft(ushort[] key)
        {
            const ushort shiftQuantity = 25;
            ushort[] bits = new ushort[8];
            ushort previousBit = 0, currentBit = 0;
            for (int j = 0; j < shiftQuantity; j++)
            {
                previousBit = GetBit(key[0]);
                for (int i = key.Length - 1; i >= 0; i--)
                {
                    currentBit = GetBit(key[i]);
                    key[i] <<= 1;
                    key[i] |= previousBit;
                    previousBit = currentBit;
                }
            }
        }
        static void GenerateKey(List<Subunit> Z)
        {
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Z.Add(key[j]);
                }
                ShiftArrayLeft(key);
            }
            for (int j = 0; j < 4; j++)
            {
                Z.Add(key[j]);
            }
        }
        static byte[] CypheringIDEA(byte[] text, bool cyphering)
        {
            if (text.Length % 8 != 0)
            {
                byte[] tempTempText = text;
                text = new byte[text.Length + (8 - text.Length % 8) % 8];
                tempTempText.CopyTo(text, 0);
            }
            byte[] tempText = new byte[text.Length + (8 - text.Length % 8) % 8];
            List<Subunit> X = new List<Subunit>();
            for (int j = 0; j < 4; j++)
            {
                X.Add(new Subunit());
            }
            for (int i = 0; i < text.Length; i += 8)
            {
                for (int j = i; j < i + 8; j += 2)
                {
                    X[(int)((j - i) / 2)] = (uint)((text[j] << 8) | text[j + 1]);
                }

                Z = new List<Subunit>();
                GenerateKey(Z);
                if (cyphering)
                    CycleIDEA(X, Z);
                else
                {
                    U = new List<Subunit>();
                    GetNewKey(Z, U);
                    CycleIDEA(X, U);
                }
                for (int j = i; j < i + 8; j += 2)
                {
                    tempText[j] = (byte)((X[(int)((j - i) / 2)] & 0xFF00) >> 8);
                    tempText[j + 1] = (byte)(X[(int)((j - i) / 2)] & 0x00FF);
                }
            }
            return tempText;
        }
        static void ReadKey(String file, ushort[] key)
        {
            byte[] tempKey = File.ReadAllBytes(file);
            for (int i = 0; i < 8; i++)
                key[i] = tempKey[i];
        }
        void WriteAllBytesByOne(String file, byte[] text)
        {
            BinaryWriter bw = new BinaryWriter(File.Open(file, FileMode.Create));
            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] == (byte)0)
                    break;
                bw.Write(text[i]);
                Console.Write(text[i]);
            }
            bw.Close();
        }
        private void goButton_Click(object sender, EventArgs e)
        {
            String readFile = readFileComboBox.Text;
            String writeFile = writeFileTextBox.Text;
            String finalFile = finalFileTextBox.Text;
            String keyFile = keyFileTextBox.Text;
            if (File.Exists(readFile))
                if (File.Exists(keyFile))
                {
                    ReadKey(keyFile, key);
                    byte[] text;
                    text = System.IO.File.ReadAllBytes(readFile);
                    text = CypheringIDEA(text, true);
                    File.WriteAllBytes(writeFile, text);
                    ReadKey(keyFile, key);
                    text = File.ReadAllBytes(writeFile);
                    text = CypheringIDEA(text, false);
                    if (readFile.Substring(readFile.Length - 4) == ".txt")
                        WriteAllBytesByOne(finalFile, text);
                    else
                        File.WriteAllBytes(finalFile, text);
                    MessageBox.Show("Cyphering is completed");
                }
                else MessageBox.Show(keyFile + " is not found");
            else MessageBox.Show(readFile + " is not found");

        }

        private void openButton_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            if (!readFileComboBox.Items.Contains(openFileDialog.FileName))
            {
                readFileComboBox.Items.Add(openFileDialog.FileName);
                readFileComboBox.SelectedIndex = readFileComboBox.Items.IndexOf(openFileDialog.FileName);
            }
            richTextBox.Text = File.ReadAllText(openFileDialog.FileName);
        }

        private void readFileComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillRichTextBox();
        }


    }
}
