﻿namespace TI_4_form
{
    partial class IDEA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.goButton = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.addButton = new System.Windows.Forms.Button();
            this.writeFileTextBox = new System.Windows.Forms.TextBox();
            this.finalFileTextBox = new System.Windows.Forms.TextBox();
            this.writeFileLabel = new System.Windows.Forms.Label();
            this.finalFileLabel = new System.Windows.Forms.Label();
            this.readFileComboBox = new System.Windows.Forms.ComboBox();
            this.keyLabel = new System.Windows.Forms.Label();
            this.keyFileTextBox = new System.Windows.Forms.TextBox();
            this.richTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // goButton
            // 
            this.goButton.Location = new System.Drawing.Point(172, 326);
            this.goButton.Name = "goButton";
            this.goButton.Size = new System.Drawing.Size(49, 26);
            this.goButton.TabIndex = 0;
            this.goButton.Text = "Cipher";
            this.goButton.UseVisualStyleBackColor = true;
            this.goButton.Click += new System.EventHandler(this.goButton_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "readFile.txt";
            // 
            // addButton
            // 
            this.addButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.addButton.AutoSize = true;
            this.addButton.Location = new System.Drawing.Point(332, 18);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(52, 25);
            this.addButton.TabIndex = 1;
            this.addButton.Text = "add...";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // writeFileTextBox
            // 
            this.writeFileTextBox.Location = new System.Drawing.Point(65, 232);
            this.writeFileTextBox.Name = "writeFileTextBox";
            this.writeFileTextBox.Size = new System.Drawing.Size(210, 20);
            this.writeFileTextBox.TabIndex = 2;
            this.writeFileTextBox.Text = "writeFile.txt";
            // 
            // finalFileTextBox
            // 
            this.finalFileTextBox.Location = new System.Drawing.Point(65, 258);
            this.finalFileTextBox.Name = "finalFileTextBox";
            this.finalFileTextBox.Size = new System.Drawing.Size(210, 20);
            this.finalFileTextBox.TabIndex = 3;
            this.finalFileTextBox.Text = "finalFile.txt";
            // 
            // writeFileLabel
            // 
            this.writeFileLabel.AutoSize = true;
            this.writeFileLabel.Location = new System.Drawing.Point(14, 235);
            this.writeFileLabel.Name = "writeFileLabel";
            this.writeFileLabel.Size = new System.Drawing.Size(45, 13);
            this.writeFileLabel.TabIndex = 4;
            this.writeFileLabel.Text = "write file";
            // 
            // finalFileLabel
            // 
            this.finalFileLabel.AutoSize = true;
            this.finalFileLabel.Location = new System.Drawing.Point(17, 261);
            this.finalFileLabel.Name = "finalFileLabel";
            this.finalFileLabel.Size = new System.Drawing.Size(42, 13);
            this.finalFileLabel.TabIndex = 5;
            this.finalFileLabel.Text = "final file";
            // 
            // readFileComboBox
            // 
            this.readFileComboBox.FormattingEnabled = true;
            this.readFileComboBox.Items.AddRange(new object[] {
            "readFile.txt"});
            this.readFileComboBox.Location = new System.Drawing.Point(12, 21);
            this.readFileComboBox.Name = "readFileComboBox";
            this.readFileComboBox.Size = new System.Drawing.Size(300, 21);
            this.readFileComboBox.TabIndex = 6;
            this.readFileComboBox.SelectedIndexChanged += new System.EventHandler(this.readFileComboBox_SelectedIndexChanged);
            // 
            // keyLabel
            // 
            this.keyLabel.AutoSize = true;
            this.keyLabel.Location = new System.Drawing.Point(17, 287);
            this.keyLabel.Name = "keyLabel";
            this.keyLabel.Size = new System.Drawing.Size(40, 13);
            this.keyLabel.TabIndex = 8;
            this.keyLabel.Text = "key file";
            // 
            // keyFileTextBox
            // 
            this.keyFileTextBox.Location = new System.Drawing.Point(65, 284);
            this.keyFileTextBox.Name = "keyFileTextBox";
            this.keyFileTextBox.Size = new System.Drawing.Size(210, 20);
            this.keyFileTextBox.TabIndex = 7;
            this.keyFileTextBox.Text = "keyFile.bin";
            // 
            // richTextBox
            // 
            this.richTextBox.Location = new System.Drawing.Point(12, 55);
            this.richTextBox.Name = "richTextBox";
            this.richTextBox.ReadOnly = true;
            this.richTextBox.Size = new System.Drawing.Size(371, 167);
            this.richTextBox.TabIndex = 9;
            this.richTextBox.Text = "";
            // 
            // IDEA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 364);
            this.Controls.Add(this.richTextBox);
            this.Controls.Add(this.keyLabel);
            this.Controls.Add(this.keyFileTextBox);
            this.Controls.Add(this.readFileComboBox);
            this.Controls.Add(this.finalFileLabel);
            this.Controls.Add(this.writeFileLabel);
            this.Controls.Add(this.finalFileTextBox);
            this.Controls.Add(this.writeFileTextBox);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.goButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "IDEA";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IDEA";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button goButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.TextBox writeFileTextBox;
        private System.Windows.Forms.TextBox finalFileTextBox;
        private System.Windows.Forms.Label writeFileLabel;
        private System.Windows.Forms.Label finalFileLabel;
        private System.Windows.Forms.ComboBox readFileComboBox;
        private System.Windows.Forms.Label keyLabel;
        private System.Windows.Forms.TextBox keyFileTextBox;
        private System.Windows.Forms.RichTextBox richTextBox;
    }
}

