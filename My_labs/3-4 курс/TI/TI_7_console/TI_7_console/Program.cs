﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;
using System.IO;

namespace TI_7_console
{
    class Program
    {
        static byte[] UIntToByteArr(uint a)
        {
            byte[] result = new byte[4];
            for (int i = 0; i < 4; i++)
                result[i] = (byte)(a >> ((3 - i) * 8));
            return result;
        }
        static uint WFunctionT(int t, byte[] block)
        {
            uint result = 0;
            if (t >= 1 & t <= 16)
            {
                for (int i = 0; i < 4; i ++)
                    result = (result << 8) | block[(t-1) * 4 + i];
            }
            else if (t >= 17 & t <= 80)
                result = CyclicShiftToLeft((WFunctionT(t - 3, block) ^ 
                                            WFunctionT(t - 8, block) ^
                                            WFunctionT(t - 14, block) ^
                                            WFunctionT(t - 16, block)), 1);
            return result;
        }
        static uint KFunctionT(int t)
        {
            uint result = 0;
            if (t >= 1 & t <= 20)
                result = 0x5A827999;
            else if (t >= 21 & t <= 40)
                result = 0x6ED9EBA1;
            else if (t >= 41 & t <= 60)
                result = 0x8F1BBCDC;
            else if (t >= 61 & t <= 80)
                result = 0xCA62C1D6;
            return result;
        }
        static uint CyclicShiftToLeft(uint a, int n)
        {
            return (a << n) | (a & (uint)((1 << n) - 1));
        }
        static uint FunctionT(int t, uint X, uint Y, uint Z)
        {
            uint result = 0;
            if (t >= 1 & t <= 20)
                result = (X & Y) | ((X ^ uint.MaxValue) & Z);
            else if (t >= 21 & t <= 40)
                result = X ^ Y ^ Z;
            else if (t >= 41 & t <= 60)
                result = (X & Y) | (X & Z) | (Y & Z);
            else if (t >= 61 & t <= 80)
                result = X ^ Y ^ Z;
            return result;
        }
        const int bytesIn512Bits = 512 / 8;
        static byte[] Get512Bit(byte[] input, uint textLength, int numberOfBlock)
        {
            byte[] result = new byte[bytesIn512Bits];
            int shift = numberOfBlock * bytesIn512Bits;
            int i = 0;
            for (; i < bytesIn512Bits & i < textLength - shift; i++)
                result[i] = input[shift + i];
            if (i < bytesIn512Bits)
            {
                result[i] = 1 << 7;
                i++;
                for (; i < bytesIn512Bits - 64; i++)
                    result[i] = 0;
                byte[] endOfBlock = UIntToByteArr(textLength);
                for (int j = 0; j < 4; j++)
                    result[result.Length - 4 + j] = endOfBlock[j];
            }
            return result;
        }
        static byte[] HashingSHA1(byte[] text)
        {
            const int lengthOfHash = 160;
            byte[] result = new byte[0];
            byte[] block = new byte[bytesIn512Bits];
            uint A = 0x67452301,
                 B = 0xEFCDAB89,
                 C = 0x98BADCFE,
                 D = 0x10325476,
                 E = 0xC3D2E1F0;
            for (int i = 0; i < text.Length / bytesIn512Bits + 1; i++)
            {
                block = Get512Bit(text, (uint)text.Length, i);
                uint a = A,
                    b = B,
                    c = C,
                    d = D,
                    e = E;
                for (int t = 1; t <= 80; t++)
                {
                    uint TMP = CyclicShiftToLeft(a, 5) + FunctionT(t, b, c, d) + e + WFunctionT(t, block) + KFunctionT(t);
                    e = d;
                    d = c;
                    c = CyclicShiftToLeft(b, 30);
                    b = a;
                    a = TMP;
                }
                A += a;
                B += b;
                C += c;
                D += d;
                E += e;
            }
            result = result.Concat(UIntToByteArr(A)).ToArray();
            result = result.Concat(UIntToByteArr(B)).ToArray();
            result = result.Concat(UIntToByteArr(C)).ToArray();
            result = result.Concat(UIntToByteArr(D)).ToArray();
            result = result.Concat(UIntToByteArr(E)).ToArray();
            return result;
        }
        static BigInteger FastExp(BigInteger a, BigInteger z, BigInteger n)
        {
            BigInteger result = 1, a1 = a, z1 = z;
            while (z1 != 0)
            {
                while ((z1 % 2) == 0)
                {
                    z1 = z1 / 2;
                    a1 = (a1 * a1) % n;
                }
                z1--;
                result = (result * a1) % n;
            }
            return result;
        }
        struct EuclidResult
        {
            public BigInteger x1, y1, d1;
            public EuclidResult(BigInteger x, BigInteger y, BigInteger d)
            {
                x1 = x;
                y1 = y;
                d1 = d;
            }
        }
        static EuclidResult Euclidex(BigInteger a, BigInteger b)
        {
            BigInteger[] d = new BigInteger[3],
                  x = new BigInteger[3],
                  y = new BigInteger[3];
            d[0] = a; d[1] = b;
            x[0] = 1; x[1] = 0;
            y[0] = 0; y[1] = 1;
            while (d[1] > 1)
            {
                BigInteger q = d[0] / d[1];
                d[2] = d[0] % d[1];
                x[2] = x[0] - q * x[1];
                y[2] = y[0] - q * y[1];
                d[0] = d[1];
                d[1] = d[2];
                x[0] = x[1];
                x[1] = x[2];
                y[0] = y[1];
                y[1] = y[2];
            }
            return (new EuclidResult(x[1], (y[1] < 0 ? y[1] + a : y[1]), d[1]));
        }
        static void Main(string[] args)
        {
            byte[] text = System.IO.File.ReadAllBytes("readFile.txt");//= { (byte)'m', (byte)'o', (byte)'u', (byte)'s', (byte)'e' };
            byte[] hash = HashingSHA1(text);
            BigInteger p = 41,
                       q = 59,
                       f = new BigInteger(),
                       closeK = new BigInteger(),
                       openK = 157,
                       r = p * q,
                       m = new BigInteger();
            for (int i = 0; i < 20; i++)
            {
                m <<= 8;
                m |= hash[i];
            }
            for (int i = 0; i < 20; i++)
                Console.WriteLine("{0:X}", hash[i]);
            f = (p - 1) * (q - 1);
            EuclidResult euclidResult = Euclidex(f, openK);
            closeK = euclidResult.y1;
            BigInteger S = FastExp(m, closeK, r);
            if (m % r == FastExp(S, openK, r))
                Console.WriteLine("Yeaaaaaah");
            else
                Console.WriteLine("Noooooooo");
            Console.ReadKey();
        }
    }
}
