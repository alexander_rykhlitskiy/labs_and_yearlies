// TI_3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>
int WriteFileInString(FILE *f, char** result)
{
	int i = 0;
	while (feof(f) == 0)
	{
		char c;
		c = fgetc(f);
		i++;
	}
	*result = (char *) calloc(i, sizeof(char));
	fseek(f, 0, SEEK_SET);
	fread(*result, i, 1, f);
	return i;
}
int IsInArray(unsigned char* arr, int length, unsigned char symbol)
{
	int result = -1;
	for (int i = 0; i < length; i++)
		if (arr[i] == symbol)
		{
			result = i;
			break;
		}
	return result;
}
void FillArray(unsigned char rotor[][26], int dimension)
{
	for (int i = 0; i < dimension - 1; i++)
		for (int j = 0; j < 26; j++)
			do
				rotor[i][j] = (unsigned char)'a' + (unsigned char)(rand() % 26);
			while (IsInArray(rotor[i], j, rotor[i][j]) != -1);
	for (int i = 0; i < 26; i++)
	{
		rotor[dimension - 1][i] = rotor[dimension - 2][25 - i];
	}
}
unsigned char SymbolENIGMACyphering(unsigned char rotor[][26], unsigned char symbol, int shiftOfRotor[])
{
	unsigned char result = symbol;
	for (int i = 0; i < 3; i++)
	{
		result = rotor[2 * i + 1][(IsInArray(rotor[2 * i], 26, result)  + shiftOfRotor[i]) % 26];
	}
	result = rotor[2 * 3 + 1][IsInArray(rotor[2 * 3], 26, result)];
	for (int i = 2; i >= 0; i--)
	{
		result = rotor[2 * i][(IsInArray(rotor[2 * i + 1], 26, result) - shiftOfRotor[i] + 26) % 26];
	}
	return result;
}
void ENIGMACyphering(char* inputText)
{
    srand (0);
	unsigned char rotor[8][26];
	FillArray(rotor, 8);
	int shiftOfRotor[3] = { 0, 0, 0 };
	for (unsigned int i = 0; i < strlen(inputText); i++)
	{
		if ((inputText[i] <= 'z') && (inputText[i] >= 'a'))
		{
			inputText[i] = SymbolENIGMACyphering(rotor, (unsigned char)inputText[i], shiftOfRotor);
		}
		if ((inputText[i] <= 'Z') && (inputText[i] >= 'A'))
		{
			inputText[i] = SymbolENIGMACyphering(rotor, (unsigned char)inputText[i] - ('A' - 'a'), shiftOfRotor) + ('A' - 'a');
		}
		for (int j = 0; j < 3; j++)
		{
			if (i % (int)pow(26.0, j) == 0)
				shiftOfRotor[j]++;
			if (i % (unsigned int)pow(26.0, j + 1) == 0)
				shiftOfRotor[j] = 0;
		}
	}
}

//RotorMachine
int GetBit(int number, int serialNumber)
{
	int result = 0;
	int temp = 0;
	temp = 1 << serialNumber;
	result = temp & number;
	result >>= serialNumber;
	return result;
}
int ShiftToLeft(int number, int quantity, int maxDegree)
{
	int result = 0;
	int temp = (int)pow(2.0, maxDegree) - 1;
	number <<= quantity;
	result = number & temp;
	return result;
}
int GetTempKey(unsigned long long *b, int a, int degree)
{
	int tempKey = 0;
	for(int i = 0; i < 8; i++)
	{
		tempKey <<= 1;
		tempKey |= GetBit(*b, degree - 1);
		int firstBit = GetBit(a, 0) & GetBit(*b, 0);
		for (int j = 1; j < degree; j++)
		{
			firstBit = firstBit ^ (GetBit(a, j) & GetBit(*b, j));
		}
		*b = ShiftToLeft(*b, 1, degree);
		*b |= firstBit;
	}
	return tempKey;
}
unsigned char SymbolRotorMachineCyphering(unsigned char rotor[][256], unsigned char symbol, int shiftOfRotor[])
{
	unsigned char result = symbol;
	for (int i = 0; i < 3; i++)
	{
		result = rotor[2 * i + 1][(IsInArray(rotor[2 * i], 256, result)  + shiftOfRotor[i]) % 256];
	}
	return result;
}
unsigned char SymbolRotorMachineDecyphering(unsigned char rotor[][256], unsigned char symbol, int shiftOfRotor[])
{
	unsigned char result = symbol;
	for (int i = 2; i >= 0; i--)
	{
		result = rotor[2 * i][(IsInArray(rotor[2 * i + 1], 256, result)  - shiftOfRotor[i] + 256*10) % 256];
	}
	return result;
}
void FillArrayForRotorMachine(unsigned char rotor[][256], int dimension)
{
	for (int i = 0; i < dimension; i++)
		for (int j = 0; j < 256; j++)
			do
				rotor[i][j] = (unsigned char)(rand() % 256);
			while (IsInArray(rotor[i], j, rotor[i][j]) != -1);
}
void RotorMachineCyphering(char* inputText, int lengthOfText, bool decipheringOr)
{
	const int degree = 27;
	const int I = 7, J = 9, K = 5;
	unsigned long long a, b;
	a = (1 << 26) + (1 << 7) + (1 << 6) + (1 << 0);
	b = pow(2.0, degree) - 1;

    srand (0);
	unsigned char rotor[6][256];
	FillArrayForRotorMachine(rotor, 6);
	int RS[3] = { 0, 0, 0 };
	unsigned char c = 'a';
	for (unsigned int i = 0; i < lengthOfText; i++)
	{
		int RCB[3];
		for (int j = 0; j < 10; j++)
		{
			if (j == I)
				RCB[0] = GetTempKey(&b, a, degree);
			if (j == J)
				RCB[1] = GetTempKey(&b, a, degree);
			if (j == K)
				RCB[2] = GetTempKey(&b, a, degree);
		}
		for (int j = 0; j < 3; j++)
		{
			RS[j] = (RS[j] + c + RCB[j]) % 256;
		}
		if (decipheringOr)
			c = (unsigned char)inputText[i];
		if (decipheringOr)
			inputText[i] = SymbolRotorMachineDecyphering(rotor, (unsigned char)inputText[i], RS);
		else
			inputText[i] = SymbolRotorMachineCyphering(rotor, (unsigned char)inputText[i], RS);
		if (!decipheringOr)
			c = (unsigned char)inputText[i];
	}
}
//void Diagram(FILE *readFile, FILE *writeFile)
//{
//	fseek(readFile, 0, 0);
//	char *text;
//	WriteFileInString(readFile, &text);
//	printf("%s\n\n", text);
////	free(text);
//
//	fseek(writeFile, 0, 0);
//	WriteFileInString(writeFile, &text);
//	printf("%s\n\n", text);
//}
struct letter
{
	int quantity;
	char sign;
};
int sorting(struct letter *a, int quantity)
{
	int i, j;
	struct letter temp;
	for (i = 0; i < quantity; i++)
		for (j = 0; j < quantity - i; j++)
			if (a[j].quantity < a[j+1].quantity)
			{
				temp = a[j];
				a[j] = a[j+1];
				a[j+1] = temp;
			}
	return 0;
}
int IsInArray(struct letter *letterArray, int quantity, char symbol)
{
	int result = 0;
	for (int i = 0; i < quantity; i++)
		if (letterArray[i].sign == symbol)
		{
			result = i;
			break;
		}
	return result;
}
void AddInArray(struct letter *letterArray, int *quantity, char symbol)
{
	(*quantity)++;
	letterArray[*quantity].sign = symbol;
	letterArray[*quantity].quantity = 1;
}
void Diagram(char *text, int lengthOfText)
{
	int quantity = 0;
	struct letter letter_array[256] = {0};
	int cipher, i;
	for(i = 0; i < lengthOfText; i++)
	{
		int position = 0;
		if (position = IsInArray(letter_array, quantity, text[i]))
			letter_array[position].quantity++;
		else
			AddInArray(letter_array, &quantity, text[i]);
	}
	sorting(letter_array, quantity);
	for (i = 0; i < quantity; i++)
		printf("%d '%c' %.3f\n", letter_array[i].quantity, letter_array[i].sign, letter_array[i].quantity/(double)lengthOfText);

}
int main(int argc, char** argv)
{
	int input = 0;
	char readFileString[100], writeFileString[100], finalFileString[100];
	//puts("Do you want to enter paths to files? (1 - yes, 0 - no)");
	//scanf("%d", &input);
	if (argc == 2)
		input = atoi(argv[1]);
	else 
		if (argc == 5)
			input = atoi(argv[4]);
	if (argc == 2)
	//if (input == 0)
	{
		strcpy(readFileString, "readFile.txt");
		strcpy(writeFileString, "writeFile.txt");
		strcpy(finalFileString, "finalFile.txt");
	}
	else
	if (argc == 5)
	{
		//flushall();
		//puts("Name of readFile:");
		//gets(readFileString);
		//puts("Name of writeFile:");
		//gets(writeFileString);
		//puts("Name of finalFile:");
		//gets(finalFileString);
		strcpy(readFileString, argv[1]);
		strcpy(writeFileString, argv[2]);
		strcpy(finalFileString, argv[3]);
	}
	FILE *readFile;
	readFile = fopen(readFileString, "r");
	if (readFile == 0)
	{
		printf("There is no file %s in folder \"Debug\"", readFileString);
		getchar();
		return 1;
	}
	char *MString;
	int lengthOfMString = WriteFileInString(readFile, &MString);
	FILE *writeFile = fopen(writeFileString, "w+");
	FILE *finalFile = fopen(finalFileString, "w+");
	//while (input != 4)
	{
		//puts("Choose type of cyphering:");
		//puts("1 - ENIGMA");
		//puts("2 - Improved rotor machine");
		//puts("3 - Build diagram");
		//puts("4 - Exit");
		//scanf("%d", &input);
		switch (input)
		{
			case 1:
				fseek(readFile, 0, 0);
				writeFile = fopen(writeFileString, "w+");
				finalFile = fopen(finalFileString, "w+");
				lengthOfMString = WriteFileInString(readFile, &MString);
				printf("%s\n\n", MString);
				ENIGMACyphering(MString);
				printf("%s\n\n", MString);
				fprintf(writeFile, MString);
				ENIGMACyphering(MString);
				printf("%s\n\n", MString);
				fprintf(finalFile, MString);
				break;
			case 2:
				fseek(readFile, 0, 0);
				writeFile = fopen(writeFileString, "w+");
				finalFile = fopen(finalFileString, "w+");
				lengthOfMString = WriteFileInString(readFile, &MString);
				printf("%s\n\n", MString);
				RotorMachineCyphering(MString, lengthOfMString, false);
				printf("%s\n\n", MString);
				fprintf(writeFile, MString);
				RotorMachineCyphering(MString, lengthOfMString, true);
				printf("%s\n\n", MString);
				fprintf(finalFile, MString);
				break;
			case 3:
				//
				break;
		}
	}
	free(MString);
	fseek(readFile, 0, 0);
	lengthOfMString = WriteFileInString(readFile, &MString);
	puts("Symbols in initial file:");
	Diagram(MString, lengthOfMString);
	fclose(writeFile);
	writeFile = fopen(writeFileString, "r");
	free(MString);
	lengthOfMString = WriteFileInString(writeFile, &MString);
	puts("Symbols in encoded file:");
	Diagram(MString, lengthOfMString);flushall();

	getchar();
	fclose(writeFile);
	fclose(finalFile);
	fclose(readFile);
	return 0;
}

