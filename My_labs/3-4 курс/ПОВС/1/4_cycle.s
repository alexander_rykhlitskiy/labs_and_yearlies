.global main
;r0 - 0
;r1 - initial value
;r8 - counter
;r9 - result

main:	addi	r1, r0, 11
	addi	r8, r0, 0

;r2 - (4), 3, 2, 1, 0
;r3 - 8, 4, 2, 1
;r4 - 1
	addi	r2, r0, 4
	addi	r4, r0, 1
check_bit:
	subi	r2, r2, 1
	sll	r3, r4, r2

	and	r8, r1, r3
	srl	r8, r8, r2
	xor	r9, r9, r8

	bnez	r2, check_bit
	nop
	nop

	trap	0
