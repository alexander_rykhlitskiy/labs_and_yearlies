.global main
main:	addi	r1, r0, 14
	
	addi	r2, r0, 1
	and	r3, r1, r2
	
	andi	r4, r1, 2
	srli	r4, r4, 1

	andi	r5, r1, 4
	srli	r5, r5, 2

	andi	r6, r1, 8
	srli	r6, r6, 3	

	jal	set		;ставим здесь, чтобы addi в set: успел выполниться до trap 

	xor	r9, r3, r4
	xor	r9, r9, r5
	xor	r9, r9, r6

exit:	trap	0
set:	addi	r10,r0, 4095
	j	exit