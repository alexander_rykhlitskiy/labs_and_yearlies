Prompt:		.asciiz		"Enter count of numbers and then numbers: \n"
PrintfFormat:	.asciiz		"Result sequence %d.\n"
				.align		2
PrintHello:	.word	Prompt
PrintParameter:	.word		PrintfFormat
PrintfValue:	.space 		0


			.text
			.global	main
main:
		addi		r14,r0,PrintHello
		trap		5
			;*** Set prompt for InputUnsigned function
			addi	r1,r0,Prompt
			jal		InputUnsigned ; call of InputUnsigned
			nop ; required for branch delay slots
			nop ; required for branch delay slots
;r6 - count of input values
;r5 - address of saved calculated value
	addi	r5, r0, 0x0
	add	r6, r0, r1
input:
	subi	r6, r6, 1
	jal	InputUnsigned
	nop
	nop

;r0 - 0
;r1 - initial value
;r8 - counter
;r9 - result
	addi	r9, r0, 0
	addi	r8, r0, 0

;r2 - (4), 3, 2, 1, 0
;r3 - 8, 4, 2, 1
;r4 - 1
	addi	r2, r0, 4
	addi	r4, r0, 1
check_bit:
	subi	r2, r2, 1
	sll	r3, r4, r2

	and	r8, r1, r3
	srl	r8, r8, r2
	xor	r9, r9, r8

	bnez	r2, check_bit
	nop

;end of counting check_bit
;r5 - address of saved calculated value
	slli	r1, r1, 1
	xor	r9, r9, r1
	sw	0(r5), r9
	addi	r5, r5, 4
	bnez	r6, input
	nop

	trap	0


		.data

		;*** Data for Read-Trap
ReadBuffer:	.space		80
ReadPar:	.word		0,ReadBuffer,80

		;*** Data for Printf-Trap
PrintfPar:	.space		4

SaveR2:		.space		4
SaveR3:		.space		4
SaveR4:		.space		4
SaveR5:		.space		4


		.text

		.global		InputUnsigned
InputUnsigned:	
		;*** save register contents
		sw		SaveR2,r2
		sw		SaveR3,r3
		sw		SaveR4,r4
		sw		SaveR5,r5

		;*** call Trap-3 to read line
		addi		r14,r0,ReadPar
		trap		3

		;*** determine value
		addi		r2,r0,ReadBuffer
		addi		r1,r0,0
		addi		r4,r0,10	;Decimal system

Loop:		;*** reads digits to end of line
		lbu		r3,0(r2)
                nop ; required because of data dependency on R3
		seqi		r5,r3,10	;LF -> Exit
		bnez		r5,Finish
		nop ; required for branch delay slots
		nop ; required for branch delay slots
		subi		r3,r3,48	;´0´
		multu		r1,r1,r4	;Shift decimal
		add		r1,r1,r3
		addi		r2,r2,1 	;increment pointer
		j		Loop
		nop ; required for branch delay slots
		nop ; required for branch delay slots
		
Finish: 	;*** restore old register contents
		lw		r2,SaveR2
		lw		r3,SaveR3
		lw		r4,SaveR4
		lw		r5,SaveR5
		jr		r31		; Return
		nop ; required for branch delay slots
		nop ; required for branch delay slots
