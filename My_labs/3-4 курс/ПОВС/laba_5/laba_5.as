opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 5239"

opt pagewidth 120

	opt lm

	processor	16F84A
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
;BANK0:	_main->_writeBinaryOnDisplay
;BANK0:	_writeBinaryOnDisplay->_dirtyHack
	FNCALL	_main,_butState
	FNCALL	_main,_greyNumber
	FNCALL	_main,_writeBinaryOnDisplay
	FNCALL	_writeBinaryOnDisplay,_getValueToWriteOnDisplay
	FNCALL	_writeBinaryOnDisplay,_dirtyHack
	FNROOT	_main
	FNCALL	intlevel1,_ISR
	global	intlevel1
	FNROOT	intlevel1
	global	_globalTimer
	global	_numberOfGrey
	global	_stop
	global	_EEADR
psect	text35,local,class=CODE,delta=2
global __ptext35
__ptext35:
_EEADR  equ     9
	global	_EEDATA
_EEDATA  equ     8
	global	_FSR
_FSR  equ     4
	global	_INDF
_INDF  equ     0
	global	_INTCON
_INTCON  equ     11
	global	_PCL
_PCL  equ     2
	global	_PCLATH
_PCLATH  equ     10
	global	_PORTA
_PORTA  equ     5
	global	_PORTB
_PORTB  equ     6
	global	_RTCC
_RTCC  equ     1
	global	_STATUS
_STATUS  equ     3
	global	_TMR0
_TMR0  equ     1
	global	_CARRY
_CARRY  equ     24
	global	_DC
_DC  equ     25
	global	_EEIE
_EEIE  equ     94
	global	_GIE
_GIE  equ     95
	global	_INT
_INT  equ     48
	global	_INTE
_INTE  equ     92
	global	_INTF
_INTF  equ     89
	global	_PD
_PD  equ     27
	global	_RA0
_RA0  equ     40
	global	_RA1
_RA1  equ     41
	global	_RA2
_RA2  equ     42
	global	_RA3
_RA3  equ     43
	global	_RA4
_RA4  equ     44
	global	_RB0
_RB0  equ     48
	global	_RB1
_RB1  equ     49
	global	_RB2
_RB2  equ     50
	global	_RB3
_RB3  equ     51
	global	_RB4
_RB4  equ     52
	global	_RB5
_RB5  equ     53
	global	_RB6
_RB6  equ     54
	global	_RB7
_RB7  equ     55
	global	_RBIE
_RBIE  equ     91
	global	_RBIF
_RBIF  equ     88
	global	_RP0
_RP0  equ     29
	global	_T0IE
_T0IE  equ     93
	global	_T0IF
_T0IF  equ     90
	global	_TO
_TO  equ     28
	global	_ZERO
_ZERO  equ     26
	global	_EECON1
_EECON1  equ     136
	global	_EECON2
_EECON2  equ     137
	global	_OPTION
_OPTION  equ     129
	global	_TRISA
_TRISA  equ     133
	global	_TRISB
_TRISB  equ     134
	global	_EEIF
_EEIF  equ     1092
	global	_INTEDG
_INTEDG  equ     1038
	global	_PS0
_PS0  equ     1032
	global	_PS1
_PS1  equ     1033
	global	_PS2
_PS2  equ     1034
	global	_PSA
_PSA  equ     1035
	global	_RBPU
_RBPU  equ     1039
	global	_RD
_RD  equ     1088
	global	_T0CS
_T0CS  equ     1037
	global	_T0SE
_T0SE  equ     1036
	global	_TRISA0
_TRISA0  equ     1064
	global	_TRISA1
_TRISA1  equ     1065
	global	_TRISA2
_TRISA2  equ     1066
	global	_TRISA3
_TRISA3  equ     1067
	global	_TRISA4
_TRISA4  equ     1068
	global	_TRISB0
_TRISB0  equ     1072
	global	_TRISB1
_TRISB1  equ     1073
	global	_TRISB2
_TRISB2  equ     1074
	global	_TRISB3
_TRISB3  equ     1075
	global	_TRISB4
_TRISB4  equ     1076
	global	_TRISB5
_TRISB5  equ     1077
	global	_TRISB6
_TRISB6  equ     1078
	global	_TRISB7
_TRISB7  equ     1079
	global	_WR
_WR  equ     1089
	global	_WREN
_WREN  equ     1090
	global	_WRERR
_WRERR  equ     1091
	file	"laba_5.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_globalTimer:
       ds      2

_numberOfGrey:
       ds      2

_stop:
       ds      2

; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
	clrf	((__pbssBANK0)+2)&07Fh
	clrf	((__pbssBANK0)+3)&07Fh
	clrf	((__pbssBANK0)+4)&07Fh
	clrf	((__pbssBANK0)+5)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initationation code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	??_butState
??_butState: ;@ 0x0
	global	??_main
??_main: ;@ 0x0
	global	??_getValueToWriteOnDisplay
??_getValueToWriteOnDisplay: ;@ 0x0
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	??_greyNumber
??_greyNumber: ;@ 0x0
	global	??_dirtyHack
??_dirtyHack: ;@ 0x0
	global	?_getValueToWriteOnDisplay
?_getValueToWriteOnDisplay: ;@ 0x0
	global	butState@result
butState@result:	; 2 bytes @ 0x0
	global	getValueToWriteOnDisplay@number
getValueToWriteOnDisplay@number:	; 2 bytes @ 0x0
	ds	1
	global	dirtyHack@j
dirtyHack@j:	; 2 bytes @ 0x1
	ds	1
	global	?_butState
?_butState: ;@ 0x2
	ds	1
	global	?_greyNumber
?_greyNumber: ;@ 0x3
	global	dirtyHack@i
dirtyHack@i:	; 2 bytes @ 0x3
	global	greyNumber@number
greyNumber@number:	; 2 bytes @ 0x3
	ds	2
	global	?_dirtyHack
?_dirtyHack: ;@ 0x5
	global	dirtyHack@timer
dirtyHack@timer:	; 2 bytes @ 0x5
	ds	2
	global	??_writeBinaryOnDisplay
??_writeBinaryOnDisplay: ;@ 0x7
	ds	1
	global	writeBinaryOnDisplay@i
writeBinaryOnDisplay@i:	; 2 bytes @ 0x8
	ds	2
	global	?_writeBinaryOnDisplay
?_writeBinaryOnDisplay: ;@ 0xA
	global	writeBinaryOnDisplay@number
writeBinaryOnDisplay@number:	; 2 bytes @ 0xA
	ds	4
	global	main@butLastState
main@butLastState:	; 2 bytes @ 0xE
	ds	2
	global	main@state
main@state:	; 2 bytes @ 0x10
	ds	2
	global	??_ISR
??_ISR: ;@ 0x12
	global	?_main
?_main: ;@ 0x12
	ds	4
	global	?_ISR
?_ISR: ;@ 0x16
;Data sizes: Strings 0, constant 0, data 0, bss 6, persistent 0 stack 0
;Auto spaces:   Size  Autos    Used
; COMMON           0      0       0
; BANK0           66     22      28


;Pointer list with targets:

;?_butState	int  size(1); Largest target is 0
;?_greyNumber	int  size(1); Largest target is 0


;Main: autosize = 0, tempsize = 0, incstack = 0, save=0


;Call graph:                      Base Space Used Autos Args Refs Density
;_main                                                6    0  250   0.00
;                                   12 BANK0    6
;           _butState
;         _greyNumber
;_writeBinaryOnDispla
;  _writeBinaryOnDisplay                              3    2  143   0.00
;                                    7 BANK0    5
;_getValueToWriteOnDi
;          _dirtyHack
;         _greyNumber (ARG)
;  _greyNumber                                        3    2   20   0.00
;                                    0 BANK0    5
;  _butState                                          2    2   19   0.00
;                                    0 BANK0    4
;    _dirtyHack                                       5    2   40   0.00
;                                    0 BANK0    7
;    _getValueToWriteOnDisplay                        0    2   16   0.00
;                                    0 BANK0    2
; Estimated maximum call depth 2
;_ISR                                                 4    0    0   0.00
;                                   18 BANK0    4
; Estimated maximum call depth 0
; Address spaces:

;Name               Size   Autos  Total    Cost      Usage
;BITCOMMON            0      0       0       0        0.0%
;NULL                 0      0       0       0        0.0%
;CODE                 0      0       0       0        0.0%
;COMMON               0      0       0       1        0.0%
;SFR0                 0      0       0       1        0.0%
;BITSFR0              0      0       0       1        0.0%
;SFR1                 0      0       0       2        0.0%
;BITSFR1              0      0       0       2        0.0%
;STACK                0      0       0       2        0.0%
;BITBANK0            42      0       0       3        0.0%
;BANK0               42     16      1C       4       42.4%
;ABS                  0      0      1C       5        0.0%
;DATA                 0      0      1C       6        0.0%
;EEDATA              40      0       0    1000        0.0%

	global	_main
psect	maintext,local,class=CODE,delta=2
global __pmaintext
__pmaintext:

; *************** function _main *****************
; Defined at:
;		line 74 in file "D:\Univer\Labs\laba_5\main.c"
; Parameters:    Size  Location     Type
;		None
; Auto vars:     Size  Location     Type
;  state           2   16[BANK0 ] int 
;  butLastState    2   14[BANK0 ] int 
;  j               2    0        int 
; Return value:  Size  Location     Type
;                  2   18[BANK0 ] int 
; Registers used:
;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
; Tracked objects:
;		On entry : 17F/0
;		On exit  : 0/0
;		Unchanged: 0/0
; Data sizes:     COMMON   BANK0
;      Locals:         0       6
;      Temp:     0
;      Total:    6
; This function calls:
;		_butState
;		_greyNumber
;		_writeBinaryOnDisplay
; This function is called by:
;		Startup code after reset
; This function uses a non-reentrant model
; 
psect	maintext
	file	"D:\Univer\Labs\laba_5\main.c"
	line	74
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
;main.c: 74: main(){
	
_main:	
	opt stack 7
; Regs used in _main: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	76
	
l30000275:	
# 76 "D:\Univer\Labs\laba_5\main.c"
CLRWDT ;#
psect	maintext
	line	77
;main.c: 77: PSA = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	(1035/8)^080h,(1035)&7
	
l30000276:	
	line	79
;main.c: 79: TMR0 = 100;
	movlw	(064h)
	bcf	status, 5	;RP0=0, select bank0
	movwf	(1)	;volatile
	
l30000277:	
	line	80
;main.c: 80: INTCON = 0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(11)	;volatile
	
l30000278:	
	line	82
;main.c: 82: OPTION = 0b00000111;
	movlw	(07h)
	bsf	status, 5	;RP0=1, select bank1
	movwf	(129)^080h
	
l30000279:	
	line	83
;main.c: 83: T0IE=1;
	bcf	status, 5	;RP0=0, select bank0
	bsf	(93/8),(93)&7
	
l30000280:	
	line	84
;main.c: 84: GIE=1;
	bsf	(95/8),(95)&7
	
l30000281:	
	line	85
;main.c: 85: globalTimer = 0;
	movlw	low(0)
	movwf	(_globalTimer)
	movlw	high(0)
	movwf	((_globalTimer))+1
	
l30000282:	
	line	87
;main.c: 87: TRISB=0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	bsf	status, 5	;RP0=1, select bank1
	movwf	(134)^080h	;volatile
	
l30000283:	
	line	88
;main.c: 88: TRISA = 0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(133)^080h	;volatile
	
l30000284:	
	line	91
;main.c: 91: int butLastState = 1;
	movlw	low(01h)
	bcf	status, 5	;RP0=0, select bank0
	movwf	(main@butLastState)
	movlw	high(01h)
	movwf	((main@butLastState))+1
	
l30000285:	
	line	92
;main.c: 92: stop = 0;
	movlw	low(0)
	movwf	(_stop)
	movlw	high(0)
	movwf	((_stop))+1
	
l30000286:	
	line	93
;main.c: 93: numberOfGrey = 0;
	movlw	low(0)
	movwf	(_numberOfGrey)
	movlw	high(0)
	movwf	((_numberOfGrey))+1
	
l30000287:	
	
l30000288:	
	line	96
# 96 "D:\Univer\Labs\laba_5\main.c"
CLRWDT ;#
psect	maintext
	
l30000289:	
	line	97
;main.c: 97: int state = butState();
	fcall	_butState
	bcf	status, 7	;select IRP bank0
	bcf	status, 5	;RP0=0, select bank0
	movf	(1+(?_butState)),w
	clrf	(main@state+1)
	addwf	(main@state+1)
	movf	(0+(?_butState)),w
	clrf	(main@state)
	addwf	(main@state)

	
l30000290:	
	line	98
;main.c: 98: if ((state != butLastState) & !state)
	movf	(main@butLastState+1),w
	xorwf	(main@state+1),w
	skipz
	goto	u205
	movf	(main@butLastState),w
	xorwf	(main@state),w
u205:

	skipnz
	goto	u201
	goto	u200
u201:
	goto	l30000293
u200:
	
l30000291:	
	movf	((main@state+1)),w
	iorwf	((main@state)),w
	skipz
	goto	u211
	goto	u210
u211:
	goto	l30000293
u210:
	
l30000292:	
	line	99
;main.c: 99: stop ^= 1;
	movlw	low(01h)
	xorwf	(_stop),f
	movlw	high(01h)
	xorwf	(_stop+1),f
	
l30000293:	
	line	100
;main.c: 100: butLastState = state;
	movf	(main@state+1),w
	clrf	(main@butLastState+1)
	addwf	(main@butLastState+1)
	movf	(main@state),w
	clrf	(main@butLastState)
	addwf	(main@butLastState)

	
l30000294:	
	line	101
;main.c: 101: writeBinaryOnDisplay(greyNumber(numberOfGrey));
	movf	(_numberOfGrey+1),w
	clrf	(?_greyNumber+1)
	addwf	(?_greyNumber+1)
	movf	(_numberOfGrey),w
	clrf	(?_greyNumber)
	addwf	(?_greyNumber)

	fcall	_greyNumber
	bcf	status, 7	;select IRP bank0
	bcf	status, 5	;RP0=0, select bank0
	movf	(1+(?_greyNumber)),w
	clrf	(?_writeBinaryOnDisplay+1)
	addwf	(?_writeBinaryOnDisplay+1)
	movf	(0+(?_greyNumber)),w
	clrf	(?_writeBinaryOnDisplay)
	addwf	(?_writeBinaryOnDisplay)

	fcall	_writeBinaryOnDisplay
	goto	l30000288
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
; =============== function _main ends ============

psect	maintext
	line	103
	signat	_main,90
	global	_writeBinaryOnDisplay
psect	text36,local,class=CODE,delta=2
global __ptext36
__ptext36:

; *************** function _writeBinaryOnDisplay *****************
; Defined at:
;		line 33 in file "D:\Univer\Labs\laba_5\main.c"
; Parameters:    Size  Location     Type
;  number          2   10[BANK0 ] int 
; Auto vars:     Size  Location     Type
;  i               2    8[BANK0 ] int 
; Return value:  Size  Location     Type
;		None               void
; Registers used:
;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
; Tracked objects:
;		On entry : 0/0
;		On exit  : 0/0
;		Unchanged: 0/0
; Data sizes:     COMMON   BANK0
;      Locals:         0       5
;      Temp:     1
;      Total:    5
; This function calls:
;		_getValueToWriteOnDisplay
;		_dirtyHack
; This function is called by:
;		_main
; This function uses a non-reentrant model
; 
psect	text36
	file	"D:\Univer\Labs\laba_5\main.c"
	line	33
	global	__size_of_writeBinaryOnDisplay
	__size_of_writeBinaryOnDisplay	equ	__end_of_writeBinaryOnDisplay-_writeBinaryOnDisplay
;main.c: 33: void writeBinaryOnDisplay(int number){
	
_writeBinaryOnDisplay:	
	opt stack 6
; Regs used in _writeBinaryOnDisplay: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	34
	
l30000267:	
;main.c: 34: for(int i = 0; i < 4; i++) {
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	movwf	(writeBinaryOnDisplay@i)
	movlw	high(0)
	movwf	((writeBinaryOnDisplay@i))+1
	movf	(writeBinaryOnDisplay@i+1),w
	xorlw	80h
	movwf	(??_writeBinaryOnDisplay+0+0)
	movlw	(high(04h))^80h
	subwf	(??_writeBinaryOnDisplay+0+0),w
	skipz
	goto	u165
	movlw	low(04h)
	subwf	(writeBinaryOnDisplay@i),w
u165:

	skipc
	goto	u161
	goto	u160
u161:
	goto	l30000269
u160:
	goto	l23
	
l30000269:	
	line	35
;main.c: 35: PORTA = (8 >> i) ^ 0b1111;
	movlw	(08h)
	bcf	status, 5	;RP0=0, select bank0
	movwf	(??_writeBinaryOnDisplay+0+0)
	incf	(writeBinaryOnDisplay@i),w
	goto	u174
u175:
	clrc
	rrf	(??_writeBinaryOnDisplay+0+0),f
u174:
	addlw	-1
	skipz
	goto	u175
	movf	0+(??_writeBinaryOnDisplay+0+0),w
	xorlw	0Fh
	movwf	(5)	;volatile
	
l30000270:	
	line	36
;main.c: 36: PORTB = getValueToWriteOnDisplay(number & 1);
	movlw	low(01h)
	andwf	(writeBinaryOnDisplay@number),w
	movwf	(?_getValueToWriteOnDisplay)
	movlw	high(01h)
	andwf	(writeBinaryOnDisplay@number+1),w
	movwf	1+(?_getValueToWriteOnDisplay)
	fcall	_getValueToWriteOnDisplay
	bcf	status, 7	;select IRP bank0
	bcf	status, 5	;RP0=0, select bank0
	movf	(0+(?_getValueToWriteOnDisplay)),w
	movwf	(6)	;volatile
	
l30000271:	
	line	37
;main.c: 37: number >>= 1;
	movlw	01h
	movwf	(??_writeBinaryOnDisplay+0+0)
u185:
	rlf	(writeBinaryOnDisplay@number+1),w
	rrf	(writeBinaryOnDisplay@number+1),f
	rrf	(writeBinaryOnDisplay@number),f
	decfsz	(??_writeBinaryOnDisplay+0+0),f
	goto	u185
	
l30000272:	
	line	38
;main.c: 38: dirtyHack(4);
	movlw	low(04h)
	movwf	(?_dirtyHack)
	movlw	high(04h)
	movwf	((?_dirtyHack))+1
	fcall	_dirtyHack
	
l30000273:	
	line	34
	movlw	low(01h)
	bcf	status, 5	;RP0=0, select bank0
	addwf	(writeBinaryOnDisplay@i),f
	skipnc
	incf	(writeBinaryOnDisplay@i+1),f
	movlw	high(01h)
	addwf	(writeBinaryOnDisplay@i+1),f
	
l30000274:	
	movf	(writeBinaryOnDisplay@i+1),w
	xorlw	80h
	movwf	(??_writeBinaryOnDisplay+0+0)
	movlw	(high(04h))^80h
	subwf	(??_writeBinaryOnDisplay+0+0),w
	skipz
	goto	u195
	movlw	low(04h)
	subwf	(writeBinaryOnDisplay@i),w
u195:

	skipc
	goto	u191
	goto	u190
u191:
	goto	l30000269
u190:
	
l23:	
	return
	opt stack 0
GLOBAL	__end_of_writeBinaryOnDisplay
	__end_of_writeBinaryOnDisplay:
; =============== function _writeBinaryOnDisplay ends ============

psect	text37,local,class=CODE,delta=2
global __ptext37
__ptext37:
	line	40
	signat	_writeBinaryOnDisplay,4216
	global	_greyNumber

; *************** function _greyNumber *****************
; Defined at:
;		line 41 in file "D:\Univer\Labs\laba_5\main.c"
; Parameters:    Size  Location     Type
;  number          2    3[BANK0 ] int 
; Auto vars:     Size  Location     Type
;		None
; Return value:  Size  Location     Type
;                  2    3[BANK0 ] int 
; Registers used:
;		wreg, status,2, status,0
; Tracked objects:
;		On entry : 0/0
;		On exit  : 0/0
;		Unchanged: 0/0
; Data sizes:     COMMON   BANK0
;      Locals:         0       5
;      Temp:     3
;      Total:    5
; This function calls:
;		Nothing
; This function is called by:
;		_main
; This function uses a non-reentrant model
; 
psect	text37
	file	"D:\Univer\Labs\laba_5\main.c"
	line	41
	global	__size_of_greyNumber
	__size_of_greyNumber	equ	__end_of_greyNumber-_greyNumber
;main.c: 41: int greyNumber(int number){
	
_greyNumber:	
	opt stack 6
; Regs used in _greyNumber: [wreg+status,2+status,0]
	line	42
	
l30000205:	
;main.c: 42: return number ^ (number >> 1);
	bcf	status, 5	;RP0=0, select bank0
	movf	(greyNumber@number+1),w
	movwf	(??_greyNumber+0+0+1)
	movf	(greyNumber@number),w
	movwf	(??_greyNumber+0+0)
	movlw	01h
	movwf	(??_greyNumber+2+0)
u75:
	rlf	(??_greyNumber+0+1),w
	rrf	(??_greyNumber+0+1),f
	rrf	(??_greyNumber+0+0),f
	decfsz	(??_greyNumber+2+0),f
	goto	u75
	movf	(greyNumber@number),w
	xorwf	0+(??_greyNumber+0+0),w
	movwf	(?_greyNumber)
	movf	(greyNumber@number+1),w
	xorwf	1+(??_greyNumber+0+0),w
	movwf	1+(?_greyNumber)
	
l27:	
	return
	opt stack 0
GLOBAL	__end_of_greyNumber
	__end_of_greyNumber:
; =============== function _greyNumber ends ============

psect	text38,local,class=CODE,delta=2
global __ptext38
__ptext38:
	line	43
	signat	_greyNumber,4218
	global	_butState

; *************** function _butState *****************
; Defined at:
;		line 44 in file "D:\Univer\Labs\laba_5\main.c"
; Parameters:    Size  Location     Type
;		None
; Auto vars:     Size  Location     Type
;  result          2    0[BANK0 ] int 
; Return value:  Size  Location     Type
;                  2    2[BANK0 ] int 
; Registers used:
;		wreg, status,2, status,0
; Tracked objects:
;		On entry : 0/0
;		On exit  : 0/0
;		Unchanged: 0/0
; Data sizes:     COMMON   BANK0
;      Locals:         0       4
;      Temp:     0
;      Total:    4
; This function calls:
;		Nothing
; This function is called by:
;		_main
; This function uses a non-reentrant model
; 
psect	text38
	file	"D:\Univer\Labs\laba_5\main.c"
	line	44
	global	__size_of_butState
	__size_of_butState	equ	__end_of_butState-_butState
;main.c: 44: int butState(){
	
_butState:	
	opt stack 6
; Regs used in _butState: [wreg+status,2+status,0]
	
l30000295:	
	
l30000296:	
	line	46
;main.c: 46: PORTB |= 1;
	bcf	status, 5	;RP0=0, select bank0
	bsf	(6)+(0/8),(0)&7	;volatile
	
l30000297:	
	line	47
;main.c: 47: if (PORTB & 1)
	btfss	(6),(0)&7	;volatile
	goto	u221
	goto	u220
u221:
	goto	l30000299
u220:
	
l30000298:	
	line	48
;main.c: 48: result = 0;
	movlw	low(0)
	movwf	(butState@result)
	movlw	high(0)
	movwf	((butState@result))+1
	goto	l30000300
	
l30000299:	
	line	50
;main.c: 49: else
;main.c: 50: result = 1;
	movlw	low(01h)
	movwf	(butState@result)
	movlw	high(01h)
	movwf	((butState@result))+1
	
l30000300:	
	line	51
;main.c: 51: return result;
	movf	(butState@result+1),w
	clrf	(?_butState+1)
	addwf	(?_butState+1)
	movf	(butState@result),w
	clrf	(?_butState)
	addwf	(?_butState)

	
l28:	
	return
	opt stack 0
GLOBAL	__end_of_butState
	__end_of_butState:
; =============== function _butState ends ============

psect	text39,local,class=CODE,delta=2
global __ptext39
__ptext39:
	line	52
	signat	_butState,90
	global	_dirtyHack

; *************** function _dirtyHack *****************
; Defined at:
;		line 4 in file "D:\Univer\Labs\laba_5\main.c"
; Parameters:    Size  Location     Type
;  timer           2    5[BANK0 ] int 
; Auto vars:     Size  Location     Type
;  j               2    1[BANK0 ] int 
;  i               2    3[BANK0 ] int 
; Return value:  Size  Location     Type
;		None               void
; Registers used:
;		wreg
; Tracked objects:
;		On entry : 0/0
;		On exit  : 0/0
;		Unchanged: 0/0
; Data sizes:     COMMON   BANK0
;      Locals:         0       7
;      Temp:     1
;      Total:    7
; This function calls:
;		Nothing
; This function is called by:
;		_writeBinaryOnDisplay
; This function uses a non-reentrant model
; 
psect	text39
	file	"D:\Univer\Labs\laba_5\main.c"
	line	4
	global	__size_of_dirtyHack
	__size_of_dirtyHack	equ	__end_of_dirtyHack-_dirtyHack
;pic1684.h: 17: volatile unsigned char INDF @ 0x00;
;pic1684.h: 18: volatile unsigned char RTCC @ 0x01;
;pic1684.h: 19: volatile unsigned char TMR0 @ 0x01;
;pic1684.h: 20: volatile unsigned char PCL @ 0x02;
;pic1684.h: 21: volatile unsigned char STATUS @ 0x03;
;pic1684.h: 22: unsigned char FSR @ 0x04;
;pic1684.h: 23: volatile unsigned char PORTA @ 0x05;
;pic1684.h: 24: volatile unsigned char PORTB @ 0x06;
;pic1684.h: 25: volatile unsigned char EEDATA @ 0x08;
;pic1684.h: 26: volatile unsigned char EEADR @ 0x09;
	
_dirtyHack:	
	opt stack 5
; Regs used in _dirtyHack: [wreg]
	line	5
	
l30000168:	
;main.c: 5: for(int i = 0; i < timer; i++)
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	movwf	(dirtyHack@i)
	movlw	high(0)
	movwf	((dirtyHack@i))+1
	goto	l5
	
l30000169:	
	line	6
;main.c: 6: for(int j = 0; j < timer; j++);
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	movwf	(dirtyHack@j)
	movlw	high(0)
	movwf	((dirtyHack@j))+1
	goto	l9
	
l30000170:	
	movlw	low(01h)
	bcf	status, 5	;RP0=0, select bank0
	addwf	(dirtyHack@j),f
	skipnc
	incf	(dirtyHack@j+1),f
	movlw	high(01h)
	addwf	(dirtyHack@j+1),f
	
l9:	
	movf	(dirtyHack@j+1),w
	xorlw	80h
	movwf	(??_dirtyHack+0+0)
	movf	(dirtyHack@timer+1),w
	xorlw	80h
	subwf	(??_dirtyHack+0+0),w
	skipz
	goto	u15
	movf	(dirtyHack@timer),w
	subwf	(dirtyHack@j),w
u15:

	skipc
	goto	u11
	goto	u10
u11:
	goto	l30000170
u10:
	
l30000171:	
	line	5
	movlw	low(01h)
	bcf	status, 5	;RP0=0, select bank0
	addwf	(dirtyHack@i),f
	skipnc
	incf	(dirtyHack@i+1),f
	movlw	high(01h)
	addwf	(dirtyHack@i+1),f
	
l5:	
	movf	(dirtyHack@i+1),w
	xorlw	80h
	movwf	(??_dirtyHack+0+0)
	movf	(dirtyHack@timer+1),w
	xorlw	80h
	subwf	(??_dirtyHack+0+0),w
	skipz
	goto	u25
	movf	(dirtyHack@timer),w
	subwf	(dirtyHack@i),w
u25:

	skipc
	goto	u21
	goto	u20
u21:
	goto	l30000169
u20:
	
l1:	
	return
	opt stack 0
GLOBAL	__end_of_dirtyHack
	__end_of_dirtyHack:
; =============== function _dirtyHack ends ============

psect	text40,local,class=CODE,delta=2
global __ptext40
__ptext40:
	line	7
	signat	_dirtyHack,4216
	global	_getValueToWriteOnDisplay

; *************** function _getValueToWriteOnDisplay *****************
; Defined at:
;		line 8 in file "D:\Univer\Labs\laba_5\main.c"
; Parameters:    Size  Location     Type
;  number          2    0[BANK0 ] int 
; Auto vars:     Size  Location     Type
;		None
; Return value:  Size  Location     Type
;                  2    0[BANK0 ] int 
; Registers used:
;		wreg, fsr0l, fsr0h, status,2, status,0
; Tracked objects:
;		On entry : 0/0
;		On exit  : 0/0
;		Unchanged: 0/0
; Data sizes:     COMMON   BANK0
;      Locals:         0       2
;      Temp:     0
;      Total:    2
; This function calls:
;		Nothing
; This function is called by:
;		_writeBinaryOnDisplay
; This function uses a non-reentrant model
; 
psect	text40
	file	"D:\Univer\Labs\laba_5\main.c"
	line	8
	global	__size_of_getValueToWriteOnDisplay
	__size_of_getValueToWriteOnDisplay	equ	__end_of_getValueToWriteOnDisplay-_getValueToWriteOnDisplay
;main.c: 8: int getValueToWriteOnDisplay(int number){
	
_getValueToWriteOnDisplay:	
	opt stack 5
; Regs used in _getValueToWriteOnDisplay: [wreg-fsr0h+status,2+status,0]
	
l30000242:	
	goto	l30000264
	
l30000243:	
	line	11
;main.c: 11: return 0b01111110;
	movlw	low(07Eh)
	movwf	(?_getValueToWriteOnDisplay)
	movlw	high(07Eh)
	movwf	((?_getValueToWriteOnDisplay))+1
	goto	l10
	
l30000245:	
	line	13
;main.c: 13: return 0b00001100;
	movlw	low(0Ch)
	movwf	(?_getValueToWriteOnDisplay)
	movlw	high(0Ch)
	movwf	((?_getValueToWriteOnDisplay))+1
	goto	l10
	
l30000247:	
	line	15
;main.c: 15: return 0b10110110;
	movlw	low(0B6h)
	movwf	(?_getValueToWriteOnDisplay)
	movlw	high(0B6h)
	movwf	((?_getValueToWriteOnDisplay))+1
	goto	l10
	
l30000249:	
	line	17
;main.c: 17: return 0b10011110;
	movlw	low(09Eh)
	movwf	(?_getValueToWriteOnDisplay)
	movlw	high(09Eh)
	movwf	((?_getValueToWriteOnDisplay))+1
	goto	l10
	
l30000251:	
	line	19
;main.c: 19: return 0b11001100;
	movlw	low(0CCh)
	movwf	(?_getValueToWriteOnDisplay)
	movlw	high(0CCh)
	movwf	((?_getValueToWriteOnDisplay))+1
	goto	l10
	
l30000253:	
	line	21
;main.c: 21: return 0b11011010;
	movlw	low(0DAh)
	movwf	(?_getValueToWriteOnDisplay)
	movlw	high(0DAh)
	movwf	((?_getValueToWriteOnDisplay))+1
	goto	l10
	
l30000255:	
	line	23
;main.c: 23: return 0b11111010;
	movlw	low(0FAh)
	movwf	(?_getValueToWriteOnDisplay)
	movlw	high(0FAh)
	movwf	((?_getValueToWriteOnDisplay))+1
	goto	l10
	
l30000257:	
	line	25
;main.c: 25: return 0b00001110;
	movlw	low(0Eh)
	movwf	(?_getValueToWriteOnDisplay)
	movlw	high(0Eh)
	movwf	((?_getValueToWriteOnDisplay))+1
	goto	l10
	
l30000259:	
	line	27
;main.c: 27: return 0b11111110;
	movlw	low(0FEh)
	movwf	(?_getValueToWriteOnDisplay)
	movlw	high(0FEh)
	movwf	((?_getValueToWriteOnDisplay))+1
	goto	l10
	
l30000261:	
	line	29
;main.c: 29: return 0b11011110;
	movlw	low(0DEh)
	movwf	(?_getValueToWriteOnDisplay)
	movlw	high(0DEh)
	movwf	((?_getValueToWriteOnDisplay))+1
	goto	l10
	
l30000264:	
	line	9
		goto	l30013
l30014:
	bcf	status, 5	;RP0=0, select bank0
	movf (getValueToWriteOnDisplay@number),w
	xorlw	0^0
	skipnz
	goto	l30000243
	xorlw	1^0
	skipnz
	goto	l30000245
	xorlw	2^1
	skipnz
	goto	l30000247
	xorlw	3^2
	skipnz
	goto	l30000249
	xorlw	4^3
	skipnz
	goto	l30000251
	xorlw	5^4
	skipnz
	goto	l30000253
	xorlw	6^5
	skipnz
	goto	l30000255
	xorlw	7^6
	skipnz
	goto	l30000257
	xorlw	8^7
	skipnz
	goto	l30000259
	xorlw	9^8
	skipnz
	goto	l30000261
	goto	l30000265

l30013:
	movf (getValueToWriteOnDisplay@number+1),w
	xorlw	0^0
	skipnz
	goto	l30014
	goto	l30000265

	
l30000265:	
	line	31
;main.c: 31: return 0;
	movlw	low(0)
	movwf	(?_getValueToWriteOnDisplay)
	movlw	high(0)
	movwf	((?_getValueToWriteOnDisplay))+1
	
l10:	
	return
	opt stack 0
GLOBAL	__end_of_getValueToWriteOnDisplay
	__end_of_getValueToWriteOnDisplay:
; =============== function _getValueToWriteOnDisplay ends ============

psect	text41,local,class=CODE,delta=2
global __ptext41
__ptext41:
	line	32
	signat	_getValueToWriteOnDisplay,4218
	global	_ISR

; *************** function _ISR *****************
; Defined at:
;		line 58 in file "D:\Univer\Labs\laba_5\main.c"
; Parameters:    Size  Location     Type
;		None
; Auto vars:     Size  Location     Type
;		None
; Return value:  Size  Location     Type
;		None               void
; Registers used:
;		wreg, status,2, status,0
; Tracked objects:
;		On entry : 0/0
;		On exit  : 0/0
;		Unchanged: 0/0
; Data sizes:     COMMON   BANK0
;      Locals:         0       4
;      Temp:     4
;      Total:    4
; This function calls:
;		Nothing
; This function is called by:
;		Interrupt level 1
; This function uses a non-reentrant model
; 
psect	text41
	file	"D:\Univer\Labs\laba_5\main.c"
	line	58
	global	__size_of_ISR
	__size_of_ISR	equ	__end_of_ISR-_ISR
;main.c: 54: int globalTimer;
;main.c: 55: int numberOfGrey;
;main.c: 56: int stop;
;main.c: 58: void interrupt ISR(){
	
_ISR:	
	opt stack 6
; Regs used in _ISR: [wreg+status,2+status,0]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+1
	movwf	saved_w
	movf	status,w
	bcf	status, 5	;RP0=0, select bank0
	movwf	(??_ISR+0)
	movf	fsr0,w
	movwf	(??_ISR+1)
	movf	pclath,w
	movwf	(??_ISR+2)
	movf	btemp+0,w
	movwf	(??_ISR+3)
	ljmp	_ISR
psect	text41
	line	59
	
i1l30000214:	
;main.c: 59: if(T0IE && T0IF){
	btfss	(93/8),(93)&7
	goto	u9_21
	goto	u9_20
u9_21:
	goto	i1l31
u9_20:
	
i1l30000215:	
	btfss	(90/8),(90)&7
	goto	u10_21
	goto	u10_20
u10_21:
	goto	i1l31
u10_20:
	
i1l30000216:	
	line	60
;main.c: 60: if (globalTimer == 10) {
	movlw	0Ah
	xorwf	(_globalTimer),w
	iorwf	(_globalTimer+1),w
	skipz
	goto	u11_21
	goto	u11_20
u11_21:
	goto	i1l33
u11_20:
	
i1l30000217:	
	line	61
;main.c: 61: globalTimer = 0;
	movlw	low(0)
	movwf	(_globalTimer)
	movlw	high(0)
	movwf	((_globalTimer))+1
	
i1l30000218:	
	line	62
;main.c: 62: if (!stop) {
	movf	((_stop+1)),w
	iorwf	((_stop)),w
	skipz
	goto	u12_21
	goto	u12_20
u12_21:
	goto	i1l33
u12_20:
	
i1l30000219:	
	line	63
;main.c: 63: if (numberOfGrey == 16) {
	movlw	010h
	xorwf	(_numberOfGrey),w
	iorwf	(_numberOfGrey+1),w
	skipz
	goto	u13_21
	goto	u13_20
u13_21:
	goto	i1l35
u13_20:
	
i1l30000220:	
	line	64
;main.c: 64: numberOfGrey = 0;
	movlw	low(0)
	movwf	(_numberOfGrey)
	movlw	high(0)
	movwf	((_numberOfGrey))+1
	
i1l35:	
	line	66
;main.c: 65: }
;main.c: 66: numberOfGrey++;
	movlw	low(01h)
	addwf	(_numberOfGrey),f
	skipnc
	incf	(_numberOfGrey+1),f
	movlw	high(01h)
	addwf	(_numberOfGrey+1),f
	
i1l33:	
	line	69
;main.c: 67: }
;main.c: 68: }
;main.c: 69: globalTimer++;
	movlw	low(01h)
	addwf	(_globalTimer),f
	skipnc
	incf	(_globalTimer+1),f
	movlw	high(01h)
	addwf	(_globalTimer+1),f
	line	70
;main.c: 70: TMR0 = 158;
	movlw	(09Eh)
	movwf	(1)	;volatile
	
i1l30000221:	
	line	71
;main.c: 71: T0IF = 0;
	bcf	(90/8),(90)&7
	
i1l31:	
	movf	(??_ISR+3),w
	movwf	btemp+0
	movf	(??_ISR+2),w
	movwf	pclath
	movf	(??_ISR+1),w
	movwf	fsr0
	movf	(??_ISR+0),w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_ISR
	__end_of_ISR:
; =============== function _ISR ends ============

psect	text42,local,class=CODE,delta=2
global __ptext42
__ptext42:
	line	73
	signat	_ISR,88
	global	btemp
	btemp set 04Eh

	DABS	1,78,2	;btemp
	end
