#include <htc.h>
#include <pic.h>

void dirtyHack(int timer){
	for(int i = 0; i < timer; i++)
		for(int j = 0; j < timer; j++);
}
int getValueToWriteOnDisplay(int number){
	switch(number){
	case 0:
		return 0b01111110;
	case 1:
		return 0b00001100;
	case 2:
		return 0b10110110;
	case 3:
		return 0b10011110;
	case 4:
		return 0b11001100;
	case 5:
		return 0b11011010;
	case 6:
		return 0b11111010;
	case 7:
		return 0b00001110;
	case 8:
		return 0b11111110;
	case 9:
		return 0b11011110;
	}
	return 0;
}
void writeBinaryOnDisplay(int number){
	for(int i = 0; i < 4; i++) {
		PORTA = (8 >> i) ^ 0b1111;
		PORTB = getValueToWriteOnDisplay(number & 1);
		number >>= 1;
		dirtyHack(4);
	}
}
int greyNumber(int number){
	return number ^ (number >> 1);
}
int butState(){
	int result = 0;
	PORTB |= 1;
	if (PORTB & 1)
		result = 0;
	else
		result = 1;
	return result;
}

int globalTimer;
int numberOfGrey;
int stop;

void interrupt ISR(){
	if(T0IE && T0IF){
		if (globalTimer == 10) {
			globalTimer = 0;
	    	if (!stop) {
				if (numberOfGrey == 16) {
					numberOfGrey = 0;
				}
				numberOfGrey++;
			}
		}
		globalTimer++;
		TMR0 = 158;
		T0IF = 0;
    }
}
main(){
//switch prescaler to tmr0
	asm("CLRWDT");
	PSA = 0;
//initialize tmr0
	TMR0 = 100;
	INTCON = 0;	
//p. 151 (pic16_manual)
	OPTION = 0b00000111;
    T0IE=1;
    GIE=1;
	globalTimer = 0;

    TRISB=0;
    TRISA = 0;
    //0 - off/false
    //1 - on/true
    int butLastState = 1;
    stop = 0;
	numberOfGrey = 0;
	int j = 0;
    while(1) {
		asm("CLRWDT");
		int state = butState();
		if ((state != butLastState) & !state)
			stop ^= 1;
		butLastState = state;
		writeBinaryOnDisplay(greyNumber(numberOfGrey));
    }
}