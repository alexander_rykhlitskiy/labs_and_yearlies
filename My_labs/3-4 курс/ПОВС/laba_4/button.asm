           LIST    p=16F84 ; 
           #include "P16F84.INC"	; Include header file
           GOTO    MAIN           		; Go to main code
	  ORG     4		; Interrupt vector.
HALT:      GOTO    HALT		; Sit in endless loop

constant LED=0x80
constant BUT=0x01
BUT_OLD EQU 0x20	; Button old value
BUT_CUR EQU 0x21	; Button current value
LED_STA EQU 0x22	; LED status
XRES    EQU 0x23	; temporarily variable

MAIN:			; Main code
	CLRF PORTB	; Port initialization
	BSF STATUS,RP0
	MOVLW 0x01
	MOVWF TRISB
	BCF STATUS,RP0
	CLRF BUT_CUR	; BUT_CUR=0
	CLRF LED_STA	; LED is OFF
RESET:	
	CLRF BUT_OLD	; BUT_OLD=0
LOOP:
	CALL BUT_STATUS
	; w=0 button is OFF
	; w=1 buttin is ON
	MOVWF BUT_CUR   	; W=BUT_CUR
	BTFSS BUT_CUR,0 	; BUT_CUR<0>?
	GOTO RESET      	; BUT_CUR<0>='0'
			; BUT_CUR<0>='1'

			

	CLRW
	MOVF BUT_OLD,0
	XORWF BUT_CUR,0
	MOVWF XRES
	BTFSS XRES,0	; RES<0>?
	GOTO LOOP	; RES<0>='0'
			; RES<0>='1'
	CALL CLED	; change the LED status
	MOVF BUT_CUR,0
	MOVWF BUT_OLD
	GOTO LOOP	
	
BUT_STATUS
	BTFSC PORTB,0   	; PORTB<0>?
	RETLW 0		; PORTB<0>='1' button is OFF
	RETLW 1		; PORTB<0>='0' button is ON
	
CLED
	MOVLW LED       	; W=0x80
	XORWF LED_STA,1 	; LED_STA=LED_STA xor 0x80
	MOVF LED_STA,0
	MOVWF PORTB
	RETURN
	
END