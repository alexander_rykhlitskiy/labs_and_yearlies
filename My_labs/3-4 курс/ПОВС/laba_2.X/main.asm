LIST        p=16F84
#include <p16f84.inc>

org 0x2100
    de  0x01
    de  0x02
    de  0x03
    de  0x04
    de  0x0b

org 0x00

cblock  0x10
    INITIAL_VALUE
    CHECK_BIT
    CHANGEABLE_INITIAL_VALUE
    WORDS_COUNTER
endc
GOTO MAIN

EEPROM_READ
;address should be passed in Wreg
    BCF     STATUS, RP0 ;EEADR is in bank 0
    ;MOVF    0x0C, W ;if you want to pass address in 0x0C
    MOVWF   EEADR
    BSF     STATUS, RP0 ;EECON1 is in bank 1
    BSF     EECON1, RD
    BCF     STATUS, RP0 ;EEDATA is in bank 0
    MOVF    EEDATA, W
RETURN
EEPROM_WRITE
;address should be passed in Wreg
;writable value should be passed in 0x0C
    BCF     STATUS, RP0 ;EEDATA & EEADR are in bank 0
    ;MOVF    0x0C, W ;if you want to pass address in 0x0C
    MOVWF   EEADR
    MOVF    0x0C, W
    MOVWF   EEDATA

    BSF     STATUS, RP0 ;EECON1/2 are in bank 1
    BCF     INTCON, GIE ;disable global interrupt (not so necessarily)
    BSF     EECON1, WREN
    MOVLW   0x55
    MOVWF   EECON2
    MOVLW   0xAA
    MOVWF   EECON2
    BSF     EECON1, WR ;write value to register by passed address
    BSF     INTCON, GIE ;enable global interrupt
RETURN
CALCULATE_CHECK_BIT
    MOVWF   CHANGEABLE_INITIAL_VALUE ;should be passed in W
    MOVLW   0x00
    MOVWF   CHECK_BIT ;set CHECK_BIT to 0
ENUMERATE_BITS:
;XOR CHECK_BIT
    BCF     STATUS, C ;rotation goes through C (Carry) (not so necessarily)
    MOVLW   0x01
    BTFSC   CHANGEABLE_INITIAL_VALUE, 0x00
    XORWF   CHECK_BIT, 0x01 ;XOR CHECK_BIT with 0x01 from w |
    ; | (if 0x00 of CHANGEABLE_INITIAL_VALUE set to 1)
    RRF     CHANGEABLE_INITIAL_VALUE, 0x01 ;shift right

;check exit conditions
    MOVLW   0x00
    BCF     STATUS, Z
    SUBWF   CHANGEABLE_INITIAL_VALUE, 0x00 ;change Z
    BTFSS   STATUS, Z ;end cycle if Z == 0 (0 - 0 == 0)
    GOTO    ENUMERATE_BITS

    MOVF    CHECK_BIT, W
RETURN

MAIN:
    MOVLW   0x05
    MOVWF   WORDS_COUNTER
GENERATE_SEQUENCES:
    DECF    WORDS_COUNTER

;read from EEPROM
    MOVF    WORDS_COUNTER, W ;load address of readable cell here
    CALL    EEPROM_READ

    MOVWF   INITIAL_VALUE ;write value from EEPROM to INITIAL_VALUE
    MOVF    INITIAL_VALUE, W ;this line is not necessary |
;    | W is set from EEPROM_READ
    CALL    CALCULATE_CHECK_BIT

;generate CHECK_SEQUENCE
    BCF     STATUS, C
    RLF     INITIAL_VALUE, 0x01 ;left shift
    ; W contents CHECK_BIT
    IORWF   INITIAL_VALUE, 0x00 ;set CHECK_SEQUENCE to W

;write to EEPROM
    MOVWF   0x0C ;load value of writable cell here
    MOVF    WORDS_COUNTER, W
    ADDLW   0x10 ;load address of writable cell here
    CALL    EEPROM_WRITE

;check exit conditions
    MOVLW   0x00
    ;?BCF     STATUS, Z
    SUBWF   WORDS_COUNTER, 0x00
    BTFSS   STATUS, Z ;end if WORDS_COUNTER == 0
    GOTO    GENERATE_SEQUENCES
    NOP
end
;?????????? ?????. ? DLX  ??? ????? ?? ????????? ???, ????? ??? ???.
;??? ? ??????? ??????????? ??? by XOR ? 1, ????????? ?????, ????? ??????? ??????.
;????? ?? ???????? ?????????? ? ?.?. ? ????? ????? ???????????? ??????: |
; | ??? ????? ???????? ?????? ? ?????????? ? ? ??????? ?????.
