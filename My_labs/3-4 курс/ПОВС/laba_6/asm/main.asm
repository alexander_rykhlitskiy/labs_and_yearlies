#include p16f84a.inc ;Include register definition file
__CONFIG   _CP_OFF & _WDT_OFF & _PWRTE_ON & _XT_OSC

; Bank0 and Bank1 definitions
#define BANK0  bcf STATUS,RP0
#define BANK1   bsf STATUS,RP0

RS equ 1
RW equ 2
EN equ 3
cblock 0x10
   vpause
   LCD_buf
   LCD_tmp
   WRITING
   COUNTER
   NUMBER_OF_GREY
   W_TEMP
   STATUS_TEMP
   TIMER_FOR_1_s
   BUT_LAST_ST
   BUT_ST_CHANGED
   WORKING
endc

; LCD instructions
constant LCD_CD=0x01 ; Clear Display   :2ms
constant LCD_CH=0x02 ; Cursor Home  :2ms
constant LCD_ON=0x0C ; Display On   :40us
constant LCD_OF=0x08 ; Display Off  :40us
constant LCD_CN=0x0E ; Cursor On :40us
constant LCD_CB=0x09 ; Cursor Blink :40us
constant LCD_2L=0x28 ; LCD has 2 lines,
constant LCD_4B=0x20 ; 4-bitinterface  :40us
constant LCD_L1=0x80 ; select 1 line   :40us
constant LCD_L2=0xC0 ; select 2 line   :40us
;########################################
;--------------------------------------------
; ~1ms delay for 4MHz
; One instruction in 1us
; Delay=2+1+1+249*4+3+2=1005
; Real delay for 1,005 ms.
org 0x00
goto MAIN
 
INT_VECTOR    CODE    0x0004  ; interrupt vector location 
   MOVWF   W_TEMP        ; save off current W register contents 
   MOVF    STATUS,w      ; move status register into W register 
   MOVWF   STATUS_TEMP   ; save off contents of STATUS register 

   MOVLW   0x9E
   MOVWF   TMR0
   BCF     INTCON, T0IF
   BTFSC   WORKING, 0x00
   CALL    COUNT_1_s_AND_DISPLAY_GREY_NUMBER

;button
   BTFSC   INTCON, RBIF
   CALL    BUTTON_WAS_PRESSED
   BCF 	  INTCON, RBIF
   BSF     PORTB, 0x00
; isr code can go here or be located as a call subroutine elsewhere 
 
   MOVF    STATUS_TEMP,w ; retrieve copy of STATUS register 
   MOVWF   STATUS        ; restore pre-isr STATUS register contents 
   SWAPF   W_TEMP,f 
   SWAPF   W_TEMP,w      ; restore pre-isr W register contents 
RETFIE                ; 

COUNT_1_s_AND_DISPLAY_GREY_NUMBER
   DECF    TIMER_FOR_1_s, f
   BTFSC   STATUS, Z
   CALL    DISPLAY_GREY_NUMBER
RETURN

BUTTON_WAS_PRESSED
   MOVLW    0x01
   XORWF    WORKING
RETURN
DISPLAY_GREY_NUMBER
   movlw    0x01
   call     LCD_cmd ; clear LCD
   MOVF     NUMBER_OF_GREY, W
   CALL     GREY_NUMBER
   CALL     SHOW_BINARY_ON_DISPLAY
   INCF     NUMBER_OF_GREY, f
   MOVLW    0x0A
   MOVWF    TIMER_FOR_1_s
RETURN

Delay_1ms

   movlw 0xFA  ; 1us, W=250
   movwf vpause   ; 1us
D0:
   nop      ; 1us
   decfsz vpause,1   ; 1us
   goto D0     ; 2us
return
;--------------------------------------------
; 2ms delay
Delay_2ms macro
   call Delay_1ms
   call Delay_1ms
endm
;--------------------------------------------
; ~40us delay for 4MHz
Delay_40us
   movlw 0x08
   movwf vpause
D1:
   nop
   decfsz vpause,1
   goto D1
return
;#######################################
;--------------------------------------------
; Write W to LCD
LCD_w_wr
   clrf PORTB  ; Clear port
   movwf PORTB ; PortB=Wreg
   call Delay_1ms ; Wait for 1ms
   bsf PORTB,EN   ; E='1'
   bcf PORTB,EN   ; E='0'
   call Delay_1ms ; Wait for 1ms
   clrf PORTB  ; Clear port
   return
;########################################
;--------------------------------------------
; Write a command to LCD
; Command is in the WReg
LCD_cmd 
   clrf LCD_buf      ; clear buffer
   movwf LCD_tmp     ; LCD_tmp=command
   andlw b'11110000'    ; WReg & 0xF0
   iorwf LCD_buf,0      ; LCD_buf=(1st half)
   call  LCD_w_wr    ; Write 1st half

   swapf LCD_tmp,0      ; swap command
   andlw b'11110000'    ; WReg & 0xF0
   iorwf LCD_buf,0      ; LCD_buf=(2nd half)
   call LCD_w_wr     ; Write 2nd half

   return

;########################################
;--------------------------------------------
; Write data to LCD
LCD_dat
   clrf LCD_buf
   bsf LCD_buf,RS
   movwf LCD_tmp
   andlw b'11110000'
   iorwf LCD_buf,0
   call LCD_w_wr

   swapf LCD_tmp,0
   andlw b'11110000'
   iorwf LCD_buf,0
   call LCD_w_wr
   return
;#######################################
;--------------------------------------------
; LCD display initialization
LCD_init
   BANK1
   clrf TRISB        ; All outputs on PORTB
   BANK0
   clrf PORTB  ; Clear PORTB

   call Delay_1ms ; Delay in 4ms after
   call Delay_1ms ; power is on
   call Delay_1ms
   call Delay_1ms
   
   movlw LCD_4B   ; 4-bit data interface
   call LCD_w_wr
   call Delay_40us


   movlw LCD_ON   ; Turn on LCD
   call LCD_cmd
   call Delay_40us

   movlw LCD_2L   ; 2 lines
   call LCD_cmd
   call Delay_40us

   movlw LCD_CD
   call LCD_cmd   ; Clear LCD
   Delay_2ms

   return
;#######################################
   ; org 0x10
SHOW_BINARY_ON_DISPLAY
   MOVWF    WRITING
   MOVLW    0x04
   MOVWF    COUNTER
DISPLAY_BIT:
   BTFSC    WRITING, 0x03
   MOVLW    0x31
   BTFSS    WRITING, 0x03
   MOVLW    0x30
   CALL     LCD_dat
   RLF      WRITING

   DECFSZ   COUNTER
   GOTO     DISPLAY_BIT
;   call Delay_40us
RETURN
GREY_NUMBER
   MOVWF    WRITING
   BCF      STATUS, C
   RRF      WRITING, W
   XORWF    WRITING, W
RETURN
TIMER_init
   CLRWDT
   BANK1
   CLRF		TRISA
   CLRF		TRISB
   BANK0
   BSF      STATUS, RP0
   BCF      OPTION_REG, PSA
   MOVLW    b'00000111'
   MOVWF    OPTION_REG

   BCF      STATUS, RP0
   CLRF     INTCON
   BSF      INTCON, T0IE
   BSF      INTCON, GIE

   MOVLW    0xF0
   MOVWF    TMR0
   MOVLW    0x0A
   MOVWF    TIMER_FOR_1_s
RETURN


MAIN:
   CALL     TIMER_init
   CALL     LCD_init
   CLRF     NUMBER_OF_GREY

;button
   BSF      PORTB, 0x00
   BSF      INTCON, RBIE
   CLRF     BUT_LAST_ST
   CLRF     BUT_ST_CHANGED
SHOW_SEQUENCE:
   BSF      PORTB, 0x00
   MOVF     BUT_LAST_ST, W
   XORWF    PORTB, W
   MOVWF 	W_TEMP
   BTFSS    W_TEMP, 0x00
   GOTO     SHOW_SEQUENCE
   
   MOVF     PORTB, W
   MOVWF    BUT_LAST_ST
   BTFSS    PORTB, 0x00
   CALL     BUTTON_WAS_PRESSED
   GOTO     SHOW_SEQUENCE
END
