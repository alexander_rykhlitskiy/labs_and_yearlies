opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 5239"

opt pagewidth 120

	opt lm

	processor	16F84A
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
;BANK0:	_main->_butState
	FNCALL	_main,_butState
	FNROOT	_main
	FNCALL	intlevel1,_ISR
	global	intlevel1
	FNROOT	intlevel1
	global	main@_LCDBLABLA
psect	nvBANK0,class=BANK0,space=1
global __pnvBANK0
__pnvBANK0:
main@_LCDBLABLA:
       ds      2

	global	main@_LCDCON
main@_LCDCON:
       ds      2

	global	main@_LCDPS
main@_LCDPS:
       ds      2

	global	main@_LCDSE
main@_LCDSE:
       ds      2

	global	_EEADR
_EEADR  equ     9
	global	_EEDATA
_EEDATA  equ     8
	global	_FSR
_FSR  equ     4
	global	_INDF
_INDF  equ     0
	global	_INTCON
_INTCON  equ     11
	global	_PCL
_PCL  equ     2
	global	_PCLATH
_PCLATH  equ     10
	global	_PORTA
_PORTA  equ     5
	global	_PORTB
_PORTB  equ     6
	global	_RTCC
_RTCC  equ     1
	global	_STATUS
_STATUS  equ     3
	global	_TMR0
_TMR0  equ     1
	global	_CARRY
_CARRY  equ     24
	global	_DC
_DC  equ     25
	global	_EEIE
_EEIE  equ     94
	global	_GIE
_GIE  equ     95
	global	_INT
_INT  equ     48
	global	_INTE
_INTE  equ     92
	global	_INTF
_INTF  equ     89
	global	_PD
_PD  equ     27
	global	_RA0
_RA0  equ     40
	global	_RA1
_RA1  equ     41
	global	_RA2
_RA2  equ     42
	global	_RA3
_RA3  equ     43
	global	_RA4
_RA4  equ     44
	global	_RB0
_RB0  equ     48
	global	_RB1
_RB1  equ     49
	global	_RB2
_RB2  equ     50
	global	_RB3
_RB3  equ     51
	global	_RB4
_RB4  equ     52
	global	_RB5
_RB5  equ     53
	global	_RB6
_RB6  equ     54
	global	_RB7
_RB7  equ     55
	global	_RBIE
_RBIE  equ     91
	global	_RBIF
_RBIF  equ     88
	global	_RP0
_RP0  equ     29
	global	_T0IE
_T0IE  equ     93
	global	_T0IF
_T0IF  equ     90
	global	_TO
_TO  equ     28
	global	_ZERO
_ZERO  equ     26
	global	_EECON1
_EECON1  equ     136
	global	_EECON2
_EECON2  equ     137
	global	_OPTION
_OPTION  equ     129
	global	_TRISA
_TRISA  equ     133
	global	_TRISB
_TRISB  equ     134
	global	_EEIF
_EEIF  equ     1092
	global	_INTEDG
_INTEDG  equ     1038
	global	_PS0
_PS0  equ     1032
	global	_PS1
_PS1  equ     1033
	global	_PS2
_PS2  equ     1034
	global	_PSA
_PSA  equ     1035
	global	_RBPU
_RBPU  equ     1039
	global	_RD
_RD  equ     1088
	global	_T0CS
_T0CS  equ     1037
	global	_T0SE
_T0SE  equ     1036
	global	_TRISA0
_TRISA0  equ     1064
	global	_TRISA1
_TRISA1  equ     1065
	global	_TRISA2
_TRISA2  equ     1066
	global	_TRISA3
_TRISA3  equ     1067
	global	_TRISA4
_TRISA4  equ     1068
	global	_TRISB0
_TRISB0  equ     1072
	global	_TRISB1
_TRISB1  equ     1073
	global	_TRISB2
_TRISB2  equ     1074
	global	_TRISB3
_TRISB3  equ     1075
	global	_TRISB4
_TRISB4  equ     1076
	global	_TRISB5
_TRISB5  equ     1077
	global	_TRISB6
_TRISB6  equ     1078
	global	_TRISB7
_TRISB7  equ     1079
	global	_WR
_WR  equ     1089
	global	_WREN
_WREN  equ     1090
	global	_WRERR
_WRERR  equ     1091
	file	"laba_6.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initationation code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	??_butState
??_butState: ;@ 0x0
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	butState@result
butState@result:	; 2 bytes @ 0x0
	ds	2
	global	?_butState
?_butState: ;@ 0x2
	ds	2
	global	??_main
??_main: ;@ 0x4
	ds	1
	global	main@stop
main@stop:	; 2 bytes @ 0x5
	ds	2
	global	main@butLastState
main@butLastState:	; 2 bytes @ 0x7
	ds	2
	global	main@j
main@j:	; 2 bytes @ 0x9
	ds	2
	global	main@i
main@i:	; 2 bytes @ 0xB
	ds	2
	global	main@state
main@state:	; 2 bytes @ 0xD
	ds	2
	global	??_ISR
??_ISR: ;@ 0xF
	global	?_main
?_main: ;@ 0xF
	ds	4
	global	ISR@a
ISR@a:	; 2 bytes @ 0x13
	ds	2
	global	?_ISR
?_ISR: ;@ 0x15
;Data sizes: Strings 0, constant 0, data 0, bss 0, persistent 8 stack 0
;Auto spaces:   Size  Autos    Used
; COMMON           0      0       0
; BANK0           66     21      29


;Pointer list with targets:

;?_butState	int  size(1); Largest target is 0


;Main: autosize = 0, tempsize = 1, incstack = 0, save=0


;Call graph:                      Base Space Used Autos Args Refs Density
;_main                                               11    0  142   0.00
;                                    4 BANK0   11
;           _butState
;  _butState                                          2    2   19   0.00
;                                    0 BANK0    4
; Estimated maximum call depth 1
;_ISR                                                 6    0    0   0.00
;                                   15 BANK0    6
; Estimated maximum call depth 0
; Address spaces:

;Name               Size   Autos  Total    Cost      Usage
;BITCOMMON            0      0       0       0        0.0%
;NULL                 0      0       0       0        0.0%
;CODE                 0      0       0       0        0.0%
;COMMON               0      0       0       1        0.0%
;SFR0                 0      0       0       1        0.0%
;BITSFR0              0      0       0       1        0.0%
;SFR1                 0      0       0       2        0.0%
;BITSFR1              0      0       0       2        0.0%
;STACK                0      0       0       2        0.0%
;BITBANK0            42      0       0       3        0.0%
;BANK0               42     15      1D       4       43.9%
;ABS                  0      0      1D       5        0.0%
;DATA                 0      0      1D       6        0.0%
;EEDATA              40      0       0    1000        0.0%

	global	_main
psect	maintext,local,class=CODE,delta=2
global __pmaintext
__pmaintext:

; *************** function _main *****************
; Defined at:
;		line 69 in file "D:\Univer\Labs\laba_6\main.c"
; Parameters:    Size  Location     Type
;		None
; Auto vars:     Size  Location     Type
;  state           2   13[BANK0 ] int 
;  j               2    9[BANK0 ] int 
;  i               2   11[BANK0 ] int 
;  stop            2    5[BANK0 ] int 
;  butLastState    2    7[BANK0 ] int 
; Return value:  Size  Location     Type
;                  2   15[BANK0 ] int 
; Registers used:
;		wreg, status,2, status,0, pclath, cstack
; Tracked objects:
;		On entry : 17F/0
;		On exit  : 0/0
;		Unchanged: 0/0
; Data sizes:     COMMON   BANK0
;      Locals:         0      11
;      Temp:     1
;      Total:   11
; This function calls:
;		_butState
; This function is called by:
;		Startup code after reset
; This function uses a non-reentrant model
; 
psect	maintext
	file	"D:\Univer\Labs\laba_6\main.c"
	line	69
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
;main.c: 69: main(){
	
_main:	
	opt stack 7
; Regs used in _main: [wreg+status,2+status,0+pclath+cstack]
	line	70
	
l30000208:	
;main.c: 70: TRISB=0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	bsf	status, 5	;RP0=1, select bank1
	movwf	(134)^080h	;volatile
	line	71
;main.c: 71: TRISA = 0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(133)^080h	;volatile
	
l30000209:	
	line	74
;main.c: 74: int butLastState = 1;
	movlw	low(01h)
	bcf	status, 5	;RP0=0, select bank0
	movwf	(main@butLastState)
	movlw	high(01h)
	movwf	((main@butLastState))+1
	
l30000210:	
	line	75
;main.c: 75: int stop = 0;
	movlw	low(0)
	movwf	(main@stop)
	movlw	high(0)
	movwf	((main@stop))+1
	
l30000211:	
	
l30000212:	
	
l30000213:	
	line	78
;main.c: 78: LCDCON = 0x0F;
	movlw	low(0Fh)
	movwf	(main@_LCDCON)
	movlw	high(0Fh)
	movwf	((main@_LCDCON))+1
	
l30000214:	
	line	79
;main.c: 79: LCDSE = 0x55;
	movlw	low(055h)
	movwf	(main@_LCDSE)
	movlw	high(055h)
	movwf	((main@_LCDSE))+1
	
l30000215:	
	line	80
;main.c: 80: LCDPS = 0xF5;
	movlw	low(0F5h)
	movwf	(main@_LCDPS)
	movlw	high(0F5h)
	movwf	((main@_LCDPS))+1
	
l30000216:	
	line	81
;main.c: 81: LCDBLABLA = 0x1F;
	movlw	low(01Fh)
	movwf	(main@_LCDBLABLA)
	movlw	high(01Fh)
	movwf	((main@_LCDBLABLA))+1
	
l30000217:	
	line	83
;main.c: 83: i = 0;
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	movwf	(main@i)
	movlw	high(0)
	movwf	((main@i))+1
	goto	l38
	
l30000218:	
	line	85
;main.c: 85: j = 0;
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	movwf	(main@j)
	movlw	high(0)
	movwf	((main@j))+1
	goto	l30000225
	
l42:	
	line	87
# 87 "D:\Univer\Labs\laba_6\main.c"
CLRWDT ;#
psect	maintext
	
l30000219:	
	line	88
;main.c: 88: int state = butState();
	fcall	_butState
	bcf	status, 7	;select IRP bank0
	bcf	status, 5	;RP0=0, select bank0
	movf	(1+(?_butState)),w
	clrf	(main@state+1)
	addwf	(main@state+1)
	movf	(0+(?_butState)),w
	clrf	(main@state)
	addwf	(main@state)

	
l30000220:	
	line	89
;main.c: 89: if ((state != butLastState) & !state)
	movf	(main@butLastState+1),w
	xorwf	(main@state+1),w
	skipz
	goto	u105
	movf	(main@butLastState),w
	xorwf	(main@state),w
u105:

	skipnz
	goto	u101
	goto	u100
u101:
	goto	l30000223
u100:
	
l30000221:	
	movf	((main@state+1)),w
	iorwf	((main@state)),w
	skipz
	goto	u111
	goto	u110
u111:
	goto	l30000223
u110:
	
l30000222:	
	line	90
;main.c: 90: stop ^= 1;
	movlw	low(01h)
	xorwf	(main@stop),f
	movlw	high(01h)
	xorwf	(main@stop+1),f
	
l30000223:	
	line	91
;main.c: 91: butLastState = state;
	movf	(main@state+1),w
	clrf	(main@butLastState+1)
	addwf	(main@butLastState+1)
	movf	(main@state),w
	clrf	(main@butLastState)
	addwf	(main@butLastState)

	
l30000224:	
	line	95
;main.c: 95: j++;
	movlw	low(01h)
	addwf	(main@j),f
	skipnc
	incf	(main@j+1),f
	movlw	high(01h)
	addwf	(main@j+1),f
	
l30000225:	
	line	86
	movf	(main@j+1),w
	xorlw	80h
	movwf	(??_main+0+0)
	movlw	(high(032h))^80h
	subwf	(??_main+0+0),w
	skipz
	goto	u125
	movlw	low(032h)
	subwf	(main@j),w
u125:

	skipc
	goto	u121
	goto	u120
u121:
	goto	l42
u120:
	
l30000226:	
	line	97
;main.c: 96: }
;main.c: 97: if (!stop)
	bcf	status, 5	;RP0=0, select bank0
	movf	((main@stop+1)),w
	iorwf	((main@stop)),w
	skipz
	goto	u131
	goto	u130
u131:
	goto	l38
u130:
	
l30000227:	
	line	98
;main.c: 98: i++;
	movlw	low(01h)
	addwf	(main@i),f
	skipnc
	incf	(main@i+1),f
	movlw	high(01h)
	addwf	(main@i+1),f
	
l38:	
	line	84
	movf	(main@i+1),w
	xorlw	80h
	movwf	(??_main+0+0)
	movlw	(high(010h))^80h
	subwf	(??_main+0+0),w
	skipz
	goto	u145
	movlw	low(010h)
	subwf	(main@i),w
u145:

	skipc
	goto	u141
	goto	u140
u141:
	goto	l30000218
u140:
	goto	l30000217
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
; =============== function _main ends ============

psect	maintext
	line	101
	signat	_main,90
	global	_butState
psect	text17,local,class=CODE,delta=2
global __ptext17
__ptext17:

; *************** function _butState *****************
; Defined at:
;		line 44 in file "D:\Univer\Labs\laba_6\main.c"
; Parameters:    Size  Location     Type
;		None
; Auto vars:     Size  Location     Type
;  result          2    0[BANK0 ] int 
; Return value:  Size  Location     Type
;                  2    2[BANK0 ] int 
; Registers used:
;		wreg, status,2, status,0
; Tracked objects:
;		On entry : 0/0
;		On exit  : 0/0
;		Unchanged: 0/0
; Data sizes:     COMMON   BANK0
;      Locals:         0       4
;      Temp:     0
;      Total:    4
; This function calls:
;		Nothing
; This function is called by:
;		_main
; This function uses a non-reentrant model
; 
psect	text17
	file	"D:\Univer\Labs\laba_6\main.c"
	line	44
	global	__size_of_butState
	__size_of_butState	equ	__end_of_butState-_butState
;main.c: 44: int butState(){
	
_butState:	
	opt stack 6
; Regs used in _butState: [wreg+status,2+status,0]
	
l30000201:	
	
l30000202:	
	line	46
;main.c: 46: PORTB |= 1;
	bcf	status, 5	;RP0=0, select bank0
	bsf	(6)+(0/8),(0)&7	;volatile
	
l30000203:	
	line	47
;main.c: 47: if (PORTB & 1)
	btfss	(6),(0)&7	;volatile
	goto	u91
	goto	u90
u91:
	goto	l30000205
u90:
	
l30000204:	
	line	48
;main.c: 48: result = 0;
	movlw	low(0)
	movwf	(butState@result)
	movlw	high(0)
	movwf	((butState@result))+1
	goto	l30000206
	
l30000205:	
	line	50
;main.c: 49: else
;main.c: 50: result = 1;
	movlw	low(01h)
	movwf	(butState@result)
	movlw	high(01h)
	movwf	((butState@result))+1
	
l30000206:	
	line	51
;main.c: 51: return result;
	movf	(butState@result+1),w
	clrf	(?_butState+1)
	addwf	(?_butState+1)
	movf	(butState@result),w
	clrf	(?_butState)
	addwf	(?_butState)

	
l28:	
	return
	opt stack 0
GLOBAL	__end_of_butState
	__end_of_butState:
; =============== function _butState ends ============

psect	text18,local,class=CODE,delta=2
global __ptext18
__ptext18:
	line	52
	signat	_butState,90
	global	_ISR

; *************** function _ISR *****************
; Defined at:
;		line 53 in file "D:\Univer\Labs\laba_6\main.c"
; Parameters:    Size  Location     Type
;		None
; Auto vars:     Size  Location     Type
;  a               2   19[BANK0 ] int 
; Return value:  Size  Location     Type
;		None               void
; Registers used:
;		wreg
; Tracked objects:
;		On entry : 0/0
;		On exit  : 0/0
;		Unchanged: 0/0
; Data sizes:     COMMON   BANK0
;      Locals:         0       6
;      Temp:     4
;      Total:    6
; This function calls:
;		Nothing
; This function is called by:
;		Interrupt level 1
; This function uses a non-reentrant model
; 
psect	text18
	file	"D:\Univer\Labs\laba_6\main.c"
	line	53
	global	__size_of_ISR
	__size_of_ISR	equ	__end_of_ISR-_ISR
;main.c: 53: void interrupt ISR(){
	
_ISR:	
	opt stack 7
; Regs used in _ISR: [wreg]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+1
	movwf	saved_w
	movf	status,w
	bcf	status, 5	;RP0=0, select bank0
	movwf	(??_ISR+0)
	movf	fsr0,w
	movwf	(??_ISR+1)
	movf	pclath,w
	movwf	(??_ISR+2)
	movf	btemp+0,w
	movwf	(??_ISR+3)
	ljmp	_ISR
psect	text18
	line	54
	
i1l30000177:	
;main.c: 54: if(T0IE && T0IF){
	btfss	(93/8),(93)&7
	goto	u2_21
	goto	u2_20
u2_21:
	goto	i1l31
u2_20:
	
i1l30000178:	
	btfss	(90/8),(90)&7
	goto	u3_21
	goto	u3_20
u3_21:
	goto	i1l31
u3_20:
	
i1l30000179:	
	line	55
;main.c: 55: T0IF = 0;
	bcf	(90/8),(90)&7
	
i1l30000180:	
	line	56
;main.c: 56: int a = 0;
	movlw	low(0)
	movwf	(ISR@a)
	movlw	high(0)
	movwf	((ISR@a))+1
	line	57
;main.c: 57: a = 8;
	movlw	low(08h)
	movwf	(ISR@a)
	movlw	high(08h)
	movwf	((ISR@a))+1
	
i1l31:	
	movf	(??_ISR+3),w
	movwf	btemp+0
	movf	(??_ISR+2),w
	movwf	pclath
	movf	(??_ISR+1),w
	movwf	fsr0
	movf	(??_ISR+0),w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_ISR
	__end_of_ISR:
; =============== function _ISR ends ============

psect	text19,local,class=CODE,delta=2
global __ptext19
__ptext19:
	line	59
	signat	_ISR,88
	global	btemp
	btemp set 04Eh

	DABS	1,78,2	;btemp
	end
