#include <htc.h>
#include <pic.h>

void dirtyHack(int timer){
	for(int i = 0; i < timer; i++)
		for(int j = 0; j < timer; j++);
}
int getValueToWriteOnDisplay(int number){
	switch(number){
	case 0:
		return 0b01111110;
	case 1:
		return 0b00001100;
	case 2:
		return 0b10110110;
	case 3:
		return 0b10011110;
	case 4:
		return 0b11001100;
	case 5:
		return 0b11011010;
	case 6:
		return 0b11111010;
	case 7:
		return 0b00001110;
	case 8:
		return 0b11111110;
	case 9:
		return 0b11011111;
	}
	return 0;
}
void writeBinaryOnDisplay(int number){
	for(int i = 0; i < 4; i++) {
		PORTA = (8 >> i) ^ 0b1111;
		PORTB = getValueToWriteOnDisplay(number & 1);
		number >>= 1;
		dirtyHack(4);
	}
}
int greyNumber(int number){
	return number ^ (number >> 1);
}
int butState(){
	int result = 0;
	PORTB |= 1;
	if (PORTB & 1)
		result = 0;
	else
		result = 1;
	return result;
}
void interrupt ISR(){
	if(T0IE && T0IF){
		T0IF = 0;
		int a = 0;
		a = 8;
    }
}
void writeContentToLCD(int content){
    PORTB = 0;
    PORTB = content;
    dirtyHack(100);
    PORTB ^= 0b00001000;
    PORTB ^= 0b00001000;
    dirtyHack(100);
    PORTB = 0;
}
main(){
    TRISB=0;
    TRISA = 0;
    //0 - off/false
    //1 - on/true
    int butLastState = 1;
    int stop = 0;
	int i = 0;
	int j = 0;
	LCDCON = 0x0F;
	LCDSE = 0x55;
	LCDPS = 0xF5;
	LCDBLABLA = 0x1F;
    while(1){
		i = 0;
		while(i < 16){
			j = 0;
			while(j < 50){
				asm("CLRWDT");
				int state = butState();
				if ((state != butLastState) & !state)
					stop ^= 1;
				butLastState = state;


				// writeBinaryOnDisplay(greyNumber(i));
				j++;
			}
	    	if (!stop)
				i++;
		}
    }
}