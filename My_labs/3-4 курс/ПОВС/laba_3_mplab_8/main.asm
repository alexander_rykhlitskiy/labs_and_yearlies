#include p16f84a.inc                ; Include register definition file
__CONFIG   _CP_OFF & _WDT_OFF & _PWRTE_ON & _XT_OSC

org     0x00
cblock  0x10
    COUNTER
	CONDB
endc
;RST   code  0x0
goto  Start

;====================================================================
; CODE SEGMENT
;====================================================================

;PGM   code
DELAY
   	MOVLW	0xFF
   	MOVWF	0x0C
   	MOVWF	0x0D
   	OUT_TIMER:
      	IN_TIMER:
	 		DECFSZ	0x0C
		GOTO	IN_TIMER
   		DECFSZ	0x0D
   	GOTO	OUT_TIMER
   	NOP
   	NOP
   	NOP
RETURN
Start
;initialize ports
   	BCF		STATUS, RP0
   	CLRF	PORTA
   	BSF		STATUS, RP0
   	MOVLW	0x00
   	MOVWF	TRISA
   	MOVWF	TRISB

   	BCF		STATUS, RP0
;set initial values of registers
	MOVLW	0x01
	MOVWF	PORTA
	MOVLW	b'11000000'
	MOVWF	PORTB
;CLEARED(0)	- forward
;SET(1) 	- backward
;depends on register PORTB:
;	if PORTB[4] == 0
;		go_forward = true
;	if PORTB[2] == 1
;		go_forward = false -> (go_backward = true)
	MOVLW	0x00
	MOVWF	CONDB
TIMER:
   	CALL    DELAY

;set condition (go forward)
	MOVLW	0x00
	BTFSS	PORTB, 0x04
	MOVWF	CONDB
;A backward
	BCF		STATUS, C
	BTFSC	CONDB, 0x00
	RRF		PORTA
;set condition (go backward)
	MOVLW	0x01
	BTFSC	PORTB, 0x02
	MOVWF	CONDB
;A forward
	BSF     STATUS, C
	BTFSS	CONDB, 0x00
	RLF		PORTA

;shift right or left depending on condition (up)
;B backward
	BCF     STATUS, C
	BTFSC	CONDB, 0x00
	RLF		PORTB
;B forward
	BSF     STATUS, C
	BTFSS	CONDB, 0x00
	RRF		PORTB

   	GOTO	TIMER

END
