USE AdventureWorks2012;
IF EXISTS (
SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_NAME = 'OrderDetails'
)
BEGIN
DROP TABLE OrderDetails
DROP VIEW vw_OrderDetails
END
--a
CREATE TABLE OrderDetails (
SalesOrderID int NOT NULL, 
CustomerID int NOT NULL,
SubTotal money NOT NULL,
TaxAmt money NOT NULL,
Freight money NOT NULL,
TotalDue AS (isnull(([SubTotal]+[TaxAmt])+[Freight],(0))),
OrderQty int NOT NULL,
LineTotal int NOT NULL,
ReasonName nvarchar(50) NOT NULL
)

--b
ALTER TABLE OrderDetails
ADD Tax AS TaxAmt/SubTotal

--c
ALTER TABLE OrderDetails
ADD primary key (SalesOrderID, ReasonName)

--d
GO
CREATE VIEW vw_OrderDetails AS
(SELECT *
FROM OrderDetails)

--e 
INSERT INTO vw_OrderDetails
(
SalesOrderID,
CustomerID,
SubTotal,
TaxAmt,
Freight,
OrderQty,
LineTotal,
ReasonName
)
 VALUES
  (43697,21678,3578.27,286.2616,89.4568,1,3578.27,'Manufacturer')
INSERT INTO vw_OrderDetails(
SalesOrderID,
CustomerID,
SubTotal,
TaxAmt,
Freight,
OrderQty,
LineTotal,
ReasonName
) VALUES (22952,22074,553.97,44.3176,13.8493,3,553.98,'Price')
INSERT INTO vw_OrderDetails(
SalesOrderID,
CustomerID,
SubTotal,
TaxAmt,
Freight,
OrderQty,
LineTotal,
ReasonName
) VALUES (51971,16609,84.95,6.796,2.1238,2,9.98,'Price')

--f
WITH [OrderDetails] (
SalesOrderID,
CustomerID,
SubTotal,
TaxAmt,
Freight,
OrderQty,
LineTotal,
ReasonName
)
AS
(
SELECT
		soh.SalesOrderID,
		soh.CustomerID, 
		soh.SubTotal, 
		soh.TaxAmt, 
		soh.Freight, 
		SUM(sod.OrderQty) AS SumOrderQty, 
		SUM(sod.LineTotal) AS SumLineTotal, 
		sr.Name

	FROM Sales.SalesOrderHeader AS soh
	JOIN Sales.SalesOrderDetail AS sod
	ON (sod.SalesOrderID = soh.SalesOrderID)
	JOIN Sales.SalesOrderHeaderSalesReason as sohsr
	ON sohsr.SalesOrderID = sod.SalesOrderID
	JOIN Sales.SalesReason as sr
	ON (sohsr.SalesReasonID = sr.SalesReasonID)
	WHERE soh.SalesOrderID IS NOT NULL
	GROUP BY soh.SalesOrderID, soh.CustomerID, soh.SubTotal, soh.TaxAmt, soh.Freight, sr.Name	
	HAVING SUM(sod.OrderQty) > 1 AND sr.Name != 'Other'
)
MERGE vw_OrderDetails AS target
USING [OrderDetails] AS source
ON (target.SalesOrderID = source.SalesOrderID)
WHEN MATCHED AND target.Tax = 0.08
    THEN UPDATE
	SET
		target.SalesOrderID = source.SalesOrderID,
		target.CustomerID = source.CustomerID,
		target.SubTotal = source.SubTotal,
		target.TaxAmt = source.TaxAmt,
		target.Freight = source.Freight,		
		target.OrderQty = source.OrderQTY,
		target.LineTotal = source.LineTotal,
		target.ReasonName = source.ReasonName
WHEN NOT MATCHED BY target 
    THEN INSERT(
					SalesOrderID,
					CustomerID,
					SubTotal,
					TaxAmt,
					Freight,
					OrderQty,
					LineTotal,
					ReasonName
				)
 VALUES( 
		 source.SalesOrderID,
		 source.CustomerID,
		 source.SubTotal,
		 source.TaxAmt,
		 source.Freight,
		 source.OrderQTY,
		 source.LineTotal,
		 source.ReasonName
		)
WHEN NOT MATCHED BY source 
    THEN DELETE;

SELECT * FROM OrderDetails;                                      