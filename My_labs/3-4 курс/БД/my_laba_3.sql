USE [AdventureWorks2012]
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id =	object_id(N'[dbo].[Address]')
											AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
	UPDATE [dbo].[Address]
	SET [dbo].[Address].[AddressLine2] = [Person].[CountryRegion].[CountryRegionCode] + ', ' +
		+ [Person].[StateProvince].[Name] + ', ' + [Address].[City];

	ALTER TABLE [dbo].[Address]
	ADD [PersonName] [varchar(100)];

	UPDATE [dbo].[Address]
	SET [PersonName] = [Person].[Person].[FirstName] + [Person].[Person].[FirstName] 
	FROM [Person].[Person]

	DELETE w FROM [dbo].[Address] w
	--FROM [Person].[AddressType]
		INNER JOIN [Person].[BusinessEntityAddress] ON [Person].[BusinessEntityAddress].[AddressID] = [Person].[Address].[AddressID]
		INNER JOIN [Person].[AddressType] ON [Person].[AddressType].[AddressTypeID] = [Person].[BusinessEntityAddress].[AddressTypeID]
	WHERE [Person].[AddressType].[Name] = 'Main Office';

	----ALTER TABLE [dbo].[PersonPhone]
	----ADD [IDBackup] [bigint];

	----EXEC sp_executesql
	----N'UPDATE [dbo].[PersonPhone]
	----SET [IDBackup] = [ID]'

	----ALTER TABLE [dbo].[PersonPhone]
	----ALTER COLUMN [IDBackup] [int] NOT NULL;

	ALTER TABLE [dbo].[Address]
	DROP CONSTRAINT [uq_AddressID];

	ALTER TABLE [dbo].[Address]
	DROP CONSTRAINT chk_StateProvinceID;
	
	ALTER TABLE [dbo].[Address]
	DROP CONSTRAINT chk_AddressLine2;

	ALTER TABLE [dbo].[Address]
	DROP COLUMN [PersonName];

	----EXEC sp_rename '[dbo].[PersonPhone].[IDBackup]', 'ID', 'COLUMN';

	--ALTER TABLE [dbo].[PersonPhone]
	--DROP CONSTRAINT [reg_PhoneNumber];

	--ALTER TABLE [dbo].[PersonPhone]
	--DROP CONSTRAINT [def_PhoneNumber];

	DROP TABLE [dbo].[Address];
END