USE AdventureWorks2012;
IF EXISTS (
SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_NAME = 'OrderDetails'
)
BEGIN
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('OrderDetails') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE OrderDetails

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('vw_OrderDetails') AND  OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW vw_OrderDetails
END

CREATE TABLE OrderDetails (
SalesOrderID int, 
CustomerID int,
SubTotal money,
TaxAmt money,
Freight money,
TotalDue AS (isnull(([SubTotal]+[TaxAmt])+[Freight],(0))),
OrderQty int,
LineTotal int,
ReasonName nvarchar(50)
);

GO
IF OBJECT_ID ('OrderDetails.insert_trigger', 'TR') IS NOT NULL
   DROP TRIGGER OrderDetails.insert_trigger;
GO
--CREATE TRIGGER insert_trigger
--ON OrderDetails

--AFTER INSERT
--AS 
--	UPDATE OrderDetails
--	set SubTotal = i.SubTotal * 0.95, 
--		TaxAmt = i.TaxAmt * 0.95,
--		Freight = i.Freight * 0.95,
--		ReasonName = 'seriously'
--	from inserted as i
--	--inner join inserted i on i.SalesOrderID = OrderDetails.SalesOrderID
--	where i.TotalDue > 2000;
--GO

--CREATE TRIGGER update_trigger
--ON OrderDetails
--FOR UPDATE
--AS 
--	UPDATE OrderDetails
--	set OrderDetails.ReasonName = d.ReasonName
--	from deleted as d
--	where OrderDetails.ReasonName != d.ReasonName;

--GO  


--CREATE TRIGGER delete_trigger
--ON OrderDetails
--FOR DELETE
--AS 
--	SELECT suser_sname() as 'name';
--	select GETDATE() as 'date';
--	select count(*) as 'deleted rows' from deleted;
--	select count(*) as 'remaining rows' from OrderDetails;
--GO

CREATE TRIGGER multifunctional_trigger
ON OrderDetails
AFTER INSERT, UPDATE, DELETE
AS
--update
	if exists (select * from inserted) and exists (select * from deleted)
	begin
		UPDATE OrderDetails
		set OrderDetails.ReasonName = d.ReasonName
		from deleted as d
		where OrderDetails.ReasonName != d.ReasonName;
		select * from inserted;
		select * from deleted;
	end
	else
--insert
	if exists (select * from inserted)
	begin
		UPDATE OrderDetails
		set SubTotal = SubTotal * 0.95, 
			TaxAmt = TaxAmt * 0.95,
			Freight = Freight * 0.95
		where TotalDue > 2000;
		select *  from inserted;
	end
	else
--delete
	if exists (select * from deleted)
	begin
		SELECT suser_sname() as 'name';
		select GETDATE() as 'date';
		select count(*) as 'deleted rows' from deleted;
		select count(*) as 'remaining rows' from OrderDetails;
		select *  from deleted;
	end
go


insert into OrderDetails values (1, 1, 1000, 1000, 1000, 1, 1, 'insert');
		
UPDATE OrderDetails
set ReasonName = 'update'
where TotalDue > 2000;

delete from OrderDetails where SalesOrderID = 1;