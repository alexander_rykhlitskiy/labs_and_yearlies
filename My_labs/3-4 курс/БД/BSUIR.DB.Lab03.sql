USE [AdventureWorks2012]
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id =	object_id(N'[dbo].[PersonPhone]')
											AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
	UPDATE [dbo].[PersonPhone]
	SET [dbo].[PersonPhone].[PhoneNumber] = REPLACE([dbo].[PersonPhone].[PhoneNumber], '-', '');

	ALTER TABLE [dbo].[PersonPhone]
	ADD [StartDate] [date];

	EXEC sp_executesql
	N'UPDATE [dbo].[PersonPhone]
	SET [StartDate] = DATEADD(day, 1, [HumanResources].[EmployeeDepartmentHistory].[StartDate])
	FROM [HumanResources].[EmployeeDepartmentHistory]
	WHERE [dbo].[PersonPhone].[BusinessEntityID] = [HumanResources].[EmployeeDepartmentHistory].[BusinessEntityID]';

	DELETE FROM [dbo].[PersonPhone]
	FROM [HumanResources].[EmployeePayHistory]
	WHERE [dbo].[PersonPhone].[BusinessEntityID] = [HumanResources].[EmployeePayHistory].[BusinessEntityID] 
		AND [HumanResources].[EmployeePayHistory].[Rate] > 50;

	--ALTER TABLE [dbo].[PersonPhone]
	--ADD [IDBackup] [bigint];

	--EXEC sp_executesql
	--N'UPDATE [dbo].[PersonPhone]
	--SET [IDBackup] = [ID]'

	--ALTER TABLE [dbo].[PersonPhone]
	--ALTER COLUMN [IDBackup] [int] NOT NULL;

	ALTER TABLE [dbo].[PersonPhone]
	DROP CONSTRAINT [uq_ID];

	ALTER TABLE [dbo].[PersonPhone]
	DROP COLUMN [ID];

	--EXEC sp_rename '[dbo].[PersonPhone].[IDBackup]', 'ID', 'COLUMN';

	ALTER TABLE [dbo].[PersonPhone]
	DROP CONSTRAINT [reg_PhoneNumber];

	ALTER TABLE [dbo].[PersonPhone]
	DROP CONSTRAINT [def_PhoneNumber];

	DROP TABLE [dbo].[PersonPhone];
END