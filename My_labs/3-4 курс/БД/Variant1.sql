USE [AdventureWorks2012]
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('OrderDetails') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE OrderDetails

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('vw_OrderDetails') AND  OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW vw_OrderDetails

SELECT sh.SalesOrderID, sh.CustomerID, sh.SubTotal, sh.TaxAmt, sh.Freight, sh.TotalDue, 
		SUM(sd.OrderQty) as SumOrderQty, SUM(sd.LineTotal) as SumLineTotal
INTO OrderDetails
FROM Sales.SalesOrderHeader sh
JOIN Sales.SalesOrderDetail sd ON sh.SalesOrderID = sd.SalesOrderID;
SELECT sr.ReasonType as ReasonName
INTO OrderDetails
FROM Sales.SalesReason sr;