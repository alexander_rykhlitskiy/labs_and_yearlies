USE [AdventureWorks2012]
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('SalesPerson') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE SalesPerson

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('vw_SalesPerson') AND  OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW vw_SalesPerson

SELECT sp.BusinessEntityID
	,sp.TerritoryID
	,sp.SalesQuota
    ,sp.Bonus
    ,sp.CommissionPct
    ,sp.SalesYTD
    ,sp.SalesLastYear
    ,sp.rowguid
    ,sp.ModifiedDate
	,qh.SalesQuota as LastSalesQuota
	,st.CountryRegionCode
INTO SalesPerson
FROM Sales.SalesPerson as sp
JOIN Sales.SalesPersonQuotaHistory qh
ON sp.BusinessEntityID = qh.BusinessEntityID
LEFT JOIN Sales.SalesTerritory as st
ON sp.TerritoryID = st.TerritoryID
LEFT OUTER JOIN Sales.SalesPersonQuotaHistory as qh2
ON (qh.BusinessEntityID = qh2.BusinessEntityID AND qh.QuotaDate < qh2.QuotaDate)
WHERE qh2.BusinessEntityID IS NULL;
GO

ALTER TABLE SalesPerson ADD CONSTRAINT PK_SalesPerson PRIMARY KEY CLUSTERED (BusinessEntityID)

ALTER TABLE SalesPerson ADD QuotaDifference AS SalesQuota - LastSalesQuota
GO

UPDATE SalesPerson
SET Bonus = 1100.00
WHERE BusinessEntityID = 275

DELETE 
FROM SalesPerson
WHERE BusinessEntityID = 274

INSERT SalesPerson
(
	 BusinessEntityID
    ,TerritoryID
    ,SalesQuota
    ,Bonus
    ,CommissionPct
    ,SalesYTD
    ,SalesLastYear
    ,rowguid
    ,ModifiedDate
    ,LastSalesQuota
    ,CountryRegionCode
)
VALUES
(
	100
	,4
	,250000.00
	,3550.00
	,0.01
	,2458535.6169
	,2073505.9999
	,'35326DDB-7278-4FEF-B3BA-EA137B69094E'
	,'2005-06-24 00:00:00.000'
	,724000.00
	,'US'
)
GO

CREATE VIEW vw_SalesPerson AS
	SELECT
	   BusinessEntityID
      ,TerritoryID
      ,SalesQuota
      ,Bonus
      ,CommissionPct
      ,SalesYTD
      ,SalesLastYear
      ,rowguid
      ,ModifiedDate
      ,LastSalesQuota
      ,CountryRegionCode
      ,QuotaDifference
	FROM SalesPerson
	/*UNION
	SELECT 
	   275
	  ,2
	  ,300000.00
	  ,1100.00
	  ,0.012
	  ,3763178.1787
	  ,1750406.4785
	  ,'1E0A7274-3064-4F58-88EE-4C6586C87169'
	  ,'2005-06-24 00:00:00.000'
	  ,869000.00
	  ,'US'
	  ,-569000.00
	UNION
	SELECT 
	   100
	  ,4
	  ,250000.00
	  ,3550.00
	  ,0.01
	  ,2458535.6169
	  ,2073505.9999
	  ,'35326DDB-7278-4FEF-B3BA-EA137B69094E'
	  ,'2005-06-24 00:00:00.000'
	  ,724000.00
	  ,'US'
	  ,-474000.00*/
GO

MERGE vw_SalesPerson as t
USING 
(
	SELECT DISTINCT(sp.BusinessEntityID)
		,sp.TerritoryID
		,sp.SalesQuota
		,sp.Bonus
		,sp.CommissionPct
		,sp.SalesYTD
		,sp.SalesLastYear
		,sp.rowguid
		,sp.ModifiedDate
		,qh.SalesQuota as LastSalesQuota
		,st.CountryRegionCode
	FROM Sales.SalesPerson as sp
	JOIN Sales.SalesPersonQuotaHistory qh
	ON sp.BusinessEntityID = qh.BusinessEntityID
	LEFT JOIN Sales.SalesTerritory as st
	ON sp.TerritoryID = st.TerritoryID
	JOIN Sales.SalesOrderHeader as soh
	ON (sp.BusinessEntityID = soh.SalesPersonID and soh.SubTotal > 20000)
	LEFT OUTER JOIN Sales.SalesPersonQuotaHistory as qh2
	ON (qh.BusinessEntityID = qh2.BusinessEntityID AND qh.QuotaDate < qh2.QuotaDate)
	WHERE qh2.BusinessEntityID IS NULL
) as s
ON t.BusinessEntityID = s.BusinessEntityID
WHEN MATCHED AND s.SalesYTD > 3000000 THEN
	UPDATE SET 
		 t.TerritoryID = s.TerritoryID
		,t.SalesQuota = s.SalesQuota
		,t.Bonus = s.Bonus
		,t.CommissionPct = s.CommissionPct
		,t.SalesYTD = s.SalesYTD
		,t.SalesLastYear = s.SalesLastYear
		,t.rowguid = s.rowguid
		,t.ModifiedDate = s.ModifiedDate
		,t.LastSalesQuota = s.LastSalesQuota
		,t.CountryRegionCode = s.CountryRegionCode
WHEN NOT MATCHED BY TARGET THEN
	INSERT
	(		 
	   BusinessEntityID
      ,TerritoryID
      ,SalesQuota
      ,Bonus
      ,CommissionPct
      ,SalesYTD
      ,SalesLastYear
      ,rowguid
      ,ModifiedDate
      ,LastSalesQuota
      ,CountryRegionCode
	)
	VALUES
	(
		 s.BusinessEntityID
		,s.TerritoryID
		,s.SalesQuota
		,s.Bonus
		,s.CommissionPct
		,s.SalesYTD
		,s.SalesLastYear
		,s.rowguid
		,s.ModifiedDate
		,s.LastSalesQuota
		,s.CountryRegionCode
	)
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;