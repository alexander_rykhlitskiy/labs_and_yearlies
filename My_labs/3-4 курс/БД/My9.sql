use AdventureWorks2012

SELECT pp.ProductID,pp.Name,pp.StandardCost,pp.ListPrice,pp.Size,pp.Weight,pd.Title,pd.FileName,pd.DocumentSummary INTO dbo.Products
FROM Production.Product as pp JOIN  Production.ProductDocument as ppd ON pp.ProductID=ppd.ProductID
 JOIN Production.Document as pd ON ppd.DocumentNode=pd.DocumentNode AND pp.ProductID=ppd.ProductID
 JOIN Production.UnitMeasure as pu ON pp.WeightUnitMeasureCode=pu.UnitMeasureCode; 
 
ALTER TABLE Products 
ADD PriceDiff AS (ListPrice-StandardCost)/NULLIF(StandardCost,0)*100;


ALTER TABLE dbo.Products
ADD CONSTRAINT PK_ProductID PRIMARY KEY CLUSTERED (ProductID);

GO
CREATE VIEW dbo.vw_Products AS
	SELECT *
	FROM Products
GO

INSERT INTO dbo.vw_Products(ProductID,Name,StandardCost,ListPrice,Size,Weight,Title,FileName,DocumentSummary) VALUES (1,'LL',23.32,34.33,56,19.00,'Introduction','Doc','Detailed instr..');
/*
INSERT INTO dbo.vw_Products(ProductID,Name,StandardCost,ListPrice,Size,Weight,Title,FileName,DocumentSummary) VALUES (2,'LL',23.32,34.33,56,19.00,'Introduction','Doc','Detailed instr..');
INSERT INTO dbo.vw_Products(ProductID,Name,StandardCost,ListPrice,Size,Weight,Title,FileName,DocumentSummary) VALUES (3,'LL',23.32,34.33,56,19.00,'Introduction','Doc','Detailed instr..');
*/

UPDATE dbo.Products 
SET Title = 'NEW TITLE'
WHERE ProductID = 935;

SELECT * FROM dbo.vw_Products;

MERGE vw_Products as TRG
USING (SELECT (pp.ProductID),
pp.Name,
pp.StandardCost,
pp.ListPrice,
pp.Size,
pp.Weight,
pd.Title,
pd.FileName,
pd.DocumentSummary, 
p.PriceDiff
FROM Production.Product as pp  JOIN  Production.ProductDocument as ppd ON pp.ProductID=ppd.ProductID
 JOIN Production.Document as pd ON ppd.DocumentNode=pd.DocumentNode AND pp.ProductID=ppd.ProductID
 JOIN Production.UnitMeasure as pu ON pp.WeightUnitMeasureCode=pu.UnitMeasureCode JOIN 
 dbo.Products as p ON pp.ProductID=p.ProductID WHERE pp.StandardCost>0
) as SRC ON 
TRG.ProductID = SRC.ProductID
WHEN MATCHED AND SRC.PriceDiff > 100 THEN
UPDATE SET 
TRG.Name = SRC.Name,
TRG.StandardCost = SRC.StandardCost,
TRG.ListPrice = SRC.ListPrice,
TRG.Size = SRC.Size,
TRG.Weight = SRC.Weight,
TRG.Title = SRC.Title,
TRG.FileName = SRC.FileName,
TRG.DocumentSummary = SRC.DocumentSummary
WHEN NOT MATCHED BY TARGET THEN
INSERT 
(
Name,
StandardCost,
ListPrice,
Size,
Weight,
Title,
FileName,
DocumentSummary
)
VALUES
(
SRC.Name,
SRC.StandardCost,
SRC.ListPrice,
SRC.Size,
SRC.Weight,
SRC.Title,
SRC.FileName,
SRC.DocumentSummary
)
WHEN NOT MATCHED BY SOURCE THEN
DELETE;

SELECT * FROM dbo.vw_Products;