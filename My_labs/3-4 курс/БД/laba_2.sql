USE [AdventureWorks2012]
CREATE TABLE [Address](
	[AddressID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[AddressLine1] [nvarchar](60) NOT NULL,
	[AddressLine2] [nvarchar](60) NULL,
	[City] [nvarchar](30) NOT NULL,
	[StateProvinceID] [int] NOT NULL,
	[PostalCode] [nvarchar](15) NOT NULL,
	[ModifiedDate] [datetime] NOT NULL);
 ALTER TABLE [Address] ADD UNIQUE ([AddressID])
 ALTER TABLE [Address] ADD CONSTRAINT [StateProvinceID] CHECK (StateProvinceID % 2 = 1)
 ALTER TABLE [Address] ADD CONSTRAINT [AddressLine2.Def] DEFAULT 'Unknown' FOR [AddressLine2];
INSERT INTO [Address]
([AddressLine1], [AddressLine2], [City], [StateProvinceID], [PostalCode], [ModifiedDate])
SELECT [AddressLine1], [AddressLine2], [City], [Person].[Address].[StateProvinceID], [Person].[Address].[PostalCode], [Person].[Address].[ModifiedDate]
 FROM [Person].[Address]
INNER JOIN [Person].[StateProvince] ON [Person].[Address].[StateProvinceID] = [Person].[StateProvince].[StateProvinceID]
INNER JOIN [Person].[CountryRegion] ON [Person].[StateProvince].[CountryRegionCode] = [Person].[CountryRegion].[CountryRegionCode]
WHERE LEFT ([Person].[CountryRegion].Name, 1) = 'a' and [Person].[StateProvince].[StateProvinceID] % 2 = 1
ALTER TABLE [Address] ALTER COLUMN [AddresLine2] [nvarchar] NOT NULL
