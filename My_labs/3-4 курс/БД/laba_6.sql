USE AdventureWorks2012;
GO
IF OBJECT_ID (N'dbo.ufnGetOrderedProductsNumber', N'FN') IS NOT NULL
    DROP FUNCTION ufnGetOrderedProductsNumber;
GO
CREATE FUNCTION dbo.ufnGetOrderedProductsNumber(@SalesOrderId int)
RETURNS int 
AS 
-- Returns the stock level for the product.
BEGIN
    DECLARE @result int;
    SELECT @result = SUM(s.OrderQty) 
    FROM Sales.SalesOrderDetail s
	WHERE s.SalesOrderID = @SalesOrderId;
    IF (@result IS NULL) 
		SET @result = 0;
    RETURN @result;
END;
GO

IF OBJECT_ID(N'ufn_EmployeeSalary', N'IF') IS NOT NULL
    DROP FUNCTION ufn_EmployeeSalary;
GO
CREATE FUNCTION ufn_EmployeeSalary
                 ( @PayFrequency int )
RETURNS table
AS
RETURN (
        SELECT p.FirstName, p.LastName, eph.Rate, eph.PayFrequency
        FROM HumanResources.EmployeePayHistory AS eph
			JOIN Person.Person as p
			ON eph.BusinessEntityID = p.BusinessEntityID
        WHERE eph.PayFrequency = @PayFrequency
       );
GO

SELECT TOP 100000 p.BusinessEntityID, p.FirstName, p.LastName, es.FirstName, es.LastName, es.PayFrequency, es.Rate FROM Person.Person as p
	CROSS APPLY ufn_EmployeeSalary(1) as es;

SELECT TOP 100000 p.BusinessEntityID, p.FirstName, p.LastName, es.FirstName, es.LastName, es.PayFrequency, es.Rate FROM Person.Person as p
	OUTER APPLY ufn_EmployeeSalary(1) as es;