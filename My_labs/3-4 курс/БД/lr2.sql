USE [AdventureWorks2012]

CREATE TABLE [StateProvince] (
	[StateProvinceID] int IDENTITY(1,1) NOT NULL,	
	[StateProvinceCode] nchar(3) NOT NULL,
	[CountryRegionCode] nvarchar(3) NOT NULL,
	[IsOnlyStateProvinceFlag] [dbo].[Flag] NOT NULL,
	[Name] [dbo].[Name] NOT NULL,
	[TerritoryID] [int] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	CONSTRAINT [PK_StateProvinceID] PRIMARY KEY (StateProvinceID)	
)


 ALTER TABLE [StateProvince] ADD UNIQUE ([Name])

 ALTER TABLE [StateProvince] ADD CONSTRAINT [CountryRegionCode.UPPER] CHECK ([CountryRegionCode] COLLATE SQL_Latin1_General_CP1_CS_AS = UPPER([CountryRegionCode]))

 ALTER TABLE [StateProvince] ADD CONSTRAINT [ModifiedDate.Def] DEFAULT GETDATE() FOR [ModifiedDate];


INSERT INTO [StateProvince] 
	( [StateProvinceCode], [CountryRegionCode], [IsOnlyStateProvinceFlag],
	[Name], [TerritoryID], [ModifiedDate])
SELECT [StateProvinceCode], [StateProvince].[CountryRegionCode], [IsOnlyStateProvinceFlag],
	[StateProvince].[Name], [TerritoryID], [StateProvince].[ModifiedDate] FROM [Person].[StateProvince] 
INNER JOIN [Person].[CountryRegion] ON [Person].[StateProvince].[CountryRegionCode] = [Person].[CountryRegion].[CountryRegionCode]
WHERE [Person].[CountryRegion].Name = 'United States'

 ALTER TABLE [StateProvince] DROP COLUMN [IsOnlyStateProvinceFlag]
 ALTER TABLE [StatePRovince] ADD [CountryNum] INT NULL