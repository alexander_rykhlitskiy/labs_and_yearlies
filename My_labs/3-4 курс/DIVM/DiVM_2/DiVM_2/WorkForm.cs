﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace DiVM_2
{
    public partial class WorkForm : Form
    {
        public WorkForm()
        {
            InitializeComponent();
        }
        SplineTuple[] splines; // Сплайн

        // Структура, описывающая сплайн на каждом сегменте сетки
        struct SplineTuple
        {
            public double a, b, c, d, x;
        }

        // Построение сплайна
        // x - узлы сетки, должны быть упорядочены по возрастанию, кратные узлы запрещены
        // y - значения функции в узлах сетки
        // n - количество узлов сетки
        public void BuildSpline(double[] x, double[] y, int n)
        {
            // Инициализация массива сплайнов
            splines = new SplineTuple[n];
            for (int i = 0; i < n; ++i)
            {
                splines[i].x = x[i];
                splines[i].a = y[i];
            }
            splines[0].c = splines[n - 1].c = 0.0;

            // Решение СЛАУ относительно коэффициентов сплайнов c[i] методом прогонки для трехдиагональных матриц
            // Вычисление прогоночных коэффициентов - прямой ход метода прогонки
            double[] alpha = new double[n - 1];
            double[] beta = new double[n - 1];
            alpha[0] = beta[0] = 0.0;
            /**
              * n - число уравнений (строк матрицы)
              * b - диагональ, лежащая над главной (нумеруется: [0;n-2])
              * c - главная диагональ матрицы A (нумеруется: [0;n-1])
              * a - диагональ, лежащая под главной (нумеруется: [1;n-1])
              * f - правая часть (столбец)
              * splines - решение, массив splines будет содержать ответ
              */
            for (int i = 1; i < n - 1; ++i)
            {
                double h_i = x[i] - x[i - 1], h_i1 = x[i + 1] - x[i];
                double A = h_i;
                double C = 2.0 * (h_i + h_i1);
                double B = h_i1;
                double F = 6.0 * ((y[i + 1] - y[i]) / h_i1 - (y[i] - y[i - 1]) / h_i);
                double z = (A * alpha[i - 1] + C);
                alpha[i] = -B / z;
                beta[i] = (F - A * beta[i - 1]) / z;
            }

            // Нахождение решения - обратный ход метода прогонки
            for (int i = n - 2; i > 0; --i)
                splines[i].c = alpha[i] * splines[i + 1].c + beta[i];

            // Освобождение памяти, занимаемой прогоночными коэффициентами
            beta = null;
            alpha = null;

            // По известным коэффициентам c[i] находим значения b[i] и d[i]
            for (int i = n - 1; i > 0; --i)
            {
                double h_i = x[i] - x[i - 1];
                splines[i].d = (splines[i].c - splines[i - 1].c) / h_i;
                splines[i].b = h_i * (2.0 * splines[i].c + splines[i - 1].c) / 6.0 + (y[i] - y[i - 1]) / h_i;
            }
        }

        // Вычисление значения интерполированной функции в произвольной точке
        public double spline(int n, double[] x, double[] y, double q)
        {
            BuildSpline(x, y, n);
            if (splines == null)
                return double.NaN; // Если сплайны ещё не построены - возвращаем NaN

            int length = splines.Length;
            SplineTuple s;

            if (q <= splines[0].x) // Если x меньше точки сетки x[0] - пользуемся первым эл-тов массива
                s = splines[1];
            else if (q >= splines[length - 1].x) // Если x больше точки сетки x[n - 1] - пользуемся последним эл-том массива
                s = splines[length - 1];
            else // Иначе x лежит между граничными точками сетки - производим бинарный поиск нужного эл-та массива
            {
                int i = 0, j = length - 1;
                while (i + 1 < j)
                {
                    int k = i + (j - i) / 2;
                    if (q <= splines[k].x)
                        j = k;
                    else
                        i = k;
                }
                s = splines[j];
            }

            double dx = (q - s.x);
            return s.a + (s.b + (s.c / 2.0 + s.d * dx / 6.0) * dx) * dx;
        }


        const int numberOfPoints = 20;
       // double[] pointsX = { 1, 2, 3, 4, 5 },
       //          pointsY = { 2.02, 1.98, 1.67, 1.65, 1.57 };

         double[] pointsX = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 },
                  pointsY = { 2.02, 1.98, 1.67, 1.65, 1.57, 1.42, 1.37, 1.07, 0.85, 0.48, 0.35, -0.30, -0.61, -1.20, -1.39, -1.76, -2.28, -2.81, -3.57, -4.06 };
      //  double[] pointsX = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
       //          pointsY = { 2.05, 1.94, 1.92, 1.87, 1.77, 1.88, 1.71, 1.60, 1.56, 1.40 };

        private double lagrange(int n, double[] x, double[] y, double q)
        {
            int i, j;
            double L = 0, sum;
            for (i = 0; i < n; i++)
            {
                sum = 1;
                for (j = 0; j < n; j++)
                    if (i != j)
                        sum = sum*((q - x[j])/(x[i] - x[j]));
                L = L + y[i] * sum;
            }
            return L;
        }
        double finalDifference(double[] y, int n, int i)
        {
            double dy = 0;
            if (n > 0)
                dy = finalDifference(y, n - 1, i + 1) - finalDifference(y, n - 1, i);
            else dy = y[i];
            return dy;
        }
        double[,] finalDifference2(double[] y, int numberOfPoints)
        {
            double[,] arrayOfDifferences = new double[numberOfPoints, numberOfPoints];
            int i, j;
            for (i = 0; i < numberOfPoints; i++)
            {
                arrayOfDifferences[i, 0] = y[i];
                for (j = i - 1; j >= 0; j--)
                {
                    arrayOfDifferences[j, i - j] = arrayOfDifferences[j + 1, i - j - 1] - arrayOfDifferences[j, i - j - 1];
                }
            }
            return arrayOfDifferences;
        }
        double newton(int n, double[] x, double[] y, double q)
        {
            double sum = 0;
            int i, j;
            double P;
            double[,] arrayOfDifferences = new double[numberOfPoints, numberOfPoints];
            arrayOfDifferences = finalDifference2(y, numberOfPoints);

            for (i = 0; i < n; i++)
            {
                P = 1;
                for (j = 0; j < i; j++)
                {
                    P = P * (q - x[j]) / (j + 1);
                }
                sum += P * arrayOfDifferences[0, i];
               // sum += P*finalDifference(y, i, 0);
            }
            return sum;
        }
        delegate double SomeMethod(int n, double[] x, double[] y, double q);
        private void chartBySmth(SomeMethod ThisMethod)
        {
            double[] fullPointsX = new double[numberOfPoints * 2 - 1], 
                     fullPointsY = new double[numberOfPoints * 2 - 1];
            int i = 0;
            fullPointsX[0] = pointsX[0];
            fullPointsY[0] = pointsY[0];
            for (i = 1; i < numberOfPoints; i++)
            {
                fullPointsX[i * 2] = pointsX[i];
                fullPointsX[i * 2 - 1] = fullPointsX[i * 2 - 2] + (fullPointsX[i * 2] - fullPointsX[i * 2 - 2]) / 2;
                fullPointsY[i * 2] = pointsY[i];
                fullPointsY[i * 2 - 1] = ThisMethod(numberOfPoints, pointsX, pointsY, fullPointsX[i * 2 - 1]);
            }
            Chart.Series["Series1"].Points.DataBindXY(fullPointsX, fullPointsY);
        }
//вывод коэффициентов на экран (не лаба)
        double[,] koeffArr = {{1,-14,71,-154,120},
                              {1,-13,59,-107,60},
                              {1,-12,49,-78,40},
                              {1,-11,41,-61,30},
                              {1,-10,35,-50,24}};
        double[] multitudes = { 2.02/24, 1.98/(-6), 1.67/(4), 1.65/(-6), 1.57/(24) };
//*************
        private void buildChart_Click(object sender, EventArgs e)
        {
            switch (choosingMethod.SelectedIndex)
            {
                case 0:
                    chartBySmth(lagrange);
                    break;
                case 1:
                    chartBySmth(newton);
                    break;
                case 2:
                    chartBySmth(spline);
                    break;
            }
        }
        private void calculateY_Click(object sender, EventArgs e)
        {
//***************
            int i = 0, j = 0;
            for (i = 0; i < 5; i++)
                for (j = 0; j < 5; j++)
                    koeffArr[i, j] *= multitudes[i];

            for (i = 0; i < 5; i++)
                for (j = 1; j < 5; j++)
                    koeffArr[0, i] += koeffArr[j, i];
            
            numericUpDown1.Value = (decimal)koeffArr[0, 0];
            numericUpDown2.Value = (decimal)koeffArr[0, 1];
            numericUpDown3.Value = (decimal)koeffArr[0, 2];
            numericUpDown4.Value = (decimal)koeffArr[0, 3];
            numericUpDown5.Value = (decimal)koeffArr[0, 4];

            i = 0;
            double x = 4.5;
            numericForX.Value = (decimal)(x * x * x * x * koeffArr[i, 0] + x * x * x * koeffArr[i, 1] + x * x * koeffArr[i, 2] + x * koeffArr[i, 3] + koeffArr[i, 4]);
//***************
            switch (choosingMethod.SelectedIndex)
            {
                case 0:
                    numericForY.Value = (decimal)lagrange(numberOfPoints, pointsX, pointsY, (double)numericForX.Value);
                    break;
                case 1:
                    numericForY.Value = (decimal)newton(numberOfPoints, pointsX, pointsY, (double)numericForX.Value);
                    break;
                case 2:
                    numericForY.Value = (decimal)spline(numberOfPoints, pointsX, pointsY, (double)numericForX.Value);
                    break;
            }

        }

    }
}
