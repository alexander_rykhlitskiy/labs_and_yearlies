﻿namespace DiVM_2
{
    partial class WorkForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.Chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.buildChart = new System.Windows.Forms.Button();
            this.numericForX = new System.Windows.Forms.NumericUpDown();
            this.calculateY = new System.Windows.Forms.Button();
            this.numericForY = new System.Windows.Forms.NumericUpDown();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.choosingMethod = new System.Windows.Forms.ComboBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.Chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericForX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericForY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
            this.SuspendLayout();
            // 
            // Chart
            // 
            chartArea1.Name = "ChartArea1";
            this.Chart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.Chart.Legends.Add(legend1);
            this.Chart.Location = new System.Drawing.Point(121, 12);
            this.Chart.Name = "Chart";
            this.Chart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.IsVisibleInLegend = false;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.Chart.Series.Add(series1);
            this.Chart.Size = new System.Drawing.Size(542, 279);
            this.Chart.TabIndex = 0;
            this.Chart.Text = "chart1";
            // 
            // buildChart
            // 
            this.buildChart.Location = new System.Drawing.Point(12, 247);
            this.buildChart.Name = "buildChart";
            this.buildChart.Size = new System.Drawing.Size(81, 27);
            this.buildChart.TabIndex = 1;
            this.buildChart.Text = "build chart";
            this.buildChart.UseVisualStyleBackColor = true;
            this.buildChart.Click += new System.EventHandler(this.buildChart_Click);
            // 
            // numericForX
            // 
            this.numericForX.DecimalPlaces = 2;
            this.numericForX.Location = new System.Drawing.Point(12, 12);
            this.numericForX.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericForX.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numericForX.Name = "numericForX";
            this.numericForX.Size = new System.Drawing.Size(81, 20);
            this.numericForX.TabIndex = 2;
            // 
            // calculateY
            // 
            this.calculateY.Location = new System.Drawing.Point(12, 38);
            this.calculateY.Name = "calculateY";
            this.calculateY.Size = new System.Drawing.Size(81, 23);
            this.calculateY.TabIndex = 3;
            this.calculateY.Text = "calculateY";
            this.calculateY.UseVisualStyleBackColor = true;
            this.calculateY.Click += new System.EventHandler(this.calculateY_Click);
            // 
            // numericForY
            // 
            this.numericForY.DecimalPlaces = 2;
            this.numericForY.Location = new System.Drawing.Point(12, 67);
            this.numericForY.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.numericForY.Minimum = new decimal(new int[] {
            2000,
            0,
            0,
            -2147483648});
            this.numericForY.Name = "numericForY";
            this.numericForY.ReadOnly = true;
            this.numericForY.Size = new System.Drawing.Size(81, 20);
            this.numericForY.TabIndex = 6;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // choosingMethod
            // 
            this.choosingMethod.FormattingEnabled = true;
            this.choosingMethod.Items.AddRange(new object[] {
            "lagrange",
            "newton",
            "spline"});
            this.choosingMethod.Location = new System.Drawing.Point(12, 220);
            this.choosingMethod.Name = "choosingMethod";
            this.choosingMethod.Size = new System.Drawing.Size(98, 21);
            this.choosingMethod.TabIndex = 7;
            this.choosingMethod.Text = "Choose method";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.DecimalPlaces = 2;
            this.numericUpDown1.Location = new System.Drawing.Point(12, 93);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(78, 20);
            this.numericUpDown1.TabIndex = 8;
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.DecimalPlaces = 2;
            this.numericUpDown2.Location = new System.Drawing.Point(14, 119);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown2.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(78, 20);
            this.numericUpDown2.TabIndex = 9;
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.DecimalPlaces = 2;
            this.numericUpDown3.Location = new System.Drawing.Point(15, 145);
            this.numericUpDown3.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown3.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(78, 20);
            this.numericUpDown3.TabIndex = 10;
            // 
            // numericUpDown4
            // 
            this.numericUpDown4.DecimalPlaces = 2;
            this.numericUpDown4.Location = new System.Drawing.Point(15, 168);
            this.numericUpDown4.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown4.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown4.Name = "numericUpDown4";
            this.numericUpDown4.Size = new System.Drawing.Size(78, 20);
            this.numericUpDown4.TabIndex = 11;
            // 
            // numericUpDown5
            // 
            this.numericUpDown5.DecimalPlaces = 2;
            this.numericUpDown5.Location = new System.Drawing.Point(12, 194);
            this.numericUpDown5.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown5.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown5.Name = "numericUpDown5";
            this.numericUpDown5.Size = new System.Drawing.Size(78, 20);
            this.numericUpDown5.TabIndex = 12;
            // 
            // WorkForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 303);
            this.Controls.Add(this.numericUpDown5);
            this.Controls.Add(this.numericUpDown4);
            this.Controls.Add(this.numericUpDown3);
            this.Controls.Add(this.numericUpDown2);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.choosingMethod);
            this.Controls.Add(this.numericForY);
            this.Controls.Add(this.calculateY);
            this.Controls.Add(this.numericForX);
            this.Controls.Add(this.buildChart);
            this.Controls.Add(this.Chart);
            this.Name = "WorkForm";
            this.Text = "WorkForm";
            ((System.ComponentModel.ISupportInitialize)(this.Chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericForX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericForY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart Chart;
        private System.Windows.Forms.Button buildChart;
        private System.Windows.Forms.NumericUpDown numericForX;
        private System.Windows.Forms.Button calculateY;
        private System.Windows.Forms.NumericUpDown numericForY;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ComboBox choosingMethod;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.NumericUpDown numericUpDown4;
        private System.Windows.Forms.NumericUpDown numericUpDown5;




    }
}

