// DIVM.cpp : Defines the entry point for the console application.
//

#include "windows.h"
#include "stdafx.h"
#include "conio.h"
#include "stdlib.h"
#include "stdio.h"
#include  <iostream>

int divide_string(char dividing_string[], float a[])
{
	int a_number = 1, string_number = 0;
	while (dividing_string[string_number] != '\0')
	{
		a[a_number] = atof(&dividing_string[string_number]);
		a_number++;
		string_number++;
		while ((dividing_string[string_number-1] != ' ') && (dividing_string[string_number] != '\0'))
			string_number++;
	}
	return 0;
}
int output_array(float a[10][11], int dimension)
{
	int i, j;
	for (i = 1; i <= dimension; i++)
	{
		for (j = 1; j <= dimension + 1; j++)
			printf("%0.4f  ", a[i][j]);
		printf("\n");
	}
	return 0;
}
int input_Data(float a[10][11])
{
	int dimension;
	int i, j;
	char input_string[30]; 
	puts("enter dimension");
	scanf("%d", &dimension);
	puts("enter array");
	for(i = 1; i <= dimension; i++)
	{
		flushall();
		gets(input_string);
		divide_string(input_string, a[i]);
	}
//	output_array(a, dimension);
	return dimension;
}
int transformation(float a[10][11], int dimension)
{
	int i, j, k, number;
	float coefficient;
	for (i = 1; i <= dimension; i++)
	{
		j = i;
		k = i;
		if (a[j][i] == 0)
		{
			while (a[j][k] == 0)
			{
				j++;
				if (j > dimension)
				{
					puts("��� ���� � �������");
					break;
				}
			}
			if (a[j][k] != 0)
				for (k = 1; k <= dimension + 1; k++)
				{
					number = a[i][k];
					a[i][k] = a[j][k];
					a[j][k] = number;
				}
		}
		for (j = 1; j < i; j++)
		{
			coefficient = (a[i][j]/a[j][j]);
			for (k = 1; k <= dimension + 1; k++)
				a[i][k] -= (a[j][k]*coefficient);
output_array(a, dimension);
puts("");
		}

	}
//	output_array(a, dimension);
	return 0;
}
int exit(char exit_string[])
{
	puts(exit_string);
	flushall();
	getchar();
	exit(1);
}
int finding_x(float a[10][11], float x[10], int dimension)
{
	int i, j;
	for (i = dimension; i >= 1; i--)
	{
		for (j = dimension; j > i; j--)
			a[i][dimension + 1] -= (a[i][j]*x[j]);
		x[i] = a[i][dimension + 1]/a[i][i];
	}
	return 0;
}
int _tmain(int argc, _TCHAR* argv[])
{
	int dimension;
	float a[10][11] =   {{0,0,0,0,0},
						{0,2.18,2.44,2.49,-4.34},
						{0,2.17,2.31,2.49,-3.91},
						{0,3.15,3.22,3.17,-5.27}};
				//		{{0,0,0,0,0,0},
				//		{0,1,1,1,6},
				//		{0,1,0,-1,-2},
				//		{0,2,-2,1,1},
				//		{0,1,2,1,8}};
					//	{0,5,6,3,2},
					//	{0,2,3,1,1}};
	dimension = 3;
	int i, j, k, var_case;
	float x[10] = {0.0};
	puts("1 - Gauss method");
	puts("2 - Choletskiy method");
	puts("3 - Zaidel method");
	scanf("%d", &var_case);
//	dimension = input_Data(a);
	switch (var_case)
	{
		case 1:
			{
				transformation(a, dimension);
			/*	for (i = 1; i <= dimension; i++)
				{
					for (j = 1; j <= dimension+1; j++)
						printf("%f ", a[i][j]);
					printf("\n");
				}*/
				for (i = 1; i <= dimension; i++)
					if (a[dimension][i] != 0)
						break;
				if (i > dimension)
					if (a[dimension][i] != 0) 
						{puts("No solutions");
						flushall();
						getchar();
						return 1;
				}
					else
					exit("Infinity quantity of solutions");
				finding_x(a, x, dimension);
			}; break;
		case 2:
			{
				float l[10][11] = {}, lT[10][11] = {}, y[10], sum;
				for (j = 1; j <= dimension; j++)
				{
					i = j;
					sum = 0.0;
					for (k = 1; k <= i-1; k++)
						sum += l[i][k]*l[i][k];
					sum = a[i][i] - sum;
					if (sum < 0)
						exit("Matrix is not positive definite");
					l[i][i] = sqrt(sum);
					for (i = j; i <= dimension; i++)
					{
						sum = 0.0;
						for (k = 1; k <= j-1; k++)
							sum += l[i][k]*l[j][k];
						l[i][j] = (a[i][j] - sum)/l[j][j];
					}
				}
				for (i = 1; i <= dimension; i++)
					l[i][dimension + 1] = a[i][dimension + 1];
				for (i = 1; i <= dimension; i++)
					for (j = 1; j <= dimension; j++)
						lT[i][j] = l[j][i];
				transformation(l, dimension);
				finding_x(l, y, dimension);
				for (i = 1; i <= dimension; i++)
					lT[i][dimension + 1] = y[i];
				transformation(lT, dimension);
				finding_x(lT, x, dimension);
			}; break;
		case 3:
			{
				float temp, e = 0.000001, sum, x_old[10] = {0.0};
				int counter = 0;
				for (i = 1; i <= dimension; i++)
				{
					sum = 0;
					for (k = 1; k <= dimension; k++)
						sum = sum + fabs(a[i][k]);
					if (a[i][i] <= sum - a[i][i])
								{puts("cycling");
								flushall();
								getchar();
								return 1;}
					sum = 0;
					for (k = 1; k <= dimension; k++)
						sum = sum + fabs(a[k][i]);
					if (a[i][i] <= sum - a[i][i])
								{puts("cycling");
								flushall();
								getchar();
								return 1;}

				}
				sum = 0;
				do
				{
					counter++;
					temp = 0.0;
					for (i = 1; i <= dimension; i++)
					{
						x_old[i] = x[i];
						sum = 0;
						for (k = 1; k <= dimension; k++)
							if (k != i) sum += a[i][k]*x[k];
						x[i] = (a[i][dimension + 1] - sum)/a[i][i];
						temp += fabs(x_old[i] - x[i]);
					}
				}
				while ((temp > e) && (counter < 50));
		
			}; break;
	}

	for (i = 1; i <= dimension; i++)
		printf("x%d = %.2f\n", i, x[i]);
	flushall();
	getchar();
	return 0;
}
