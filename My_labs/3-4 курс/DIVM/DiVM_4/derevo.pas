program saod5;
uses crt;
type
  adres = ^zveno;
  tekst = integer;
  adrT = ^Tekst;
  zveno = record
            element : ^tekst;
            right, left : adres;
            bLeft, bRight : boolean;
            kl : integer
          end;
var
  a, x, y, i : integer;
  D, QQ, head : adres;
  kol, koler : integer;
  bool : boolean;

function search(kl : integer; D : adres; var rez : adres) : boolean;
var
  q, p : adres;
  b : boolean;
begin
  q := D;
  b := false;
  p := D;
  while (not(b)) and (q <> nil) do
    begin
      p := q;
      if kl = q^.kl
        then b := true
        else if kl < q^.kl
               then q := q^.left
               else q := q^.right
    end;
  rez := p;
  search := b;
end;

function searchToDelete(kl : integer; D : adres; var parent : adres; var son : adres) : boolean;
var
  q, p, qq : adres;
  b : boolean;
begin
  q := D;
  b := false;
  p := D;
  while (not(b)) and (q <> nil) do
    begin
      qq := p;
      p := q;
      if kl = q^.kl
        then b := true
        else if kl < q^.kl
               then q := q^.left
               else q := q^.right;
    end;
  parent := qq;
  son := p;
  searchToDelete := b;
end;

procedure delete(var D : adres; kl : integer);
var
  son, parent, qq, q : adres;
begin
  if (not searchToDelete(kl, D, parent, son))
    then writeln('��������� �������� �� ������')
  else
    begin
      q := son;
          if (q^.left = nil)
            then q := q^.right
            else
              if (q^.right = nil)
                then q := q^.left
                else
                   begin
                     q := q^.left;
                     qq := q;
                     while (q^.right <> nil) do
                       begin
                         qq := q;
                         q := q^.right;
                       end;
                     qq^.right := q^.left;
                     q^.right := son^.right;
                     if (son^.left <> q) then
                        begin
                             q^.left := son^.left;
                             q^.right := son^.right;
                        end;
                   end;
          if (son = D) then
              D := q
          else
          if (kl < parent^.kl)
            then parent^.left := q
            else parent^.right := q;
    end;

end;
procedure dobavl(var D : adres; kl : integer; elem : tekst);
var
  q, p : adres;
  T : adrT;
begin
  if not(search(kl, D, q))
  
    then
      begin
        new(p);
        p^.kl := kl;
        new(T);
        T^ := elem;
        p^.element := T;
        if D = nil
          then D := p
          else if kl < q^.kl then q^.left := p
                             else q^.right := p;
        p^.left := nil;
        p^.right := nil;
        p^.bLeft := true;
        p^.bRight := true;
      end
    else writeln('������� � ������ ',kl,' ��� ���� � ������')
end;

function step(k : integer) : integer;
var
  i, b : integer;
begin
  b := 1;
  for i := 1 to k do
    b := b * 2;
  step := b;
end;
procedure vivod(d1 : adres; k : integer; x2 : integer);
var
  q : adres;
  x1, a, i : integer;
begin
  if (k = 1)
    then y := k else
     y := 4 * k - 3;
  gotoXY(x2, y);
  write(d1^.kl);
  a := d1^.kl;
  i := 0;
  while a >= 10 do
    begin
      a := a div 10;
      i := i + 1
    end;
  if d1^.left <> nil
    then begin
           gotoXY(x2 - 1, y + 1);
           write('/')
         end;
  if (d1^.right <> nil) and (d1^.bright = true)
    then begin
           gotoXY(x2 + i + 1, y + 1);
           write('\')
         end;
         
  if d1^.left <> nil
    then begin
           x1 := x2 - round(x/step(k));
           vivod(d1^.left, k + 1, x1);
         end;
  if (d1^.right <> nil) and (d1^.bright = true)
    then begin
           x1 := x2 + round(x/step(k));
           vivod(d1^.right, k + 1, x1);
         end;
end;

procedure obhodPryamoy(D : adres);
begin
  textColor(green);
  write(D^.element^, '  ');
  textColor(black);
  if D^.left = nil
    then write(0, '  ');
  if D^.left <> nil
    then obhodPryamoy(d^.left);
  write(D^.element^, '  ');
  if (D^.right = nil) or (D^.bright = false)
    then write(0, '  ');
  if (D^.right <> nil) and (D^.bright = true)
    then obhodPryamoy(d^.right);
  write(D^.element^, '  ');
end;

procedure obhodSimmetr(D : adres);
begin
  write(D^.element^, '  ');
  if D^.left = nil
    then write(0, '  ');
  if D^.left <> nil
    then obhodSimmetr(d^.left);
  textColor(green);
  write(D^.element^, '  ');
  textColor(black);
  if (D^.right = nil) or (D^.bright = false)
    then write(0, '  ');
  if (D^.right <> nil) and (D^.bright = true)
    then obhodSimmetr(d^.right);
  write(D^.element^, '  ');
end;


procedure obhodObr(D : adres);
begin
  write(D^.element^, '  ');
  if D^.left = nil
    then write(0, '  ');
  if D^.left <> nil
    then obhodObr(d^.left);
  write(D^.element^, '  ');
  if (D^.right = nil) or (D^.bright = false)
    then write(0, '  ');
  if (D^.right <> nil) and (D^.bright = true)
    then obhodObr(d^.right);
  textColor(green);
  write(D^.element^, '  ');
  textColor(black);
end;

procedure proshivkaSimmetr(D : adres);
begin
  if D^.left <> nil
    then proshivkaSimmetr(d^.left);
  if bool
    then begin
           qq^.right := D;
           textColor(koler);
           write(D^.element^, '  ');
           bool := false;
         end;
  if D^.right = nil
    then begin
           D^.bright := false;
           koler := not(koler) + 4;
           textcolor(koler);
           write(D^.element^, '  ');
           qq := D;
           bool := true;
         end;
  if D^.right <> nil
    then proshivkaSimmetr(d^.right);
end;

begin
  a := 8; dobavl(D, a, a);
  a := 6; dobavl(D, a, a);
  a := 1; dobavl(D, a, a);
  a := 7; dobavl(D, a, a);
  a := 5; dobavl(D, a, a);
  a := 3; dobavl(D, a, a);
  a := 4; dobavl(D, a, a);
  a := 12; dobavl(D, a, a);
  a := 9; dobavl(D, a, a);
  a := 10; dobavl(D, a, a);
  a := 16; dobavl(D, a, a);
  a := 18; dobavl(D, a, a);
  repeat
  writeln('1 - ���������� ������ �� ������');
  writeln('2 - �������� �������');
  writeln('3 - ������� ������� �� ������');
  writeln('4 - �������� ������� � ������');
  writeln('5 - �����');
  readln(i);
  clrscr;
  koler := 1;
  bool := false;
  case i of
    1 : begin
          x := 40;
          y := 0;
          gotoXY(x, y);
          vivod(d, 1, x);
        end;
    2 : begin
          writeln('������ �����:');
          obhodPryamoy(D);
          writeln;
          writeln('������������ �����:');
          obhodSimmetr(D);
          writeln();
          writeln('��������� �����:');
          obhodObr(D);
        end;
  {  3 : begin
          writeln('');
          proshivkaSimmetr(d);
          textcolor(black);
          if qq^.right = nil
            then begin
                   new(head);
                   head^.right := d;
                   qq^.right := head;
                   textcolor(koler);
                   writeln('head');
                   textcolor(black)
                 end;
        end;
  }
      3 : begin
          writeln('������� �������� ��������');
          readln(a);
          delete(D, a);
        end;
    4 : begin
          writeln('������� �������� ��������');
          readln(a);
          dobavl(D, a, a);
        end;
  end;
  readln;
  clrscr;
  until i = 5;
end.