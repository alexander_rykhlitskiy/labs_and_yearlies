﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiVM_3
{
    class Integration
    {
        static public double Simpson(double a, double b, double h, WorkForm.someFunction function)
        {
            double result = 0;
            int N;
            N = (int)((b - a) / h);
            double xk = a;
            result += function(xk);
            int k;
            double subResult = 0;
            for (k = 1; k <= N - 1; k++)
            {
                xk += h;
                subResult += function(xk);
            }
            result += subResult * 2;
            subResult = 0;
            double prevXK = a;
            xk = a;
            for (k = 1; k <= N; k++)
            {
                xk += h;
                subResult += function((prevXK + xk) / 2);
                prevXK = xk;
            }
            result += subResult * 4;
            result += function(b);
            result *= h / 6;
            return result;
        }
        static public double Gauss(double a, double b, WorkForm.someFunction function)
        {

            const int n = 4;
            double result = 0;
            double[] x = { -0.861136, -0.339981, 0.339981, 0.861136 },
                     c = {0.347855, 0.652145, 0.652145, 0.347855},
                     z = new double[n];
            for (int i = 0; i < n; i++)
            {
                z[i] = (b + a) / 2 + ((b - a) / 2) * x[i];
            }
            for (int i = 0; i < n; i++)
            {
                result += c[i] * function(z[i]);
            }
            result *= (b - a) / 2;
            return result;
        }
    }
}
