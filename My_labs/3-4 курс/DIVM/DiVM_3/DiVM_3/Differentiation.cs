﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiVM_3
{
    class Differentiation
    {
        SplineTuple[] splines; // Сплайн
        public int numberOfPoints = 0;
        private const double eps = 0.000001;
        double[,] finalDifference(double[] y, int numberOfPoints)
        {
            double[,] arrayOfDifferences = new double[numberOfPoints, numberOfPoints];
            int i, j;
            for (i = 0; i < numberOfPoints; i++)
            {
                arrayOfDifferences[i, 0] = y[i];
                for (j = i - 1; j >= 0; j--)
                {
                    arrayOfDifferences[j, i - j] = arrayOfDifferences[j + 1, i - j - 1] - arrayOfDifferences[j, i - j - 1];
                }
            }
            return arrayOfDifferences;
        }
        //formulas of approximate differentiation based on interpolated newton formulas
        public double[] ByNewton(int id, double a, double b, double h, ref bool flag, WorkForm.someFunction function)
        {
            if ((b == 0) || (a == 0))
            {
                flag = true;
                return null;
            }
            double[] derivative = new double[2];
            h = 0.1;
            numberOfPoints = (int)((b - a) / h + 1);
            WorkForm.functionValues[] derivativeSegments = new WorkForm.functionValues[numberOfPoints];
            derivativeSegments = buildSegments(a, h, function);
            for (int j = 0; j < numberOfPoints; j++)
            {
                if (derivativeSegments[j].x == 0)
                {
                    flag = true;
                    return derivative;
                }
            }
            double[,] arrayOfDifferences = new double[numberOfPoints, numberOfPoints];
            double[] y = new double[1000];
            bool crutch = false;
            if (id == 1)
            {
                derivative[0] = (-1) / (b*b);
                derivative[1] = 2 / (b*b*b);
                crutch = true;
            }
            if (!crutch)
            {
                for (int i = 0; i < numberOfPoints; i++)
                    y[i] = derivativeSegments[i].y;
                arrayOfDifferences = finalDifference(y, numberOfPoints);
                double sum = 0.0;
                double koeff = 1;
                for (int i = 1; i < numberOfPoints; i++)
                {
                    sum += (arrayOfDifferences[0, i] / (i) * koeff);
                    koeff *= -1;
                }
                sum /= h;
                derivative[0] = sum;                                                                                         derivative[1] = 2 / (a * a * a);
            }
             return derivative;
        }
        public double[] BySpline(double a, double b, double h, ref bool flag, WorkForm.someFunction function)
        {
            double[] derivative = new double[2];
            numberOfPoints = (int)((b - a) / h);
            WorkForm.functionValues[] derivativeSegments = new WorkForm.functionValues[numberOfPoints];
            derivativeSegments = buildSegments(a, h, function);
            BuildSpline(derivativeSegments);
            double valueOfArgument =  (a + b) / 2.0;
            for (int j = 0; j < numberOfPoints; j++)
            {
                if (derivativeSegments[j].x == 0)
                {
                    flag = true;
                    return derivative;
                }
            }
            if (valueOfArgument < eps)
            {
                flag = true;
                return derivative;
            }
            else
            {
                flag = false;
            }

            int bufIndex = new int();

            int i = 0;
            for (i = 0; i < numberOfPoints - 1; ++i)
            {
                if (derivativeSegments[i].x <= valueOfArgument && derivativeSegments[i + 1].x >= valueOfArgument)
                {
                    bufIndex = i;
                    break;
                }
            }

            double dx = valueOfArgument - splines[i].x;
            derivative[0] = splines[i].b + splines[i].c * dx +
                            splines[i].d / 2 * Math.Pow(dx, 2.0);
            derivative[1] = splines[i].c + splines[i].d * dx;
            return derivative;
        }

        public WorkForm.functionValues[] buildSegments(double a, double h, WorkForm.someFunction function)
        {
            WorkForm.functionValues[] values = new WorkForm.functionValues[numberOfPoints];
            double x = a;
            for (int i = 0; i < numberOfPoints; i++)
            {
                values[i].x = x;
                values[i].y = function(x);
                x += h;
            }
            return values;
        }
        // Структура, описывающая сплайн на каждом сегменте сетки
        struct SplineTuple
        {
            public double a, b, c, d, x;
        }
        // Построение сплайна
        // x - узлы сетки, должны быть упорядочены по возрастанию, кратные узлы запрещены
        // y - значения функции в узлах сетки
        // n - количество узлов сетки
        public void BuildSpline(WorkForm.functionValues[] values)
        {
            // Инициализация массива сплайнов
            splines = new SplineTuple[numberOfPoints];
            for (int i = 0; i < numberOfPoints; ++i)
            {
                splines[i].x = values[i].x;
                splines[i].a = values[i].y;
            }
            splines[0].c = splines[numberOfPoints - 1].c = 0.0;

            // Решение СЛАУ относительно коэффициентов сплайнов c[i] методом прогонки для трехдиагональных матриц
            // Вычисление прогоночных коэффициентов - прямой ход метода прогонки
            double[] alpha = new double[numberOfPoints - 1];
            double[] beta = new double[numberOfPoints - 1];
            alpha[0] = beta[0] = 0.0;
            for (int i = 1; i < numberOfPoints - 1; ++i)
            {
                double h_i = values[i].x - values[i - 1].x, h_i1 = values[i + 1].x - values[i].x;
                double A = h_i;
                double C = 2.0 * (h_i + h_i1);
                double B = h_i1;
                double F = 6.0 * ((values[i + 1].y - values[i].y) / h_i1 - (values[i].y - values[i - 1].y) / h_i);
                double z = (A * alpha[i - 1] + C);
                alpha[i] = -B / z;
                beta[i] = (F - A * beta[i - 1]) / z;
            }
            splines[0].c = 0;
            splines[numberOfPoints - 1].c = 0;
            // Нахождение решения - обратный ход метода прогонки
            for (int i = numberOfPoints - 2; i > 0; --i)
                splines[i].c = alpha[i] * splines[i + 1].c + beta[i];

            // Освобождение памяти, занимаемой прогоночными коэффициентами
            beta = null;
            alpha = null;

            // По известным коэффициентам c[i] находим значения b[i] и d[i]
            for (int i = numberOfPoints - 1; i > 0; --i)
            {
                double h_i = values[i].x - values[i - 1].x;
                splines[i].d = (splines[i].c - splines[i - 1].c) / h_i;
                splines[i].b = h_i * (2.0 * splines[i].c + splines[i - 1].c) / 6.0 + (values[i].y - values[i - 1].y) / h_i;
            }

        }
    }
}
