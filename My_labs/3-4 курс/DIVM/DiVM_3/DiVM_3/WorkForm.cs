﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DiVM_3
{
    public partial class WorkForm : Form
    {
        public delegate double someFunction(double d);
        public double Hyperbola(double d)
        {
            double result;
            result = 1 + 1 / d;
            return result;
        }
        public struct functionValues
        {
            public double x, y;
        }
        public WorkForm()
        {
            InitializeComponent();
        }


        private void buttonGetResult_Click(object sender, EventArgs e)
        {
            simpsonNumeric.Value = (decimal)Integration.Simpson((double)numericForA.Value, (double)numericForB.Value,
                         (double)numericForH.Value, Math.Cos);
            gaussNumeric.Value = (decimal)Integration.Gauss((double)numericForA.Value, (double)numericForB.Value, Math.Cos);
            double[] derivative = new double[2];
            bool flag = false;
            if (numericForA.Value < numericForB.Value)
            {
                Differentiation objectForDifferentiation = new Differentiation();
                derivative = objectForDifferentiation.BySpline((double)numericForA.Value, (double)numericForB.Value,
                             (double)numericForH.Value, ref flag, Hyperbola);
                //ByNewton: 1 - for b,  0 - for a, without and BySpline - for spline
                if (flag == true)
                    label10.Text = "Error";
                else
                {
                    firstDerivativeNumeric.Value = (decimal)derivative[0];
                    secondDerivativeNumeric.Value = (decimal)derivative[1];
                }
            }
            else
                label10.Text = "Error";
        }
    }
}
