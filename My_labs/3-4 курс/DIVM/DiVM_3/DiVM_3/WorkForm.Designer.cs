﻿namespace DiVM_3
{
    partial class WorkForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numericForA = new System.Windows.Forms.NumericUpDown();
            this.numericForB = new System.Windows.Forms.NumericUpDown();
            this.numericForH = new System.Windows.Forms.NumericUpDown();
            this.labelForA = new System.Windows.Forms.Label();
            this.labelForB = new System.Windows.Forms.Label();
            this.labelForH = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gaussNumeric = new System.Windows.Forms.NumericUpDown();
            this.buttonGetResult = new System.Windows.Forms.Button();
            this.firstDerivativeLabel = new System.Windows.Forms.Label();
            this.firstDerivativeNumeric = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.secondDerivativeNumeric = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.simpsonNumeric = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericForA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericForB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericForH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gaussNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstDerivativeNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondDerivativeNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpsonNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // numericForA
            // 
            this.numericForA.Location = new System.Drawing.Point(50, 58);
            this.numericForA.Name = "numericForA";
            this.numericForA.Size = new System.Drawing.Size(82, 20);
            this.numericForA.TabIndex = 0;
            this.numericForA.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericForB
            // 
            this.numericForB.Location = new System.Drawing.Point(50, 84);
            this.numericForB.Name = "numericForB";
            this.numericForB.Size = new System.Drawing.Size(82, 20);
            this.numericForB.TabIndex = 1;
            this.numericForB.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // numericForH
            // 
            this.numericForH.DecimalPlaces = 2;
            this.numericForH.Location = new System.Drawing.Point(50, 110);
            this.numericForH.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericForH.Minimum = new decimal(new int[] {
            1100,
            0,
            0,
            -2147483648});
            this.numericForH.Name = "numericForH";
            this.numericForH.Size = new System.Drawing.Size(82, 20);
            this.numericForH.TabIndex = 2;
            this.numericForH.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            // 
            // labelForA
            // 
            this.labelForA.AutoSize = true;
            this.labelForA.Location = new System.Drawing.Point(19, 60);
            this.labelForA.Name = "labelForA";
            this.labelForA.Size = new System.Drawing.Size(16, 13);
            this.labelForA.TabIndex = 3;
            this.labelForA.Text = "a:";
            // 
            // labelForB
            // 
            this.labelForB.AutoSize = true;
            this.labelForB.Location = new System.Drawing.Point(19, 86);
            this.labelForB.Name = "labelForB";
            this.labelForB.Size = new System.Drawing.Size(16, 13);
            this.labelForB.TabIndex = 4;
            this.labelForB.Text = "b:";
            // 
            // labelForH
            // 
            this.labelForH.AutoSize = true;
            this.labelForH.Location = new System.Drawing.Point(19, 112);
            this.labelForH.Name = "labelForH";
            this.labelForH.Size = new System.Drawing.Size(16, 13);
            this.labelForH.TabIndex = 5;
            this.labelForH.Text = "h:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(156, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Integration by Gauss:";
            // 
            // gaussNumeric
            // 
            this.gaussNumeric.DecimalPlaces = 10;
            this.gaussNumeric.Location = new System.Drawing.Point(279, 125);
            this.gaussNumeric.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.gaussNumeric.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.gaussNumeric.Name = "gaussNumeric";
            this.gaussNumeric.Size = new System.Drawing.Size(82, 20);
            this.gaussNumeric.TabIndex = 6;
            // 
            // buttonGetResult
            // 
            this.buttonGetResult.Location = new System.Drawing.Point(279, 21);
            this.buttonGetResult.Name = "buttonGetResult";
            this.buttonGetResult.Size = new System.Drawing.Size(82, 22);
            this.buttonGetResult.TabIndex = 8;
            this.buttonGetResult.Text = "to get result";
            this.buttonGetResult.UseVisualStyleBackColor = true;
            this.buttonGetResult.Click += new System.EventHandler(this.buttonGetResult_Click);
            // 
            // firstDerivativeLabel
            // 
            this.firstDerivativeLabel.AutoSize = true;
            this.firstDerivativeLabel.Location = new System.Drawing.Point(156, 70);
            this.firstDerivativeLabel.Name = "firstDerivativeLabel";
            this.firstDerivativeLabel.Size = new System.Drawing.Size(78, 13);
            this.firstDerivativeLabel.TabIndex = 10;
            this.firstDerivativeLabel.Text = "First derivative:";
            // 
            // firstDerivativeNumeric
            // 
            this.firstDerivativeNumeric.DecimalPlaces = 2;
            this.firstDerivativeNumeric.Location = new System.Drawing.Point(279, 70);
            this.firstDerivativeNumeric.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.firstDerivativeNumeric.Minimum = new decimal(new int[] {
            10000000,
            0,
            0,
            -2147483648});
            this.firstDerivativeNumeric.Name = "firstDerivativeNumeric";
            this.firstDerivativeNumeric.Size = new System.Drawing.Size(82, 20);
            this.firstDerivativeNumeric.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(156, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Second derivative:";
            // 
            // secondDerivativeNumeric
            // 
            this.secondDerivativeNumeric.DecimalPlaces = 2;
            this.secondDerivativeNumeric.Location = new System.Drawing.Point(279, 99);
            this.secondDerivativeNumeric.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.secondDerivativeNumeric.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.secondDerivativeNumeric.Name = "secondDerivativeNumeric";
            this.secondDerivativeNumeric.Size = new System.Drawing.Size(82, 20);
            this.secondDerivativeNumeric.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(156, 151);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Integration by Simpson:";
            // 
            // simpsonNumeric
            // 
            this.simpsonNumeric.DecimalPlaces = 10;
            this.simpsonNumeric.Location = new System.Drawing.Point(279, 151);
            this.simpsonNumeric.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.simpsonNumeric.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.simpsonNumeric.Name = "simpsonNumeric";
            this.simpsonNumeric.Size = new System.Drawing.Size(82, 20);
            this.simpsonNumeric.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(276, 46);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 13);
            this.label10.TabIndex = 15;
            // 
            // WorkForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(393, 173);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.simpsonNumeric);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.secondDerivativeNumeric);
            this.Controls.Add(this.firstDerivativeLabel);
            this.Controls.Add(this.firstDerivativeNumeric);
            this.Controls.Add(this.buttonGetResult);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gaussNumeric);
            this.Controls.Add(this.labelForH);
            this.Controls.Add(this.labelForB);
            this.Controls.Add(this.labelForA);
            this.Controls.Add(this.numericForH);
            this.Controls.Add(this.numericForB);
            this.Controls.Add(this.numericForA);
            this.Name = "WorkForm";
            this.Text = "WorkForm";
            ((System.ComponentModel.ISupportInitialize)(this.numericForA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericForB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericForH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gaussNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstDerivativeNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondDerivativeNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpsonNumeric)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numericForA;
        private System.Windows.Forms.NumericUpDown numericForB;
        private System.Windows.Forms.NumericUpDown numericForH;
        private System.Windows.Forms.Label labelForA;
        private System.Windows.Forms.Label labelForB;
        private System.Windows.Forms.Label labelForH;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown gaussNumeric;
        private System.Windows.Forms.Button buttonGetResult;
        private System.Windows.Forms.Label firstDerivativeLabel;
        private System.Windows.Forms.NumericUpDown firstDerivativeNumeric;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown secondDerivativeNumeric;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown simpsonNumeric;
        private System.Windows.Forms.Label label10;
    }
}

