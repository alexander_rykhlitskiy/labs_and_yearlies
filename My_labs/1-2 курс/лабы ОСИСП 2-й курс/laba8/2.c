#include <sys/types.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <sys/mman.h>
int *arProc;
int *numbStr;
char *mesg;
int x = 0;
int *readyAr;
int indicate(int mas[8], int check)
{
	int i;
	if (check == 1)
	{
		for (i = 1; i <= 4; i++)
			if (readyAr[i] == 0)
				return 0; 
		return 1;
	}
	else
	{
		for (i = 1; i <= 4; i++)
			if (readyAr[i] == 1)
				return 0; 
		return 1;
	}
}
long int presenttime()
{
	struct timeval tv;
	struct timezone tz;
	gettimeofday(&tv, &tz);
	return (tv.tv_usec);
}//N pid ppid текущее время (мсек)  процесс_такой-то.
void actProc1(int sig)
{
	if (x < 1000)
	{
		if (sig == SIGUSR1)
		{
//			printf("1 %d %d got USR1 %ld\n", getpid(), getppid(), 	presenttime());
			puts(mesg);
		}
		char str[100];
		sprintf(mesg, "%d %d %d %ld 1", numbStr[0], getpid(), getppid(), presenttime());
		numbStr[0]++;
		kill(arProc[2], SIGUSR1);
//		printf("1 %d %d sent USR1 %ld\n", getpid(), getppid(), 	presenttime()); 
		x++; 
	}
	else 
	{
		kill(arProc[2], SIGTERM);
		readyAr[1] = 0;
		wait();
		exit(0);
	}
}
void actProc2(int sig)
{
	if (sig == SIGUSR1)
	{
//		printf("2 %d %d got USR1 %ld\n", getpid(), getppid(), 	presenttime()); 
		puts(mesg);
		sprintf(mesg, "%d %d %d %ld 2", numbStr[0], getpid(), getppid(), presenttime());
		numbStr[0]++;
//		printf("2 %d %d sent USR1 %ld\n", getpid(), getppid(), 	presenttime());
		kill(arProc[3], SIGUSR1); 
		x++;
	}
	if (sig == 	SIGTERM)
	{
//		printf("2 %d %d  завершил работу после %d-го сигнала SIGUSR1\n", getpid(), getppid(), x);
		kill(arProc[3], SIGTERM);
		wait();
		readyAr[2] = 0;
		exit(0);
	}
}
void actProc3(int sig)
{
	if (sig == SIGUSR1)
	{
//		printf("3 %d %d got USR1 %ld\n", getpid(), getppid(), 	presenttime());
		puts(mesg);
		sprintf(mesg, "%d %d %d %ld 3", numbStr[0], getpid(), getppid(), presenttime());
		numbStr[0]++;
//		printf("3 %d %d sent USR1 %ld\n", getpid(), getppid(), 	presenttime());  
		kill(arProc[4], SIGUSR1);
		x++;
	}
	if (sig == 	SIGTERM)
	{
//		printf("3 %d %d  завершил работу после %d-го сигнала SIGUSR1\n", getpid(), getppid(), x);
		kill(arProc[4], SIGTERM);
		wait();
		readyAr[3] = 0;
		exit(0);
	}
}
void actProc4(int sig)
{
	if (sig == SIGUSR1)
	{
//		printf("4 %d %d got USR1 %ld\n", getpid(), getppid(), 	presenttime()); 
		puts(mesg);
		sprintf(mesg, "%d %d %d %ld 4", numbStr[0], getpid(), getppid(), presenttime());
		numbStr[0]++;
//		printf("4 %d %d sent USR1 %ld\n", getpid(), getppid(), 	presenttime()); 
		kill(arProc[1], SIGUSR1);
		x++;
	}
	if (sig == 	SIGTERM)
	{
//		printf("4 %d %d  завершил работу после %d-го сигнала SIGUSR2\n", getpid(), getppid(), x);
		readyAr[4] = 0;
		exit(0);
	}
}
void main(char *argv[], int argc[])
{
	readyAr = mmap(NULL, sizeof(int)*9, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, -1, 0);
	arProc = mmap(NULL, sizeof(int)*9, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, -1, 0);
	mesg = mmap(NULL, sizeof(char)*100, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, -1, 0);
	numbStr = mmap(NULL, sizeof(int)*2, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, -1, 0);
	numbStr[0] = 1;
	int i, ind = 4;
	struct sigaction act;	
	sigemptyset(&act.sa_mask);
	act.sa_flags = 0;
	for (i = 0; i <= 8; i++)
		readyAr[i] = 0;
printf("I am process 1 %ld %d %d\n", presenttime(), getpid(), getppid());
	arProc[1] = getpid();

	act.sa_handler = actProc1;	
	sigaction(SIGUSR1, &act, 0);
	sigaction(SIGUSR2, &act, 0);
	readyAr[1] = 1;

	arProc[2] = fork();
	switch (arProc[2])
	{
		case -1:
		{
			fprintf(stderr, "%s: Error creating new process", argv[0]);
			break;
		}
		case 0:
		{
printf("I am process 2 %ld %d %d\n", presenttime(), getpid(), getppid());
			arProc[2] = getpid();

			act.sa_handler = actProc2;	
			sigaction(SIGUSR1, &act, 0);
			sigaction(SIGTERM, &act, 0);
			readyAr[2] = 1;

			arProc[3] = fork();
			switch (arProc[3])
			{
				case -1:
					fprintf(stderr, "%s: Error creating new process", argv[0]);
					break;
				case 0:
				{
printf("I am process 3 %ld %d %d\n", presenttime(), getpid(), getppid());	
					arProc[3] = getpid();

					act.sa_handler = actProc3;	
					sigaction(SIGUSR1, &act, 0);
					sigaction(SIGTERM, &act, 0);
					readyAr[3] = 1;

					arProc[4] = fork();
					switch (arProc[4])
					{
						case -1:
							fprintf(stderr, "%s: Error creating new process", argv[0]);
						case 0:
						{
printf("I am process 4 %ld %d %d\n", presenttime(), getpid(), getppid());
							arProc[4] = getpid();	
							act.sa_handler = actProc4;	
							sigaction(SIGUSR1, &act, 0);
							sigaction(SIGTERM, &act, 0);
							readyAr[4] = 1;	
							break;
						}
					}//end arProc[4]
					break;
				}
			}//end arProc[3]
		}
	}//end arProc[2]
	while(ind) 
		if (indicate(readyAr, 1)) break;
	if (arProc[1] == getpid()) {
		kill(arProc[1], SIGUSR2);
	}
	if (getpid() == arProc[1]){
		while(ind)
			if (indicate(readyAr, 0)) break;
	}
	else for(;;) pause();
exit(0);
}
