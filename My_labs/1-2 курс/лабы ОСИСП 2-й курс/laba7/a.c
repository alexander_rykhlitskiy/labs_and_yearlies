#include <unistd.h>
#include <stdio.h>
char *msg = "Hello, world#1";
int main()
{
	char input[15];
	int p[2];
	if (pipe(p) != 0)
	{
		fprintf(stderr, "Creation of pipe failed\n");
		exit(1);
	}
	write(p[1], msg, 15);
	read(p[0], input, 20);
//	printf("%s\n", input);	
	exit(0);
}
