#include <stdlib.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
int main(int argc[], char *argv[])
{
  int i, x, y;
  x = atoi(argv[1]);
  y = atoi(argv[2]);
  for (i = x; i <= y; i++)
   kill(i, SIGTERM);
  //  kill(i, SIGKILL);
  exit(0);
}
