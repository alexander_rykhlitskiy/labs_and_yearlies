#include <stdio.h>
#include <sys/sem.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/time.h>
#include <string.h>
#include <signal.h>
static int sem_id;
typedef union _semun
{
	int val;
	struct semid_ds *buf;
	unsigned short *array;
} semun;
long int presenttime()
{
	struct timeval tv;
	struct timezone tz;
	gettimeofday(&tv, &tz);
	return(tv.tv_usec);
}
static int set_semvalue(void)
{
	semun sem_union;
	sem_union.val = 0;
	if (semctl(sem_id, 0, SETVAL, sem_union) == -1)
		return(0);
	return(1);
}
static void del_semvalue(void)
{
	semun sem_union;
	if (semctl(sem_id, 0, IPC_RMID, sem_union) == -1)
		fprintf(stderr, "Filed to delete semaphore\n");
}
static int semaphore_v(void)
{
	struct sembuf sem_b;
	sem_b.sem_num = 0;
	sem_b.sem_op = 1;
	sem_b.sem_flg = SEM_UNDO;
	if (semop(sem_id, &sem_b, 1) == -1)
	{
		fprintf(stderr, "semaphore_v failed\n");
		return(0);
	}
	return(1);
}
static int semaphore_p(void)
{
	struct sembuf sem_b;
	sem_b.sem_num = 0;
	sem_b.sem_op = -1;
	sem_b.sem_flg = SEM_UNDO;
	if (semop(sem_id, &sem_b, 1) == -1)
	{
		fprintf(stderr, "semaphore_p failed\n");
		return(0);
	}
	return(1);
}
int msg(int p[][2], int i)
{
	char str[20];

}
int main()
{
//управление - работники: 1, 2, 3
//работники - управление 4, 5, 6
//1-2 - 7; 1-3 - 8
//2-1 - 9; 2-3 - 10
//3-1 - 11; 3-2 - 12
	pid_t pid[3] = {0};
	sem_id = semget((key_t)1234, 1, 0666|IPC_CREAT);
	if (!set_semvalue())
	{
		fprintf(stderr, "Failed to initialize semaphore\n");
		exit(EXIT_FAILURE);
	}
	int p[13][2], i;
	for (i = 1; i <= 12; i++)
		if(pipe(p[i]) == -1)
		{
			perror("Error of creating pipe");
			exit(1);
		}
	int pointFile = 0;
	char str[20] = "sasha";
	pid[0] = fork();
	switch (pid[0])
	{
		case (-1):
			fprintf(stderr, "Failed to create process\n");
			exit(EXIT_FAILURE);
			break;
		case(0):
		while(1){
			sprintf(str, "%d %ld", getpid(), presenttime());
			write(p[4][1], str, sizeof(str));
			sprintf(str, "%d %ld", getpid(), presenttime());
			write(p[7][1], str, sizeof(str));
			sprintf(str, "%d %ld", getpid(), presenttime());
			write(p[8][1], str, sizeof(str));
			read(p[1][0], str, sizeof(str));
			printf("%d %s\n", getpid(), str);
			read(p[9][0], str, sizeof(str));
			printf("%d %s\n", getpid(), str);
			read(p[11][0], str, sizeof(str));
			printf("%d %s\n", getpid(), str);
		}
			exit(0);
	}
	pid[1] = fork();
	switch(pid[1])
	{
		case (-1):
			fprintf(stderr, "Failed to create process\n");
			exit(EXIT_FAILURE);
		case (0):
		while(1){
			sprintf(str, "%d %ld", getpid(), presenttime());
			write(p[5][1], str, sizeof(str));
			sprintf(str, "%d %ld", getpid(), presenttime());
			write(p[9][1], str, sizeof(str));
			sprintf(str, "%d %ld", getpid(), presenttime());
			write(p[10][1], str, sizeof(str));
			read(p[2][0], str, sizeof(str));
			printf("%d %s\n", getpid(), str);
			read(p[7][0], str, sizeof(str));
			printf("%d %s\n", getpid(), str);
			read(p[12][0], str, sizeof(str));
			printf("%d %s\n", getpid(), str);
		}
			exit(0);
	}
	pid[2] = fork();
	switch(pid[2])
	{
		case (-1):
			fprintf(stderr, "Failed to create process\n");
			exit(EXIT_FAILURE);
		case (0):
		while(1){
			sprintf(str, "%d %ld", getpid(), presenttime());
			write(p[6][1], str, sizeof(str));
			sprintf(str, "%d %ld", getpid(), presenttime());
			write(p[11][1], str, sizeof(str));
			sprintf(str, "%d %ld", getpid(), presenttime());
			write(p[12][1], str, sizeof(str));
			read(p[3][0], str, sizeof(str));
			printf("%d %s\n", getpid(), str);
			read(p[8][0], str, sizeof(str));
			printf("%d %s\n", getpid(), str);
			read(p[10][0], str, sizeof(str));
			printf("%d %s\n", getpid(), str);
		}
			exit(0);
	}
printf("parent %d\n", getpid());
printf("son1 %d\n", pid[0]);
printf("son2 %d\n", pid[1]);
printf("son3 %d\n", pid[2]);
	//for (i = 1; i <= 10; i++)
	{
		sprintf(str, "%d %ld", getpid(), presenttime());
		write(p[1][1], str, sizeof(str));
		sprintf(str, "%d %ld", getpid(), presenttime());
		write(p[2][1], str, sizeof(str));
		sprintf(str, "%d %ld", getpid(), presenttime());
		write(p[3][1], str, sizeof(str));

		fd_set readset;
		FD_ZERO(&readset);
		FD_SET(p[4][0], &readset);
		FD_SET(p[5][0], &readset);
		FD_SET(p[6][0], &readset);
int a = 3;
while (a > 0)
{
		select(FD_SETSIZE, &readset, NULL, NULL, NULL);
		if (FD_ISSET(p[4][0], &readset))
		{
			read(p[4][0], str, sizeof(str));
			printf("%d %d %s\n", i, getpid(), str);
			a = a - 1;puts("444");
			FD_CLR(p[4][0], &readset);
		}
		if (FD_ISSET(p[5][0], &readset))
		{
			read(p[5][0], str, sizeof(str));
			printf("%d %d %s\n", i, getpid(), str);
			a = a - 1;puts("555");
			FD_CLR(p[5][0], &readset);
		}
		if (FD_ISSET(p[6][0], &readset))
		{
			read(p[6][0], str, sizeof(str));
			printf("%d %d %s\n", i, getpid(), str);
			a = a - 1;puts("666");
			FD_CLR(p[6][0], &readset);
		}
}
	}
	kill(pid[0], SIGTERM);
	kill(pid[1], SIGTERM);
	kill(pid[2], SIGTERM);
	exit(0);
}
