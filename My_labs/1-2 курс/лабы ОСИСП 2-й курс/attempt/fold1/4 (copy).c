#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <dirent.h>
void printdir(char *dir, FILE *f, char *path, int sizeDown, int sizeUp)
{
  DIR *dp;
  struct dirent *entry;
  struct stat statbuf;

  if ((dp = opendir(dir)) == NULL)
  {
    fprintf(stderr, "cannot open directory: %s\n", dir);
    return;
  }
  chdir(dir);
  while((entry = readdir(dp)) != NULL)
  {
    stat(entry->d_name, &statbuf);
    if (S_ISDIR(statbuf.st_mode))
    {
      if (strcmp(".", entry->d_name) == 0 ||
         strcmp("..", entry->d_name) == 0)
      continue;
     // fprintf(f, "%*s%s/", depth, "", entry->d_name);
      char pathTemp[1000];
      strcpy(pathTemp, path);
      strcat(pathTemp, "/");
      strcat(pathTemp, entry->d_name);
      printdir(entry->d_name, f, pathTemp, sizeDown, sizeUp);
    }
    else
    {
      int size = 0;
      char path1[10000];
      strcpy(path1, path);
      strcat(path1, "/");
      strcat(path1, entry->d_name);
      struct stat statbuf;
      stat(entry->d_name, &statbuf);
      size = statbuf.st_size;
      if ((size <= sizeUp) && (size >= sizeDown))
      {
        fprintf(f, "%s   %d\n", path1, size);
        printf("%s   %d  %s\n", path1, size, path);
      }
    }
  }
  chdir("..");
  closedir(dp);
}
int main(int argc[], char *argv[])
{
  FILE *f;
  f = fopen(argv[2], "w");
  printdir(argv[1], f, argv[1], atoi(argv[3]), atoi(argv[4]));
  exit(0);
}
