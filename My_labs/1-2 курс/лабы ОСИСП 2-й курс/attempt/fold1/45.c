#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <dirent.h>
void printdir(char *dir, int depth, FILE *f, char *path /*, int sizeDown, int sizeUp*/)
{
  DIR *dp;
  struct dirent *entry;
  struct stat statbuf;

  if ((dp = opendir(dir)) == NULL)
  {
    fprintf(stderr, "cannot open directory: %s\n", dir);
    return;
  }
  chdir(dir);
  while((entry = readdir(dp)) != NULL)
  {
    stat(entry->d_name, &statbuf);
    if (S_ISDIR(statbuf.st_mode))
    {
      if (strcmp(".", entry->d_name) == 0 ||
         strcmp("..", entry->d_name) == 0)
      continue;
     // fprintf(f, "%*s%s/", depth, "", entry->d_name);
      strcat(path, "/");
      strcat(path, entry->d_name);
      printdir(entry->d_name, depth+4, f, path/*, sizeDown, sizeUp*/);
    }
    else
    {
      char path1[10000];
      strcpy(path1, path);
      strcat(path1, "/");
      strcat(path1, entry->d_name);
    /*  struct stat *statbuf;
      stat(path1, &statbuf);
      if ((statbuf.st_size < sizeUp) && (statbuf.st_size > sizeDown))
    */    fprintf(f, "%s\n", path1);
    }
  }
  chdir("..");
  closedir(dp);
}
int main(int argc[], char *argv[])
{
  FILE *f;
  f = fopen(argv[2], "w");
  printf("Directory scan of /home:\n");
  printdir(argv[1], 0, f, argv[1] /*, atoi(argv[3]), atoi(argv[4])*/);
  printf("done.\n");
  exit(0);
}
