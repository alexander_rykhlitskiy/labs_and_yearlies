#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>

struct item
	{
		char lk[256];
		off_t sz;
		ino_t id;
		struct item *next;
		
	};

struct item *list=NULL;
struct item *list1=NULL;

char modulname[128];
extern int errno;
char *topdir;
char linkfile[256];
ino_t inode;
off_t size;
int M1,M2;

void ins(char[],off_t,ino_t);
void printdir (char*);
void fndcp(void);
int searchlist(ino_t ino);
void insertlist(char[],off_t,ino_t);
void displaylist1(void);
void displaylist(void);


void ins(char linki[256],off_t size,ino_t ino)
{
	struct item *p,*cur=list1,*prev=NULL;
	p=(struct item*)malloc(sizeof(struct item));
	strcpy(p->lk,linki);
	p->sz=size;
	p->id=ino;
	p->next=NULL;
	while (cur!= NULL)
		{
			prev=cur;
			cur= cur->next;
		}
	if (prev == NULL)
		{
			p->next=list1;
			list1=p;
		}
	else
		{
			prev->next=p;
			p->next=cur;
		}
}


int searchlist(ino_t ino)
{
	if (list==NULL)
		return 1;
	else
		{
			struct item *prev=list, *cur=list->next, *p;
			if (ino == list->id)
				return 0;
			while (cur!=NULL && cur->id != ino)
				{
							prev=cur;
							cur=cur->next;
				}
			if (cur!=NULL)
				return 0;
			else
				return 1;
			
		}
}


void delellist(ino_t value)
{
	struct item *prev=list1, *cur=list1->next, *p;
		if (value == list1->id)
			{
				p=list1;
				list1=list1->next;
				free(p);
			}
		else
			{
				while (cur!=NULL && cur->id != value)
				{
							prev=cur;
							cur=cur->next;
				}
			if (cur!=NULL)
				{
					p=cur;
					prev->next=cur->next;
					free(p);
				}
			}
}


void insertlist(char linki[256],off_t size,ino_t ino)
{
	struct item *p,*cur=list,*prev=NULL;
	p=(struct item*)malloc(sizeof(struct item));
	strcpy(p->lk,linki);
	p->sz=size;
	p->id=ino;
	p->next=NULL;
	while (cur!= NULL)
		{
			prev=cur;
			cur= cur->next;
		}
	if (prev == NULL)
		{
			p->next=list;
			list=p;
		}
	else
		{
			prev->next=p;
			p->next=cur;
		}
}

void displaylist1()
{
	struct item *p=list1;
	while (p!=NULL)
		{
			printf("%s %d\n",p->lk,(int)p->sz);
			p=p->next;
		}
}
void displaylist()
{
	struct item *p=list;
	while (p!=NULL)
		{
			printf("%s %d\n",p->lk,(int)p->sz);
			p=p->next;
		}
}







void fndcp(void)
{
	int i=0;
	int st=0;
	int cp;
	FILE *in1,*in2;
	struct item *p=list1;
	struct item *q;
	char r,z;
//	printf("LIST::::::::::::\n");
//	displaylist1();
//	printf("END::::::::::::\n\n\n");
	while (p!=NULL)
		{
			if (p->next!=NULL)
				q=p->next;
			else
				break;
			while (q!=NULL)
				{	
						
					in1=fopen(p->lk,"r");
					if (!in1)
						{
							fprintf(stderr,"%s %s %s\n",modulname,p->lk,strerror(errno));
							delellist(p->id);
							break;
						}
					in2=fopen(q->lk,"r");
					if (!in2)
						{
							fprintf(stderr,"%s %s %s\n",modulname,q->lk,strerror(errno));
							delellist(q->id);
							break;
						}
					i=0;
					while (!feof(in1) && !feof(in2))
						{
							if(!fscanf(in1,"%c",&r))
								{
									fprintf(stderr,"%s %s %s\n",modulname,p->lk,strerror(errno));
									break;
								}
							if(!fscanf(in2,"%c",&z))
								{
									fprintf(stderr,"%s %s %s\n",modulname,q->lk,strerror(errno));
									break;
								}
							if (r==z && (p->sz==q->sz))
								i=1;
							else
								{
									i=0;
									break;
								}
						}
					fclose(in1);
					fclose(in2);
					if (i==1)
						{
							if (searchlist(p->id))
								insertlist(p->lk,p->sz,p->id);
							if (searchlist(q->id))
								insertlist(q->lk,q->sz,q->id);
						}
					q=q->next;
				}
			p=p->next;
		}
}			

void printdir (char *dir)
{
	DIR *dp;
	char temp[256];
	int desc;
	struct dirent *entry;
	struct stat statbuf;
		
	if ((dp = opendir(dir)) == NULL)
		{
			getcwd(temp,100);
			if (strlen(temp) != 1)
				strcat(temp,"/");
			else
				strcat(temp,"");
			strcat(temp,dir);
			fprintf(stderr,"%s %s %s\n",modulname,temp,strerror(errno));
			return ;
		}
	
	chdir(dir);
	
	while((entry = readdir(dp)) !=NULL)
		{
			getcwd(temp,100);
			strcat(temp,"/");
			strcat(temp,entry->d_name);

			if(lstat(temp, &statbuf) == 0)
				{
					
					if (S_ISDIR(statbuf.st_mode))
						{

							if (strcmp(".", entry->d_name) == 0 || strcmp("..",entry->d_name) == 0)
							continue;
							printdir(entry->d_name);

					
						}
					else 
						{

							if (S_ISREG(statbuf.st_mode))
								{
									if ((statbuf.st_size >= M1) && (statbuf.st_size <= M2) )
										{
											getcwd(linkfile,100);
											strcat(linkfile,"/");
											strcat(linkfile,entry->d_name);
											inode=statbuf.st_ino;
											size=statbuf.st_size;
											ins(linkfile,size,inode);
											
										}	
								}		
						}
				}
			else	
				fprintf(stderr,"%s %s %s\n",modulname,temp,strerror(errno));
		}
	chdir("..");
	closedir(dp);
}
		

int main(int argc, char *argv[])
{
	int i,j=0;
	DIR *dp;

	for (i=2; i<=strlen(argv[0]); i++)
		{
			modulname[j]=argv[0][i];
			j++;
		}

	if (argc == 4)
		{
			M1=atoi(argv[2]);
			M2=atoi(argv[3]);
			topdir=argv[1];
			if ((dp = opendir(topdir)) == NULL)
				{
					fprintf(stderr,"%s %s %s\n",modulname,topdir,strerror(errno));
					return -1;
				}
			printdir(topdir);
			fndcp();
			displaylist();
			exit(0);
		}
	else
		{
			fprintf(stderr,"%s Too few arguments\n",modulname);
			exit(-1);
		}
	return 0;
}
