#include <stdio.h>
#include <sys/sem.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/time.h>
#include <string.h>
#include <sys/stat.h>
#define BUF_SIZE 10
static int sem_id;
typedef union _semun
{
	int val;
	struct semid_ds *buf;
	unsigned short *array;
} semun;
long int presenttime()
{
	struct timeval tv;
	struct timezone tz;
	gettimeofday(&tv, &tz);
	return(tv.tv_usec);
}
static int set_semvalue(void)
{
	semun sem_union;
	sem_union.val = 1;
	if (semctl(sem_id, 0, SETVAL, sem_union) == -1)
		return(0);
	return(1);
}
static void del_semvalue(void)
{
	semun sem_union;
	if (semctl(sem_id, 0, IPC_RMID, sem_union) == -1)
		fprintf(stderr, "Filed to delete semaphore\n");
}
static int semaphore_v(void)
{
	struct sembuf sem_b;
	sem_b.sem_num = 0;
	sem_b.sem_op = 1;
	sem_b.sem_flg = SEM_UNDO;
	if (semop(sem_id, &sem_b, 1) == -1)
	{
		fprintf(stderr, "semaphore_v failed\n");
		return(0);
	}
	return(1);
}
static int semaphore_p(void)
{
	struct sembuf sem_b;
	sem_b.sem_num = 0;
	sem_b.sem_op = -1;
	sem_b.sem_flg = SEM_UNDO;
	if (semop(sem_id, &sem_b, 1) == -1)
	{
		fprintf(stderr, "semaphore_p failed\n");
		return(0);
	}
	return(1);
}
int copy(int numb, int count, char *pathcopy, char *pathpaste, FILE *fcopy, FILE *fpaste)
{

	struct stat statbuf;
	stat(pathcopy, &statbuf);
	chmod(pathpaste, statbuf.st_mode);
	int  pointFile, pointBeg, pointEnd;
	pointBeg = (statbuf.st_size/(float)count)*numb + 0.5;
	pointEnd = (statbuf.st_size/(float)count)*(numb+1) + 1;
	pointFile = pointBeg;printf("point = %d\n", pointFile);
	char buf[BUF_SIZE] = {0};
		semaphore_p();
	while(pointFile <= pointEnd)
	{

		if (pointEnd-pointFile < sizeof(buf))
		{
			fread(buf, pointEnd-pointFile-1, 1, fcopy);
			fwrite(buf, pointEnd-pointFile-1, 1, fpaste);
			printf("son %s", buf);
		}
		else
		{
			fseek(fcopy, pointFile, SEEK_SET);
			fread(buf, sizeof(buf), 1, fcopy);
			fwrite(buf, sizeof(buf), 1, fpaste);
			printf("%d son %s", numb, buf);
		}
		pointFile = pointFile + sizeof(buf);

	}		semaphore_v();

	exit(1);
} 
int main(int argc[], char *argv[])
{
	FILE *f;
	f = fopen(argv[2], "w");
	fclose(f);
	char buf[1];
		FILE *fcopy, *fpaste;
		fcopy = fopen(argv[1], "r");
		fpaste = fopen(argv[2], "r+");
			fseek(fpaste, 2, SEEK_SET);
			buf[0] = 'a';
			fwrite(buf, sizeof(buf), 1, fpaste);
	fclose(fcopy);
	fclose(fpaste);
		fcopy = fopen(argv[1], "r");
		fpaste = fopen(argv[2], "r+");
			fseek(fpaste, 0, SEEK_SET);
			fwrite(buf, sizeof(buf), 1, fpaste);
	fclose(fcopy);
	fclose(fpaste);
		fcopy = fopen(argv[1], "r");
		fpaste = fopen(argv[2], "r+");
			fseek(fpaste, 1, SEEK_SET);
			fwrite(buf, sizeof(buf), 1, fpaste);
	fclose(fcopy);
	fclose(fpaste);
	exit(1);
}
