#include <stdio.h>
#include <sys/sem.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/time.h>
#include <string.h>
static int sem_id;
typedef union _semun
{
	int val;
	struct semid_ds *buf;
	unsigned short *array;
} semun;
long int presenttime()
{
	struct timeval tv;
	struct timezone tz;
	gettimeofday(&tv, &tz);
	return(tv.tv_usec);
}
static int set_semvalue(void)
{
	semun sem_union;
	sem_union.val = 1;
	if (semctl(sem_id, 0, SETVAL, sem_union) == -1)
		return(0);
	return(1);
}
static void del_semvalue(void)
{
	semun sem_union;
	if (semctl(sem_id, 0, IPC_RMID, sem_union) == -1)
		fprintf(stderr, "Filed to delete semaphore\n");
}
static int semaphore_v(void)
{
	struct sembuf sem_b;
	sem_b.sem_num = 0;
	sem_b.sem_op = 1;
	sem_b.sem_flg = SEM_UNDO;
	if (semop(sem_id, &sem_b, 1) == -1)
	{
		fprintf(stderr, "semaphore_v failed\n");
		return(0);
	}
	return(1);
}
static int semaphore_p(void)
{
	struct sembuf sem_b;
	sem_b.sem_num = 0;
	sem_b.sem_op = -1;
	sem_b.sem_flg = SEM_UNDO;
	if (semop(sem_id, &sem_b, 1) == -1)
	{
		fprintf(stderr, "semaphore_p failed\n");
		return(0);
	}
	return(1);
}
int main()
{
	pid_t pid = 1;
	sem_id = semget((key_t)1234, 1, 0666|IPC_CREAT);
	if (!set_semvalue()) 
	{
		fprintf(stderr, "Failed to initialize semaphore\n");
		exit(EXIT_FAILURE);
	}
	FILE *f;
	f = fopen("/home/sasha/stud/file", "r+");
//	fclose(f);
	pid = fork();
	int i, pointFile = 0;
	char str[100];
	if (pid != 0) semaphore_p();
	for (i = 1; i <= 100; i++)
	switch (pid)
	{
		case (-1):
			fprintf(stderr, "Failed to create process\n");
			exit(EXIT_FAILURE);
			break;
		case(0):
			if (i != 1)
				semaphore_p();
			fseek(f, pointFile, SEEK_SET);
			fprintf(f, "%d %d %ld\n", i, getpid(), presenttime());
			printf("son %d\n", i);
			pointFile = ftell(f);		
			semaphore_v();
			break;
		default:
			semaphore_p();
			fseek(f, pointFile, SEEK_SET);
			fgets(str, 100, f);
			printf("parent %d %s", i, str);
			pointFile = ftell(f);
			semaphore_v();
	}
	if (pid != 0)
	{ 
		sleep(2);
		del_semvalue();
		fclose(f);
	}
}
