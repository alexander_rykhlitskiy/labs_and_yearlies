#include <sys/types.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>
static int alarm_fired  = 0;
void ding(int sig)
{
  alarm_fired = 1;
}
int n;

int main()
{
  struct timeval tv;
  struct timezone tz;
  long int t;
  t = tv.tv_usec;
  printf("%ld\n", t);
  pid_t pid = 1, pid_1 = 1;
  printf("alarm application starting\n");
  pid = fork();
  if (pid != 0)
    pid_1 = fork();
  while(3)
  {
  if ((pid != 0) && (pid_1 != 0))
  {
    usleep(1000);
    kill(pid, SIGUSR1);
    kill(pid_1, SIGUSR1);
  }
//объявление переменной act
  struct sigaction act;
  act.sa_handler = ding;
//  sigemptyset(&act.sa_mask);
//  act.sa_flags = 0;
  switch(pid)
  {
    case -1:
	{
      perror("fork failed");
      exit(1);
	}
    case 0:
	{
      while(3)
      {
      sigaction(SIGUSR1, &act, 0);
	  pause();
      if (alarm_fired == 1)
      {
        alarm_fired = 0;
        n++;
        gettimeofday(&tv, &tz);
        printf("%d pid=%d ppid=%d %ld сын1 get SIGUSR1\n", n,getpid(),getppid(), tv.tv_usec);
      }
      kill(getppid(), SIGUSR2);
	  }
      exit(0);
	}
  }
  switch(pid_1)
  {
    case -1:
	{
      perror("fork failed");
      exit(1);
	}
    case 0:
	{
      while (3)
      {
      sigaction(SIGUSR1, &act, 0);
	  pause();
      if (alarm_fired == 1)
      {
        alarm_fired = 0;
        n++;
        gettimeofday(&tv, &tz);
        printf("%d pid=%d ppid=%d %ld сын2 get SIGUSR1\n", n,getpid(),getppid(), tv.tv_usec);
      }
      kill(getppid(), SIGUSR2);
	  }
      exit(0);
	}
  }
  sigaction(SIGUSR2, &act, 0);
  pause();
      if (alarm_fired == 1)
      {
        alarm_fired = 0;
        n++;
        gettimeofday(&tv, &tz);
        printf("%d pid=%d %ld отец get SIGUSR2\n", n,getpid(), tv.tv_usec);
      }
  }
      exit(0);
}
