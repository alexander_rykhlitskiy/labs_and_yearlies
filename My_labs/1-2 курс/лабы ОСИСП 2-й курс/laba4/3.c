#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/wait.h>
#include <errno.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <time.h>
#include <wait.h>
int *readyAr;
main (int argc, char **argv)
{
	readyAr = mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE, 
					 MAP_ANONYMOUS | MAP_SHARED, -1, 0);
	*readyAr = 5;
	int pid;
	pid = fork();
	*readyAr = 1;
	if (pid == 0)
	{
		*readyAr = 5;
	}
	else sleep(1);
	printf("%d\n", *readyAr);
}
