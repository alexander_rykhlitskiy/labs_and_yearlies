#include <sys/types.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>
#include <errno.h>
int arProc[9];
struct timeval tv;
struct timezone tz;
long int presenttime()
{
	gettimeofday(&tv, &tz);
	return (tv.tv_usec);
}
int fired = 0;
void actProc1(int sig)
{
	fired++;
	if (sig == SIGUSR1)
		printf("1 %d %d got USR1 %ld\n", getpid(), getppid(), 	presenttime());
	if (sig == SIGUSR2)
		printf("1 %d %d got USR2 %ld\n", getpid(), getppid(), 	presenttime()); 
	kill(arProc[2], SIGUSR1);
	printf("1 %d %d sent USR1 %ld\n", getpid(), getppid(), 	presenttime()); 
	kill(arProc[3], SIGUSR1);
	printf("1 %d %d sent USR1 %ld\n", getpid(), getppid(), presenttime()); 
	kill(arProc[4], SIGUSR1);
	printf("1 %d %d sent USR1 %ld\n", getpid(), getppid(), presenttime()); 
//exit(0);
}
void actProc2(int sig)
{
	printf("2 %d %d got USR1 %ld\n", getpid(), getppid(), 	presenttime()); 
	kill(arProc[5], SIGUSR2);
	printf("2 %d %d sent USR2 %ld\n", getpid(), getppid(), 	presenttime()); 
	kill(arProc[6], SIGUSR2);
	printf("2 %d %d sent USR2 %ld\n", getpid(), getppid(), presenttime()); 
exit(0);
}
void actProc3(int sig)
{
	printf("3 %d %d got USR1 %ld\n", getpid(), getppid(), 	presenttime()); 
exit(0);
}
void actProc4(int sig)
{
	printf("4 %d %d got USR1 %ld\n", getpid(), getppid(), 	presenttime()); 
exit(0);
}
void actProc5(int sig)
{
	printf("5 %d %d got USR2 %ld\n", getpid(), getppid(), 	presenttime()); 
}
void actProc6(int sig)
{
	printf("6 %d %d got USR2 %ld\n", getpid(), getppid(), 	presenttime()); 
	kill(arProc[7], SIGUSR1);
	printf("6 %d %d sent USR1 %ld\n", getpid(), getppid(), 	presenttime()); 
}
void actProc7(int sig)
{
	printf("7 %d %d got USR1 %ld\n", getpid(), getppid(), 	presenttime());
	kill(arProc[8], SIGUSR1);
	printf("7 %d %d sent USR1 %ld\n", getpid(), getppid(), 	presenttime());
}
void actProc8(int sig)
{
	printf("8 %d %d got USR1 %ld\n", getpid(), getppid(), 	presenttime()); 
	//kill(arProc[1], SIGUSR1);
	//printf("8 %d %d sent USR1 %ld\n", getpid(), getppid(), 	presenttime()); 
}
void main(char *argv[], int argc[])
{
printf("I am process 0 %ld %d %d\n", presenttime(), getpid(), getppid());
	arProc[0] = getpid();
	arProc[1] = fork();
	switch (arProc[1])
	{
		case -1:
		{
			fprintf(stderr, "%s: Error creating new process", argv[0]);
			break;
		}
		case 0:
		{
printf("I am process 1 %ld %d %d\n", presenttime(), getpid(), getppid());
			arProc[1] = getpid();
			arProc[2] = fork();
			switch (arProc[2])
			{
				case -1:
					fprintf(stderr, "%s: Error creating new process", argv[0]);
				case 0:
				{
printf("I am process 2 %ld %d %d\n", presenttime(), getpid(), getppid());	
					arProc[2] = getpid();
					arProc[5] = fork();
					switch (arProc[5])
					{
						case -1:
							fprintf(stderr, "%s: Error creating new process", argv[0]);
						case 0:
						{
printf("I am process 5 %ld %d %d\n", presenttime(), getpid(), getppid());
							arProc[5] = getpid();		
							break;
						}
						default:
						{
							arProc[6] = fork();
							switch (arProc[6])
							{
								case -1:
									fprintf(stderr, "%s: Error creating new process", argv[0]);
								case 0:
								{
printf("I am process 6 %ld %d %d\n", presenttime(), getpid(), getppid());
									arProc[6] = getpid();
									arProc[7] = fork();
									switch (arProc[7])
									{
										case -1:
											fprintf(stderr, "%s: Error creating new process", argv[0]);
										case 0:
										{
printf("I am process 7 %ld %d %d\n", presenttime(), getpid(), getppid());
											arProc[7] = getpid();	
											arProc[8] = fork();
											switch (arProc[8])
											{
												case -1:
													fprintf(stderr, "%s: Error creating new process", argv[0]);
												case 0:
												{
printf("I am process 8 %ld %d %d\n", presenttime(), getpid(), getppid());
													arProc[8] = getpid();
													break;
												}
											}//end arProc[8]
										}
									}//end arProc[7]
									break;
								}
							}//end arProc[6]
						}
					}//end arProc[5]

					break;
				}
				default:
				{
					arProc[3] = fork();
					switch (arProc[3])
					{
						case -1:
							fprintf(stderr, "%s: Error creating new process", argv[0]);
						case 0:
						{
printf("I am process 3 %ld %d %d\n", presenttime(), getpid(), getppid());
							arProc[3] = getpid();	
							break;
						}
						default:
						{
							arProc[4] = fork();
							switch (arProc[4])
							{
								case -1:
									fprintf(stderr, "%s: Error creating new process", argv[0]);
								case 0:
								{
printf("I am process 4 %ld %d %d\n", presenttime(), getpid(), getppid());
									arProc[4] = getpid();		
									break;
								}
							}//end arProc[4]
						}
					}//end arProc[3]
				}
			}//end arProc[2]
		}
	}//end arProc[1]
	struct sigaction act;	
	sigemptyset(&act.sa_mask);
	act.sa_flags = 0;
//printf("pid %d ppid %d\n", getpid(), getppid());
		if (getpid() == arProc[1])
		{
//printf("arProc[0] %d arProc[1] %d\n", arProc[0], arProc[1]);
			act.sa_handler = actProc1;	
			sigaction(SIGUSR2, &act, 0);
			sigaction(SIGUSR1, &act, 0);
			pause();
		}
		if (getpid() == arProc[2])
		{
			act.sa_handler = actProc2;	
			sigaction(SIGUSR1, &act, 0);
			pause();
		}
		if (getpid() == arProc[3])
		{
			act.sa_handler = actProc3;	
			sigaction(SIGUSR1, &act, 0);
			pause();
		}
		if (getpid() ==  arProc[4])
		{
			act.sa_handler = actProc4;	
			sigaction(SIGUSR1, &act, 0);
			pause();
		}
		if (getpid() ==  arProc[5])
		{
			act.sa_handler = actProc5;	
			sigaction(SIGUSR2, &act, 0);
			pause();
		}
		if (getpid() ==  arProc[6])
		{
			act.sa_handler = actProc6;	
			sigaction(SIGUSR2, &act, 0);
			pause();
		}
		if (getpid() ==  arProc[7])
		{
			act.sa_handler = actProc7;	
			sigaction(SIGUSR1, &act, 0);
			pause();
		}
		if (getpid() ==  arProc[8])
		{
			act.sa_handler = actProc8;
			sigaction(SIGUSR1, &act, 0);
			pause();
		}
	if (getpid() == arProc[1])
		printf("well done\n");
	if (getpid() == arProc[0])
	{
		sleep(1);
		kill(arProc[1], SIGUSR2);
	}
sleep(1);
exit(1);
}
