#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/wait.h>
#include <errno.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <time.h>
#include <wait.h>

#define MAX_PROC 9 //максимальное число процессов
#define GROUP_PROC 2 //кол-во групп процессов
#define MAX_MESSAGES 101 //максимальное кол-во сообщений

extern int errno;

pid_t *procPidAr; //указаель на массив, хранящий pid всех процессов
pid_t *procPidGr;//указатель на массив, хранящий gid всех процессов
int *readyAr; //указатель на массив готовности процессов к взаимодействию
int sigSend = 0; //кол-во отосланных сигналов 
int sigGet = 0; //кол-во полученных сигналов

//удаление из строки strIn строки strDel и занесение результата в strOut
void strSegRemove(char *strIn, char *strDel, char *strOut);
//вывод сообщения об ошибке при создании процесса
void createProcEr(char progName[], int errorCode);
//проверка состояния готовности всех процессов
int checkReadyState(int *readyAr);
//вычисление текущего времени
int curTime(void);
void processHandler(struct sigaction *proceed, char progName[]);
//обработка 1-го пользовательского сигнала 
void sigProcessing1(int signo, siginfo_t *info, void *context);
//void processHandler1(struct sigaction *proceed, char progName[]);
//обработка 2-го пользовательского сигнала
void sigProcessing2(int signo, siginfo_t *info, void *context);
//обработка 3-го пользовательского сигнала
void sigProcessing3(int signo, siginfo_t *info, void *context);
//обработка 4-го пользовательского сигнала
void sigProcessing4(int signo, siginfo_t *info, void *context);
//обработка 5-го пользовательского сигнала
void sigProcessing5(int signo, siginfo_t *info, void *context);
//обработка 6-го пользовательского сигнала
void sigProcessing6(int signo, siginfo_t *info, void *context);
//обработка 7-го пользовательского сигнала
void sigProcessing7(int signo, siginfo_t *info, void *context);
//обработка 8-го пользовательского сигнала
void sigProcessing8(int signo, siginfo_t *info, void *context);


int main(int argc, char *argv[]) {
	
	pid_t procPid;
	
	strSegRemove(argv[0], "./", argv[0]);
	//использование разделяемой памяти для взаимодействия процессов
	//выделение памяти для массива, хранящего pidы всех процессов
	procPidAr = mmap(NULL, sizeof(pid_t) * MAX_PROC, PROT_READ | PROT_WRITE, 
					  MAP_ANONYMOUS | MAP_SHARED, -1, 0);
	//выделение памяти для массива, хранящего gid групп процессов
	procPidGr = mmap(NULL, sizeof(pid_t) * GROUP_PROC, PROT_READ | PROT_WRITE, 
					 MAP_ANONYMOUS | MAP_SHARED, -1, 0);
	//выделение памяти для массива о состоянии готовности процессов
	readyAr = mmap(NULL, sizeof(int) * MAX_PROC, PROT_READ | PROT_WRITE, 
					 MAP_ANONYMOUS | MAP_SHARED, -1, 0);
	//задание начальных значений массива готовности
	int i;
	for (i = 0; i < MAX_PROC; ++i)
		readyAr[i] = 1;
					 
	procPidAr[0] = getpid();
	procPidAr[1] = fork(); //порождение процесса №1
	switch (procPidAr[1]) {
		case -1: {
			createProcEr(argv[0], errno);
			return 1;
		}
		case 0: { //процесс №1
			procPidAr[1] = getpid();
			procPidAr[2] = fork();//порождение процесса №2
			switch (procPidAr[2]) {
				case -1: {
					createProcEr(argv[0], errno);
					return 1;
				}
				case 0: { //процесс №2
					procPidAr[2] = getpid();
					break;
				}
				default: { //процесс №1
					//формирование первой группы процессов
					if (setpgid(procPidAr[2], procPidAr[2]) == -1) {
						fprintf(stderr, "%d ", getpid());
						fprintf(stderr, "%s: Cannot change the group of the process %d!\n", argv[0], (int)procPidAr[2]);
						return 1;
					} else
						procPidGr[0] = procPidAr[2];
					procPidAr[3] = fork();//порождение процесса №3
					switch (procPidAr[3]) {
						case -1: {
							createProcEr(argv[0], errno);
							return 1;
						}
						case 0: { //процесс №3
							procPidAr[3] = getpid();
						//	sleep(2);
							procPidAr[4] = fork();//порождение процесса №4
							switch (procPidAr[4]) {
								case -1: {
									createProcEr(argv[0], errno);
									return 1;
								}
								case 0: {//процесс №4
									procPidAr[4] = getpid();
									procPidAr[5] = fork(); //порождение процесса №5
									switch (procPidAr[5]) {
										case -1: {
											createProcEr(argv[0], errno);
											return 1;
										}
										case 0: { //процесс №5
											procPidAr[5] = getpid();
											break;
										}
										default: {//процесс №4
											procPidAr[6] = fork(); //порождение процесса №6
											switch (procPidAr[6]) {
												case -1: {
													createProcEr(argv[0], errno);
													return -1;
												}
												case 0: {//процесс №6
													procPidAr[6] = getpid();
												//	sleep(2);
													procPidAr[7] = fork();//порождение процесса №7
													switch (procPidAr[7]) {
														case -1: {
															createProcEr(argv[0], errno);
															return 1;
														}
														case 0: {//процесс №7
															procPidAr[7] = getpid();
															procPidAr[8] = fork();//порождение процесса №8
															switch (procPidAr[8]) {
																case -1: {
																	createProcEr(argv[0], errno);
																	return 1;
																}
																case 0: {
																	procPidAr[8] = getpid();
																	break;
																}
																default: {
																	break;
																}
															} //end "switch (procPidAr[8])"
														}
													}//end "switch (procPidAr[7])"
													break;
												}
												default: {
													if (setpgid(procPidAr[6], procPidAr[6]) == -1) {
														fprintf(stderr, "%d ", getpid());
														fprintf(stderr, "%s: Cannot change the group of the process %d!\n", argv[0], (int)procPidAr[6]);
														return 1;
													} else
														procPidGr[1] = procPidAr[6];
													break;
												}
											}//end "switch (procPidAr[6])"
											break;
										}//end "default"
									}//end "switch (procPidAr[5])"
									break;
								}
								default: {
									break;
								}
							}// end "switch (procPidAr[4])"
							
							break;
						}
						default: {//процесс №1
							if (setpgid(procPidAr[3], procPidGr[0]) == -1) {
								fprintf(stderr, "%d ", getpid());
								fprintf(stderr, "%s: Cannot change the group of the process %d!\n", argv[0], (int)procPidAr[3]);
								return 1;
							}
							break;
						}
					} //end "switch (procPidAr[3])"
					break;
				}//end "default"
			} //end "switch (procPidAr[2])"
			break;
		}
		default: { //процесс №1
			break;
		}
	}//end "switch (procPidAr[1])"
	
	struct sigaction proceed, oProceed;
			
	
	sigemptyset(&proceed.sa_mask);
	proceed.sa_flags = SA_SIGINFO;
	
	if (getpid() == procPidAr[0]) {
		for (;;) {
			if (checkReadyState(readyAr)) {
				fprintf(stdout, "0 %d %d sent USR1 %d\n", (int)getpid(), (int)getppid(), curTime());
				fflush(stdout);
				if (kill(procPidAr[1], SIGUSR1) < 0) {
					fprintf(stderr, "%d %s Error: kill failed!\n", (int)getpid(), argv[0]);
					return 1;
				}
				break;
			}
		}//end for(;;)
	}
	//активация обработчика сигналов 1-го процесса
	if (getpid() == procPidAr[1]) {
		proceed.sa_sigaction = sigProcessing1;
		if ((readyAr[1] = sigaction(SIGUSR1, &proceed, &oProceed)) < 0) {
			fprintf(stderr, "%d %s Error: sigaction failed!\n", (int)getpid(), argv[0]);
			return 1;
		}
		processHandler(&proceed, argv[0]);
	}
	//активация обработчика сигналов 2-го процесса
	if (getpid() == procPidAr[2]) {
		proceed.sa_sigaction = sigProcessing2;
		if ((readyAr[2] = sigaction(SIGUSR1, &proceed, &oProceed)) < 0) {
			fprintf(stderr, "%d %s Error: sigaction failed!\n", (int)getpid(), argv[0]);
			return 1;
		}
		processHandler(&proceed, argv[0]);
	}
	//активация обработчика сигналов 3-го процесса
	if (getpid() == procPidAr[3]) {
		proceed.sa_sigaction = sigProcessing3;
		if ((readyAr[3] = sigaction(SIGUSR1, &proceed, &oProceed)) < 0) {
			fprintf(stderr, "%d %s Error: sigaction failed!\n", (int)getpid(), argv[0]);
			return 1;
		}
		processHandler(&proceed, argv[0]);
	}
	//активация обработчика сигналов 4-го процесса
	if (getpid() == procPidAr[4]) {
		proceed.sa_sigaction = sigProcessing4;
		if ((readyAr[4] = sigaction(SIGUSR1, &proceed, &oProceed)) < 0) {
			fprintf(stderr, "%d %s Error: sigaction failed!\n", (int)getpid(), argv[0]);
			return 1;
		}
		processHandler(&proceed, argv[0]);
	}
	//активация обработчика сигналов 5-го процесса
	if (getpid() == procPidAr[5]) {
		proceed.sa_sigaction = sigProcessing5;
		if ((readyAr[5] = sigaction(SIGUSR1, &proceed, &oProceed)) < 0) {
			fprintf(stderr, "%d %s Error: sigaction failed!\n", (int)getpid(), argv[0]);
			return 1;
		}
		processHandler(&proceed, argv[0]);
	}
	//активация обработчика сигналов 6-го процесса
	if (getpid() == procPidAr[6]) {
		proceed.sa_sigaction = sigProcessing6;
		if ((readyAr[6] = sigaction(SIGUSR1, &proceed, &oProceed)) < 0) {
			fprintf(stderr, "%d %s Error: sigaction failed!\n", (int)getpid(), argv[0]);
			return 1;
		}
		processHandler(&proceed, argv[0]);
	}
	//активация обработчика сигналов 7-го процесса
	if (getpid() == procPidAr[7]) {
		proceed.sa_sigaction = sigProcessing7;
		if ((readyAr[7] = sigaction(SIGUSR1, &proceed, &oProceed)) < 0) {
			fprintf(stderr, "%d %s Error: sigaction failed!\n", (int)getpid(), argv[0]);
			return 1;
		}
		processHandler(&proceed, argv[0]);
	}
	//активация обработчика сигналов 8-го процесса
	if (getpid() == procPidAr[8]) {
		proceed.sa_sigaction = sigProcessing8;
		if ((readyAr[8] = sigaction(SIGUSR1, &proceed, &oProceed)) < 0) {
			fprintf(stderr, "%d %s Error: sigaction failed!\n", (int)getpid(), argv[0]);
			return 1;
		}
		processHandler(&proceed, argv[0]);
	}
		
	//ожидание завершения 1-го процесса
	if (waitpid(procPidAr[1], NULL, 0) < 0) {
		fprintf(stderr, "%d Error: There is no such process!\n", (int)getpid());
	}

	return 0;
}

//удаление из строки strIn строки strDel и занесение результата в strOut
void strSegRemove(char *strIn, char *strDel, char *strOut) {
	
	char *bufStr; //bufStr - вспомогательная строка
	char *strDelH;
	
	bufStr = strstr(strIn, strDel);
	//если такой подстроки не найдено, то выход
	if (bufStr == NULL)
		return;
	strDelH = strDel;
	//копируем участок до strDel
	while (strIn < bufStr) {
		*strOut = *strDel;
		strIn++;
		strOut++;
		*strOut = '\0';
	}
	//пропускаем участок strDel 
	while (*strDelH) {
		strIn++;
		strDelH++;
	}
	//копируем участок после strDel
	strcpy(strOut, strIn);
	return;
}

void createProcEr(char progName[], int errorCode) {
	switch (errno) {
		fprintf(stderr, "%d ", getpid());
		case EAGAIN: {
			fprintf(stderr, "%s: Cannot create new process!"
					"\n", progName);
			break;
		}
		case ENOMEM: {
			fprintf(stderr, "%s: There is no enough memory "
					"to create the process!\n", progName);
			break;
		}
	} //end "switch (errno)"
	return;
}
//анализ готовности процессов к обработке
int checkReadyState(int *readyAr) {

	int i, resValue = 1;
	 
	for (i = 1; i < MAX_PROC; ++i) {
		if ((readyAr[i] == 1) || (readyAr[i] == -1)) { 
			resValue = 0;
			break;
		}
	}
	return resValue;
}

//нахождение текущего времени в микросекундах
int curTime(void) {
	struct timeval getTime;
	
	gettimeofday(&getTime, NULL);
	return ((int)(getTime.tv_usec));
}

void processHandler1(struct sigaction *proceed, char progName[]) {
	
	for (;;) {
			if (sigaction(SIGUSR1, proceed, NULL) < 0) {
				fprintf(stderr, "%d %s Error: sigaction failed!\n", (int)getpid(), progName);
				return;
			}
			if (sigaction(SIGTERM, proceed, NULL) < 0) {
				fprintf(stderr, "%d %s Error: sigaction failed!\n", (int)getpid(), progName);
				return;
			}
		/*	if (flag == 0) {
				fprintf(stdout, "1 %d %d sent USR1 %d\n", (int)getpid(), (int)getppid(), curTime());
				fflush(stdout);
				if (kill((-1)*procPidGr[0], SIGUSR1) < 0) {
					fprintf(stderr, "%d %s Error: kill failed!\n", (int)getpid(), progName);
					return;
				} else
					flag = 1; 
			} */
	}
	return;
}

void sigProcessing1(int signo, siginfo_t *info, void *context) {
	
	switch (signo) {
		case SIGUSR1: {
			fprintf(stdout, "1 %d %d %d got USR1 %d\n", (int)getpid(), (int)getppid(), (int) (*info).si_pid, curTime());
			fflush(stdout);
			++sigGet;
			if (sigGet < MAX_MESSAGES) {
				fprintf(stdout, "1 %d %d sent USR1 %d\n", (int)getpid(), (int)getppid(), curTime());
				fflush(stdout);
				if (kill((-1)*procPidGr[0], SIGUSR1) < 0) {
					fprintf(stderr, "%d Error: kill failed!\n", (int)getpid());
					return;
				} else
					sigSend += 4;
			} else {
				int i;
				for (i = MAX_PROC - 1; i >= 2; --i) {
					if (kill(procPidAr[i], SIGTERM) < 0) {
						fprintf(stderr, "%d Error: kill failed!\n", (int)getpid());
						return;
					}
				//	sleep(1);
				}
				for (i = 2; i < MAX_PROC; ++i)
					wait(NULL);
				fprintf(stdout, "1 %d %d finished his work after %d SIGUSR1\n", (int)getpid(), (int)getppid(), sigSend);
				fflush(stdout);
				exit(1);
			}
			break;
		}
		case SIGTERM: {
			break;
		}
	}// end switch (signo)
}

void processHandler(struct sigaction *proceed, char progName[]) {
	
	for (;;) {
		if (sigaction(SIGUSR1, proceed, NULL) < 0) {
			fprintf(stderr, "%d %s Error: sigaction failed!\n", (int)getpid(), progName);
			return;
		}
		if (sigaction(SIGTERM, proceed, NULL) < 0) {
			fprintf(stderr, "%d %s Error: sigaction failed!\n", (int)getpid(), progName);
			return;
		}
		pause();
	}
	return;
}

void sigProcessing2(int signo, siginfo_t *info, void *context) {
	switch (signo) {
		case SIGUSR1: {
			fprintf(stdout, "2 %d %d %d got USR1 %d\n", (int)getpid(), (int)getppid(), (int) (*info).si_pid, curTime());
			fflush(stdout);
			++sigGet;
			break;
		}
		case SIGTERM: {
			fprintf(stdout, "2 %d %d finished his work after %d SIGUSR1\n", (int)getpid(), (int)procPidAr[1], sigSend);
			fflush(stdout);
			//sleep(1);
			exit(1);
			break;
		}
	}// end switch (signo)
}

void sigProcessing3(int signo, siginfo_t *info, void *context) {
	switch (signo) {
		case SIGUSR1: {
			fprintf(stdout, "3 %d %d %d got USR1 %d\n", (int)getpid(), (int)getppid(), (int) (*info).si_pid, curTime());
			fflush(stdout);
			++sigGet;
			break;
		}
		case SIGTERM: {
			fprintf(stdout, "3 %d %d finished his work after %d SIGUSR1\n", (int)getpid(), (int)procPidAr[1], sigSend);
			fflush(stdout);
		//	sleep(1);
			exit(1);
			break;
		}
	}// end switch (signo)
}
void sigProcessing4(int signo, siginfo_t *info, void *context) {
	switch (signo) {
		case SIGUSR1: {
			fprintf(stdout, "4 %d %d %d got USR1 %d\n", (int)getpid(), (int)getppid(), (int) (*info).si_pid, curTime());
			fflush(stdout);
			++sigGet;
			break;
		}
		case SIGTERM: {
			fprintf(stdout, "4 %d %d finished his work after %d SIGUSR1\n", (int)getpid(), (int)procPidAr[3], sigSend);
			fflush(stdout);
		//	sleep(1);
			exit(1);
			break;
		}
	}// end switch (signo)
}
void sigProcessing5(int signo, siginfo_t *info, void *context) {
	switch (signo) {
		case SIGUSR1: {
			fprintf(stdout, "5 %d %d %d got USR1 %d\n", (int)getpid(), (int)getppid(), (int) (*info).si_pid, curTime());
			fflush(stdout);
			++sigGet;
			fprintf(stdout, "5 %d %d sent USR1 %d\n", (int)getpid(), (int)getppid(), curTime());
			fflush(stdout);
			if (kill((-1)*procPidGr[1], SIGUSR1) < 0) {
				fprintf(stderr, "%d Error: kill failed!\n", (int)getpid());
				return;
			} else
				sigSend += 3;
			break;
		}
		case SIGTERM: {
			fprintf(stdout, "5 %d %d finished his work after %d SIGUSR1\n", (int)getpid(), (int)procPidAr[4], sigSend);
			fflush(stdout);
		//	sleep(1);
			exit(1);
			break;
		}
	}// end switch (signo)
}
void sigProcessing6(int signo, siginfo_t *info, void *context) {
	switch (signo) {
		case SIGUSR1: {
			fprintf(stdout, "6 %d %d %d got USR1 %d\n", (int)getpid(), (int)getppid(), (int) (*info).si_pid, curTime());
			fflush(stdout);
			++sigGet;
			break;
		}
		case SIGTERM: {
			fprintf(stdout, "6 %d %d finished his work after %d SIGUSR1\n", (int)getpid(), (int)procPidAr[4], sigSend);
			fflush(stdout);
		//	sleep(1);
			exit(1);
			break;
		}
	}// end switch (signo)
}
void sigProcessing7(int signo, siginfo_t *info, void *context) {
	switch (signo) {
		case SIGUSR1: {
			fprintf(stdout, "7 %d %d %d got USR1 %d\n", (int)getpid(), (int)getppid(), (int) (*info).si_pid, curTime());
			fflush(stdout);
			++sigGet;
			break;
		}
		case SIGTERM: {
			fprintf(stdout, "7 %d %d finished his work after %d SIGUSR1\n", (int)getpid(), (int)procPidAr[6], sigSend);
			fflush(stdout);
		//	sleep(1);
			exit(1);
			break;
		}
	}// end switch (signo)
}
void sigProcessing8(int signo, siginfo_t *info, void *context) {
	switch (signo) {
		case SIGUSR1: {
			fprintf(stdout, "8 %d %d %d got USR1 %d\n", (int)getpid(), (int)getppid(), (int) (*info).si_pid, curTime());
			fflush(stdout);
			++sigGet;
			fprintf(stdout, "8 %d %d sent USR1 %d\n", (int)getpid(), (int)getppid(), curTime());
			fflush(stdout);
			if (kill(procPidAr[1], SIGUSR1) < 0) {
				fprintf(stderr, "%d Error: kill failed!\n", (int)getpid());
				return;
			} else
				++sigSend;
			break;
		}
		case SIGTERM: {
			fprintf(stdout, "8 %d %d finished his work after %d SIGUSR1\n", (int)getpid(), (int)procPidAr[7], sigSend);
			fflush(stdout);
	//		sleep(1);
			exit(1);
			break;
		}
	}// end switch (signo)
}
