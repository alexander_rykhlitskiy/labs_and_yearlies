#define _REENTRANT
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <errno.h>
#include <syscall.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
char *functPath(char *fullPath, char *buf)
{

  int i = strlen(fullPath)-2;
  while ((i >= 0) && (fullPath[i] != '/'))
    i--;
  char str[1000];
  int j = 0;
  while (fullPath[++i] != '\0')
    str[j++] = fullPath[i];
  str[j] = '\0';
  strcpy(buf, str);
  return(buf);
}
int copyPaste(char *fileCopy, char *dirPaste, char *nameofprogram)
{
  FILE *f1, *f2;
  char pathPaste[1000];
  errno = 0;
  f1 = fopen(fileCopy, "r");
  if (errno != 0)
  {
    printf("%s: error of opening1 %s\n", nameofprogram, fileCopy);
    return 0;
  }
  strcpy(pathPaste, dirPaste);
  char str[1000];
  strcat(pathPaste, functPath(fileCopy, str));
  errno = 0;
  f2 = fopen(pathPaste, "w");
  if (errno != 0)
  {
    printf("%s: error of opening2 %s\n", nameofprogram, pathPaste);
    return 0;
  }
  struct stat statbuf1;
  stat(fileCopy, &statbuf1);
  chmod(pathPaste, statbuf1.st_mode);

  char c;
  long int numb = 0;
  while((c = getc(f1)) != EOF)
  {
    putc(c, f2);
    numb++;
  }
  printf("%ld %s %ld\n", syscall(SYS_gettid), fileCopy, numb);
  errno = 0;
  fclose(f1);
  if (errno != 0) puts("Error of closing file1");
  errno = 0;
  fclose(f2);
  if (errno != 0) puts("Error of closing file2");
}
int printdir(char *dirictory, char *nameofprogram, char arrDir[][1000], int *numbDir)
{
  int i = 0;
  DIR *dp;
  struct dirent *entry;
  struct stat statbuf;

  if ((dp = opendir(dirictory)) == NULL)
  {
    fprintf(stderr, "%s: cannot open directory(pr): %s\n", nameofprogram, dirictory);
    return;
  }
  errno = 0;
  chdir(dirictory);
  char pathTo[500];
  if(errno != 0)
    ("%s: error of changing dirictory: %s\n", nameofprogram, dirictory);
  else
  while((entry = readdir(dp)) != NULL)
  { 
    strcpy(pathTo, dirictory);
    strcat(pathTo, entry->d_name);
    stat(pathTo, &statbuf);
    if (S_ISDIR(statbuf.st_mode))
    { 
//код для рекурсивного обхода подкатологов
      if (strcmp(".", entry->d_name) == 0 ||
         strcmp("..", entry->d_name) == 0)
      continue;
      strcat(pathTo, "/");
      printdir(pathTo, nameofprogram, arrDir, numbDir);
    }
    else
    if (S_ISREG(statbuf.st_mode))
     strcpy(arrDir[(*numbDir)++], pathTo);
  }
  chdir("..");
  closedir(dp);
}
struct parcel
{
	int state, tid;
	char pathCopy[1000], pathPaste[1000], nameofprogram[1000];
};
struct parcel arrIndic[100] = {0};
void *thread_function(void *arg)
{
	int i = *(int *)arg;
//	printf("%ld %s\n", syscall(SYS_gettid), arrIndic[i].pathCopy);
	copyPaste(arrIndic[i].pathCopy, arrIndic[i].pathPaste, arrIndic[i].nameofprogram);
	arrIndic[i].state = 1;
	pthread_exit("Thank you for the CPU time");
}
int main(int argc[], char *argv[])
{
  struct parcel buf;
  int numbThread = 0, i, j;
long int k;
  char name[1000], name1[1000];
  char arrDirCopy[3000][1000], arrDirPaste[3000][1000];
  int numbDirCopy = 0, numbDirPaste = 0;
  printdir(argv[1], functPath(argv[0], name), arrDirCopy, &numbDirCopy);
  printdir(argv[2], functPath(argv[0], name), arrDirPaste, &numbDirPaste);
  for (i = 0; i < numbDirPaste; i++)
    for (j = 0; j < numbDirCopy; j++)
    {
      if (!strcmp(functPath(arrDirPaste[i], name), functPath(arrDirCopy[j], name1)))
      {
		for (k = j; k < numbDirCopy - 1; k++)
          strcpy(arrDirCopy[k], arrDirCopy[k+1]);
        numbDirCopy--;
      }
    }

	int res;
	pthread_t a_thread;
	void *thread_result;
	int maxThread = atoi(argv[3]);
	functPath(argv[0], name);
//	for (i = 0; i < numbDirCopy; i++)
//	  puts(arrDirCopy[i]);
	j = 0;
	i = 0;
	int arrNinja[50], numbNinja = 0;
	while ((j < maxThread) && (i < numbDirCopy))
	{
//printf("label %d\n", j);
		strcpy(arrIndic[j].pathCopy, arrDirCopy[i]);
		strcpy(arrIndic[j].pathPaste, argv[2]);
		strcpy(arrIndic[j].nameofprogram, name);
		arrIndic[j].state = 2;
//puts(arrIndic[j].pathCopy);
		arrNinja[numbNinja] = j;
		res = pthread_create(&(arrIndic[j].tid), NULL, thread_function, (void *)&arrNinja[numbNinja]);//usleep(1000);
		if (numbNinja < 19)
			numbNinja++;
		else numbNinja = 0;
		if (res != 0)
		{
			perror("Thread creation failed");
			exit (EXIT_FAILURE);
		}	
		i++;
		j++;	
	}
//printf("%d\n", i);
	for (i = i; i < numbDirCopy; i++)
	{
		for (j = 0; j < maxThread; j++)
		{
//printf("state = %d\n", arrIndic[j].state);
			if (arrIndic[j].state == 1)
			{
				res = pthread_join(arrIndic[j].tid, &thread_result);
				if (res != 0)
				{
					perror("Thread join failed");
					exit(EXIT_FAILURE);
				}
			}
			if ((arrIndic[j].state == 1) || (arrIndic[j].state == 0))
			{
				strcpy(arrIndic[j].pathCopy, arrDirCopy[i]);
				strcpy(arrIndic[j].pathPaste, argv[2]);
				strcpy(arrIndic[j].nameofprogram, name);
				arrIndic[j].state = 2;
				arrNinja[numbNinja] = j;
				res = pthread_create(&(arrIndic[j].tid), NULL, thread_function, (void *)&arrNinja[numbNinja]);//usleep(1000);
				if (numbNinja < 49)
					numbNinja++;
				else numbNinja = 0;
				if (res != 0)
				{
					perror("Thread creation failed");
					exit (EXIT_FAILURE);
				}
				break;
			}
		}
		if (j == maxThread)
		{//puts("abrakadabra");
			j = 0;
			res = pthread_join(arrIndic[0].tid, &thread_result);
			if (res != 0)
			{
				perror("Thread join failed");
				exit(EXIT_FAILURE);
			}	
			strcpy(arrIndic[0].pathCopy, arrDirCopy[i]);
			strcpy(arrIndic[0].pathPaste, argv[2]);
			strcpy(arrIndic[0].nameofprogram, name);
			arrIndic[j].state = 2;
			arrNinja[numbNinja] = j;
			res = pthread_create(&(arrIndic[0].tid), NULL, thread_function, (void *)&j);//usleep(1000);
			if (numbNinja < 19)
				numbNinja++;
			else numbNinja = 0;
			if (res != 0)
			{
				perror("Thread creation failed");
				exit (EXIT_FAILURE);
			}
		}
	}
	for (j = 0; j < maxThread; j++)
	{
		if ((arrIndic[j].state == 1) || (arrIndic[j].state == 2))
		{
			res = pthread_join(arrIndic[j].tid, &thread_result);
			if (res != 0)
			{
				perror("Thread join failed");
				exit(EXIT_FAILURE);
			}
		}
	}
printf("");
//	for (j = 0; j < maxThread; j++)
//		printf("%d\n", arrIndic[j].state);
/*int numbProc = 0, pid;
  functPath(argv[0], name);
  for (i = 0; i < numbDirCopy; i++)
  {
    if (numbProc >= atoi(argv[3]))
    {  
      wait();
      numbProc--;
    }
    pid = fork();
    numbProc++;
    switch (pid)
    {
      case -1:
      { 
        printf("%s: error of createing process\n", name);
        numbProc--;
        break;
      }
      case 0:
      {
        copyPaste(arrDirCopy[i], argv[2], name);
        exit(0);
      } 
    }
  }
  while (numbProc >= 1)
  {
    numbProc--;
    wait();
  }*/
  exit(0);
}
