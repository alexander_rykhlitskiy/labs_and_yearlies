#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
char *functPath(char *fullPath, char *buf)
{

  int i = strlen(fullPath)-2;
  while ((i >= 0) && (fullPath[i] != '/'))
    i--;
  char str[100];
  int j = 0;
  while (fullPath[++i] != '\0')
    str[j++] = fullPath[i];
  str[j] = '\0';
  strcpy(buf, str);
  return(buf);
}


int copyPaste(char *fileCopy, char *dirPaste, char *nameofprogram)
{
  FILE *f1, *f2;
  char pathPaste[1000];
  errno = 0;
  f1 = fopen(fileCopy, "r");
  if (errno != 0)
  {
    printf("%s: error of opening1 %s\n", nameofprogram, fileCopy);
    return 0;
  }
  strcpy(pathPaste, dirPaste);
  char str[100];
  strcat(pathPaste, functPath(fileCopy, str));
  errno = 0;
  f2 = fopen(pathPaste, "w");
  if (errno != 0)
  {
    printf("%s: error of opening2 %s\n", nameofprogram, pathPaste);
    return 0;
  }
  struct stat statbuf1;
  stat(fileCopy, &statbuf1);
  chmod(pathPaste, statbuf1.st_mode);

  char c;
  long int numb = 0;
  while((c = getc(f1)) != EOF)
  {
    putc(c, f2);
    numb++;
  }
  printf("pid = %d %s %ld\n", getpid(), fileCopy, numb);
  errno = 0;
  fclose(f1);
  if (errno != 0) puts("Error of closing file1");
  errno = 0;
  fclose(f2);
  if (errno != 0) puts("Error of closing file2");
}

int printdir(char *dirictory, char *nameofprogram, char arrDir[][100], int *numbDir)
{
  int i = 0;
  DIR *dp;
  struct dirent *entry;
  struct stat statbuf;

  if ((dp = opendir(dirictory)) == NULL)
  {
    fprintf(stderr, "%s: cannot open directory(pr): %s\n", nameofprogram, dirictory);
    return;
  }
  errno = 0;
  chdir(dirictory);
  char pathTo[500];
  if(errno != 0)
    ("%s: error of changing dirictory: %s\n", nameofprogram, dirictory);
  else
  while((entry = readdir(dp)) != NULL)
  { 
    strcpy(pathTo, dirictory);
    strcat(pathTo, entry->d_name);
    stat(pathTo, &statbuf);
    if (S_ISDIR(statbuf.st_mode))
    { 
//код для рекурсивного обхода подкатологов
      if (strcmp(".", entry->d_name) == 0 ||
         strcmp("..", entry->d_name) == 0)
      continue;
      strcat(pathTo, "/");
      printdir(pathTo, nameofprogram, arrDir, numbDir);
    }
    else
    if (S_ISREG(statbuf.st_mode))
     strcpy(arrDir[(*numbDir)++], pathTo);
  }
  chdir("..");
  closedir(dp);
}

int main(int argc[], char *argv[])
{
  pid_t pid = 1;
  int numbProc = 0, i, j, k;
  char name[100], name1[100];
  char arrDirCopy[3000][100], arrDirPaste[3000][100];
  int numbDirCopy = 0, numbDirPaste = 0;
  printdir(argv[1], functPath(argv[0], name), arrDirCopy, &numbDirCopy);
  printdir(argv[2], functPath(argv[0], name), arrDirPaste, &numbDirPaste);
  for (i = 0; i < numbDirPaste; i++)
    for (j = 0; j < numbDirCopy; j++)
    {
      if (!strcmp(functPath(arrDirPaste[i], name), functPath(arrDirCopy[j], name1)))
      {
		for (k = j; k < numbDirCopy - 1; k++)
          strcpy(arrDirCopy[k], arrDirCopy[k+1]);
        numbDirCopy--;
      }
    }
  functPath(argv[0], name);
  for (i = 0; i < numbDirCopy; i++)
  {
    if (numbProc >= atoi(argv[3]))
    {  
      wait();
      numbProc--;
    }
    pid = fork();
    numbProc++;
    switch (pid)
    {
      case -1:
      { 
        printf("%s: error of createing process\n", name);
        numbProc--;
        break;
      }
      case 0:
      {
        copyPaste(arrDirCopy[i], argv[2], name);
        exit(0);
      } 
      }
    }
  while (numbProc >= 1)
  {
    numbProc--;
    wait();
  }
  exit(0);
}
