#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct info {
   int i;
   pid_t pid;
   long time;
} ;

void sending(int[][2], int, int, int, int, int);

int main()
{
	int fd_mas[9][2];
	int i;
	int res;
	char emsg[50];

 for(i = 0; i < 9; i++) {
    res = pipe(fd_mas[i]);
    if(res) {
       perror("lr7: cannot creat pipe");
       exit(1);
    }
 }

 for(i = 0; i < 3; i++) {
    res = fork();
    switch(res) {
       case -1 : 
          perror("lr7: cannot creat process");
          exit(1);;
       case 0: 
          sending(fd_mas, fd_mas[i][0], fd_mas[2*i+3][1], fd_mas[2*i+4][1], fd_mas[5-i][0], fd_mas[8-i][0]);
          break;;
    }
 }  
 for(i = 0; i < 3; i++) {
    close(fd_mas[i][0]);
 }	  	

 for(i = 3; i < 9; i++) {
    close(fd_mas[i][0]);
    close(fd_mas[i][1]);
 }
 sleep(3);

 for(i = 0; i < 3; i++) {
    write(fd_mas[i][1], "end", 3);
    (void)close(fd_mas[i][1]);
 }

 wait(NULL); wait(NULL); wait(NULL);
 exit(0);
}

void sending(int fd_mas[][2], int fd_mr, int fd_w1, int fd_w2, int fd_r1, int fd_r2)
{
	int i, j, max_d, res1, res2;
	struct timeval tv;
	struct info data, g_data;
	pid_t my_pid;
	fd_set set;

 max_d = fd_mr;
 
 for(i = 0; i < 9; i++) {
    if( (fd_mas[i][1] != fd_w1) && (fd_mas[i][1] != fd_w2) ) 
       close(fd_mas[i][1]);
    else if(fd_mas[i][1] > max_d) max_d = fd_mas[i][1];
    if( (fd_mas[i][0] != fd_r1) && (fd_mas[i][0] != fd_r2) && (fd_mas[i][0] != fd_mr) ) 
       close(fd_mas[i][0]);
    else if(fd_mas[i][0] > max_d) max_d = fd_mas[i][0];
 }

 i = 1; j = 1; 
 data.pid = getpid(); 
 while(1) {
    FD_ZERO(&set);
    FD_SET(fd_w1, &set);
    FD_SET(fd_w2, &set);
    select(max_d+1, NULL, &set, NULL, NULL);
    gettimeofday(&tv, NULL);
    if(FD_ISSET(fd_w1, &set)) {
       data.i = i++;
       gettimeofday(&tv, NULL);
       data.time = tv.tv_usec;
       write(fd_w1, &data, sizeof(struct info));
    printf("%d отправил: %ld\n", data.pid, data.time);
    }
    if(FD_ISSET(fd_w2, &set)) {
       data.i = j++;
       gettimeofday(&tv, NULL);
       data.time = tv.tv_usec;
       write(fd_w2, &data, sizeof(struct info));
    printf("%d отправил: %ld\n", data.pid, data.time);
    }
    FD_ZERO(&set);
    FD_SET(fd_mr, &set);
    FD_SET(fd_r1, &set);
    FD_SET(fd_r2, &set);  
    select(max_d+1, &set, NULL, NULL, NULL);
    if(FD_ISSET(fd_mr, &set)) {
       close(fd_w1); close(fd_w2); close(fd_mr);
       break;
    }
    if(FD_ISSET(fd_r1, &set)) {
       read(fd_r1, &g_data, sizeof(struct info));
       printf("%d получил %d от %d: %ld\n", data.pid, g_data.i, g_data.pid, g_data.time);
    }
    if(FD_ISSET(fd_r2, &set)) {
       read(fd_r2, &g_data, sizeof(struct info));
       printf("%d получил %d от %d: %ld\n", data.pid, g_data.i, g_data.pid, g_data.time);
   }
 }
 res1 = read(fd_r1, &data, sizeof(struct info));
 res2 = read(fd_r2, &g_data, sizeof(struct info));
 while(res1 > 0 || res2 > 0) {
    if(res1 > 0)  {}
 //      printf("%d получил %d от %d: %ld\n", getpid(), data.i, data.pid, data.time);
    if(res2 > 0) 
       printf("%d получил %d от %d: %ld\n", getpid(), g_data.i, g_data.pid, g_data.time);
    res1 = read(fd_r1, &data, sizeof(struct info));
    res2 = read(fd_r2, &g_data, sizeof(struct info));
 }
 close(fd_r1); close(fd_r2);
 exit(0);
}
