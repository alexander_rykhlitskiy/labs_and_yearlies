#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
char *path(char *fullPath)
{
  int i = strlen(fullPath);
  while ((i >= 0) && (fullPath[i] != '/'))
    i--;
  i++;
  char *str;
  str = &fullPath[i];
  return(str);
}
int main(int argc[], char *argv[])
{
  FILE *f1, *f2;
  char pathPaste[1000];
  f1 = fopen(argv[1], "r");
  if (errno != 0) puts("Error of opening file1");
  strcpy(pathPaste, argv[2]);
//  puts(pathPaste);
  strcat(pathPaste, path(argv[1]));
  puts(pathPaste);
  f2 = fopen(pathPaste, "w");
  if (errno != 0) puts("Error of opening file2");

  struct stat statbuf1, statbuf2;
 // mode_t modes1, modes2;
  stat(argv[1], &statbuf1);
 // modes1 = statbuf1.st_mode;
 // stat(argv[2], &statbuf2);
 // modes2 = statbuf1.st_mode;
 // printf("%d\n", statbuf1.st_size);
  chmod(argv[2], statbuf1.st_mode);

  char c;
  while((c = getc(f1)) != EOF)
    putc(c, f2);
//  chmod(argv[1], 0777);
  fclose(f1);
  if (errno != 0) puts("Error of closing file1");
  fclose(f2);
  if (errno != 0) puts("Error of closing file2");
  printf("well done\n");
  //puts(path("/home/sasha/s"));
}
