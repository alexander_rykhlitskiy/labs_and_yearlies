#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <dirent.h>
#include <time.h>
#include <errno.h>
void workstr(char str[11], int *day, int *month, int *year)
{
  char ch[10];
  int i = 0, j = 0, cases = 0;
  while(str[i] != '\0')
  {
    cases++;
    j = 0;
    while ((str[i] != '.') && (str[i] != '\0'))
    {
      ch[j] = str[i];
      j++;
      i++;
    }
    i++;
    ch[j] = '\0';
    switch (cases)
    {
      case 1: *day = atoi(ch);break;
      case 2: *month = atoi(ch);break;
      case 3: *year = atoi(ch);break;
    }
  }
}
void printdir(char *dir, FILE *f, int sizeDown, int sizeUp, int dateDown, int dateUp, char *nameofprogram)
{
  DIR *dp;
  struct dirent *entry;
  struct stat statbuf;

  if ((dp = opendir(dir)) == NULL)
  {
    fprintf(stderr, "%s: cannot open directory: %s\n", nameofprogram, dir);
    return;
  }
  errno = 0;
  chdir(dir);
  if(errno != 0)
    ("%s: error of changing dirictory: %s\n", nameofprogram, dir);
 else
  while((entry = readdir(dp)) != NULL)
  {
    stat(entry->d_name, &statbuf);
    if (S_ISDIR(statbuf.st_mode))
    {
      if (strcmp(".", entry->d_name) == 0 ||
         strcmp("..", entry->d_name) == 0)
      continue;
      char path[500];
      strcpy(path, dir);
      strcat(path, "/");
      strcat(path, entry->d_name);
      printdir(path, f, sizeDown, sizeUp, dateDown, dateUp, nameofprogram);
    }
    else
    {
      int size, Date;
      struct stat statbuf;
      stat(entry->d_name, &statbuf);
      size = statbuf.st_size;
      time_t ttt;
      char buffer[256];
      ttt = statbuf.st_atime;
      Date = statbuf.st_atime;
      struct tm *l;
      l = localtime(&ttt);
      if ((size <= sizeUp) && (size >= sizeDown) && (Date <= dateUp) && (Date >= dateDown))
      {
        fprintf(f, "%s/%s   %d  ",dir,entry->d_name,size);
        strftime(buffer, 256, "%d.%m.%Y\n", l);
        fputs(buffer, f);
        printf("%s/%s   %d  ", dir,entry->d_name,size);
        strftime(buffer, 256, "%d.%m.%Y\n", l);
        fputs(buffer, stdout);
      }
    }
  }
  if (!feof(f))//не надо
    ("%s: error of reading: %s\n", nameofprogram, entry->d_name);
  chdir("..");
  closedir(dp);
}
int main(int argc[], char *argv[])
{
  int day, month, year;
  int dateDown, dateUp;
  char str[20];
  strcpy(str, argv[5]);
  workstr(str, &day, &month, &year);
  struct tm t;
  t.tm_mday = day;
  t.tm_mon = month-1;
  t.tm_year = year - 1900;
  t.tm_sec = 1;
  t.tm_min = 1;
  t.tm_hour = 1;
  dateDown = mktime(&t);
  printf("%d  %d  %d\n",day, month, year);

  strcpy(str, argv[6]);
  workstr(str, &day, &month, &year);
  t.tm_mday = day;
  t.tm_mon = month-1;
  t.tm_year = year - 1900;
  t.tm_sec = 1;
  t.tm_min = 1;
  t.tm_hour = 1; 
  dateUp = mktime(&t);

  int i = 2;
  while(argv[0][i] != '\0')
  {
    str[i - 2] = argv[0][i];
    i++;
  }
  str[i - 2] = '\0';
  FILE *f;
  f = fopen(argv[2], "w");
  if(errno != 0)
    ("%s: error of opening file\n", argv[0]);
   else
   printdir(argv[1], f, atoi(argv[3]), atoi(argv[4]), dateDown, dateUp, str);
  if(fclose(f) != 0)
    printf("%s: error of reading file\n", argv[0]);
  exit(0);
}
