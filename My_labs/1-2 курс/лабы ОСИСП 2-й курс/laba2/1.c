#include <stdio.h>
#include <errno.h>
int main(int argc[], char *argv[])
{
  extern int errno;
  FILE *f, *f1;
  f = fopen (argv[1], "w+");
  if (errno != 0)
    puts("Error of opening the file");
  char a;
  a = getchar();
  while(a != '0')
  {
    fputc(a, f);
    if (errno != 0)
      puts("Error of writing in file");
    a = getchar();
  }
  fclose(f);
  if (errno != 0)
    puts("Error of closeing the file");
  return 0;
}
