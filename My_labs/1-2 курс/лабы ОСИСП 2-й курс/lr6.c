#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/sem.h>
#include <pthread.h>
#include <errno.h>
#include <syscall.h>

#define BUF_SIZE 102400

union semum {
   int val;
   struct semid_ds *buf;
   unsigned short *array;
} ;

struct info {
   char input[50];
   char output[50];
   int num, curr_num;
   mode_t perm;
   off_t size;
} ;

	int semP, semR, semW;

void *copy_thr(void*);
void setsemval(int, int);
void sem_p(int);
void sem_v(int);

int main(int argc, char **argv)
{
	struct stat stbf;
	static struct info *arg_thr;
	int n, res, tr, i;
        int ex_st;
        short int set[2];
	pthread_t *a_thread;

 if(argc != 4) {
   fprintf(stderr, "lr6: Wrong number of aguments\n");
   exit(1);
 }
 semP = semget((key_t)1234, 1, 0666|IPC_CREAT);
 semR = semget((key_t)1235, 1, 0666|IPC_CREAT);
 semW = semget((key_t)1236, 1, 0666|IPC_CREAT); 
 if( (semP == -1) || (semR == -1) || (semW == -1)) { 
    perror("lr6: semget failed");
    exit(1);
 }
 setsemval(semP, 0);
 setsemval(semR, 0);
 setsemval(semW, 0);

 lstat(argv[1], &stbf);
 n = atoi(argv[3]);
 a_thread = calloc(n, sizeof(pthread_t));
 arg_thr = malloc(sizeof(struct info));
 n--; 

 strcpy(arg_thr->input, argv[1]);  
 strcpy(arg_thr->output, argv[2]);
 arg_thr->perm = stbf.st_mode%1024;
 arg_thr->size = stbf.st_size;
 arg_thr->num = n+1;

 for(i = 0; i < n; i++) {
    arg_thr->curr_num = i; 
    res = pthread_create(&(a_thread[i]), NULL, copy_thr, (void*)arg_thr);
    if(res != 0) {
       perror("lr6: pthread_create failed");
       exit(1);
    }
    sem_p(semP);
 }
 sem_v(semR);
 sem_v(semW);
 arg_thr->curr_num = i;
 copy_thr((void*)arg_thr);
 for(i = 0; i < n; i++) {
    res = pthread_join(a_thread[i], (void*)&ex_st);
    if(ex_st) {
       perror("lr6: fatal error");
       exit(1);
    }
 }
 semctl(semP, 0, IPC_RMID);
 semctl(semR, 0, IPC_RMID);
 semctl(semW, 0, IPC_RMID);
 puts("Done.");
 exit(0);
}

void setsemval(int sem, int value)
{
	union semum arg;
 
 arg.val = value;
 semctl(sem, 0, SETVAL, arg);
}

void sem_p(int sem)
{
	struct sembuf s;
 
 s.sem_num = 0;
 s.sem_op = -1;
 s.sem_flg = 0;
 semop(sem, &s, 1);
}

void sem_v(int sem)
{
	struct sembuf s;
 
 s.sem_num = 0;
 s.sem_op = 1;
 s.sem_flg = 0;
 semop(sem, &s, 1);
}

void *copy_thr(void *arg)
{
	int fwr, fr, n;	
        struct info *targ;
        char emsg[80], buf[BUF_SIZE];
        off_t lb, rb, i;

 targ = (struct info*)arg;
 n = targ->curr_num;
 fwr = open(targ->output, O_WRONLY|O_CREAT|O_TRUNC, targ->perm);
 if(errno != 0) {
    strcpy(emsg, "lr5: "); strcat(emsg, targ->output);
    perror(emsg);
    exit(1);
 }
 sem_v(semP); 
 fr = open(targ->input, O_RDONLY);
 if(errno != 0) {
    strcpy(emsg, "lr5: "); strcat(emsg, targ->input);
    perror(emsg);
    exit(1);
 }
 lb = ((float)targ->size / targ->num) * n + 0.5;
 rb = ((float)targ->size / targ->num) * (n + 1) - 0.5;
 printf("%ld %ld %ld\n", syscall(SYS_gettid), (long)lb, (long)rb);
 if(targ->num == (n+1)) puts("Please wait...");
 i = (long)rb - (long)lb + 1;
 while((lb + BUF_SIZE) <= rb) {
    sem_p(semR);
    lseek(fr, lb, SEEK_SET);
    read(fr, buf, BUF_SIZE);
    sem_v(semR);
    sem_p(semW);
    lseek(fwr, lb, SEEK_SET); 
    write(fwr, buf, BUF_SIZE);
    sem_v(semW);
    lb += BUF_SIZE;
 }
 sem_p(semR);
 lseek(fr, lb, SEEK_SET);
 read(fr, buf, rb-lb+1);
 sem_v(semR);
 sem_p(semW);
 lseek(fwr, lb, SEEK_SET); 
 write(fwr, buf, rb-lb+1);
 sem_v(semW);
 close(fwr);
 close(fr);
 printf("%ld has copied %ld bytes\n", syscall(SYS_gettid), (long)i);
 if(targ->num != (n+1))
    pthread_exit((void*)0);
}
