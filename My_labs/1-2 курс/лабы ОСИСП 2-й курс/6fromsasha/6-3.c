#define _FILE_OFFSET_BITS 64

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/sem.h>
#include <pthread.h>
#include <errno.h>
#include <syscall.h>

#define SIZE (104857600 / 1024)

union semum {
  int              val;
  struct semid_ds *buf;
  unsigned short  *array;
};

struct file_info {
  char   name_in[512];
  char   name_out[512];
  mode_t access;
  off_t  beg;
  off_t  end;
};


char       modulname[128];
extern int errno;
char      *file_in;
char      *file_out;
int        N;
int        sem_read,
           sem_write,
           sem_pthreads;

void *file_copy       (void*);
void  sem_operation   (int, int);
void  pthreads_create (void);
int   gettid          (void);

int main(int argc, char **argv)
{
   int         i,j=0;
   int         set_1,set_2,set_3;
   FILE       *file_open;
   union semum sem_init;
   for (i=2; i<=strlen(argv[0]); i++) {
     modulname[j]=argv[0][i];
     j++;
   }
   if (argc == 4) {
     file_in=argv[1];
     file_out=argv[2];
     N=atoi(argv[3]);
     if (N > 1)
       N--;
     sem_pthreads = semget((key_t)3333, 1, 0666|IPC_CREAT);
     sem_read     = semget((key_t)7777, 1, 0666|IPC_CREAT);
     sem_write    = semget((key_t)9999, 1, 0666|IPC_CREAT); 
     if((sem_write == -1) || (sem_read == -1) || (sem_pthreads == -1)) { 
       fprintf(stderr,"%d %s %s\n",gettid(),modulname,strerror(errno));
       exit(EXIT_FAILURE);
     }
     sem_init.val=0;
     set_1=semctl(sem_read,     0, SETVAL, sem_init);
     set_2=semctl(sem_write,    0, SETVAL, sem_init);
     set_3=semctl(sem_pthreads, 0, SETVAL, sem_init);
     if((set_1 != 0) || (set_2 != 0) || (set_3 != 0)) { 
       fprintf(stderr,"%d %s %s\n",gettid(),modulname,strerror(errno));
       exit(EXIT_FAILURE);
     }
     pthreads_create();
   } else {
     fprintf(stderr,"%d %s Too few arguments\n",gettid(),modulname);
     return -1;
    }
   semctl(sem_pthreads, 0, IPC_RMID);
   semctl(sem_read,     0, IPC_RMID);
   semctl(sem_write,    0, IPC_RMID);
   exit(EXIT_SUCCESS);
}

void pthreads_create(void)
{
  int i;
  struct stat      file_stat;
  struct file_info file_arguments;
  pthread_t        pthreads[N];
  unsigned long    part_size;
  unsigned long    file_pos_beg=0;
  unsigned long    file_pos_end=0;
  lstat(file_in, &file_stat);
  strcpy(file_arguments.name_in, file_in);  
  strcpy(file_arguments.name_out, file_out);
  file_arguments.access = file_stat.st_mode;
  part_size=file_stat.st_size / N;
  file_pos_end+=part_size;
  remove(file_out);
  for(i = 0; i<N; i++) { 
    file_arguments.beg=file_pos_beg;
    file_arguments.end=file_pos_end;
    if (pthread_create(&pthreads[i],NULL,(void*)file_copy,(void*)(&file_arguments))!=0) {
      fprintf(stderr,"%d %s %s\n",gettid(),modulname,strerror(errno));
      exit(EXIT_FAILURE);
    }
    if (i == (N-2)) {
        file_pos_beg+=part_size;
        file_pos_end=file_stat.st_size;
    } else {
        file_pos_beg+=part_size;
        file_pos_end+=part_size;
      }
    sem_operation(sem_pthreads,-1);
  }
  sem_operation(sem_read,1); 
  sem_operation(sem_write,1); 
  for(i = 0; i<N; i++) {
    if(pthread_join(pthreads[i],NULL)!=0)
      {
        fprintf(stderr,"%d %s %s\n",gettid(),modulname,strerror(errno));
        exit(EXIT_FAILURE);
      }
  }
}

void sem_operation(int sem, int operation)
{
  struct sembuf sem_operation;
  sem_operation.sem_num=0;
  sem_operation.sem_op=operation;
  sem_operation.sem_flg=0;
  semop(sem, &sem_operation, 1);
}

void *file_copy(void *arg)
{
  int               file_input;
  int               file_output;
  struct file_info *file_attributes;
  char              buf[SIZE];
  off_t             position_beg;
  off_t             position_end;
  off_t             print,blocs_was_write;
  off_t             bytes_was_write;
  off_t             total_size_was_write;
  
  blocs_was_write=0;
  total_size_was_write=0;
  file_attributes = (struct file_info*)arg;
  file_output = open(file_attributes->name_out, O_WRONLY|O_CREAT|O_TRUNC, file_attributes->access);
  if(!file_output) {
    fprintf(stderr,"%d %s %s\n",gettid(),modulname,strerror(errno));
    exit(EXIT_FAILURE);
  }
  file_input = open(file_attributes->name_in, O_RDONLY);
  if(!file_output) {
    fprintf(stderr,"%d %s %s\n",gettid(),modulname,strerror(errno));
    exit(EXIT_FAILURE);
  }
  position_beg=file_attributes->beg;
  position_end=file_attributes->end;
print=position_beg;
  sem_operation(sem_pthreads,1);
  while((position_beg + SIZE) <= position_end) {
    sem_operation(sem_read,-1); 
    lseek(file_input, position_beg, SEEK_SET);
    read(file_input, buf, SIZE);
    sem_operation(sem_read,1); 

    sem_operation(sem_write,-1); 
    lseek(file_output, position_beg, SEEK_SET); 
    write(file_output, buf, SIZE);
    sem_operation(sem_write,1);
    blocs_was_write++;
    position_beg += SIZE;
  }
  bytes_was_write=position_end-position_beg;
  sem_operation(sem_read,-1); 
  lseek(file_input, position_beg, SEEK_SET);
  read(file_input, buf, position_end-position_beg);
  sem_operation(sem_read,1); 

  sem_operation(sem_write,-1); 
  lseek(file_output, position_beg, SEEK_SET); 
  write(file_output, buf, position_end-position_beg);
  sem_operation(sem_write,1);

  close(file_input);
  close(file_output);
  blocs_was_write*=SIZE;
  total_size_was_write=blocs_was_write+bytes_was_write;

  sem_operation(sem_write,-1); 
  printf("%d %d %ld\n",gettid(),(int)print,(unsigned long)total_size_was_write);
  sem_operation(sem_write,1);
  pthread_exit(NULL);
}

int gettid(void) {
  return syscall(SYS_gettid);
}
