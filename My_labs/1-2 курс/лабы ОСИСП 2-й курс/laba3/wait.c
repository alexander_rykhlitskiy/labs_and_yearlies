#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
void main()
{
  pid_t pid;
  char *message;
  int n;
  int exit_code;

  printf("begin\n");
  pid = fork();
  printf("pid = %d\n", pid);
  switch(pid)
  {
    case -1:
      perror("fork failed");
      exit(1);
    case 0:
      message = "This is the child";
      n = 5;
      break;
    default:
      message = "This is the parent";
      n = 3;
      break;
  }
  for(; n > 0; n--)
  {
    puts(message);
    sleep(1);
  }
  if (pid != 0)
  {
    int stat_val;
    pid_t child_pid;
    child_pid = wait(&stat_val);
    printf("Child has finished: PID = %d\n", child_pid);
    if (WIFEXITED(stat_val))
      printf("Child exited with code %d\n", WEXITSTATUS(stat_val));
    else
      printf("Child terminated abnormally\n");
  }
  if (pid == 0)
    printf("EndOfChild\n");
  else
    printf("EndOfParent\n");
  exit(0);
}
