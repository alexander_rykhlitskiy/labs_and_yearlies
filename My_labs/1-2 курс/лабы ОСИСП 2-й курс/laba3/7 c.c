#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>

//&&&&&&&&&&&&&&&&&&&&
char *path(char *fullPath)
{
  int i = strlen(fullPath);
  while ((i >= 0) && (fullPath[i] != '/'))
    i--;
  i++;
  char *str;
  str = &fullPath[i];
  return(str);
}

//&&&&&&&&&&&&&&&&&&&&
int copyPaste(char *strArgv1, char *strArgv2, char *nameofprogram)
{
  FILE *f1, *f2;
  char pathPaste[1000];
  errno = 0;
  f1 = fopen(strArgv1, "r");
  if (errno != 0)
  {
    printf("%s: error of opening %s\n", nameofprogram, strArgv1);
    return 0;
  }
  strcpy(pathPaste, strArgv2);
  strcat(pathPaste, path(strArgv1));
  errno = 0;
  f2 = fopen(pathPaste, "w");
  if (errno != 0)
  {
    printf("%s: error of opening %s\n",nameofprogram, pathPaste);
    return 0;
  }
  struct stat statbuf1, statbuf2;
  stat(strArgv1, &statbuf1);
  chmod(pathPaste, statbuf1.st_mode);

  char c;
  long int numb = 0;
  while((c = getc(f1)) != EOF)
  {
    putc(c, f2);
    numb++;
  }
  printf("pid = %d %s %ld\n", getpid(), strArgv1, numb);
  errno = 0;
  fclose(f1);
  if (errno != 0) puts("Error of closing file1");
  errno = 0;
  fclose(f2);
  if (errno != 0) puts("Error of closing file2");
}

//&&&&&&&&&&&&&&&&&&&&
int scanDir(char *dir, char *name, char *nameofprogram)
{
  DIR *dp;
  struct dirent *entry;
  struct stat statbuf;
  if ((dp = opendir(dir)) == NULL)
  {
    fprintf(stderr, "%s: cannot open directory(sc): %s\n", nameofprogram, dir);
    return;
  }
  errno = 0;
  chdir(dir);
  if(errno != 0)
    ("%s: error of changing dirictory: %s\n", nameofprogram, dir);
 else
  while((entry = readdir(dp)) != NULL)
  {
    stat(entry->d_name, &statbuf);
    if ((!S_ISDIR(statbuf.st_mode)) && (!strcmp(entry->d_name, name)))
        return 1;
  }
  chdir("..");
  closedir(dp);
  return 0;
}
//&&&&&&&&&&&&&&&&&&&&
int printdir(char *dirCopy, char *dirPaste, char *nameofprogram, int limit)
{
  int i = 0;
  FILE *f;
        f = fopen("/home/sasha/file", "w");
        fprintf(f, "%d", i);
        fclose(f);
  pid_t pid = 1;
  DIR *dp;
  struct dirent *entry;
  struct stat statbuf;

  if ((dp = opendir(dirCopy)) == NULL)
  {
    fprintf(stderr, "%s: cannot open directory(pr): %s\n", nameofprogram, dirCopy);
    return;
  }
  errno = 0;
  chdir(dirCopy);
  char str[100];
  if(errno != 0)
    ("%s: error of changing dirictory: %s\n", nameofprogram, dirCopy);
 else
  while(((entry = readdir(dp)) != NULL) && (pid != 0))
  { 
    strcpy(str, dirCopy);
    strcat(str, entry->d_name);
    stat(str, &statbuf);
    if (S_ISDIR(statbuf.st_mode))
    { 
//код для рекурсивного обхода подкатологов
      if (strcmp(".", entry->d_name) == 0 ||
         strcmp("..", entry->d_name) == 0)
      continue;
      char path[500];
      strcpy(path, dirCopy);
      strcat(path, "/");
      strcat(path, entry->d_name);
      strcat(path, "/");
      printdir(path, dirPaste, nameofprogram, limit);
    }
    else
    if (S_ISREG(statbuf.st_mode))
    {
      if (!scanDir(dirPaste, entry->d_name, nameofprogram))
      {
	    f = fopen("/home/sasha/file", "r");
            fscanf(f, "%d", &i);
            fclose(f);
          if (i >= limit)
          {
            wait();
            f = fopen("/home/sasha/file", "w");
            i--;
            fprintf(f, "%d", i);
            fclose(f);
          }
        pid = fork();
	f = fopen("/home/sasha/file", "r");
        fscanf(f, "%d", &i);
        fclose(f);
        f = fopen("/home/sasha/file", "w");
        i++;
        fprintf(f, "%d", i);
        fclose(f);
        switch (pid)
        {
        case -1:
        { 
          printf("%s: error of createing process\n", nameofprogram);
	  f = fopen("/home/sasha/file", "r");
          fscanf(f, "%d", &i);
          fclose(f);
          f = fopen("/home/sasha/file", "w");
          i--;
          fprintf(f, "%d", i);
          fclose(f);
          break;
        }
        case 0:
        {
          copyPaste(str, dirPaste, nameofprogram);
	 /* f = fopen("/home/sasha/file", "r");
          fscanf(f, "%d", &i);
          fclose(f);
          f = fopen("/home/sasha/file", "w");
          i--;
          fprintf(f, "%d", i);
          fclose(f);*/
          return 1;
        } 
        }
      }
    //  printf("%s\n", str);
    }
  }
  chdir("..");
  closedir(dp);

}

int main(int argc[], char *argv[])
{
	FILE *f;
        f = fopen("/home/sasha/file", "w");
        fprintf(f, "%d", 0);
        fclose(f);
  char str[20];
  int i = 2;
  while(argv[0][i] != '\0')
  {
    str[i - 2] = argv[0][i];
    i++;
  }
  str[i - 2] = '\0';

   printdir(argv[1], argv[2], str, atoi(argv[3]));
  exit(0);
}
