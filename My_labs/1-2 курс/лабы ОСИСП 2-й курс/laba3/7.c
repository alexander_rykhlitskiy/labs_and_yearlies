#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
char *functPath(char *fullPath)
{
  int i = strlen(fullPath);
  while ((i >= 0) && (fullPath[i] != '/'))
    i--;
  i++;
  char *str;
  str = &fullPath[i];
  return(str);
}


int copyPaste(char *fileCopy, char *dirPaste, char *nameofprogram)
{
  FILE *f1, *f2;
  char pathPaste[1000];
  errno = 0;
  f1 = fopen(fileCopy, "r");
  if (errno != 0)
  {
    printf("%s: error of opening1 %s\n", nameofprogram, fileCopy);
    return 0;
  }
  strcpy(pathPaste, dirPaste);
  strcat(pathPaste, functPath(fileCopy));
  errno = 0;
  f2 = fopen(pathPaste, "w");
  if (errno != 0)
  {
    printf("%s: error of opening2 %s\n", nameofprogram, pathPaste);
    return 0;
  }
  struct stat statbuf1;
  stat(fileCopy, &statbuf1);
  chmod(pathPaste, statbuf1.st_mode);

  char c;
  long int numb = 0;
  while((c = getc(f1)) != EOF)
  {
    putc(c, f2);
    numb++;
  }
  printf("pid = %d %s %ld\n", getpid(), fileCopy, numb);
  errno = 0;
  fclose(f1);
  if (errno != 0) puts("Error of closing file1");
  errno = 0;
  fclose(f2);
  if (errno != 0) puts("Error of closing file2");
}


int scanDir(char *dir, char *name, char *nameofprogram)
{
  DIR *dp;
  struct dirent *entry;
  struct stat statbuf;
  if ((dp = opendir(dir)) == NULL)
  {
    fprintf(stderr, "%s: cannot open directory(sc): %s\n", nameofprogram, dir);
    return 1;
  }
  errno = 0;
  chdir(dir);
  if(errno != 0)
    ("%s: error of changing dirictory: %s\n", nameofprogram, dir);
 else
  while((entry = readdir(dp)) != NULL)
  {
    stat(entry->d_name, &statbuf);
    if ((S_ISREG(statbuf.st_mode)) && (!strcmp(entry->d_name, name)))
        return 1;
  }
  chdir("..");
  closedir(dp);
  return 0;
}

char arrayOfPathes[3000][100];
int countOfPathes = -1;
int printdir(char *dirCopy, char *dirPaste, char *nameofprogram)
{

  int i = 0;
  DIR *dp;
  struct dirent *entry;
  struct stat statbuf;

  if ((dp = opendir(dirCopy)) == NULL)
  {
    fprintf(stderr, "%s: cannot open directory(pr): %s\n", nameofprogram, dirCopy);
    return;
  }
  errno = 0;
  chdir(dirCopy);
  char pathTo[100];
  if(errno != 0)
    ("%s: error of changing dirictory: %s\n", nameofprogram, dirCopy);
  else
  while((entry = readdir(dp)) != NULL)
  { 
    strcpy(pathTo, dirCopy);
    strcat(pathTo, entry->d_name);
    stat(pathTo, &statbuf);
    if (S_ISDIR(statbuf.st_mode))
    { 
//код для рекурсивного обхода подкатологов
      if (strcmp(".", entry->d_name) == 0 ||
         strcmp("..", entry->d_name) == 0)
      continue;
      strcat(pathTo, "/");
      printdir(pathTo, dirPaste, nameofprogram);
    }
    else
    if (S_ISREG(statbuf.st_mode))
    {
puts(entry->d_name);
      if (!scanDir(dirPaste, entry->d_name, nameofprogram))
      {
        countOfPathes++;
        strcpy(arrayOfPathes[countOfPathes], pathTo);
      }
    }
  }
    //  printf("%s\n", str);
  chdir("..");
  closedir(dp);

}

int main(int argc[], char *argv[])
{
  pid_t pid = 1;
  int numbProc = 0;
  char name[20];
  int i = 2;
  while(argv[0][i] != '\0')
  {
    name[i - 2] = argv[0][i];
    i++;
  }
  name[i - 2] = '\0';
  printdir(argv[1], argv[2], name);

  for (i = 0; i <= countOfPathes; i++)
    puts(arrayOfPathes[i]);
      for (i = 0; i <= countOfPathes; i++)
      {
        if (numbProc >= (atoi(argv[3]) - 1))
        {  
          wait();
          numbProc--;
        }
        pid = fork();
        numbProc++;
        switch (pid)
        {
        case -1:
        { 
          printf("%s: error of createing process\n", name);
          numbProc--;
          break;
        }
        case 0:
        {
          copyPaste(arrayOfPathes[i], argv[2], name);
          exit(0);
        } 
        }
      }
  while (numbProc >=1)
  {
    numbProc--;
    wait();
  }
  exit(0);
}
