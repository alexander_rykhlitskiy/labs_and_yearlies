#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
void main()
{
  pid_t pid;
  char *message;
  int n;

  printf("begin\n");
  pid = fork();
  printf("pid = %d\n", pid);
  switch(pid)
  {
    case -1:
      perror("fork failed");
      exit(1);
    case 0:
      message = "This is the child";
      n = 3;
      break;
    default:
      message = "This is the parent";
      n = 5;
      break;
  }
  for(; n > 0; n--)
  {
    puts(message);
    sleep(1);
  }
  if (pid == 0)
    printf("EndOfChild\n");
  else
    printf("EndOfParent\n");
  exit(0);
}
