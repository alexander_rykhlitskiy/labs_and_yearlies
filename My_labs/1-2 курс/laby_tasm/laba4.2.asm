data	segment
upText		db 'Up $'
downText 	db 'Down $'
rightText	db 'Right $'
leftText	db 'Left $'
wellDone	db 'well done!$'
data 	ends
stk		segment stack
		db 256 dup (0)
stk 	ends
text	segment
assume	CS:text, DS:data
begin:	mov AX, data
		mov DS, AX
		xor AX, AX
		
cycle:	mov AH, 08h
		int 21h
		cmp AL, 00h
		jne ifexit
		int 21h
		cmp AL, 72
		je upLabel
		cmp AL, 80
		je downLabel
		cmp AL, 77
		je rightLabel
		cmp AL, 75
		je leftLabel
ifexit:	cmp AL, '0'
		jne cycle

		mov AH, 09h
		mov DX, offset wellDone
		int 21h
		
		mov AX, 4C00h
		int 21h
		
upLabel:	mov AH, 09h
			mov DX, offset upText
			int 21h
			jmp cycle
downLabel:	mov AH, 09h
			mov DX, offset downText
			int 21h
			jmp cycle
rightLabel:	mov AH, 09h
			mov DX, offset rightText
			int 21h
			jmp cycle
leftLabel:	mov AH, 09h
			mov DX, offset leftText
			int 21h
			jmp cycle

text	ends
		end begin