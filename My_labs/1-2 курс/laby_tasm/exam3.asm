data	segment
newline	db 10, 13, '$'
max	db 20
db		4
summand	db 10 dup (?)
result	db 10 dup (?)
stroverflow	db 'overflow$'
data 	ends
stk		segment stack
		db 256 dup (0)
stk 	ends
text	segment
assume	CS:text, DS:data
begin:	mov AX, data
		mov DS, AX
		mov es, ax
;���� ������� �����
		mov di, offset max
		call inputSummond
		call procNewLine
;�������������� �� ������ � �������� ������� �����
		mov si, offset summand
		call strToInt
		push di ;������ �������� ����� � ����
		cmp cx, 0 ;��������� � ������������, ���� ���� cx = 0
		je overflow
;���� �����
		mov ah, 01h
		int 21h
;� ���� ���������� ���� ��������
		xor ah, ah
		push ax
		call procNewLine
;���� ������� �����
		mov di, offset max
		call inputSummond
		call procNewLine
;�������������� �� ������ � �������� ������� �����
		mov si, offset summand
		call strToInt
		push di ;������ ��������� ������� ����� � ����
		cmp cx, 0;��������� � ������������, ���� ���� cx = 0
		je overflow
		pop dx ;�� ����� ������� ������ ���������
		pop bx ;�� ����� ������� ���� ��������
		pop ax ;�� ����� ������� ������ ���������
		cmp bx, '+' ;��������� �������� �����
		je plus	;� ������������ �� ������ ��������� �� ������
		jne minus
minus:
		sub ax, dx
		jo overflow ;jo - ������� � ������������ � of (���� ������������)
		jmp outputValue
plus:
		add ax, dx
		jo overflow ;jo - ������� � ������������ � of (���� ������������)
;����� ����� �� ax	
outputValue:
		call procOutput
exit:	mov ax, 4C00h
		int 21h
;����� ��������� � ������������
overflow:
		mov dx, offset strOverflow
		mov ah, 09h
		int 21h
		jmp exit
;--------------------------------
;��������� �������������� ������ (si) � ����� (di)		
strToInt	proc
		xor cx, cx
		xor ax, ax
		xor di, di
;		xor dx, dx
		xor bh, bh
;� bx ������� ���������� �������� � ������ (���������� ��������)
		mov bl, [si-1] ;max+1
		cmp byte ptr [si], '-' ;���� ������ ������ ������ = '-',
		jne positive		   
		dec bl					;�������� ���������� ��������
		inc si					;������� ������ ������ �� ������
		mov ax, 12				;��������� ���� ax � �������� != 0
positive:
		push ax
label1:
		xor ax, ax
;� cx �������� ������� ������������ �������
		inc cx
;��������� ����� ��������
		dec bx
;� al ������� ������
		mov al, [si+bx]
;����������� ��� � �����
		sub al, 30h
;�������� cx, bx, ��� ��� ��� ������������ � ���������� al*10^n
		push cx
		push bx
		mov bx, 10
;� ����� ������� al*10^n
degree:
		cmp cx, 1
		je continue
;��������� ��������� � ax
		mul bx
continue:
		loop degree
;��������������� bx � cx
		pop bx
		pop cx
;� ��������� ���������� di ���������� ��������� al*10^n
		add di, ax
		jno noOverflow
		xor cx, cx ;���� ���� ������������, ������� ���� cx
		pop ax ;��������� ����
		ret
noOverflow:
;���� bx != 0, ��������� ��������� ��� ���������� �������
		test bx, bx
		jnz label1
		pop ax ;������� �� ����� ���� �����
		test ax, ax
		jz positive2 ;���� �� ����� ����, ������ ����� �������������
		neg di ;���� ���, ������� ��� �� �������������
		xor ax, ax
positive2:
		mov dx, di
		ret
strToInt	endp
;--------------------------------
;��������� ������ ����� �� �������� ax �� �����
procOutput	proc
		cmp ax, 0
		jnl positive3
;���� ax < 0
		push ax 
		mov dl, '-' ;������� �� ����� '-'
		mov ah, 02h
		int 21h
		pop ax
		mov dx, 0ffffh
		sub dx, ax ;������� ������ ����� (65535 - ax)
		inc dx
		mov ax, dx ;������� ������ � ax
positive3:
		xor bx, bx
;� cx - ������� ��� ���������� �� �������
		mov cx, 10
;������ ������ result  ��������� ����� � �������� �������
labelDivision:	
		xor dx, dx
;��� ������� ��������� ��������� � ax, ������� - � dx
		div cx
;� dx - ������� �� ������� (���� �����)
		add dx, 30h
;������� ��� ����� � ������
		mov result[bx], dl
		inc bx
;���� � ax - 0, ������ ��� ������� ��������
;���� ���, �� ��������� �������
		test ax, ax
		jnz labelDivision
		mov result[bx], '$'
;������� ������ �� �����
output:
		dec bx
;�� ������ ������� ������ ������ ������ � �����,
;��� ��� ������ � �������� �������
		mov dl, result[bx]
		mov ah, 02h
		int 21h
;���� bx != 0, ���������
		test bx, bx
		jnz output
		ret
procOutput	endp
;--------------------------------
procNewLine		proc
		mov ah, 09h
		mov dx, offset newline
		int 21h
		ret
procNewLine		endp
;--------------------------------
;���� ������ � di (max1). ����������� ������ � summand
inputSummond 	proc
		mov dx, di
		mov ah, 0ah
		int 21h
;� bx ���������� ���������� �������� � ������
		xor bh, bh
		mov bl, [di]+1
;�� ��������� ����� ���������� ������� ����� ������
		mov byte ptr [di+2+bx], '$'	
		ret
inputSummond 	endp
text 	ends
		end begin