data	segment
str1		db '14783165789$'
data 	ends
stk		segment stack
		db 256 dup (0)
stk 	ends
text	segment
assume	CS:text, DS:data
begin:	mov AX, data
		mov DS, AX
		mov ES, AX
		mov AL, '8'
		mov DI, offset str1
		cld
		mov CX, 15
		push AX
cycle:
		pop AX
repne	scasb
		je change
		
		mov AH, 09h
		mov DX, offset str1
		int 21h
		mov AX, 4C00h
		int 21h
change:	push AX
		sub DI, offset str1
		dec DI
		
		mov AL, '0'
		stosb
		jmp cycle
		
text	ends
		end begin