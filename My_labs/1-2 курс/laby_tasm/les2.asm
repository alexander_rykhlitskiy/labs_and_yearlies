data	segment
string1	db "123789$"
string2	db 10 dup (?)
data	ends
stk		segment stack
		db 256 dup (?)
stk		ends
text	segment
assume	CS: text, DS: data
begin:	mov AX, data
		mov DS, AX
		mov ES, AX
		call rewrite
		mov AX, 4C00h
		int 21h	
rewrite	proc		
count:	inc BX
		cmp string1[BX], '$'
		jne count
		mov CX, BX
		dec BX

cycle:	mov AL, string1[BX]
		mov string2[SI], AL
		inc SI
		dec BX
		loop cycle
		mov string2[SI], '$'
		
		mov AH, 09h
		mov DX, offset string2
		int 21h
		ret
rewrite	endp

text	ends
		end begin
