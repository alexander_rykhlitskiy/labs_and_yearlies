data	segment
mas		dw 1,2,3,4,5,0
per		db ?
data	ends
stk		segment stack
		db 256 dup (?)
stk		ends
text	segment
assume	CS: text, DS: data
begin:	mov AX, data
		mov DS, AX
		mov ES, AX
		xor AX, AX
		xor DX, DX
		xor BX, BX
		xor CX, CX
		xor SI, SI

		call sort
		mov AX, 4C00h
		int 21h
sort	proc
count:	add BX, type mas
        inc CX
		cmp mas[BX], 0h
		jne count
		sub BX, type mas
		xor BX, BX
		dec CX
		mov per, CL

cycle1:	
		push CX
cycle:	mov AX, mas[SI]
		mov BX, SI
		add BX, type mas
		cmp AX, mas[BX]
		jl perem
back:	add SI, type mas
		loop cycle
		pop CX
		dec CX
		mov DL, per
		dec DL
		mov per, DL
		xor SI, SI
		cmp DX, 0
		jne cycle1
		xor BX, BX
		jmp exit
		
perem:	mov DX, mas[BX]
		mov mas[BX], AX
		mov mas[SI], DX
		jmp back
		
exit:	cmp mas[BX], 0
		je quit
		mov AH, 02h
		mov DX, mas[BX]
		add DL, 30h
		int 21h
		add BX, type mas
		jmp exit
		
quit:	ret
sort	endp

text	ends
		end begin
