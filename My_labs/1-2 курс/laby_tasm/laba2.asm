data	segment
array	db -4,0,0,0,0,2,0,9,0,7
msg1	db 'There are $'
msg2	db ' elements that is not equal 0$'
data 	ends
stk		segment stack
		db 256 dup (0)
stk 	ends
text	segment
assume	CS:text, DS:data
begin:	mov AX, data
		mov DS, AX
		mov CX, 10
		mov BX, 0
		mov AX, 10
cycle:	cmp array[BX], 0
		jne j
		dec AX
j:		add BX, type array
		loop cycle
		mov BX, AX

		mov AH, 09h
		mov DX, offset msg1
		int 21h
		
		mov AX, BX
		add AX, 48
		mov DL, AL
		mov AH, 02h
		mov BX, 1
		int 21h
		
		mov AH, 09h
		mov DX, offset msg2
		int 21h
		
		mov AX, 4C00h
		int 21h
text	ends
		end begin