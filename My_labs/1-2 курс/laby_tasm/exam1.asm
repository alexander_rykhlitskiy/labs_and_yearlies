data	segment
;���� ��� ����������� ����� ������
max db 50
;���� ��� ����� �������� � ������
db ?
;�������� ������
string	db 50 dup (?)
;������� �� ����� ������
newline	db 10, 13, '$'
data	ends
stk		segment stack
		db 256 dup (0)
stk		ends
text	segment
assume	cs:text, ds:data
begin:	mov ax, data
		mov ds, ax
		xor ax, ax
;���� � ��������
		mov ah, 0ch
;���� ������ � �����
		mov al, 0ah
		mov dx, offset max
		int 21h
;� ������ ����� ������ ����� ������		
		xor bh, bh
		mov bl, max+1
		mov string[bx], '$'
		
		xor bx, bx
		dec bx
		xor ax, ax
;� al ������ ���������� ������, � ah - ���������� ����
;� ������ � al �������� ������
		mov al, ' '
counting:
		inc bx
		cmp string[bx], '$'
		je exit
;���� �� ������, ���� � ����� �����
		cmp string[bx], ' '
		jne value
;���� ������, � ���������� ������ -  ������, ���� � ����� �����
		cmp al, ' '
		je value
;���� ������ ������, � ���������� ������ - �� ������, ���������� ������
		inc ah
value:	
		mov al, string[bx]
		jmp counting
;���� ������������� ������ - ������, �� ���������� �������, ��� ��� ���
;��� �������		
exit:	cmp al, ' '
		je noword
		inc ah
noword:
		push ax
;enter
		mov ah, 09h
		mov dx, offset newline
		int 21h
;����� ���������� ����		
		mov ah, 02h
		pop dx
		mov dl, dh
		add dl, 30h
		int 21h
;���������� ���������
		mov ax, 4c00h
		int 21h
text	ends
		end begin