data	segment
wrongs	db ' Wrong symbol $'
data 	ends
stk		segment stack
		db 256 dup (0)
stk 	ends
text	segment
assume	CS:text, DS:data
begin:	mov AX, data
		mov DS, AX
		xor AX, AX
		
cycle:	mov AH, 08h
		int 21h
		cmp AL, 30h
		jb label1
		cmp AL, 39h
		ja label1
		mov AH, 02h
		mov DL, AL
		int 21h
		jmp cycle
		
labend:	mov AX, 4C00h
		int 21h
		
label1:	cmp AL, 61h
		jb label2
		cmp AL, 7ah
		ja label2
		mov AH, 02h
		mov DL, AL
		int 21h
		jmp cycle
		
label2:	cmp AL, 20h
		je labend
		mov AH, 09h
		mov DX, offset wrongs
		int 21h
		jmp cycle

text	ends
		end begin