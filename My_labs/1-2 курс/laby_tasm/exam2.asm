data	segment
strp1	db '1 USD = 6 CNY$'
strp2	db ' USD = $'
strp3	db ' CNY$'
newline	db 10, 13, '$'
currency	db 6
max1	db 20
db		?
summand	db 10 dup (?)
result	db 10 dup (?)
data 	ends
stk		segment stack
		db 256 dup (0)
stk 	ends
text	segment
assume	CS:text, DS:data
begin:	mov AX, data
		mov DS, AX
		mov es, ax
;����� ��������
		mov ah, 09h
		mov dx, offset strp1
		int 21h
		call procNewLine
;���� �����
		mov di, offset max1
		call inputSummond
;����� ���������� �����
		mov ah, 09h
		mov dx, offset summand
		int 21h
;����� ������ strp2
		mov ah, 09h
		mov dx, offset strp2
		int 21h
;������� ��������� ������ � �����
		mov si, offset summand
		call strToInt
;� di - ����� �� strToInt; ������� ����� ����� �� �������
		mov ax, di
		xor bx, bx
		xor dx,dx
		mov bl, [currency]
		div bx
;��������� ������� �� ������� � �����
		push dx
;����� ����� ����� �� ������� (��������� � ax)	
		call procOutput
;����� ����������� "."
		mov ah, 02h
		mov dl, '.'
		int 21h
;�� ����� ������� ������� �� �������		
		pop ax
;�������� ������� �� 100
		mov dx, 100
		mul dx
		xor dx, dx
		xor bx, bx
;������� ��������� ������� ������� �� ����
		mov bl, [currency]
		div bx
;������� ��������� (������� ����� �� ���������) �� �������� ax
		call procOutput
;����� ������ ' CNY$'
		mov ah, 09h
		mov dx, offset strp3
		int 21h
exit:	mov ax, 4C00h
		int 21h
;--------------------------------
;��������� �������������� ������ (si) � ����� (di)		
strToInt	proc
		xor cx, cx
		xor ax, ax
		xor di, di
		xor bh, bh
;� bx ������� ���������� �������� � ������ (���������� ��������)
		mov bl, max1+1
label1:
;� cx �������� ������� ������������ �������
		inc cx
;��������� ����� ��������
		dec bx
;� al ������� ������
		mov al, [si+bx]
;����������� ��� � �����
		sub al, 30h
;�������� cx, bx, ��� ��� ��� ������������ � ���������� al*10^n
		push cx
		push bx
		mov bx, 10
;� ����� ������� al*10^n
degree:
		cmp cx, 1
		je continue
;��������� ��������� � ax
		mul bx
continue:
		loop degree
;��������������� bx � cx
		pop bx
		pop cx
;� ��������� ���������� di ���������� ��������� al*10^n
		add di, ax
		xor ax, ax
;���� bx != 0, ��������� ��������� ��� ���������� �������
		test bx, bx
		jnz label1
		ret
strToInt	endp
;--------------------------------
;��������� ������ ����� �� �������� ax �� �����
procOutput	proc
		xor bx, bx
;� cx - ������� ��� ���������� �� �������
		mov cx, 10
;������ ������ result  ��������� ����� � �������� �������
labelDivision:	
		xor dx, dx
;��� ������� ��������� ��������� � ax, ������� - � dx
		div cx
;� dx - ������� �� ������� (���� �����)
		add dx, 30h
;������� ��� ����� � ������
		mov result[bx], dl
		inc bx
;���� � ax - 0, ������ ��� ������� ��������
;���� ���, �� ��������� �������
		test ax, ax
		jnz labelDivision
		mov result[bx], '$'
;������� ������ �� �����
output:
		dec bx
;�� ������ ������� ������ ������ ������ � �����,
;��� ��� ������ � �������� �������
		mov dl, result[bx]
		mov ah, 02h
		int 21h
;���� bx != 0, ���������
		test bx, bx
		jnz output
		ret
procOutput	endp
;--------------------------------
procNewLine		proc
		mov ah, 09h
		mov dx, offset newline
		int 21h
		ret
procNewLine		endp
;--------------------------------
;���� ������ � di (max1). ����������� ������ � summand
inputSummond 	proc
		mov dx, di
		mov ah, 0ah
		int 21h
;� bx ���������� ���������� �������� � ������
		xor bh, bh
		mov bl, [di]+1
;�� ��������� ����� ���������� ������� ����� ������
		mov byte ptr [di+2+bx], '$'	
		ret
inputSummond 	endp
text 	ends
		end begin