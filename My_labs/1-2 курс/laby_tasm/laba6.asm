data	segment
mesString	db 'enter string: $'
mesSubString	db '   enter subString: $'
mesOn	db '	+$'
mesOff	db '	-$'
perem	dw 1 dup (?)
string	db 10 dup (?)
subString	db 5 dup (?)
data 	ends
stk		segment stack
		db 256 dup (0)
stk 	ends
text	segment
assume	CS:text, DS:data
begin:	mov AX, data
		mov DS, AX
		mov ES, AX
		xor AX, AX
		xor BX, BX

;���� ������
		mov AH, 09h
		mov DX, offset mesString
		int 21h		
cycle1:	mov AH, 01h
		int 21h
		mov string[BX], AL
		inc BX
		cmp AL, '0'
		jne cycle1
		dec BX
		mov string[BX], '$'
		xor BX, BX

;���� ���������		
		mov AH, 09h
		mov DX, offset mesSubString
		int 21h		
cycle2:	mov AH, 01h
		int 21h
		mov subString[BX], AL
		inc BX
		cmp AL, '0'
		jne cycle2
		dec BX
		mov subString[BX], '$'
		inc BX
		push BX

;��������
		xor BX, BX
		pop perem[0]
cycle3:	mov DI, offset string
		add DI, BX
		mov SI, offset subString
		mov CX, perem[0]
repe	cmpsb
		inc BX
		sub SI, offset subString
		dec SI
		
		cmp subString[SI], '$'
		je on
		cmp string[BX], '$'
		je off
		jne cycle3
		
on:		mov AH, 09h
		mov DX, offset mesOn
		int 21h
		jmp exit
		
off:	mov AH, 09h
		mov DX, offset mesOff
		int 21h
		jmp exit	
		
exit:	mov AX, 4C00h
		int 21h
		
text	ends
		end begin