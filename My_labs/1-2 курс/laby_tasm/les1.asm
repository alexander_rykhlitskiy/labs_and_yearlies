data	segment
string1	db "12321$"
string2	db "12345$"
strmatch db "  +$"
strnotmatch db "  -$"
data	ends
stk		segment stack
		db 256 dup (?)
stk		ends
text	segment
assume	CS: text, DS: data
begin:	mov AX, data
		mov DS, AX
		mov ES, AX
		mov DI, offset string1
		call palindrom
		mov DI, offset string2
		call palindrom
		mov AX, 4C00h
		int 21h
palindrom	proc
		mov SI, DI
count:	inc BX
		inc DI
		mov AL, '$'
		cmp [DI], AL
		jne count
		mov AX, BX
		mov BL, 2
		div BL
		mov BL, AL
		mov CX, BX
		dec DI
		
cycle:	mov AL, [SI]
		cmp [DI], AL
		jne notmatch
		inc SI
		dec DI
		loop cycle
match:	mov AH, 09h
		mov DX, offset strmatch
		int 21h
		ret
notmatch:	mov AH, 09h
			mov DX, offset strnotmatch
			int 21h
		ret
palindrom	endp

text	ends
		end begin
