data	segment
array	dw 3,5,-9,0,1,-3,0,-50,-36,0
msgminus	db 'Quant of minus: $'
msgplus		db '  Sum of plus: $'
data 	ends
stk		segment stack
		db 256 dup (0)
stk 	ends
text	segment
assume	CS:text, DS:data
begin:	mov AX, data
		mov DS, AX
		mov CX, 10
		xor BX, BX
		xor AX, AX
		xor DX, DX
cycle:	cmp array[BX], 0
		jl minus
		jnl plus
cycle1:	add BX, type array
		loop cycle
		
		push DX
		push AX
		
		mov AH, 09h
		mov DX, offset msgminus
		int 21h
		
		pop AX
		add AX, 48
		mov DL, AL
		mov AH, 02h
		mov BX, 1
		int 21h

		mov AH, 09h
		mov DX, offset msgplus
		int 21h
		
		pop DX
		add DX, 48
		mov AH, 02h
		mov BX, 1
		int 21h
		
		mov AX, 4C00h
		int 21h
		
minus:	inc AX
		jmp cycle1
plus:	add DX, array[BX]
		jmp cycle1
text	ends
		end begin