data	segment
max		db '3'
db		?
string	db 30 dup (?)
newline	db 10, 13, '$'
strNotPalind	db 'not palindrom$'
strPalind	db 'palindrom$'
data 	ends
stk		segment stack
		db 256 dup (0)
stk 	ends
text	segment
assume	CS:text, DS:data
begin:	mov AX, data
		mov DS, AX
		mov es, ax
		xor ax, ax
;���� ������
		mov ah, 0ch
		mov al, 0ah
		mov dx, offset max
		int 21h
		xor bh, bh
		mov bl, max+1
		mov string[bx], '$'
		dec bx
;� di - ����� ������, � si - ������
		mov di, offset string
		add di, bx
		mov si, offset string
;��������� ����, ���� di > si
cycle:
		cmp si, di
		jnl labPalind
		mov al, [di]
		mov al, [di]		
		cmp [si], al
		jne labNotPalind
		inc si
		dec di
		jmp cycle
		
labNotPalind:
		mov ah, 09h
		mov dx, offset newline
		int 21h
		mov ah, 09h
		mov dx, offset strNotPalind
		int 21h
		jmp exit
labPalind:		
		mov ah, 09h
		mov dx, offset newline
		int 21h
		mov ah, 09h
		mov dx, offset strPalind
		int 21h
		jmp exit
		
exit:
		mov ah, 09h
		mov dx, offset newline
		int 21h
		mov ax, 4c00h
		int 21h
text	ends
		end begin