;������ - 1234
data	segment
strInputLogin	db 'enter your login:', 10, 13, '$'
strInputPass	db 'enter password:', 10, 13, '$'
newline	db 10, 13, '$'
;������ ��� �������������� ������, ����������� � ���������
pass	db '4761$'
;������ ��� ������
login	db 'sasha$'
;������ ��� ��������� ������ � ������
enterpass	db 10 dup (?)
;���� ��� ����������
key		db 5
wrongValue	db 'wrong value. try again:', 10, 13, '$'
strEqual db 'access permitted$'
data 	ends
stk		segment stack
		db 256 dup (0)
stk 	ends
text	segment
assume	CS:text, DS:data
begin:	mov AX, data
		mov DS, AX
		mov es, ax
		xor bx, bx
;��������� � ����� ������
		mov dx, offset strInputLogin
		mov ah, 09h
		int 21h
;���� ������
inputLogin:
		mov dl, 01h ;���� �������� � ����
		call input
;��������� ����� login � enterpass	
		mov si, offset login
		mov dx, 1
		call compareStrings
;���� ���� ax = 0, ������ ����� ������ �����������
		cmp ax, 0
		je inputLogin ;��������� ���� ������
;����� ��������� � ����� ������
		mov dx, offset strInputPass
		mov ah, 09h
		int 21h
;�������� ������ �� ����������� �����
inputPass:
		mov dl, 08h ;���� ������� ��� ���
		call input
;��������� ����� pass � enterpass	
		mov si, offset pass	
		xor dx, dx
		call compareStrings
;���� ���� ax = 0, ������ ������ ������ �����������
		cmp ax, 0
		je inputPass ;��������� ���� ������
;����� ��������� � ��������� �������
		mov ah, 09h
		mov dx, offset strEqual
		int 21h		
exit:	mov ax, 4C00h
		int 21h
;--------------------------------
input	proc
cycle:	
		xor bx, bx
		dec bx ;��������� �������� bx = -1
labelEnterpass:
		mov ah, dl ;��� ����� � ����������� �� ���������� �������� dl
		int 21h
;���� ������ enter, ��������� ������		
		cmp al, 13
		je check
;�������� �� ����������� ������
		cmp al, 00h
		jne notmanage
		int 21h
		jmp labelEnterpass
notmanage:
;���������� ����� (0-9, A-z)
		cmp al, '0'
		jl labelEnterpass
		cmp al, '9'
		jle continue
		cmp al, 'A'
		jl labelEnterpass
		cmp al, 'z'
		jg labelEnterpass
continue:
		inc bx
		mov enterpass[bx], al
		jmp labelEnterpass
;���� �������� ����� ������		
check:
		inc bx
		mov enterpass[bx], '$'
		ret
input	endp
;--------------------------------
compareStrings	proc
		xor bx, bx
compare:
;� si ����� ������ ��� ������ � ������
		mov al, [si+bx]
		mov ah, enterpass[bx]
		cmp ah, '$'
		jne continue1 ;���� enterpass[bx] = '$'
		cmp al, '$'	  ;���������� [si+bx] � '$'
		je equal	  ;���� [si+bx] = '$', ������ �����
		jne notequal  ;����� - �� �����
continue1:
		cmp dx, 0
		jne continue2 ;���� ���� dx = 0, ������� ������
		xor ah, key
continue2:
		cmp ah, al
		jne notequal;���� [si+bx] � enterpass[bx] �� �����, ������ �� �����
		inc bx		;����� ���������� ���������
		jmp compare
;������ �� �����		
notequal:	
		mov ah, 09h
		mov dx, offset wrongValue
		int 21h
		xor ax, ax
		ret
;������ �����		
equal:	
		ret
compareStrings	endp
text 	ends
		end begin