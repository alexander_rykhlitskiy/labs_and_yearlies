data	segment
newline	db 10, 13, '$'
max1	db 20
db		?
summand1	db 10 dup (?)
max2	db 20
db		?
summand2	db 10 dup (?)
result	db 10 dup (?)
data 	ends
stk		segment stack
		db 256 dup (0)
stk 	ends
text	segment
assume	CS:text, DS:data
begin:	mov AX, data
		mov DS, AX
		mov es, ax
		mov di, offset max1
		call inputSummond
		call procNewLine
		mov ah, 01h
		int 21h
;� ���� ���������� ���� ��������
		xor ah, ah
		push ax
		call procNewLine
		mov di, offset max2
		call inputSummond
		call procNewLine

;�� ������ � ��������
		mov si, offset summand1
		call strToInt
		push di	
		mov si, offset summand2
		call strToInt
;�� ����� ������� ������ ���������
		pop ax
;�� ����� ������� ���� ��������
		pop bx
		cmp bx, 43
		je plus
		jne minus
minus:
		sub ax, di
		jmp outputValue
plus:
		add ax, di
;����� ����� �� ax	
outputValue:
		call procOutput
exit:	mov ax, 4C00h
		int 21h
;--------------------------------
;��������� �������������� ������ (si) � ����� (di)		
strToInt	proc
		xor cx, cx
		xor ax, ax
		xor di, di
		xor bh, bh
		xor dx, dx
;� bx ������� ���������� �������� � ������ (���������� ��������)
		mov bl, [si-1]
label1:
;� cx �������� ������� ������������ �������
		inc cx
;��������� ����� ��������
		dec bx
;� al ������� ������
		mov al, [si+bx]
;����������� ��� � �����
		sub al, 30h
;�������� cx, bx, ��� ��� ��� ������������ � ���������� al*10^n
		push cx
		push bx
		mov bx, 10
;� ����� ������� al*10^n
degree:
		cmp cx, 1
		je continue
;��������� ��������� � ax
		mul bx
continue:
		loop degree
;��������������� bx � cx
		pop bx
		pop cx
;� ��������� ���������� di ���������� ��������� al*10^n
		add di, ax
		xor ax, ax
;���� bx != 0, ��������� ��������� ��� ���������� �������
		test bx, bx
		jnz label1
		ret
strToInt	endp
;--------------------------------
;��������� ������ ����� �� �������� ax �� �����
procOutput	proc
		xor bx, bx
;� cx - ������� ��� ���������� �� �������
		mov cx, 10
;������ ������ result  ��������� ����� � �������� �������
labelDivision:	
		xor dx, dx
;��� ������� ��������� ��������� � ax, ������� - � dx
		div cx
;� dx - ������� �� ������� (���� �����)
		add dx, 30h
;������� ��� ����� � ������
		mov result[bx], dl
		inc bx
;���� � ax - 0, ������ ��� ������� ��������
;���� ���, �� ��������� �������
		test ax, ax
		jnz labelDivision
		mov result[bx], '$'
;������� ������ �� �����
output:
		dec bx
;�� ������ ������� ������ ������ ������ � �����,
;��� ��� ������ � �������� �������
		mov dl, result[bx]
		mov ah, 02h
		int 21h
;���� bx != 0, ���������
		test bx, bx
		jnz output
		ret
procOutput	endp
;--------------------------------
procNewLine		proc
		mov ah, 09h
		mov dx, offset newline
		int 21h
		ret
procNewLine		endp
;--------------------------------
;���� ������ � di (max1). ����������� ������ � summand
inputSummond 	proc
		mov dx, di
		mov ah, 0ah
		int 21h
;� bx ���������� ���������� �������� � ������
		xor bh, bh
		mov bl, [di]+1
;�� ��������� ����� ���������� ������� ����� ������
		mov byte ptr [di+2+bx], '$'	
		ret
inputSummond 	endp
text 	ends
		end begin