data	segment
string1	db "Hello$"
string2	db ", wor$"
string3	db "ld!$"
per1	db 5
per2	db 6
data	ends
stk		segment stack
		db 256 dup (?)
stk		ends
text	segment
assume	CS: text, DS: data
begin:	mov AX, data
		mov DS, AX
		mov Al, 's'
		mov CX, 5
cycle:	call outputc
		loop cycle
		call outputn
		push offset string3
		push offset string2
		push offset string1
		call far ptr outputs
		mov AX, 4C00h
		int 21h
outputc	proc
		mov AH, 02h
		mov DL, AL
		int 21h
		ret
outputc	endp
outputn	proc
		mov AH, 02h
		mov DL, per1
		add DL, 30h
		int 21h
		mov DL, per2
		add DL, 30h
		int 21h
		ret
outputn	endp
outputs	proc far
		push BP
		mov BP, SP
		mov AH, 09h
		mov DX, [BP+6]
		int 21h
		mov DX, [BP+8]
		mov AH, 09h
		int 21h
		mov DX, [BP+10]
		mov AH, 09h
		int 21h
		pop BP
		ret
outputs	endp
text	ends
		end begin
