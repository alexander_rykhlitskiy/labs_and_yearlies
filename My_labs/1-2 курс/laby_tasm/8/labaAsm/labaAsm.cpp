// labaAsm.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>


int _tmain(int argc, _TCHAR* argv[])
{
	int i, mas[5], a;
	char c;
	for (i = 0; i <= 3; i++)
	{
		mas[i] = 5-i;
		printf("%d  ", mas[i]);
	}
	printf("\n");
	_asm{
		lea esi, mas
		mov ecx, 3
cycle:
		push ecx
		xor ebx, ebx
cycle1:
		mov al, [esi+ebx]
		add ebx, 4
		cmp al, [esi+ebx]
		jnle perestan
next:
		loop cycle1
		jmp quit

perestan:
		mov ah, [esi+ebx]
		mov [esi+ebx], al
		sub ebx, 4
		mov [esi+ebx], ah
		add ebx, 4
		jmp next
quit:
		pop ecx
		loop cycle
	}
	for (i = 0; i <= 3; i++)
		printf("%d  ", mas[i]);
	getchar();
	return 0;
}

