program laba_4;
uses crt;
type
  typeElement = integer;
  adrZveno = ^zveno;
  adrElement = ^typeElement;
  zveno = record
            kl : integer;
            element : adrElement;
            left,right : adrZveno
          end;
var
  Q,D : adrZveno;
  list,uzel,glub,max,i : integer;
function search(k : integer; var D, rez : adrZveno) : boolean;
var
  p,q : adrZveno;
  b : boolean;
begin
  p := D;
  b := false;
  if D <> nil then
    repeat
      q := p;
      if p^.kl = k
        then b := true
        else if k < p^.kl then p := p^.left
                          else p := p^.right;
    until (b) or (p = nil);
  rez := q;
  search := b;
end;

procedure dobavl(k : integer; var D : adrZveno; elem : typeElement);
var
  q, p : adrZveno;
  el : adrElement;
begin
  if not(search(k,d,q))
    then
      begin
        new(el);
        el^ := elem;
        new(p);
        p^.left := nil;
        p^.right := nil;
        p^.kl := k;
        p^.element := el;
        if D = nil then D := p
                   else if k < q^.kl then q^.left := p
                                     else q^.right := p
      end
    else q^.element^ := elem
end;

procedure obhod(q : adrZveno);
begin
  glub := glub +1;
  if (q^.left = nil) and (q^.right = nil)
    then list := list + 1;
  if ((q^.left = nil) and (q^.right <> nil)) or
     ((q^.left <> nil) and (q^.right = nil)) then uzel := uzel + 1;
  if q^.left <> nil
    then obhod(q^.left);
  if q^.right <> nil
    then obhod(q^.right);
  if glub > max
    then max := glub;
  glub := glub - 1
end;
            
begin
  D := nil;
  i := 5;   dobavl(i,d,i);
  i := 6;   dobavl(i,d,i);
  i := 3;   dobavl(i,d,i);
  i := 4;   dobavl(i,d,i);
  i := 2;   dobavl(i,d,i);
  i := 1;   dobavl(i,d,i);
  i := 7;   dobavl(i,d,i);
  i := 9;   dobavl(i,d,i);
  i := 10;  dobavl(i,d,i);
  i := 8;   dobavl(i,d,i);
  obhod(d);
  writeln('���������� ������� = ',list);
  writeln('���������� ����� � ����� ������������ ����� = ',uzel);
  writeln('������������ ������� ������ = ',max)
end.