unit zvenya;
interface
  type
    adres1=^zveno;
    zveno=record
            element:byte;
            adrcled:adres1
          end;
  var q:adres1;
  procedure dobavl(var right:adres1;elem:byte);
  procedure udal(adrpred:adres1);
  procedure vivod(left:adres1);
  procedure sortirovka(left:adres1;kol:integer);
implementation
  procedure dobavl(var right:adres1;elem:byte);
  var q:adres1;
  begin
    new(q);
    q^.adrcled:=nil;
    q^.element:=elem;
    right^.adrcled:=q;
    right:=q;
  end;
  procedure udal(adrpred:adres1);
  begin
    q:=adrpred^.adrcled;
    adrpred^.adrcled:=q^.adrcled;
    dispose(q)
  end;
  procedure vivod(left:adres1);
  var q:adres1;
  begin
    q:=left;
    while q<> nil do
      begin
        write(q^.element);
        q:=q^.adrcled
      end;
    writeln
  end;
  procedure sortirovka(left:adres1;kol:integer);
  var
    i,j:integer;
    x:integer;
  begin
    for i:=kol-1 downto 1 do
      begin
        q:=left;
        for j:=i downto 1 do
          begin
            if q^.element>q^.adrcled^.element
              then begin
                     x:=q^.element;
                     q^.element:=q^.adrcled^.element;
                     q^.adrcled^.element:=x
                   end;
            q:=q^.adrcled;
          end;
      end;

  end;
begin
end.