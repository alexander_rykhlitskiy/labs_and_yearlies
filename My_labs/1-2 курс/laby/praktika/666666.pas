program praktika6;
uses crt;
type
  rec = record
          name : string;
          kol : integer;
          data : string;
          zena : longint;
        end;
var
  f : file of rec;
  mas : array[1..10] of rec;
  i, j, k : integer;
  element : rec;
  str : string;
function time(var str1, str2 : string) : boolean;
var
  i, j, iMas, cod : integer;
  mas1, mas2 : array[1..4] of integer;
begin
  iMas := 0;
  j := 0;
  for i := 1 to length(str2) do
    begin
      j := j + 1;
      if str[i] = '.' then
        begin
          iMas := iMas + 1;
          val(copy(str2, i - j + 1, j - 1), mas2[iMas], cod);
          j := 0;
        end;
    end;
    
  iMas := 0;
  j := 0;
  for i := 1 to length(str1) do
    begin
      j := j + 1;
      if str[i] = '.' then
        begin
          iMas := iMas + 1;
          val(copy(str1, i - j + 1, j - 1), mas1[iMas], cod);
          j := 0;
        end;
    end;
  mas1[4] := 365 * mas1[3] + 30 * mas1[2] + mas1[1];
  mas2[4] := 365 * mas2[3] + 30 * mas2[2] + mas2[1];
  if mas1[4] - mas2[4] > 30 then time := true
                            else time := false;
end;
begin
  assign(f,'file');
  rewrite(f);
  mas[1].name := 'audi';
  mas[1].kol := 4;
  mas[1].data := '1.3.2011.';
  mas[1].zena := 100001;
  write(f, mas[1]);
  mas[2].name := 'ford';
  mas[2].kol := 2;
  mas[2].data := '1.6.2011.';
  mas[2].zena := 200000;
  write(f, mas[2]);
  mas[3].name := 'reno';
  mas[3].kol := 7;
  mas[3].data := '1.3.2011.';
  mas[3].zena := 100001;
  write(f, mas[3]);
  mas[4].name := 'pego';
  mas[4].kol := 5;
  mas[4].data := '1.7.2011.';
  mas[4].zena := 20000;
  write(f, mas[4]);
  mas[5].name := 'benz';
  mas[5].kol := 9;
  mas[5].data := '1.2.2011.';
  mas[5].zena := 150000;
  write(f, mas[5]);
  close(f);
  reset(f);
  i := 0;
  str := '1.5.2011';
  while not(eof(f)) do
    begin
      read(f, element);
      if (element.zena > 100000) and (time(str, element.data))
        then begin
               i := i + 1;
               mas[i] := element;
             end;
    end;
  for j := 1 to i do
    for k := 1 to i - j do
      if mas[k].name > mas[k + 1].name
        then begin
               element := mas[k];
               mas[k] := mas[k + 1];
               mas[k + 1] := element;
             end;
  writeln;
  for j := 1 to i do
    writeln(mas[j].name);
  close(f)
end.