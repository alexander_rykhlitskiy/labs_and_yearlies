program laba9999;
uses crt;
type
  tekst = integer;
  adrT = ^tekst;
  adrZv = ^zveno;
  zveno = record
            elem : adrT;
            left, right : adrZv;
            kl : integer
          end;
var
  D : adrzv;
  x : integer;

function search(kl : integer; D : adrzv; var rez : adrZv) : boolean;
var
  b : boolean;
  q, p : adrZv;
begin
  b := false;
  q := D;
  p := D;
  while (not(b)) and (q <> nil) do
    begin
      p := q;
      if kl = q^.kl
        then b := true
        else if kl < q^.kl
               then q := q^.left
               else q := q^.right
    end;
  rez := p;
  search := b;
end;

procedure dobavl(kl : integer; var D : adrzv; zap : Tekst);
var
  q, p : adrZv;
  T : adrT;
begin
  if not(search(kl, D, q)) then
    begin
      new(p);
      p^.kl := kl;
      if D = nil
        then D := p
        else if kl < q^.kl then q^.left := p
                           else q^.right := p;
      p^.left := nil;
      p^.right := nil;
      new(T);
      T^ := zap;
      p^.elem := T
    end;
end;

procedure udal(kl : integer; var D : adrZv);

procedure ud(var R : adrZv);
begin
  if R^.right = nil then D := R
                    else ud(R^.right);
end;

begin
  if D = nil then writeln('�������� �� �������')
             else
  if kl < D^.kl
    then udal(kl, D^.left)
    else if kl > D^.kl
           then udal(kl, D^.right)
           else begin
                  if D^.right = nil
                    then D := D^.left
                    else if D^.left = nil
                           then D := D^.right
                           else ud(D^.left);
                end;
end;

begin
  x := 50; dobavl(x, D, x);
  x := 10; dobavl(x, D, x);
  x := 30; dobavl(x, D, x);
  x := 40; dobavl(x, D, x);
  x := 20; dobavl(x, D, x);
  x := 80; dobavl(x, D, x);
  x := 90; dobavl(x, D, x);
  x := 60; dobavl(x, D, x);
  x := 6; dobavl(x, D, x);
  x := 8; dobavl(x, D, x);
  x := 7; dobavl(x, D, x);
  x := 9; dobavl(x, D, x);
  write(d^.left^.kl);
  udal(10, D);
  writeln(d^.left^.kl);
end.