program laba8888;
uses crt;
type
  element = integer;
  adresStek = ^zvenoStek;
  zvenoStek = record
                element : element;
                adrcled : adresStek
              end;
  adresQeue = ^zvenoQeue;
  zvenoQeue = record
                element : element;
                adrcled : adresQeue;
                adrpred : adresQeue
              end;
var
  stek, qStek : adresStek;
  right, left, qQeue : adresQeue;
  a : element;
  i, k : integer;
  mas : array[1..10] of element;
  
procedure dobavlStek(elem : element; var st : adresStek);
var
  q : adresStek;
begin
  new(q);
  q^.element := elem;
  q^.adrcled := st;
  st := q;
end;
procedure vibor(var elem : element; var st : adresStek);
var
  q : adresStek;
begin
  q := st;
  elem := st^.element;
  st := st^.adrcled;
  dispose(q)
end;
procedure dobavlQeue(elem : element; var righ : adresQeue);
var
  q : adresQeue;
begin
  new(q);
  righ^.adrcled := q;
  q^.adrpred := righ;
  q^.adrcled := nil;
  righ := q;
  q^.element := elem;
end;
begin
  new(qStek);
  stek := qStek;
  stek^.adrcled := nil;
  readln(a);
  stek^.element := a;
  while a <> 0 do
    begin
      dobavlStek(a, stek);
      readln(a)
    end;
  qStek := stek;
  i := 0;
  qStek := stek;
  while stek^.adrcled <> nil do
    begin
      i := i + 1;
      vibor(mas[i],stek)
    end;
  writeln('mas');
  for k := 1 to i do
    writeln(mas[k]);
  new(qQeue);
  left := qQeue;
  right := qQeue;
  right^.element := mas[i];
  right^.adrcled := nil;
  right^.adrpred := nil;
  for k := i - 1 downto 1 do
    dobavlQeue(mas[k], right);
  qQeue := left;
  writeln('Qeue');
  while qQeue <> nil do
    begin
      writeln(qQeue^.element);
      qQeue := qQeue^.adrcled;
    end;
end.