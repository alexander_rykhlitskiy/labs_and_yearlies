unit moy;
interface
type
  mass2 = array[1..3,1..3] of byte;
  mass1 = array[1..9] of byte;
var
  n, k : integer;
  mas : array[1..100] of integer;
  mas1 : mass1;
  mas2 : mass2;
  i, j : integer;
  function massiv(var m1 : mass1; m2 : mass2) : integer;
  function func(k : integer) : real;
  
implementation

function func(k : integer) : real;
begin
  if k = 1 then func := mas[k]*cos(k*pi/4)
           else
  func := func(k - 1) + mas[k]*cos(k*pi/4);
end;

function massiv(var m1 : mass1; m2 : mass2) : integer;
var i, j, max : integer;
begin
  max := 0;
  for i := 1 to 3 do
    for j := 1 to 3 do
      if m2[i,j] = 1 then
        begin
          max := max + 1;
          m1[max] := m2[i,j]
        end;
  massiv := max;
end;
begin
end.