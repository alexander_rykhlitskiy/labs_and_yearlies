program praktika3;
uses crt;
type
  mas = array[1..3,1..3] of integer;
var
  m6, m, m1 : mas;
  i, j : integer;
procedure rec(a, b : mas; var c : mas);
var
  i, j, k : integer;
  y : integer;
begin
  for i := 1 to 3 do
    for j := 1 to 3 do
      begin
        y := 0;
        for k := 1 to 3 do
          y := y + a[i,k] * b[k,j];
        c[i,j] := y;
      end;
end;

function step(x : integer) : mas;
begin
  if x = 1 then step := m else
  if x = 2 then begin
                  rec(m, m, m6);
                  step := m6;
                end
           else rec(step(x - 1),m ,m6);
  step := m6;
end;

begin
  {for i := 1 to 3 do
    for j :=1 to 3 do
      m[i,j] := 1; }
  m[1,1] := 1;m[1,2]:=3;
  m[1,3]:=2;m[2,1]:=3;m[2,2]:=1;m[2,3]:=2;m[3,1]:=2;m[3,2]:=1;m[3,3]:=3;
  m1 := step(6);
  for i := 1 to 3 do
    begin
      for j :=1 to 3 do
        write(m1[i,j],'  ');
      writeln
    end;
end.