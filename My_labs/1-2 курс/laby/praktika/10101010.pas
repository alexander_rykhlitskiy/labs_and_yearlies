program laba10;
uses crt, graphAbc;
var
  X : array[1..5] of integer;
  Y : array[1..5] of integer;
  i, a : integer;
  ch : char;
begin
  x[1] := 0;
  y[1] := 120;
  x[2] := 110;
  y[2] := -80;
  x[3] := -130;
  y[3] := -80;
  line(x[1] + 200, y[1] + 200, x[2] + 200, y[2] + 200);
  line(x[2] + 200, y[2] + 200, x[3] + 200, y[3] + 200);
  line(x[3] + 200, y[3] + 200, x[1] + 200, y[1] + 200);
  while ch <> #27 do
    begin
      ch := readKey;
      if ch = #0 then
        if readkey = #77 then
        begin
          clrscr;
          a := y[1];
          y[1] := round (x[1]*sin(-0.03) + y[1]*cos(-0.03));
          x[1] := round (x[1]*cos(-0.03) - a*sin(-0.03));
          
          a := y[2];
          y[2] := round (x[2]*sin(-0.03) + y[2]*cos(-0.03));
          x[2] := round (x[2]*cos(-0.03) - a*sin(-0.03));
          
          a := y[3];
          y[3] := round (x[3]*sin(-0.03) + y[3]*cos(-0.03));
          x[3] := round (x[3]*cos(-0.03) - a*sin(-0.03));
          
          line(x[1] + 200, y[1] + 200, x[2] + 200, y[2] + 200);
          line(x[2] + 200, y[2] + 200, x[3] + 200, y[3] + 200);
          line(x[3] + 200, y[3] + 200, x[1] + 200, y[1] + 200);
        end;
    end;
end.