program praktika2;
uses crt;
type
  arr = array[1..100] of integer;
var
  n, k : integer;
  mas : arr;
function step(a : integer; k : integer) : integer;
var
  i, s : integer;
begin
  s := 1;
  for i := 1 to k do
    s := s * a;
  step := s
end;
function func(k : integer; mas : arr) : real;
var
  i : integer;
  sum : real;
begin
  sum := 0;
  for i := 1 to k do
    sum := sum + step(mas[k], i)*cos((i * pi)/4);
  func := sum;
end;
begin
  write('n = ');
  readln(n);
  writeln('������� ������');
  for k := 1 to n do
    readln(mas[k]);
  for k := 1 to n do
    writeln('s(x[',k,']) = ', func(k, mas):3:3);
end.