program praktika1;
uses crt;
type
  mass2 = array[1..3,1..3] of byte;
  mass1 = array[1..9] of byte;
var
  mas1 : mass1;
  mas2 : mass2;
  i, j : integer;
  
function massiv(var m1 : mass1; m2 : mass2) : integer;
var i, j, max : integer;
begin
  max := 0;
  for i := 1 to 3 do
    for j := 1 to 3 do
      if m2[i,j] = 1 then
        begin
          max := max + 1;
          m1[max] := m2[i,j]
        end;
  massiv := max;
end;
begin
  randomize;
  for i := 1 to 3 do
    begin
      for j := 1 to 3 do
        begin
          mas2[i,j] := random(2);
          write(mas2[i,j]:4);
        end;
      writeln
    end;
  j := (massiv(mas1, mas2));
  writeln('max = ', j);
  for i := 1 to j do
    write(mas1[i],'  ');
end.