program labaTermin;
uses crt;
type
  str = string;
  adrTermin = ^Termin;
  adrPodTermin = ^PodTermin;
  adrStran = ^stran;
  termin = record
             element : str;
             podTermin : adrPodTermin;
             stran1 : adrStran;
             adrcled : adrTermin
           end;
  PodTermin = record
                element : str;
                stran1 : adrStran;
                adrcled : adrPodTermin
              end;
  stran = record
            element : str;
            adrcled : adrStran
          end;
var
  bool : boolean;
  i,j,k : integer;
  q_ : adrstran;
  q,qq : adrTermin;
  q1,qq1 : adrPodTermin;
  vibor, numb : byte;
  strvv,strvv1 : str;
  masTermin : array[1..30] of adrTermin;
  f : text;
  
  leftTermin, rightTermin : adrTermin;
  
procedure dobavl_Termin(elem:str; var rightTermin:adrTermin);
var
  q:adrTermin;
  q1:adrPodTermin;
begin
  new(q);
  new(q1);
  q1^.adrcled := nil;
  q1^.stran1 := nil;
  rightTermin^.adrcled := q;
  rightTermin := q;
  q^.adrcled :=nil;
  q^.element := elem;
  q^.PodTermin := q1;
  q^.stran1 :=nil
end;

procedure dobavl_PodTermin(elem:str;var q1:adrPodTermin);
var ql:adrPodTermin;
begin
  new(ql);
  q1^.adrcled :=ql;
  ql^.element := elem;
  ql^.stran1 :=nil;
  ql^.adrcled := nil
end;

procedure udal_PodTermin(var adrpred:adrPodTermin);
var
  q:adrPodTermin;
begin
  q := adrpred^.adrcled;
  adrpred^.adrcled := q^.adrcled;
  dispose(q)
end;

procedure udal_Termin(var adrpred:adrTermin);
var
  q:adrTermin;
  q1:adrPodTermin;
begin
  q := adrpred^.adrcled;
  q1 := q^.podTermin;
  while q1^.adrcled <> nil do
    udal_PodTermin(q1);
  adrPred^.adrcled := q^.adrcled;
  dispose(q)
end;

procedure udal_poisk_Termin(boole:boolean);
begin
  if boole then writeln('����� ������ �� ������ �����?')
           else writeln('����� ������ �� ������ �������?');
  i:= 1;
  numb := 0;
  bool := true;
  if boole then numb := 1;
  readln(strvv);
  q := leftTermin;
  while (q^.adrcled <> nil) and bool do
    if q^.adrcled^.element = strvv then bool := false
                                   else begin
                                          if boole then numb := numb + 1;
                                          q := q^.adrcled;
                                          i := i +1
                                        end;
  if bool
    then writeln('������ ������� �� �������.')
    else begin
           if boole then writeln('��� ������ ',i)
                    else begin
                           udal_Termin(q);
                           writeln('�������� ������ �������.')
                         end
         end
end;

procedure udal_poisk_PodTermin(boole:boolean);
begin
  if boole then writeln('��������� ������ ������� �� ������ �����?')
           else writeln('��������� ������ ������� �� ������ �������?');
  readln(strvv);
  i := 1;
  q := leftTermin^.adrcled;
  while (q <> nil) and (q^.element <> strvv)  do
    begin
      q := q^.adrcled;
      i := i + 1
    end;
  if q = nil then writeln('������ ������� �� �������.')
             else begin
                    if boole then writeln('����� ��������� �� ������ �����?')
                             else writeln('����� ��������� �� ������ �������?');
                    readln(strvv);
                    q1 := q^.PodTermin;
                    j := 1;
                    while (q1^.adrcled <> nil) and (q1^.adrcled^.element <> strvv) do
                      begin
                       j := j+1;
                       q1 := q1^.adrcled
                      end;
                    if q1^.adrcled = nil then writeln('������ ���������� �� �������.')
                                         else if boole then writeln('������ ',i,', ��������� ',j)
                                                       else begin
                                                              udal_PodTermin(q1);
                                                              writeln('�������� ������ �������.')
                                                           end
                  end
end;

procedure zamena_Termin(q:adrTermin);
var q1,q2 : adrTermin;
begin
  q1 := q^.adrcled;
  q2 := q^.adrcled^.adrcled^.adrcled;
  q^.adrcled := q^.adrcled^.adrcled;
  q^.adrcled^.adrcled := q1;
  q1^.adrcled := q2
end;

procedure zamena_PodTermin(q1:adrPodTermin);
var q2,q3 : adrPodTermin;
begin
  q2 := q1^.adrcled;
  q3 := q1^.adrcled^.adrcled^.adrcled;
  q1^.adrcled := q1^.adrcled^.adrcled;
  q1^.adrcled^.adrcled := q2;
  q2^.adrcled := q3
end;

begin
new(q);
leftTermin := q;
q^.adrcled := nil;
rightTermin := leftTermin;
q^.adrcled := nil;
q^.stran1 := nil;
repeat
  writeln('1. �������� ���������� ���������');
  writeln;
  writeln('����������');
  writeln('   2.�������');
  writeln('   3.����������');
  writeln;
  writeln('��������');
  writeln('   4.�������');
  writeln('   5.����������');
  writeln;
  writeln('�����');
  writeln('   6.������� �� ��������');
  writeln('   7.���������� �� �������');
  writeln('   8.������� �� ����������');
  writeln;
  writeln('�����������');
  writeln('   9.������� � ���������� �� ��������');
  writeln('  10.������� � ���������� �� ������� �������');
  writeln;
  writeln('������ � �������:');
  writeln('  11.������');
  writeln('  12.������');
  writeln;
  writeln('13. �����');
  readln(vibor);
  clrscr;
  case vibor of
    1 : begin
        i := 0;
        q := leftTermin^.adrcled;
        if q = nil then writeln('� ���������� ��������� �������� ���.');
        while q <> nil do
          begin
            j := 0;
            i := i+1;
            if i = numb then textcolor(red);
            writeln(i,') ',q^.element,' ���.', q^.stran1^.element);
            textcolor(black);
            q1 := q^.podTermin^.adrcled;
            while q1 <>nil do
              begin
                j := j+1;
                write('  ',j,'. ',q1^.element,' ���.');writeln(q1^.stran1^.element);
                q1 := q1^.adrcled
              end;
            writeln;
            q := q^.adrcled;
          end;
        end;
    2 : begin
          writeln('����� ������ �� ������ ��������?');
          writeln('����� ������ ������� �������� �������.');
          bool := true;
          readln(strvv);
          k := 0;
          i := 1;
          j := 0;
          while strvv[i] <> ' ' do
            i := i + 1;
          strvv1 := copy(strvv, i+1, length(strvv)-i);
          strvv := copy(strvv, 1, i-1);
          q := leftTermin;
          while q^.adrcled <> nil do
            begin
              if q^.adrcled^.element = strvv
                then bool := false;
              q := q^.adrcled
            end;
          if bool then begin
                         dobavl_Termin(strvv,q);
                         new(q_);
                         q_^.element := strvv1;
                         q^.stran1 := q_;
                         writeln('������ ��������.')
                       end
                  else writeln('����� ������ ��� ����������.');
        end;
    3 : begin
          writeln('��������� ������ ������� �� ������ ��������?');
          readln(strvv);
          q := leftTermin;
          q := q^.adrcled;
          while (q <> nil) and (q^.element <> strvv) do
            q := q^.adrcled;
          if q = nil
            then
              writeln('������ ������� �� �������.')
            else
              begin
                bool := true;
                writeln('����� ��������� �� ������ ��������?');
                writeln('����� ������ ������� �������� ����������.');
                readln(strvv);
                k := 0;
                i := 1;
                j := 0;
                while strvv[i] <> ' ' do
                  i := i + 1;
                strvv1 := copy(strvv, i+1, length(strvv)-i);
                strvv := copy(strvv, 1, i-1);
                q1 := q^.podTermin;
                while (q1^.adrcled <> nil) and bool do
                  if q1^.adrcled^.element = strvv then bool := false
                                                  else q1 := q1^.adrcled;
                if bool then begin
                               dobavl_PodTermin(strvv,q1);
                               new(q_);
                               q_^.element := strvv1;
                               q1^.adrcled^.stran1 := q_;
                               writeln('��������� ��������.')
                             end
                        else writeln('����� ��������� ��� ����������.')
              end
        end;
    4 : udal_Poisk_termin(false);
    5 : udal_Poisk_PodTermin(false);
    6 : udal_poisk_Termin(true);
    7 : udal_poisk_PodTermin(true);
    8 : begin
          writeln('������ ������ ���������� �� ������ �����?');
          readln(strvv);
          bool := true;
          i := 0;
          q := leftTermin^.adrcled;
          while q <> nil do
            begin
              i := i+1;
              q1 := q^.PodTermin^.adrcled;
              while q1 <> nil do
                begin
                  if q1^.element = strvv
                    then begin
                           bool := false;
                           if i = 2 then writeln('������ ��������� ��������� �� ',i,'-�� �������.')
                                    else writeln('������ ��������� ��������� � ',i,'-�� �������.')
                         end;
                  q1 := q1^.adrcled
                end;
              q := q^.adrcled
            end;
          if bool then writeln('��������� ���� ��������� �� ������.')
        end;
    9 : begin
          qq := leftTermin;
          while qq^.adrcled <> nil do
            begin
              q := leftTermin;
              while q^.adrcled^.adrcled <> nil do
                begin
                  if q^.adrcled^.element > q^.adrcled^.adrcled^.element
                    then zamena_Termin(q);
                  q := q^.adrcled;
                end;
              qq := qq^.adrcled
            end;
          q := leftTermin^.adrcled;
          while q <> nil do
            begin
                  qq1 := q^.podTermin;
                  while qq1^.adrcled <> nil do
                    begin
                      q1 := q^.podTermin;
                      while q1^.adrcled^.adrcled <> nil do
                        begin
                          if q1^.adrcled^.element > q1^.adrcled^.adrcled^.element
                            then zamena_PodTermin(q1);
                          q1 := q1^.adrcled
                        end;
                      qq1 := qq1^.adrcled
                    end;
                  q := q^.adrcled
            end;
          writeln('������� � ���������� ������������� �� ��������.')
        end;
     10 : begin
          qq := leftTermin;
          while qq^.adrcled <> nil do
            begin
              q := leftTermin;
              while q^.adrcled^.adrcled <> nil do
                begin
                  if q^.adrcled^.stran1^.element > q^.adrcled^.adrcled^.stran1^.element
                    then zamena_Termin(q);
                  q := q^.adrcled;
                end;
              qq := qq^.adrcled
            end;
          q := leftTermin^.adrcled;
          while q <> nil do
            begin
                  qq1 := q^.podTermin;
                  while qq1^.adrcled <> nil do
                    begin
                      q1 := q^.podTermin;
                      while q1^.adrcled^.adrcled <> nil do
                        begin
                          if q1^.adrcled^.stran1^.element > q1^.adrcled^.adrcled^.stran1^.element
                            then zamena_PodTermin(q1);
                          q1 := q1^.adrcled
                        end;
                      qq1 := qq1^.adrcled
                    end;
                  q := q^.adrcled
            end;
          writeln('������� � ���������� ������������� �� ������� �������.')
        end;
     11 : begin
            assign(f,'file.txt');
            rewrite(f);
            q := leftTermin;
            while q^.adrcled <> nil do
              begin
                q := q^.adrcled;
                strvv := q^.element + ' ' + q^.stran1^.element;
                writeln(f,strvv);
                q1 := q^.podTermin;
                while q1^.adrcled <> nil do
                  begin
                    q1 := q1^.adrcled;
                    strvv := '    ' + q1^.element + ' ' + q1^.stran1^.element;
                    writeln(f,strvv)
                  end
              end;
            close(f);
            writeln('������ � ���� ��������� �������.');
          end;
     12 : begin
            assign(f,'file.txt');
            reset(f);
            q := leftTermin;
            while q^.adrcled <> nil do
              begin
                q1 := q^.podTermin;
                while q1 <> nil do
                  begin
                    udal_podTermin(q1);
                    q1 := q1^.adrcled
                  end;
                udal_Termin(q);
              end;
            q := leftTermin;
            while not(eof(f)) do
              begin
                readln(f,strvv);
                if strvv[1] <> ' '
                  then
                    begin
                      i := 1;
                      while strvv[i] <> ' ' do
                        i := i+1;
                      strvv1 := copy(strvv,i+1,length(strvv)-i);
                      delete(strvv,i,length(strvv)-i+1);
                      dobavl_Termin(strvv,q);
                      new(q_);
                      q_^.element := strvv1;
                      q^.stran1 := q_;
                      q1 := q^.podTermin
                    end
                  else
                    begin
                      i := 5;
                      while strvv[i] <> ' ' do
                        i := i+1;
                      strvv1 := copy(strvv, i+1, length(strvv)-i);
                      strvv := copy(strvv,5,i-5);
                      dobavl_PodTermin(strvv,q1);
                      q1 := q1^.adrcled;
                      new(q_);
                      q_^.element := strvv1;
                      q_^.adrcled := nil;
                      q1^.stran1 := q_;
                    end
              end;
            close(f);
            writeln('������ ����� ��������� �������.');
          end;

  end;
readln;
clrscr;
until vibor = 13;
end.