program ochered;
uses crt;
type
  adres = ^zveno;
  zveno = record
            element : integer;
            adrcled : adres;
            prioritet : integer
          end;
var
  a : array[1..6,1..10] of integer;
  b : array[1..6] of integer;
  sum, i, j : integer;
  left, right, q : adres;
procedure dobavl(var right : adres; elem : integer);
var
  q : adres;
begin
  new(q);
  q^.element := elem;
  q^.adrcled := nil;
  right^.adrcled := q;
  right := q;
end;
begin
  sum := 0;
  a[1,1]:=8; a[1,2]:=6; a[1,3]:=3; a[1,4]:=1; a[1,5]:=9; a[1,6]:=6; a[1,7]:=4; a[1,8]:=1; a[1,9]:=2; a[1,10]:=4;
  a[2,1]:=2; a[2,2]:=4; a[2,3]:=6; a[2,4]:=7; a[2,5]:=1; a[2,6]:=8; a[2,7]:=6; a[2,8]:=9; a[2,9]:=7; a[2,10]:=3;
  a[3,1]:=3; a[3,2]:=3; a[3,3]:=3; a[3,4]:=2; a[3,5]:=1; a[3,6]:=6; a[3,7]:=8; a[3,8]:=4; a[3,9]:=3;
  a[4,1]:=2; a[4,2]:=1; a[4,3]:=2; a[4,4]:=1; a[4,5]:=2; a[4,6]:=1; a[4,7]:=4; a[4,8]:=6; a[4,9]:=1;
  a[5,1]:=4; a[5,2]:=6; a[5,3]:=6; a[5,4]:=3; a[5,5]:=3; a[5,6]:=2; a[5,7]:=1; a[5,8]:=7; a[5,9]:=8;
  a[6,1]:=3; a[6,2]:=2; a[6,3]:=4; a[6,4]:=6; a[6,5]:=3; a[6,6]:=7; a[6,7]:=8; a[6,8]:=9; a[6,9]:=4;
  for i := 1 to 6 do
    for j := 1 to 10 do
      sum := sum + a[i, j];
  new(q);
  right := q;
  left := q;
  dobavl(right, 1);
  right^.prioritet := 1;
  dobavl(right, 2);
  right^.prioritet := 1;
  dobavl(right, 3);
  right^.prioritet := 2;
  dobavl(right, 4);
  right^.prioritet := 3;
  dobavl(right, 5);
  right^.prioritet := 3;
  dobavl(right, 6);
  right^.prioritet := 3;

  
  
  writeln(sum);
end.

