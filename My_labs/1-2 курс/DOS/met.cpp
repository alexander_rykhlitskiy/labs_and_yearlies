#include <stdio.h>
#include <graph.h>
#include "sysp.h"
#include "sysgraph.h"


void main(void) {

   int            crt_port;
   unsigned char  h_pos, l_pos;
   BIOS_VAR _far  *bios_var_ptr;
   // �������� ��������� �� ������� ���������� ������������ BIOS
   bios_var_ptr = (BIOS_VAR _far *) FP_MAKE(0x0000, 0x0410);
   // ���������� ����� ����� ���������� �������� ����������� ���
   crt_port = bios_var_ptr -> crt_address;
   // �������� ������� ���� �������� ��������� �������
   WriteReg(crt_port, 0x0E);
   // ��������� �������� �������� ����� �������� ��������� �������
   h_pos = ReadReg(crt_port + 1);
   // �������� ������� ���� �������� ��������� �������
   WriteReg(crt_port, 0x0F);
   // ��������� �������� �������� ����� �������� ��������� �������
   l_pos = ReadReg(crt_port + 1);
   printf("\n������� ����� ������� %X:%X\n",
            (unsigned char) h_pos, (unsigned char) l_pos );
}
