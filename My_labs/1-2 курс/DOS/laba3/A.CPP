#include <stdio.h>
#include <conio.h>
#include <dos.h>
#include <graphics.h>
#include <stdlib.h>
#include <math.h>
struct point
{
  int x, y;
};
int windows[4][4][4] = {{{250, 200, 200, 200},
			 {200, 200, 200, 250},
			 {200, 250, 250, 200}},
			{{200, 250, 200, 300},
			 {200, 300, 250, 300},
			 {250, 300, 200, 250}},
			{{250, 300, 300, 300},
			 {300, 300, 300, 250},
			 {300, 250, 250, 300}},
			{{300, 250, 300, 200},
			 {300, 200, 250, 200},
			 {250, 200, 300, 250}}};
void paint(point mas[10], int count);
int vision(int x1, int y1, int x2, int y2, int numbW)
{
  int a;
  int i = -1;
  float t, tdown = 0, tup = 1;
  signed long int D[2], w[2], Dxn, wxn, n[2];
  while (i < 2)
  {
    i++;
    n[0]=windows[numbW][i][3] - windows[numbW][i][1];
    n[1]=-(windows[numbW][i][2] - windows[numbW][i][0]);
    D[0] = x2 - x1;
    D[1] = y2 - y1;
    w[0] = x1 - windows[numbW][i][0];
    w[1] = y1 - windows[numbW][i][1];
    Dxn = D[0]*n[0] + D[1]*n[1];
    wxn = w[0]*n[0] + w[1]*n[1];
    if (Dxn == 0)
      {
	if (wxn < 0)
	  return 0;
	else continue;
      }
    else
    {
      t = (-1)*wxn/(float)Dxn;
      if (Dxn > 0)
      {
	if (t > 1)
	  return 0;
	if (t > tdown)
	  tdown = t;
      }
      else
      {
	if (t >= 0)
	{
	  if (t < tup)
	    tup = t;
	}
	else
	  return 0;
      }
    }
  }
    if (tdown <= tup)
    {
      int p1[2], p2[2];
      p1[0] = x1 + (x2 - x1)*tdown;
      p1[1] = y1 + (y2 - y1)*tdown;
      p2[0] = x1 + (x2 - x1)*tup;
      p2[1] = y1 + (y2 - y1)*tup;
      line(p1[0], p1[1], p2[0], p2[1]);
    }
  return 0;
}
void main()
{
  clrscr();
  int driver  = DETECT, mode, errorcode;
  initgraph(&driver, &mode, "c:\\tc\\bgi");
  errorcode = graphresult();
  if (errorcode != grOk)
    printf("error: %s\n", grapherrormsg(errorcode));
  point ps[6];
  point pt[3];
  for (int l = 0; l < 4; l++)
  for (int k = 0; k < 3; k++)
    line(windows[l][k][0], windows[l][k][1], windows[l][k][2], windows[l][k][3]);
  pt[0].x = 550;
  pt[0].y = 405;
  pt[1].x = 595;
  pt[1].y = 469;
  pt[2].x = 505;
  pt[2].y = 469;
  ps[0].x = 50;
  ps[0].y = 5;
  ps[5].x = 90;
  ps[5].y = 25;
  ps[4].x = 90;
  ps[4].y = 70;
  ps[3].x = 50;
  ps[3].y = 90;
  ps[2].x = 10;
  ps[2].y = 70;
  ps[1].x = 10;
  ps[1].y = 25;
  paint(pt, 3);
  paint(ps, 6);
  for (int i = 30; i > 1; i--)
  {
   for (k = 0; k < 4; k++)
    for (int j = 0; j <= 5; j++)
      vision(ps[j].x, ps[j].y, ps[(j+1)%6].x, ps[(j+1)%6].y, k);
    for (j = 0; j <= 5; j++)
    {
      ps[j].x = ps[j].x + (pt[j%3].x-ps[j].x)/i;
      ps[j].y = ps[j].y + (pt[j%3].y-ps[j].y)/i;
    }
    delay(100);
  }
  getchar();
  closegraph();
}

void paint(point mas[10], int count)
{
  for(int i = 0; i <= (count - 2); i++)
    line(mas[i].x, mas[i].y, mas[i + 1].x, mas[i + 1].y);
  line(mas[count - 1].x, mas[count - 1].y, mas[0].x, mas[0].y);
}