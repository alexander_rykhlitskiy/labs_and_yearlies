#include <graphics.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <conio.h>
/////////////////////////////////////////////////////
/////         ����������� �����                 /////
////////////////////////////////////////////////////
int code (float x,float y,float win[],int i)
{
    return 
    (x<win[i-4])<<3 | 
    (x>win[i-2])<<2 | 
    (y<win[i-3])<<1 | 
    (y>win[i-1]); 
}
/////////////////////////////////////////////////////
/////             ��������� �����               /////
////////////////////////////////////////////////////
void clip (float x1,float y1,float x2,float y2,float window[],int j)
{
     float dx,dy;
     int c1=code(x1,y1,window,j), c2=code(x2,y2,window,j);
     while(c1|c2) // ���� ���� �� ����� ������� ��� ��������������
     {
        if (c1&c2) // ���� ��� ����� � ����� ������� ��������������, �� ������� �� ���������� �������������
          return;
        dx=x2-x1;
        dy=y2-y1;
        if (c1)  // ���� ����� ������ ��� ����
        {
          if (x1<window[j-4]) // ���� ������ ����� ��� ���� �����
          {
            y1+=dy*(window[j-4]-x1)/dx;   //  ������� ����� �����
            x1=window[j-4];
          }
          else
            if (x1>window[j-2])    // ����  ������
            {
              y1+=dy*(window[j-2]-x1)/dx;  // ������� ������ �����
              x1=window[j-2];
            }
            else
              if (y1<window[j-3])    // ���� ����
              {
                x1+=dx*(window[j-3]-y1)/dy;   // ������� ������� �����
                y1=window[j-3];
              }
              else
                if (y1>window[j-1])    // ���� ���� 
                {
                  x1+=dx*(window[j-1]-y1)/dy;
                  y1=window[j-1];
                }
        c1=code(x1,y1,window,j);  // �������� ����
        }
        else  // ���� ����� ����� ����� ��� �������
        {
          if (x2<window[j-4])  // ������� ����� �����
          {
            y2+=dy*(window[j-4]-x2)/dx;
            x2=window[j-4];
          }
          else
            if (x2>window[j-2])   // ������� ������ �����
            {
              y2+=dy*(window[j-2]-x2)/dx;
              x2=window[j-2];
            }
            else
              if (y2<window[j-3])  // ������� ������� �����
              {
                x2+=dx*(window[j-3]-y2)/dy;
                y2=window[j-3];
              }
              else
                if (y2>window[j-3])  // ������� ������ �����
                {
                  x2+=dx*(window[j-3]-y2)/dy;
                  y2=window[j-3];
                }
          c2=code(x2,y2,window,j);  // �������� ����� 
        }
     }
     setcolor(0);  // ������������� ������ ����
     line((int)x1+1,(int)y1+1,(int)x2+1,(int)y2+1);  
     line((int)x1-1,(int)y1-1,(int)x2-1,(int)y2-1); 
     line((int)x1+1,(int)y1,(int)x2+1,(int)y2); 
     line((int)x1-1,(int)y1,(int)x2-1,(int)y2); 
     line((int)x1,(int)y1+1,(int)x2,(int)y2+1);
     line((int)x1,(int)y1-1,(int)x2,(int)y2-1);
     line((int)x1,(int)y1,(int)x2,(int)y2);
     setcolor(14); 
}
void figura (float koord[][2], int n, float win1[], float win2[], float win3[])
{
     int i;
     for(i=0;i<n;i++)
     {
       if (i<(n-1))
       {
         line((int)koord[i][0],(int)koord[i][1],(int)koord[i+1][0],(int)koord[i+1][1]);
         clip(koord[i][0],koord[i][1],koord[i+1][0],koord[i+1][1],win1,4);
         clip(koord[i][0],koord[i][1],koord[i+1][0],koord[i+1][1],win2,4);
         clip(koord[i][0],koord[i][1],koord[i+1][0],koord[i+1][1],win3,4);
       }
       else
       {
         line((int)koord[i][0],(int)koord[i][1],(int)koord[0][0],(int)koord[0][1]);
         clip(koord[i][0],koord[i][1],koord[0][0],koord[0][1],win1,4);
         clip(koord[i][0],koord[i][1],koord[0][0],koord[0][1],win2,4);
         clip(koord[i][0],koord[i][1],koord[0][0],koord[0][1],win3,4);
       }  
     }
}
void shag (float prirost[][2],float x1, float y1, float x2, float y2, int i)
{
     prirost[i][0]=(x2-x1)/30;
     prirost[i][1]=(y2-y1)/30;
}
/////////////////////////////////////////////////////
/////            ��������� ����                /////
////////////////////////////////////////////////////
void draw_window (float win1[], float win2[], float win3[])
{
    setcolor(7);
    line((int)win2[0],(int)win2[1],(int)win1[2],(int)win2[1]);
    line((int)win1[2],(int)win2[1],(int)win1[2],(int)win1[3]);
    line((int)win1[2],(int)win3[1],(int)win1[2],(int)win3[3]);
    line((int)win2[0],(int)win2[3],(int)win3[2],(int)win3[3]);
    line((int)win2[0],(int)win2[1],(int)win2[0],(int)win2[3]);
    line((int)win1[0],(int)win1[3],(int)win3[0],(int)win3[1]);
    line((int)win1[0],(int)win1[3],(int)win1[2],(int)win1[3]);
    line((int)win3[0],(int)win3[1],(int)win3[2],(int)win3[1]);
    setcolor(14);
}
void promezh_fig (float pt[][2],float koord[][2], float shag[][2], int n, int i, float pr1[], float pr2[], float pr3[])
{
     int k;
     if (i==2)
     {
       for (k=0;k<n;k++)
       {
           if (k<(n-1))
           {
             koord[k][0]=pt[k][0]+shag[k][0];
             koord[k][1]=pt[k][1]+shag[k][1];
           }
           else
           {
             koord[k][0]=pt[k-1][0]+shag[k][0];
             koord[k][1]=pt[k-1][1]+shag[k][1];
           }
       }
     }
     else
     {
         for (k=0;k<n;k++)
         {
             koord[k][0]+=shag[k][0];
             koord[k][1]+=shag[k][1];
         }
     }
     cleardevice();
     draw_window(pr1,pr2,pr3);
     setcolor(14);
     figura(koord,n,pr1,pr2,pr3);    
}
  
/////////////////////////////////////////////////////
/////         ������� ���������                 /////
////////////////////////////////////////////////////
int main(void)
{
    int j,m=6,sh=30,kol=4;
    float pryam1[4]={450.0,100.0,750.0,250.0};  // ���������� ������� ��������������
    float pryam2[4]={250.0,100.0,450.0,550.0};  // ���������� ������� ��������������
    float pryam3[4]={450.0,400.0,750.0,550.0};  // ���������� �������� ��������������
    float pyatiug[5][2]={{25.0,175.0},{125.0,25.0},{225.0,100.0},{300.0,225.0},{200.0,300.0}}; // ���������� �������������
    float shestiug[6][2]={{750.0,450.0},{800.0,550.0},{950.0,600.0},{1050.0,550.0},{1050.0,550.0},{900.0,350.0}};
    float prirost[6][2];
    float koord[6][2];
    initwindow(1200,650); 
	setlinestyle(0,0,1); 
    figura(pyatiug,kol+1,pryam1,pryam2,pryam3);
    for (j=0;j<m;j++)
    {
        if (j<(m-1))
           shag(prirost,pyatiug[j][0],pyatiug[j][1],shestiug[j][0],shestiug[j][1],j);
        else
           shag(prirost,pyatiug[j-1][0],pyatiug[j-1][1],shestiug[j][0],shestiug[j][1],j);   
    }
    for (j=1;j<=sh+1;j++)
    {
        promezh_fig(pyatiug,koord,prirost,m,j,pryam1,pryam2,pryam3);
        delay(100);
    }
    getch();       
    closegraph(); 
    return 0;
}
