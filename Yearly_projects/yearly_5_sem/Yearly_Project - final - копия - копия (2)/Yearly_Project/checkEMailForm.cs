﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using sendEmail;
using System.Threading;

namespace Yearly_Project
{
    public partial class checkEMailForm : Form
    {
        public checkEMailForm()
        {
            InitializeComponent();
        }
        Form mainForm;
        Mutex mut = new Mutex();
        EMailThread mail;
        public checkEMailForm(Form main)
        {
            InitializeComponent();
            mainForm = main;
            mail = new EMailThread(addressTextBox.Text, passwordTextBox.Text, 1);
        }
        private void CheckEMail()
        {
            mail = new EMailThread(addressTextBox.Text, passwordTextBox.Text, 1);
            //Thread thread = new Thread(new ThreadStart(mail.ReadEMails));
            //thread.Start();
            Cursor = Cursors.WaitCursor;
            mail.ReadEMails();
            for (int i = 0; i < mail.readEMails.Count & i < 13; i++)
            {
                dataGridView.Rows.Add(mail.fromEMails[i], mail.readEMails[i]);
            }
            Cursor = Cursors.Default;
            
        }
        private void checkButton_Click(object sender, EventArgs e)
        {
            CheckEMail();
            if (!addressTextBox.AutoCompleteCustomSource.Contains(addressTextBox.Text))
                addressTextBox.AutoCompleteCustomSource.Add(addressTextBox.Text);
        }

        private void checkEMailForm_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void addressTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                CheckEMail();
        }

        private void passwordTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                CheckEMail();
        }

        private void checkEMailForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            mainForm.Enabled = true;
        }

        private void dataGridView_DoubleClick(object sender, EventArgs e)
        {

        }

        private void dataGridView_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < mail.bodiesEMails.Count; i++)
                if ((dataGridView.Rows[i].Cells[0].Selected) | (dataGridView.Rows[i].Cells[1].Selected))
                    bodyRichTextBox.Text = mail.bodiesEMails[i];
        }

    }
}
