﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Yearly_Project
{
    public partial class inputPassword : Form
    {
        private WorkForm mainForm;
        public inputPassword()
        {
            InitializeComponent();
        }
        public inputPassword(WorkForm form)
        {
            mainForm = form;
            InitializeComponent();
        }
        private void CheckThePassword()
        {
            const String pathToHashingResult = "HashingResult";
            byte[] passwordFromFile = new byte[5];
            byte[] byteArray = new byte[inPassMaskedTextBox.Text.Length * 2];
            for (int i = 0; i < inPassMaskedTextBox.Text.Length; i++)
            {
                byteArray[i * 2] = (byte)((inPassMaskedTextBox.Text[i] & 0xFF00) >> 8);
                byteArray[i * 2 + 1] = (byte)(inPassMaskedTextBox.Text[i] & 0x00FF);
            }
            try
            {
                passwordFromFile = File.ReadAllBytes(pathToHashingResult);
            }
            catch (Exception e)
            {
                File.WriteAllBytes(pathToHashingResult, (Hashing.HashingSHA1(byteArray)));
            }
            byte[] delete = Hashing.HashingSHA1(byteArray);
            bool boolEquals = true;
            for (int i = 0; i < 20; i++)
                if (passwordFromFile[i] != delete[i])
                {
                    boolEquals = false;
                    break;
                }

            if (boolEquals)
            {
                this.Visible = false;
                mainForm.Enabled = true;
                mainForm.UseWaitCursor = false;
            }
            else
            {
                MessageBox.Show("Wrong password");
                inPassMaskedTextBox.Text = "";
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            CheckThePassword();
        }

        private void inPassMaskedTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                CheckThePassword();
        }

        private void inputPassword_FormClosed(object sender, FormClosedEventArgs e)
        {
            mainForm.Close();
        }
    }
}
