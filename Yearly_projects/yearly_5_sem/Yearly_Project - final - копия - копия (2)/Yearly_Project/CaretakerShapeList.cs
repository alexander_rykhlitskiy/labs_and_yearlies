﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace laba_1
{
    class CaretakerShapeList
    {
        private List<MementoShape> _memento;
        public List<MementoShape> undoMemento
        {
            set { _memento = value; }
            get { return _memento; }
        }
        private int _undoPoint;
        public int undoPoint
        {
            set { _undoPoint = value; }
            get { return _undoPoint; }
        }

    }
}
