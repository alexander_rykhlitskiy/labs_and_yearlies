﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace laba_1
{
    [Serializable]
    class BuilderEllipse : Builder
    {
        public override Shape createShape(MouseEventArgs e, Color colorOfShape, int thicknessOut)
        {
            ShapeEllipse creatingShape = new ShapeEllipse(e.X, e.Y, colorOfShape, thicknessOut);
            return creatingShape;
        }
        public BuilderEllipse(string strName)
        {
            name = strName;
        }
    }
}
