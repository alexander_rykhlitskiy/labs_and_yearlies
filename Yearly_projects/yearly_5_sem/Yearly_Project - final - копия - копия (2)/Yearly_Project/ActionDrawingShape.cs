﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;
using System.Drawing;
namespace laba_1
{
    class ActionDrawingShape : ActionChangingShape
    {
        public ActionDrawingShape(int numberOfShape)
            : base(numberOfShape)
        {
            this.numberOfShape = numberOfShape;
        }
        public ActionDrawingShape()
        {
            this.numberOfShape = 0;
        }
        public override void ChangeShape(MouseEventArgs e, List<Shape> shapeList)
        {
            shapeList[shapeList.Count - 1].xFinal = e.X;
            shapeList[shapeList.Count - 1].yFinal = e.Y;
        }
        public override Cursor MakePreparations(Cursor cursor, List<Shape> shapeList, System.Drawing.Bitmap mainBitmap)
        {
            return GetCursor();
        }
        public override Cursor GetCursor()
        {
            return Cursors.Cross;
        }
    }
}
