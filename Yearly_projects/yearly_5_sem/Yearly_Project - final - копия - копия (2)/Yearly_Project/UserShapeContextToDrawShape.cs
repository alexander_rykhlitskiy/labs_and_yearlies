﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace laba_1
{
    class UserShapeContextToDrawShape
    {
        public double scaleX, scaleY;
        public int shiftX, shiftY;
        public UserShapeContextToDrawShape(double scaleX, double scaleY, int shiftX, int shiftY)
        {
            this.scaleX = scaleX;
            this.scaleY = scaleY;
            this.shiftX = shiftX;
            this.shiftY = shiftY;
        }
    }
}
