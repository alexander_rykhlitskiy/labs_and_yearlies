﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

namespace laba_1
{
    class SaveLoad
    {
        public static void WriteBuilderInFile(int numberOfBaseShapes, List<Builder> builderList)
        {
            for (int i = numberOfBaseShapes; i < builderList.Count; i++)
            {
                FileStream fs = new FileStream(Environment.CurrentDirectory + "\\" + "filesForBuilders" + "\\"
                                               + builderList[i].name, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, builderList[i]);
                fs.Close();
            }
        }
        public static void ReadBuilderFromFile(List<Builder> builderList, ComboBox.ObjectCollection items)
        {
            //string[] fileEntries = Directory.GetFiles("filesForBuilders");
            //BuilderUserShape tempBuilderUserShape;
            //foreach (string fileName in fileEntries)
            //{
            //    FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            //    BinaryFormatter bf = new BinaryFormatter();
            //    tempBuilderUserShape = new BuilderUserShape(fileName);
            //    tempBuilderUserShape = (BuilderUserShape)bf.Deserialize(fs);
            //    builderList.Add(tempBuilderUserShape);
            //    items.Add(builderList[builderList.Count - 1].name);
            //    fs.Close();
            //}
        }
        private static String screensPath = "SavedScreens";
        public static void SaveScreen(List<Shape> shapeList, String name)
        {
            FileStream fs = new FileStream(screensPath + "\\" + name, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, shapeList);
            fs.Close();
        }
        public static List<String> LoadListScreenFile()
        {
            List<String> listFile = Directory.GetFiles(screensPath).ToList();
            return listFile;
        }
        public static void LoadScreen(ref List<Shape> shapeList, String name)
        {
            FileStream fs = new FileStream(screensPath + "\\" + name, FileMode.Open, FileAccess.Read, FileShare.Read);
            BinaryFormatter bf = new BinaryFormatter();
            shapeList = (List<Shape>)bf.Deserialize(fs);
            fs.Close();
        }
        public static void DeleteScreen(String name)
        {
            File.Delete(screensPath + "\\" + name);
        }
    }
}
