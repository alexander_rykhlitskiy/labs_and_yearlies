﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace laba_1
{
    [Serializable]
    class BuilderLine : Builder
    {
        public override Shape createShape(MouseEventArgs e, Color colorOfShape, int thicknessOut)
        {
            ShapeLine creatingShape = new ShapeLine(e.X, e.Y, colorOfShape, thicknessOut);
            return creatingShape;
        }
        public BuilderLine(string strName)
        {
            name = strName;
        }
    }
}