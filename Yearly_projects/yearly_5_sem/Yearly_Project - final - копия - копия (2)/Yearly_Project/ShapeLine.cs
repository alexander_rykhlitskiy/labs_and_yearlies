﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace laba_1
{
    [Serializable]
    class ShapeLine : Shape
    {
        public ShapeLine(int xInitial, int yInitial, Color chosenColor, int thickness) : 
                       base(xInitial, yInitial, chosenColor, thickness)
        {
        }
//        private int vxInitial, vyInitial, vxFinal, vyFinal;
        //private void SetVertexes(UserShapeContextToDrawShape data)
        //{
        //    this.vxInitial = (int)(xInitial * data.scaleX) + data.shiftX;
        //    this.vyInitial = (int)(yInitial * data.scaleY) + data.shiftY;
        //    this.vxFinal = (int)(xFinal * data.scaleX) + data.shiftX;
        //    this.vyFinal = (int)(yFinal * data.scaleY) + data.shiftY;
        //}
        public override void DrawShape(Bitmap someBitmap, UserShapeContextToDrawShape data = null)
        {
        //    SetVertexes(data);
            Graphics formGraphics = Graphics.FromImage(someBitmap);
            System.Drawing.Pen myPen;
            myPen = new Pen(chosenColor, thickness);
            formGraphics.DrawLine(myPen, (int)(xInitial * data.scaleX + data.shiftX), 
                                         (int)(yInitial * data.scaleY + data.shiftY),
                                         (int)((xFinal) * data.scaleX + data.shiftX),
                                         (int)((yFinal) * data.scaleY + data.shiftY));
            myPen.Dispose();
            formGraphics.Dispose();
        }
        public override bool IsPointInOrOut(int xPoint, int yPoint, UserShapeContextToDrawShape data = null)
        {
            int vxInitial = (int)(xInitial * data.scaleX) + data.shiftX;
            int vyInitial = (int)(yInitial * data.scaleY) + data.shiftY;
            int vxFinal = (int)(xFinal * data.scaleX) + data.shiftX;
            int vyFinal = (int)(yFinal * data.scaleY) + data.shiftY;
            bool result = false;
            const int halfWidthOfBorder = 18;
            int xCentre = vxFinal;
            int yCentre = vyFinal;
            int a = Math.Abs(vxInitial - vxFinal) / 2;
            int b = Math.Abs(vyInitial - vyFinal) / 2;
            if (((Math.Abs(xPoint - xCentre) < halfWidthOfBorder)
                & (Math.Abs(yPoint - yCentre) < halfWidthOfBorder)) |
                ((Math.Abs(yPoint - yCentre) < halfWidthOfBorder)
                & (Math.Abs(xPoint - xCentre) < halfWidthOfBorder)))
                result = true;
            else
                result = false;
            return result;
        }
    }
}
