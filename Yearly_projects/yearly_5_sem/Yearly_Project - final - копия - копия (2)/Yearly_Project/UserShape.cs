﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace laba_1
{
    [Serializable]
    class UserShape : Shape
    {
        public UserShape(int xInitial, int yInitial, Color chosenColor, int thickness, List<Shape> partsOfUserShape) : 
                       base(xInitial, yInitial, chosenColor, thickness)
        {
            this.partsOfUserShape = partsOfUserShape;
        }
        private List<Shape> partsOfUserShape;
        private double scaleX, scaleY;
        public override void DrawShape(Bitmap someBitmap, UserShapeContextToDrawShape data = null)
        {
            this.scaleX = (this.xFinal - this.xInitial) / (double)someBitmap.Width;
            this.scaleY = (this.yFinal - this.yInitial) / (double)someBitmap.Height;
            foreach (Shape drawingShape in partsOfUserShape)
            {
          //      drawingShape.chosenColor = this.chosenColor; //if it'll be necessary to change parameters of userShape drawing it
                data = new UserShapeContextToDrawShape(this.scaleX, this.scaleY, this.xInitial, this.yInitial);
                drawingShape.DrawShape(someBitmap, data);
            }
        }
        public override bool IsPointInOrOut(int xPoint, int yPoint, UserShapeContextToDrawShape data = null)
        {
            bool result = false;
            for (int i = 0; i < partsOfUserShape.Count; i++)
            {
                data = new UserShapeContextToDrawShape(scaleX, scaleY, xInitial, yInitial);
                if (partsOfUserShape[i].IsPointInOrOut(xPoint, yPoint, data))
                {
                    result = true;
                    break;
                }
            }
            return result;
        }
    }
}