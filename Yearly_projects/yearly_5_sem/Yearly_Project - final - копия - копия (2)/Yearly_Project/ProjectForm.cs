﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using laba_1;
using TI_4_form;
using System.Net.Mail;
using System.Net.Mime;
using System.Net;
using System.Collections;
using sendEmail;
using System.Threading;
namespace Yearly_Project
{
    public partial class WorkForm : Form
    {
        public WorkForm()
        {
            InitializeComponent();
            inputPassword temp = new inputPassword(this);
            temp.Show();
            this.Enabled = false;

            //newNotesRow.Text = "33333333333333333";
                //notesTableBindingSource.DataMember.dat
                //addNoteRichTextBox.Text = DecypheringNote(newNotesRow.Text);
        }
        private void SaveBinding()
        {

            this.Validate();
            this.notesTableBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.notesDataSet);
           
        }
        private void notesTableBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            SaveBinding();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'notesDataSet.NotesTable' table. You can move, or remove it, as needed.
            //foreach (NotesDataSet.NotesTableRow newRow in notesDataSet.NotesTable)
            //    newRow.Text = this.DecypheringNote(newRow.Text);
            this.notesTableTableAdapter.Fill(this.notesDataSet.NotesTable);
        }
        private String DecypheringNote(String text)
        {
            byte[] temp = new byte[text.Length * 2];
            for (int i = 0; i < text.Length; i++)
            {
                temp[i * 2] = (byte)((text[i] & 0xFF00) >> 8);
                temp[i * 2 + 1] = (byte)(text[i] & 0x00FF);
            }
            char[] strTemp = new char[text.Length];
            temp = IDEA.CypheringIDEA(temp, false);
            for (int i = 0; i < text.Length; i++)
            {
                strTemp[i] = (char)((temp[i * 2] << 8) | temp[i * 2 + 1]);
            }
            return new String(strTemp);
        }
        private String CypheringNote(String text)
        {
            byte[] temp = new byte[text.Length * 2];
            for (int i = 0; i < text.Length; i++)
            {
                temp[i * 2] = (byte)((text[i] & 0xFF00) >> 8);
                temp[i * 2 + 1] = (byte)(text[i] & 0x00FF);
            }
            temp = IDEA.CypheringIDEA(temp, true);
            char[] strTemp = new char[text.Length];
            for (int i = 0; i < text.Length; i++)
            {
                strTemp[i] = (char)((temp[i * 2] << 8) | temp[i * 2 + 1]);
            }
            return new String(strTemp);
        }
        private void AddNote()
        {
            string[] monthsOfYear = { "January", "February", "Mart", "April", "May", 
                                        "June", "July", "August", "September", "October",
                                        "November", "December" };
            NotesDataSet.NotesTableRow newNotesRow = notesDataSet.NotesTable.NewNotesTableRow();

            newNotesRow.Text = (addNoteRichTextBox.Text);
            newNotesRow.Date = String.Format("{0} {1} {2}", newNoteDateTimePicker.Value.Day.ToString(),
                monthsOfYear[newNoteDateTimePicker.Value.Month - 1], newNoteDateTimePicker.Value.Year.ToString());

            notesDataSet.NotesTable.Rows.Add(newNotesRow);
            MessageBox.Show("The note is added");
            addNoteRichTextBox.Text = "";
            SaveBinding();
        }
        private void addNoteBbutton_Click(object sender, EventArgs e)
        {
            AddNote();
        }


        private void drawButton_Click(object sender, EventArgs e)
        {
            Drawing tempForm = new Drawing();
            tempForm.Show();
        }

        private void sendToEmailButton_Click(object sender, EventArgs e)
        {
            string[] monthsOfYear = { "January", "February", "Mart", "April", "May", 
                                        "June", "July", "August", "September", "October",
                                        "November", "December" };
            sendToEMailTextBox.AutoCompleteCustomSource.Add(sendToEMailTextBox.Text);
            EMailThread mail = new EMailThread(sendToEMailTextBox.Text, addNoteRichTextBox.Text + String.Format("   {0} {1} {2}", newNoteDateTimePicker.Value.Day.ToString(),
                monthsOfYear[newNoteDateTimePicker.Value.Month - 1], newNoteDateTimePicker.Value.Year.ToString()));
            Thread thread = new Thread(new ThreadStart(mail.SendEMail));
            thread.Start();
        }

        private void changePasswordButton_Click(object sender, EventArgs e)
        {
            ChangePasswordForm form = new ChangePasswordForm(this);
            form.Show();
            this.Enabled = false;

        }

        private void checkEMailButton_Click(object sender, EventArgs e)
        {
            checkEMailForm form = new checkEMailForm(this);
            form.Show();
            this.Enabled = false;
        }


        private void deleteButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < notesTableDataGridView.Rows.Count; i++)
                if ((notesTableDataGridView.Rows[i].Selected) | (notesTableDataGridView.Rows[i].Cells[0].Selected)
                    | (notesTableDataGridView.Rows[i].Cells[1].Selected))
                    notesTableBindingSource.RemoveAt(i);
            SaveBinding();
        }

        private void WorkForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (NotesDataSet.NotesTableRow newRow in notesDataSet.NotesTable)
            {
                string str = this.CypheringNote(newRow.Text);
                newRow.Text = str;
            }
        }

        private void addNoteRichTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                AddNote();
        }

        private void notesTableDataGridView_DoubleClick(object sender, EventArgs e)
        {
            for (int i = 0; i < notesTableDataGridView.Rows.Count; i++)
                if ((notesTableDataGridView.Rows[i].Selected) | (notesTableDataGridView.Rows[i].Cells[0].Selected)
                    | (notesTableDataGridView.Rows[i].Cells[1].Selected))
                    addNoteRichTextBox.Text = notesTableDataGridView.Rows[i].Cells[0].Value.ToString();
        }

    }
}
