﻿namespace Yearly_Project
{
    partial class WorkForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.notesDataSet = new Yearly_Project.NotesDataSet();
            this.notesTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.notesTableTableAdapter = new Yearly_Project.NotesDataSetTableAdapters.NotesTableTableAdapter();
            this.tableAdapterManager = new Yearly_Project.NotesDataSetTableAdapters.TableAdapterManager();
            this.addNoteBbutton = new System.Windows.Forms.Button();
            this.addNoteRichTextBox = new System.Windows.Forms.RichTextBox();
            this.newNoteDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.notesTableDataGridView = new System.Windows.Forms.DataGridView();
            this.drawButton = new System.Windows.Forms.Button();
            this.sendToEmailButton = new System.Windows.Forms.Button();
            this.changePasswordButton = new System.Windows.Forms.Button();
            this.sendToEMailTextBox = new System.Windows.Forms.TextBox();
            this.checkEMailButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.notesDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.notesTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.notesTableDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // notesDataSet
            // 
            this.notesDataSet.DataSetName = "NotesDataSet";
            this.notesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // notesTableBindingSource
            // 
            this.notesTableBindingSource.DataMember = "NotesTable";
            this.notesTableBindingSource.DataSource = this.notesDataSet;
            // 
            // notesTableTableAdapter
            // 
            this.notesTableTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.NotesTableTableAdapter = this.notesTableTableAdapter;
            this.tableAdapterManager.UpdateOrder = Yearly_Project.NotesDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // addNoteBbutton
            // 
            this.addNoteBbutton.Location = new System.Drawing.Point(173, 49);
            this.addNoteBbutton.Name = "addNoteBbutton";
            this.addNoteBbutton.Size = new System.Drawing.Size(47, 23);
            this.addNoteBbutton.TabIndex = 2;
            this.addNoteBbutton.Text = "Add";
            this.addNoteBbutton.UseVisualStyleBackColor = true;
            this.addNoteBbutton.Click += new System.EventHandler(this.addNoteBbutton_Click);
            // 
            // addNoteRichTextBox
            // 
            this.addNoteRichTextBox.Location = new System.Drawing.Point(12, 78);
            this.addNoteRichTextBox.Name = "addNoteRichTextBox";
            this.addNoteRichTextBox.Size = new System.Drawing.Size(261, 178);
            this.addNoteRichTextBox.TabIndex = 4;
            this.addNoteRichTextBox.Text = "";
            this.addNoteRichTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.addNoteRichTextBox_KeyPress);
            // 
            // newNoteDateTimePicker
            // 
            this.newNoteDateTimePicker.Location = new System.Drawing.Point(12, 52);
            this.newNoteDateTimePicker.Name = "newNoteDateTimePicker";
            this.newNoteDateTimePicker.Size = new System.Drawing.Size(138, 20);
            this.newNoteDateTimePicker.TabIndex = 5;
            // 
            // notesTableDataGridView
            // 
            this.notesTableDataGridView.AutoGenerateColumns = false;
            this.notesTableDataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.notesTableDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.notesTableDataGridView.DataSource = this.notesTableBindingSource;
            this.notesTableDataGridView.Location = new System.Drawing.Point(296, 0);
            this.notesTableDataGridView.Name = "notesTableDataGridView";
            this.notesTableDataGridView.ReadOnly = true;
            this.notesTableDataGridView.Size = new System.Drawing.Size(383, 323);
            this.notesTableDataGridView.TabIndex = 9;
            this.notesTableDataGridView.DoubleClick += new System.EventHandler(this.notesTableDataGridView_DoubleClick);
            // 
            // drawButton
            // 
            this.drawButton.Location = new System.Drawing.Point(49, 289);
            this.drawButton.Name = "drawButton";
            this.drawButton.Size = new System.Drawing.Size(46, 22);
            this.drawButton.TabIndex = 10;
            this.drawButton.Text = "Draw";
            this.drawButton.UseVisualStyleBackColor = true;
            this.drawButton.Click += new System.EventHandler(this.drawButton_Click);
            // 
            // sendToEmailButton
            // 
            this.sendToEmailButton.Location = new System.Drawing.Point(12, 262);
            this.sendToEmailButton.Name = "sendToEmailButton";
            this.sendToEmailButton.Size = new System.Drawing.Size(83, 21);
            this.sendToEmailButton.TabIndex = 11;
            this.sendToEmailButton.Text = "Send to e-mail";
            this.sendToEmailButton.UseVisualStyleBackColor = true;
            this.sendToEmailButton.Click += new System.EventHandler(this.sendToEmailButton_Click);
            // 
            // changePasswordButton
            // 
            this.changePasswordButton.Location = new System.Drawing.Point(12, 12);
            this.changePasswordButton.Name = "changePasswordButton";
            this.changePasswordButton.Size = new System.Drawing.Size(100, 23);
            this.changePasswordButton.TabIndex = 12;
            this.changePasswordButton.Text = "Change password";
            this.changePasswordButton.UseVisualStyleBackColor = true;
            this.changePasswordButton.Click += new System.EventHandler(this.changePasswordButton_Click);
            // 
            // sendToEMailTextBox
            // 
            this.sendToEMailTextBox.AutoCompleteCustomSource.AddRange(new string[] {
            "sashkavalent@gmail.com",
            "sashka.rav@yandex.ru"});
            this.sendToEMailTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.sendToEMailTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.sendToEMailTextBox.Location = new System.Drawing.Point(101, 262);
            this.sendToEMailTextBox.Name = "sendToEMailTextBox";
            this.sendToEMailTextBox.Size = new System.Drawing.Size(137, 20);
            this.sendToEMailTextBox.TabIndex = 13;
            // 
            // checkEMailButton
            // 
            this.checkEMailButton.Location = new System.Drawing.Point(128, 12);
            this.checkEMailButton.Name = "checkEMailButton";
            this.checkEMailButton.Size = new System.Drawing.Size(79, 23);
            this.checkEMailButton.TabIndex = 14;
            this.checkEMailButton.Text = "Check e-mail";
            this.checkEMailButton.UseVisualStyleBackColor = true;
            this.checkEMailButton.Click += new System.EventHandler(this.checkEMailButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(226, 49);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(47, 23);
            this.deleteButton.TabIndex = 15;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Text";
            this.dataGridViewTextBoxColumn2.HeaderText = "Text";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 225;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Date";
            this.dataGridViewTextBoxColumn3.HeaderText = "Date";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 115;
            // 
            // WorkForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 323);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.checkEMailButton);
            this.Controls.Add(this.sendToEMailTextBox);
            this.Controls.Add(this.changePasswordButton);
            this.Controls.Add(this.sendToEmailButton);
            this.Controls.Add(this.drawButton);
            this.Controls.Add(this.notesTableDataGridView);
            this.Controls.Add(this.newNoteDateTimePicker);
            this.Controls.Add(this.addNoteRichTextBox);
            this.Controls.Add(this.addNoteBbutton);
            this.Name = "WorkForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Organizer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WorkForm_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.notesDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.notesTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.notesTableDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private NotesDataSet notesDataSet;
        private System.Windows.Forms.BindingSource notesTableBindingSource;
        private NotesDataSetTableAdapters.NotesTableTableAdapter notesTableTableAdapter;
        private NotesDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Button addNoteBbutton;
        private System.Windows.Forms.RichTextBox addNoteRichTextBox;
        private System.Windows.Forms.DateTimePicker newNoteDateTimePicker;
        private System.Windows.Forms.DataGridView notesTableDataGridView;
        private System.Windows.Forms.Button drawButton;
        private System.Windows.Forms.Button sendToEmailButton;
        private System.Windows.Forms.Button changePasswordButton;
        private System.Windows.Forms.TextBox sendToEMailTextBox;
        private System.Windows.Forms.Button checkEMailButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    }
}

