﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Collections;
using System.Windows.Forms;

namespace sendEmail
{
    class EMailThread
    {
        public List<string> readEMails;
        public List<string> bodiesEMails;
        public List<string> fromEMails;
        String toEMail { get; set; }
        String toEMailPassword { get; set; }
        String text { get; set; }
        public EMailThread(String toemail, String txt)
        {
            text = txt;
            toEMail = toemail;
            toEMailPassword = "";
            readEMails = new List<string>(); ;
        }
        public EMailThread(String toemail, String toemailpassword, int a)
        {
            toEMail = toemail;
            toEMailPassword = toemailpassword;
            readEMails = new List<string>();
            bodiesEMails = new List<string>();
            fromEMails = new List<string>();
        }
        public void ReadEMails()
        {
            try
            {
                // create client, connect and log in 
                Rebex.Net.Pop3 client = new Rebex.Net.Pop3();
                // client.Connect("pop.gmail.com");
                client.Connect("pop.gmail.com", 995, null, Rebex.Net.Pop3Security.Implicit);
                client.Login(toEMail, toEMailPassword);

                // get message list 
                Rebex.Net.Pop3MessageCollection list = client.GetMessageList();

                if (list.Count == 0)
                {
                    Console.WriteLine("There are no messages in the mailbox.");
                }
                else
                {
                    // download the first message 
                    for (int i = list.Count - 1; i > list.Count - 15; i--)
                    {
                        Rebex.Mail.MailMessage message = client.GetMailMessage(list[i].SequenceNumber);
                        readEMails.Add(message.Subject);
                        bodiesEMails.Add(message.BodyText);
                        fromEMails.Add(message.From.ToString());
                    }
                }

                client.Disconnect();
                MessageBox.Show("e-mail has been updated");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        public void SendEMail()
        {
            try
            {
                Console.WriteLine("SendEMail begins");
                var fromAddress = new MailAddress("sashaorganizer@gmail.com", "sashaorganizer");
                var toAddress = new MailAddress(toEMail);
                const string fromPassword = "organizer123";
                const string subject = "Hello";
                string body = text.ToString();

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }
                MessageBox.Show("The letter has been sent");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
