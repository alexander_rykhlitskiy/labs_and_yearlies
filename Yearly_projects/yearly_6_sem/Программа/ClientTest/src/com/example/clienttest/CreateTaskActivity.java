package com.example.clienttest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

public class CreateTaskActivity extends Activity {

	public static String TASK_TAG = "TASK";
	private Button addButton;
	private EditText nameEditText;
	private EditText contentEditText;
	private ProgressBar progressBar;
	private int userId;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_task);
		initUiComponents();
		if (getIntent() != null && getIntent().getExtras() != null) {
			userId = getIntent().getExtras().getInt(
					TasksListActivity.USER_ID_KEY);
			Log.d("TEST", "CreateTaskActivity.userId: " + userId);
		}
	}

	private void initUiComponents() {
		addButton = (Button) findViewById(R.id.addButton);
		addButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				progressBar.setVisibility(View.VISIBLE);
				new Thread(new Runnable() {
					@Override
					public void run() {
						final String response = new ServerApi().addTask(userId,
								new Task(0, nameEditText.getText().toString(),
										contentEditText.getText().toString(),
										new SimpleDateFormat(
												"dd/MM/yyyy hh:mm:ss",
												Locale.US).format(new Date())));
						CreateTaskActivity.this.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								if (!processResponse(response)) {
									Toast.makeText(CreateTaskActivity.this,
											"Inserting error",
											Toast.LENGTH_SHORT).show();
								}
							}
						});
					}
				}).start();
			}
		});

		nameEditText = (EditText) findViewById(R.id.nameEditText);
		contentEditText = (EditText) findViewById(R.id.contentEditText);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		progressBar.setVisibility(View.GONE);
	}

	private boolean processResponse(final String response) {
		if (response != null && !response.equals("")) {
			progressBar.setVisibility(View.GONE);
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(response);
				if (jsonObject.getInt("result") == 1) {
					CreateTaskActivity.this.finish();
					return true;
				} else {
					return false;
				}
			} catch (JSONException e) {
				e.printStackTrace();
				return false;
			}
		} else {
			return false;
		}

	}
}
