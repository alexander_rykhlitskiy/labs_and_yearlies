package com.example.clienttest;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

public class TaskActivity extends Activity {

	public static String TASK_TAG = "TASK";
	private Task task;
	private Button deleteButton;
	private Button updateButton;
	private EditText nameEditText;
	private EditText contentEditText;
	private ProgressBar progressBar;
	private int userId;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_task);

		if (getIntent() != null && getIntent().getExtras() != null
				&& getIntent().getExtras().getParcelable(TASK_TAG) != null) {
			task = getIntent().getExtras().getParcelable(TASK_TAG);
		}
		if (getIntent() != null && getIntent().getExtras() != null) {
			userId = getIntent().getExtras().getInt(
					TasksListActivity.USER_ID_KEY);
			Log.d("TEST", "TaskActivity.userId: " + userId);
		}
		initUiComponents();
	}

	private void initUiComponents() {
		deleteButton = (Button) findViewById(R.id.deleteButton);
		deleteButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				progressBar.setVisibility(View.VISIBLE);
				new Thread(new Runnable() {
					@Override
					public void run() {
						final String response = new ServerApi().deleteTask(
								userId, task.getId());
						TaskActivity.this.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								progressBar.setVisibility(View.GONE);
								if (response != null && !response.equals("")) {
									JSONObject jsonObject;
									try {
										jsonObject = new JSONObject(response);
										if (jsonObject.getInt("result") == 1) {
											TaskActivity.this.finish();
										} else {
											Toast.makeText(TaskActivity.this,
													"Deleting error",
													Toast.LENGTH_SHORT).show();
										}
									} catch (JSONException e) {
										e.printStackTrace();
									}
								}
							}
						});
					}
				}).start();
			}
		});
		updateButton = (Button) findViewById(R.id.updateButton);
		updateButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				progressBar.setVisibility(View.VISIBLE);
				new Thread(new Runnable() {
					@Override
					public void run() {
						final String response = new ServerApi().updateTask(
								userId, task.getId(), new Task(task.getId(),
										nameEditText.getText().toString(),
										contentEditText.getText().toString(),
										task.getDate()));
						TaskActivity.this.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								progressBar.setVisibility(View.GONE);
								if (response != null && !response.equals("")) {
									JSONObject jsonObject;
									try {
										jsonObject = new JSONObject(response);
										if (jsonObject.getInt("result") == 1) {
											Toast.makeText(TaskActivity.this,
													"Updated successfully",
													Toast.LENGTH_SHORT).show();
										} else {
											Toast.makeText(TaskActivity.this,
													"Updating error",
													Toast.LENGTH_SHORT).show();
										}
									} catch (JSONException e) {
										e.printStackTrace();
										Toast.makeText(TaskActivity.this,
												"Updating error",
												Toast.LENGTH_SHORT).show();
									}
								}
							}
						});
					}
				}).start();
			}
		});
		nameEditText = (EditText) findViewById(R.id.nameEditText);
		nameEditText.setText(task.getName());
		contentEditText = (EditText) findViewById(R.id.contentEditText);
		contentEditText.setText(task.getContent());
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		progressBar.setVisibility(View.GONE);
	}
}
