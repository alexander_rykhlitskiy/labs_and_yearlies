package com.example.clienttest;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends Activity {

	private Button connectButton;
	private EditText loginEditText;
	private EditText passwordEditText;
	private ProgressBar progressBar;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		initUiComponents();

		connectButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				progressBar.setVisibility(View.VISIBLE);
				new Thread(new Runnable() {
					@Override
					public void run() {
						final String response = new ServerApi().login(loginEditText.getText().toString(),
								passwordEditText.getText().toString());
						MainActivity.this.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								progressBar.setVisibility(View.GONE);
								if (!processLoginResult(response)) {
									Toast.makeText(MainActivity.this, "Login error", Toast.LENGTH_SHORT).show();
								}
							}
						});
					}
				}).start();
			}
		});
	}

	private boolean processLoginResult(String response) {
		if (response != null && !response.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(response);
				if (jsonObject.getInt("result") == 1) {
					Intent intent = new Intent(MainActivity.this, TasksListActivity.class);
					intent.putExtra(TasksListActivity.USER_ID_KEY, jsonObject.getInt("User_id"));
					startActivity(intent);
					finish();
					return true;
				} else {
					return false;
				}
			} catch (JSONException e) {
				e.printStackTrace();
				return false;
			}
		} else {
			return false;
		}
	}

	private void initUiComponents() {
		connectButton = (Button) findViewById(R.id.connectButton);
		loginEditText = (EditText) findViewById(R.id.loginEditText);
		passwordEditText = (EditText) findViewById(R.id.passwordEditText);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		progressBar.setVisibility(View.GONE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
}
