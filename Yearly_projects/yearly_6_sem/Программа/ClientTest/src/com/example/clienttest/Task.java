package com.example.clienttest;

import android.os.Parcel;
import android.os.Parcelable;

public class Task implements Parcelable {

	public Task(int id, String name, String content, String date) {
		this.name = name;
		this.content = content;
		this.date = date;
		this.id = id;
	}

	private int id;
	private String name;
	private String content;
	private String date;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int describeContents() {
		return 0;
	}

	// ����������� ������ � Parcel
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeString(date);
		parcel.writeString(content);
		parcel.writeString(name);
		parcel.writeInt(id);
	}

	public static final Parcelable.Creator<Task> CREATOR = new Parcelable.Creator<Task>() {
		// ������������� ������ �� Parcel
		public Task createFromParcel(Parcel in) {
			return new Task(in);
		}

		public Task[] newArray(int size) {
			return new Task[size];
		}
	};

	// �����������, ����������� ������ �� Parcel
	private Task(Parcel parcel) {
		date = parcel.readString();
		content = parcel.readString();
		name = parcel.readString();
		id = parcel.readInt();
	}
}
