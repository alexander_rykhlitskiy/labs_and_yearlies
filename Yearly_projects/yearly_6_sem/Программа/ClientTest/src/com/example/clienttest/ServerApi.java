package com.example.clienttest;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.example.clienttest.UrlConnection.HttpMethod;

public class ServerApi {

	private static final String URL = "http://192.168.1.111:48530/";
	private final static String TASKS = "api/tasks/";
	private final static String USERS = "api/users/";

	public String login(String login, String password) {
		UrlConnection connection = new UrlConnection();
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("login", login));
		params.add(new BasicNameValuePair("password", password));
		return connection.call(URL + USERS, HttpMethod.POST, params);
	}

	public String addTask(int userId, Task task) {
		UrlConnection connection = new UrlConnection();
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("Name", task.getName()));
		params.add(new BasicNameValuePair("Content", task.getContent()));
		params.add(new BasicNameValuePair("Date", task.getDate()));
		params.add(new BasicNameValuePair("User_id", String.valueOf(userId)));
		return connection.call(URL + TASKS, HttpMethod.POST, params);
	}

	public String deleteTask(int user_id, int id) {
		UrlConnection connection = new UrlConnection();
		return connection.call(URL + TASKS + id, HttpMethod.DELETE, null);
	}

	public String updateTask(int user_id, int id, Task task) {
		UrlConnection connection = new UrlConnection();
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("Name", task.getName()));
		params.add(new BasicNameValuePair("Content", task.getContent()));
		params.add(new BasicNameValuePair("Id", String.valueOf(id)));
		params.add(new BasicNameValuePair("User_id", String.valueOf(user_id)));
		return connection.call(URL + TASKS + id, HttpMethod.PUT, params);
	}

	public String getTasks(int userId) {
		UrlConnection connection = new UrlConnection();
		return connection.call(URL + TASKS.substring(0, TASKS.length() - 1)
				+ "?user_id=" + userId, HttpMethod.GET, null);
	}
}
