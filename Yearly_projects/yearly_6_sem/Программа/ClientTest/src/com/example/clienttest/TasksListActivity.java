package com.example.clienttest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class TasksListActivity extends Activity {

	private ListView tasksListView;
	private ProgressBar progressBar;
	public static final String USER_ID_KEY = "USER_ID_KEY";
	private Button createTaskButton;
	private int userId;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tasks);
		if (getIntent() != null && getIntent().getExtras() != null) {
			userId = getIntent().getExtras().getInt(USER_ID_KEY);
		}
		initUiComponents();
	}

	@Override
	protected void onResume() {
		super.onResume();
		progressBar.setVisibility(View.VISIBLE);
		new Thread(new Runnable() {
			@Override
			public void run() {
				final ArrayList<Task> tasks = processTasksListResponse(new ServerApi()
						.getTasks(userId));
				if (tasks == null) {
					TasksListActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							progressBar.setVisibility(View.GONE);
							Toast.makeText(TasksListActivity.this,
									"Loading tasks list error",
									Toast.LENGTH_SHORT).show();
						}
					});
				} else {
					TasksListActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							progressBar.setVisibility(View.GONE);
							tasksListView.setAdapter(new TasksArrayAdapter(
									TasksListActivity.this, tasks));
						}
					});
				}
			}
		}).start();
	}

	private ArrayList<Task> processTasksListResponse(String response) {
		try {
			final ArrayList<Task> tasks = new ArrayList<Task>();
			if (response != null && !response.equals("")) {
				JSONArray tasksJsonArray = new JSONArray(response);
				for (int i = 0; i < tasksJsonArray.length(); i++) {
					JSONObject jsonObject = tasksJsonArray.getJSONObject(i);
					tasks.add(new Task(jsonObject.getInt("Id"), jsonObject
							.getString("Name"),
							jsonObject.getString("Content"), jsonObject
									.getString("User_id")));
				}
				return tasks;
			} else {
				return null;
			}
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	private void initUiComponents() {
		tasksListView = (ListView) findViewById(R.id.tasksListView);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		progressBar.setVisibility(View.GONE);
		createTaskButton = (Button) findViewById(R.id.createTaskButton);
		createTaskButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(TasksListActivity.this,
						CreateTaskActivity.class);
				Log.d("TEST", "TasksListActivity.userId: " + userId);
				intent.putExtra(TasksListActivity.USER_ID_KEY, userId);
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	private class TasksArrayAdapter extends ArrayAdapter<Task> {
		private List<Task> items;

		public TasksArrayAdapter(Context context, List<Task> items) {
			super(context, 0, items);
			this.items = items;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			View row = convertView;
			TaskHolder holder = null;
			if (row == null) {
				row = getLayoutInflater()
						.inflate(R.layout.list_item_task, null);
				holder = new TaskHolder();
				holder.nameTextView = (TextView) row
						.findViewById(R.id.nameTextView);
				holder.contentTextView = (TextView) row
						.findViewById(R.id.contentTextView);
				holder.dateTextView = (TextView) row
						.findViewById(R.id.dateTextView);
				row.setTag(holder);
			} else {
				holder = (TaskHolder) row.getTag();
			}
			final Task item = items.get(position);
			holder.nameTextView.setText(item.getName());
			holder.contentTextView.setText(item.getContent());
			// holder.dateTextView.setText(new
			// SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa", Locale.US).format(Long
			// .parseLong(item.getDate) * 1000));
			//holder.dateTextView.setText(item.getDate());
			holder.dateTextView.setText(new SimpleDateFormat(
					"dd/MM/yyyy",
					Locale.US).format(new Date()));

			row.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(TasksListActivity.this,
							TaskActivity.class);
					Log.i("test", "itemId:" + item.getId());
					intent.putExtra(TaskActivity.TASK_TAG, item);
					intent.putExtra(TasksListActivity.USER_ID_KEY, userId);
					startActivity(intent);
				}
			});

			return row;
		}

		class TaskHolder {
			TextView nameTextView;
			TextView contentTextView;
			TextView dateTextView;
		}
	}
}
