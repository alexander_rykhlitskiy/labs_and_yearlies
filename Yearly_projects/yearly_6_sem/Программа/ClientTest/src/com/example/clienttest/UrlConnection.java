package com.example.clienttest;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;

import org.apache.http.NameValuePair;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class UrlConnection {

	private static final String TAG = "ClientTest";

	public enum HttpMethod {
		GET, POST, PUT, DELETE
	}

//	public boolean isOnline(Context context) {
//		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//		NetworkInfo nInfo = cm.getActiveNetworkInfo();
//		if (nInfo != null && nInfo.isConnected()) {
//			Log.v(TAG, "[UrlConnection.isOnline()] ONLINE");
//			return true;
//		} else {
//			Log.v(TAG, "[UrlConnection.isOnline()] OFFLINE");
//			return false;
//		}
//	}

	public String call(String url, HttpMethod method, List<NameValuePair> params) {
		try {
			InputStream is = openHttpConnection(url, method, params);
			String resultJsonString = getResponseString(is);
			return resultJsonString;
		} catch (Exception e) {
			Log.e(TAG, "UrlConnection.call] Error");
			e.printStackTrace();
			return "Error";
		}
	}

	private InputStream openHttpConnection(String strURL, HttpMethod method, List<NameValuePair> params)
			throws IOException {
		InputStream inputStream = null;
		URL url = new URL(strURL);
		URLConnection conn = url.openConnection();
		try {
			HttpURLConnection httpConn = (HttpURLConnection) conn;
			httpConn.setReadTimeout(10000);
			httpConn.setConnectTimeout(15000);
			httpConn.setDoInput(true);

			httpConn.setRequestMethod(method.name());
			if (params != null && (method == HttpMethod.POST || method == HttpMethod.PUT)) {
				httpConn.setDoOutput(true);
				OutputStream os = httpConn.getOutputStream();
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
				writer.write(getQuery(params));
				writer.close();
				os.close();
			}
			httpConn.connect();
			inputStream = httpConn.getInputStream();
		} catch (IOException e) {
			Log.e(TAG, "[UrlConnection.openHttpConnection()] Failed OpenHttpConnection", e);
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return inputStream;
	}

	private String getResponseString(InputStream is) {
		String resultJsonString = "";
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			resultJsonString = sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "[UrlConnection.getResponseString()] Error converting result " + e.toString());
		}
		return resultJsonString;
	}

	private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException {
		StringBuilder result = new StringBuilder();
		boolean first = true;

		for (NameValuePair pair : params) {
			if (first)
				first = false;
			else
				result.append("&");

			result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
			result.append("=");
			result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
		}

		return result.toString();
	}
}