﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Yearly.Models;

namespace Yearly.Controllers
{
    public class TasksController : ApiController
    {
        private TasksBaseContext db = new TasksBaseContext();
        public TasksController()
        {
            this.db.Configuration.ProxyCreationEnabled = false;
        }

        // GET api/Tasks
        public IEnumerable<Tasks> GetTasks()
        {
        //    var tasks = db.Tasks.Include(t => t.Users);
            var tasks = db.Tasks;
            return tasks.AsEnumerable();
        }
        // GET localhost:48530/api/tasks?user_id=1
        public IEnumerable<Tasks> GetTasks(int user_id)
        {
            var tasks = db.Tasks.AsEnumerable();
            return tasks.Where((t) => int.Equals(t.User_id, user_id));
        }

        // GET api/Tasks/5
        //public Tasks GetTasksByID(int id)
        //{
        //    Tasks tasks = db.Tasks.Find(id);
        //    if (tasks == null)
        //    {
        //        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
        //    }

        //    return tasks;
        //}

        // PUT api/Tasks/5
        public HttpResponseMessage PutTasks(int id, Tasks tasks)
        {
            if (ModelState.IsValid && id == tasks.Id)
            {
                db.Entry(tasks).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

                return Request.CreateResponse(HttpStatusCode.OK, new Result(1));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // POST api/Tasks
        public HttpResponseMessage PostTasks(Tasks tasks)
        {
   //         if (ModelState.IsValid)
            {
                db.Tasks.Add(tasks);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, new Result(1));
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = tasks.Id }));
                return response;
            }
            //else
            //{
            //    return Request.CreateResponse(HttpStatusCode.BadRequest);
            //}
        }

        // DELETE api/Tasks/5
        public HttpResponseMessage DeleteTasks(int id)
        {
            Tasks tasks = db.Tasks.Find(id);
            if (tasks == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Tasks.Remove(tasks);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, new Result(1));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}