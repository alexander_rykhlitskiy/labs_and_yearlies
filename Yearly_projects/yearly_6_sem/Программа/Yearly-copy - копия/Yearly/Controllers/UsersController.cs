﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Yearly.Models;

namespace Yearly.Controllers
{
    public class UsersController : ApiController
    {
        private TasksBaseContext db = new TasksBaseContext();
        public UsersController()
        {
            db.Configuration.ProxyCreationEnabled = false;
        }
        // GET api/Users
        public IEnumerable<Users> GetUsers()
        {
            return db.Users.AsEnumerable();
        }
        // GET localhost:48530/api/users?login=sasha&password=123
        public IEnumerable<Users> GetUsers(string login, string password)
        {
        //    var sameUser = (db.Users.Where((u) => (String.Equals(u.login, users.login,
        //StringComparison.OrdinalIgnoreCase)))).FirstOrDefault();
            var users = db.Users.AsEnumerable();
            return users.Where((u) => (String.Equals(u.login, login,
                    StringComparison.OrdinalIgnoreCase) & (String.Equals(u.password, password,
                    StringComparison.OrdinalIgnoreCase))));
        }
        // GET api/Users/5
        public Users GetUsers(int id)
        {
            Users users = db.Users.Find(id);
            if (users == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return users;
        }

        // PUT api/Users/5
        public HttpResponseMessage PutUsers(int id, Users users)
        {
            if (ModelState.IsValid && id == users.Id)
            {
                db.Entry(users).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // POST api/Users
        public HttpResponseMessage PostUsers(Users users)
        {
            var sameUser = (db.Users.Where((u) => (String.Equals(u.login, users.login)))).FirstOrDefault();
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, new LoginSuccess(1, sameUser.Id));
            return response;
        }
        //public HttpResponseMessage PostUsers(Users users)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Users.Add(users);
        //        db.SaveChanges();

        //        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, users);
        //        response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = users.Id }));
        //        return response;
        //    }
        //    else
        //    {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest);
        //    }
        //}
        // DELETE api/Users/5
        public HttpResponseMessage DeleteUsers(int id)
        {
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Users.Remove(users);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, users);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}