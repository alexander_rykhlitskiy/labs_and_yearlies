﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Yearly.Models
{
    public class LoginSuccess
    {
        public LoginSuccess(int _result, int _user_id)
        {
            this.result = _result;
            this.User_id = _user_id;
        }
        public int result { get; set; }
        public int User_id { get; set; }
    }
}