﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Yearly.Models
{
    public class Result
    {
        public Result(int result)
        {
            this.result = result;
        }
        public int result { get; set; }
    }
}