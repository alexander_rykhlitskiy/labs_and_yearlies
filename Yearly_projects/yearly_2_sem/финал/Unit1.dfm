object Field: TField
  Left = 263
  Top = 128
  Width = 537
  Height = 579
  Caption = 'Small Tanks'
  Color = clBlack
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnActivate = FormActivate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 368
    Top = 48
    Width = 49
    Height = 49
  end
  object Label1: TLabel
    Left = 176
    Top = 32
    Width = 6
    Height = 32
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object healthLabel: TLabel
    Left = 24
    Top = 464
    Width = 5
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -21
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object MediaPlayer1: TMediaPlayer
    Left = 32
    Top = 0
    Width = 253
    Height = 30
    AutoOpen = True
    FileName = 'battle_city.wma'
    Visible = False
    TabOrder = 0
  end
  object shot: TMediaPlayer
    Left = 0
    Top = 144
    Width = 253
    Height = 30
    AutoOpen = True
    FileName = 'shot.wma'
    Visible = False
    TabOrder = 1
  end
  object miss: TMediaPlayer
    Left = 0
    Top = 176
    Width = 253
    Height = 30
    AutoOpen = True
    FileName = 'miss.wma'
    Visible = False
    TabOrder = 2
  end
  object bang: TMediaPlayer
    Left = 0
    Top = 208
    Width = 253
    Height = 30
    AutoOpen = True
    FileName = 'bang.wma'
    Visible = False
    TabOrder = 3
  end
  object racing: TMediaPlayer
    Left = 0
    Top = 240
    Width = 253
    Height = 30
    AutoOpen = True
    FileName = 'racing.wma'
    Visible = False
    TabOrder = 4
  end
  object MainMenu1: TMainMenu
    object N1: TMenuItem
      Caption = #1053#1086#1074#1072#1103' '#1080#1075#1088#1072
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #1042#1099#1093#1086#1076
      OnClick = N2Click
    end
  end
  object Timer1: TTimer
    Interval = 50
    OnTimer = Timer1Timer
    Top = 32
  end
  object Timer2: TTimer
    Interval = 15
    OnTimer = Timer2Timer
    Top = 64
  end
end
