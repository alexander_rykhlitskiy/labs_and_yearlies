unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ExtCtrls, StdCtrls, MPlayer;

type
  RecSnar = record
              napravl : byte;
              im : TImage
            end;
  RecVragSnar = record
                  im : TImage;
                  napr : byte;
                  vipustVrag : integer //����� �����, ������� �������� ������
                end;
  RecVrag = record
              x, y : integer;
              bool : boolean;
              im : TImage;    //bool �������� �� ��������� ��������� x � y
              napr : integer
            end;


  TField = class(TForm)
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    Image1: TImage;
    Timer1: TTimer;
    Label1: TLabel;
    MediaPlayer1: TMediaPlayer;
    Timer2: TTimer;
    shot: TMediaPlayer;
    miss: TMediaPlayer;
    bang: TMediaPlayer;
    racing: TMediaPlayer;
    healthLabel: TLabel;
    procedure N2Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure Timer2Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  speed, snarNapr, iSnar, level, level1, health, iHealth : integer;    {snarNapr, isnar - ������� � ����� ������� � �������}
  iVrag, perezar, iVragSnar, dopVrag : integer;       {iVrag - ����� ����� � �������}
  tank, gerb : TImage;                                      {dopVrag - ��� ���������� ������}
  vragSnar : array[1..100] of RecVragSnar;          {level1 - ��� ������������ �������� � ����� ������}
  snar : array[1..10] of RecSnar;
  vrag : array[1..10] of RecVrag;
  boolEnd, pauseBool, zvezdaBool, racingBool : boolean;
  maxDopVrag : integer;
  Field: TField;

implementation

{$R *.dfm}

procedure vystrel(j, napr : byte);
begin
  iVragSnar := iVragSnar + 1;
  VragSnar[iVragSnar].im := TImage.Create(Field);
  VragSnar[iVragSnar].im.Parent := field;
  case napr of
    1 : VragSnar[iVragSnar].im.Picture.LoadFromFile('snarDown.bmp');
    2 : VragSnar[iVragSnar].im.Picture.LoadFromFile('snar.bmp');
    3 : VragSnar[iVragSnar].im.Picture.LoadFromFile('snarRight.bmp');
    4 : VragSnar[iVragSnar].im.Picture.LoadFromFile('snarLeft.bmp');
  end;
  VragSnar[iVragSnar].im.top := vrag[j].im.top + 18;
  VragSnar[iVragSnar].im.left := vrag[j].im.left + 20;
  VragSnar[iVragSnar].vipustVrag := j;
  VragSnar[iVragSnar].napr := napr
end;

procedure new;
var j : byte;
begin
  zvezdaBool := false;
  iHealth := 0;
  racingBool := true;
  dopVrag := 0;
  level1 := 0;
  level := level + 1;
  for j := 1 to iSnar do
    snar[j].im.Destroy;
  for j := 1 to iVragSnar do
    VragSnar[j].im.Destroy;
  perezar := 10;
  speed := 5;
//�������� �������
  if tank <> nil then tank.Destroy;
  gerb := TImage.Create(Field);
  gerb.Parent := field;
  gerb.Picture.LoadFromFile('gerb.bmp');
  gerb.top := 420;
  gerb.left := 250;
  tank := TImage.Create(Field);
  tank.Parent := field;
  tank.Picture.LoadFromFile('tank.bmp');
  tank.top := 210;
  tank.left := 250;
  SnarNapr := 1;
  iSnar :=0;
//�������� ������
  if vrag[1].im <> nil
    then for j := 1 to iVrag do
           vrag[j].im.Destroy;
  iVrag := 4;
  for j := 1 to iVrag do
    with vrag[j] do
      begin
        im := TImage.Create(field);
        im.Picture.LoadFromFile('tankvrag.bmp');
        im.Parent := field;
      end;
  vrag[1].im.top := 50;
  vrag[1].im.left := 50;
  vrag[2].im.top := 400;
  vrag[2].im.left := 50;
  vrag[3].im.top := 50;
  vrag[3].im.left := 400;
  vrag[4].im.top := 400;
  vrag[4].im.left := 400;
//���� ������ ����������
  for j := 1 to iVrag do
    begin
      vrag[j].bool := true;
      vrag[j].x := random(83) * 5 + 20;
      vrag[j].y := random(83) * 5 + 20
    end;
  iVragSnar := 0;
end;

procedure TField.N2Click(Sender: TObject);
begin
  close;
end;

procedure TField.N1Click(Sender: TObject);
begin
  health := 3;
  pauseBool := false;
  boolEnd := false;
  level := 0;
  new;
  mediaPlayer1.Play;
  label1.Caption := '������� ' + intToStr(level);
end;

//������� ������ ���������� ����� ��� �����
function fDopVrag(a, b : integer) : boolean;
var
  j3 : integer;
  bool, bool1 : boolean;
begin
  bool1 := true;
  for j3 := 1 to iVrag do
    begin
      bool := not
      (((vrag[j3].im.Top < a + 100) and
      (vrag[j3].im.Top > a - 100) and
      (vrag[j3].im.left > b - 100) and
      (vrag[j3].im.left < b + 100)) or
      ((tank.Top < a + 100) and
      (tank.Top > a - 100) and
      (tank.left > b - 100) and
      (tank.left < b + 100)));
      if bool = false
        then begin
               bool1 := false;
               break;
             end
    end;
    fDopVrag := bool1;
end;

procedure TField.FormActivate(Sender: TObject);
var
  gran : TImage;
  x, y, j : integer;
begin
  x := 0;
  y := 0;
  gran := TImage.Create(field);
  gran.Picture.LoadFromFile('gran.bmp');
  gran.Parent := field;
  gran.Top := y;
  gran.Left := x;
  for j := 1 to 88 do
    begin
      if j < 44 then
        if (j > 22) then x := x + 22
                    else y := y + 22;
      if j > 44 then
        if j > 66 then y := y + 22
                  else x := x + 22;
      gran := TImage.Create(field);
      gran.Picture.LoadFromFile('gran.bmp');
      gran.Parent := field;
      gran.Top := y;
      gran.Left := x;
      if j = 44 then begin
                       y := 0;
                       x := 0
                     end;
    end;
  boolEnd := true;
  racingBool := false;
  health := 3;
  randomize;
  speed := 5;
end;

procedure TField.Timer1Timer(Sender: TObject);
var
  j, j1, j2, j3 : integer;
  zvezda : TImage;
begin
if not(pauseBool) then
begin
  if not(BoolEnd) then
    if health = 1
      then healthLabel.caption := '� ��� �������� 1 �����'
      else  healthLabel.caption := '� ��� �������� ' + inttostr(health) + ' �����';
  case level of
    1 : maxDopVrag := 2;
    2 : maxDopVrag := 3;
    3 : maxDopVrag := 4;
    4..9 : maxDopVrag := level + 1;
    10 : maxDopVrag := 8
  end;
  if perezar > 100 then perezar :=11;
  if iHealth > 100 then iHealth := 51;
  if not(boolEnd) then begin
                         perezar := perezar + 1;
                         iHealth := iHealth + 1
                       end;

if not(boolEnd) then
//���� ��������� ������
  for j := 1 to iVrag do
    begin
//�������� ��������� � ����������� ���� �����
      if vrag[j].bool
        then begin
               if vrag[j].im.top = vrag[j].y
                 then begin
                        vrag[j].y := random(83) * 5 + 20;
                        vrag[j].bool := false
                      end;
               if vrag[j].im.top < vrag[j].y
                 then begin
                        vrag[j].im.Picture.LoadFromFile('tankvragdown.bmp');
                        vrag[j].im.top := vrag[j].im.top + speed;
                        vrag[j].napr := 1;
                      end;
               if vrag[j].im.top > vrag[j].y
                 then begin
                        vrag[j].im.Picture.LoadFromFile('tankvrag.bmp');
                        vrag[j].im.top := vrag[j].im.top - speed;
                        vrag[j].napr := 2;
                      end
             end

        else begin
               if vrag[j].im.Left = vrag[j].x
                 then begin
                        vrag[j].x := random(83) * 5 + 20;
                        vrag[j].bool := true
                      end;
               if vrag[j].im.Left < vrag[j].x
                 then begin
                        vrag[j].im.Picture.LoadFromFile('tankvragright.bmp');
                        vrag[j].im.Left := vrag[j].im.Left + speed;
                        vrag[j].napr := 3;
                      end;
               if vrag[j].im.Left > vrag[j].x
                 then begin
                        vrag[j].im.Picture.LoadFromFile('tankvragleft.bmp');
                        vrag[j].im.Left := vrag[j].im.Left - speed;
                        vrag[j].napr := 4;
                      end
             end;
//��������� ������� � ����� ��� ���������� ������
      if (vrag[j].im.Top < tank.Top + 45) and
         (vrag[j].im.Top > tank.Top - 45) and
         (vrag[j].im.left > tank.left - 45) and
         (vrag[j].im.left < tank.left + 45)
         then begin
                vrag[j].bool := not(vrag[j].bool);
                case vrag[j].napr of
                  1 : vrag[j].im.top := vrag[j].im.top - speed;
                  2 : vrag[j].im.top := vrag[j].im.top + speed;
                  3 : vrag[j].im.left := vrag[j].im.left - speed;
                  4 : vrag[j].im.left := vrag[j].im.left + speed
                end;
              end;
//��������� ��������� �������� ��� ���������� ������
      for j2 := j + 1 to iVrag do
        if (vrag[j].im.Top < vrag[j2].im.Top + 50) and
           (vrag[j].im.Top > vrag[j2].im.Top - 50) and
           (vrag[j].im.left > vrag[j2].im.left - 50) and
           (vrag[j].im.left < vrag[j2].im.left + 50)
          then begin
                 vrag[j2].bool := not(vrag[j2].bool);
                 vrag[j].bool := not(vrag[j].bool);
                 vrag[j].x := random(83) * 5 + 20;
                 vrag[j].y := random(83) * 5 + 20;
                 vrag[j2].x := random(83) * 5 + 20;
                 vrag[j2].y := random(83) * 5 + 20;
                 case vrag[j2].napr of
                   1 : vrag[j2].im.top := vrag[j2].im.top - speed;
                   2 : vrag[j2].im.top := vrag[j2].im.top + speed;
                   3 : vrag[j2].im.left := vrag[j2].im.left - speed;
                   4 : vrag[j2].im.left := vrag[j2].im.left + speed
                 end;
                 case vrag[j].napr of
                   1 : vrag[j].im.top := vrag[j].im.top - speed;
                   2 : vrag[j].im.top := vrag[j].im.top + speed;
                   3 : vrag[j].im.left := vrag[j].im.left - speed;
                   4 : vrag[j].im.left := vrag[j].im.left + speed
                 end
               end;

//��������� �������


//������ ���������� �������
      if random(30 - level) = 3 then
        vystrel(j, vrag[j].napr);
//������ ���������� �������, ����� ���� ���� �� ������
      if level > 9 then
      case vrag[j].napr of
        1 : if vrag[j].im.top < tank.Top
               then if (vrag[j].im.Left < tank.Left + 20) and
                       (vrag[j].im.Left > tank.Left - 20)
                      then begin
                             vystrel(j,vrag[j].napr);
                             vrag[j].bool := not(vrag[j].bool)
                           end;
        2 : if vrag[j].im.top > tank.Top
               then if (vrag[j].im.Left < tank.Left + 20) and
                       (vrag[j].im.Left > tank.Left - 20)
                      then begin
                             vystrel(j,vrag[j].napr);
                             vrag[j].bool := not(vrag[j].bool)
                           end;
        3 : if vrag[j].im.left < tank.left
               then if (vrag[j].im.top < tank.top + 20) and
                       (vrag[j].im.top > tank.top - 20)
                      then begin
                             vystrel(j,vrag[j].napr);
                             vrag[j].bool := not(vrag[j].bool)
                           end;
        4 : if vrag[j].im.left > tank.left
               then if (vrag[j].im.top < tank.top + 20) and
                       (vrag[j].im.top > tank.top - 20)
                      then begin
                             vystrel(j,vrag[j].napr);
                             vrag[j].bool := not(vrag[j].bool)
                           end;
      end;
    end;
//������ ������ �������
  label1.Caption := inttostr(iHealth);
  if (iHealth = 5) and (tank <> nil) and (not(zvezdaBool)) then
    begin
      zvezdaBool := true;
      zvezda := TImage.Create(Field);
      zvezda.Parent := field;
      zvezda.Picture.LoadFromFile('nimb.bmp');
      zvezda.Top := tank.top;
      zvezda.Left := tank.left
    end;
  if zvezdaBool then
    begin
      zvezda.Top := tank.top;
      zvezda.Left := tank.left;
    end;
  {if (iHealth > 50) and (zvezdaBool) then
    begin
      zvezdaBool := false;
      zvezda.Destroy
    end; }


//��������� ������
  if (perezar = 9) or (perezar = 20) then
    label1.Caption := '';
//�������� ������
  if (level  = 10) and (iVrag  = 0)
    then begin
           speed := 0;
           label1.Caption := '�� ��������'
         end
  else
  if (iVrag = 0) and (tank <> nil) and (not(boolEnd))
    then begin
           level1 := level1 + 1;
           if  (level1 > 30) and (level < 10)
             then begin
                    new;
                    mediaPlayer1.Play;
                    level1 := 0;
                    perezar := 0;
                    label1.Caption := '������� ' + intToStr(level);
                  end;
         end;

end;
end;


procedure TField.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (key = 'p') or (key = '�') then pauseBool := not(pauseBool);
  if pauseBool = true
    then label1.Caption := '�����'
    else if not(boolEnd) then label1.caption := '';
  if key = #27 then close
end;

procedure TField.Timer2Timer(Sender: TObject);
var
  j, j1, j2, j3, iVrez : integer;
begin

if tank <> nil then
begin
//����������� ����� � �������
  for j := 1 to iVrag do
    begin
      if (tank.Top < vrag[j].im.Top + 50) and
         (tank.top > vrag[j].im.Top + 10) and
         (tank.left < vrag[j].im.left + 45) and
         (tank.Left > vrag[j].im.left - 45)
         then iVrez := 1;
      if (tank.Top < vrag[j].im.Top + 10) and
         (tank.top > vrag[j].im.Top - 50) and
         (tank.left < vrag[j].im.left + 45) and
         (tank.Left > vrag[j].im.left - 45)
         then iVrez := 2;
      if (tank.top < vrag[j].im.Top + 45) and
         (tank.top > vrag[j].im.Top - 45) and
         (tank.left < vrag[j].im.left + 50) and
         (tank.Left > vrag[j].im.left + 10)
         then iVrez := 3;
      if (tank.top < vrag[j].im.Top + 45) and
         (tank.top > vrag[j].im.Top - 45) and
         (tank.left < vrag[j].im.left + 10) and
         (tank.Left > vrag[j].im.left - 50)
         then iVrez := 4;
    end;
//�������
  if (not(boolEnd)) and (not(pauseBool)) then
  begin
    if GetKeyState(vk_space) < -5 then
             if perezar > 10 then
               begin
                 shot.Play;
                 perezar := 0;
                 iSnar := iSnar + 1;
                 with snar[iSnar] do
                   begin
                     im := TImage.Create(field);
                     napravl := snarNapr;
                     im.parent := field;
                     case snarNapr of
                       1 : im.Picture.loadFromFile('snar1.bmp');
                       2 : im.Picture.loadFromFile('snarDown1.bmp');
                       3 : im.Picture.loadFromFile('snarLeft1.bmp');
                       4 : im.Picture.loadFromFile('snarRight1.bmp');
                     end;
                     im.Top := tank.Top + 18;
                     im.Left := tank.Left + 20
                   end
               end;
    if GetKeyState(vk_up) < -5 then
            begin
              tank.Picture.LoadFromFile('tank.bmp');
              if iVrez <> 1 then
                if tank.Top > 20
                  then tank.Top := tank.Top - 2;
              snarNapr := 1;
            end else
    if GetKeyState(vk_down) < -5 then
              begin
                tank.Picture.LoadFromFile('tankDown.bmp');
                if iVrez <> 2 then
                  if tank.Top < field.height - 144
                    then tank.Top := tank.Top + 2;
                snarNapr := 2;
              end else
    if GetKeyState(vk_left) < -5 then
              begin
                tank.Picture.LoadFromFile('tankLeft.bmp');
                if iVrez <> 3 then
                  if tank.Left > 20
                    then tank.left := tank.left - 2;
                snarNapr := 3;
              end else
    if GetKeyState(vk_right) < -5 then
               begin
                 tank.Picture.LoadFromFile('tankRight.bmp');
                 if iVrez <> 4 then
                   if tank.left < field.width - 102
                     then tank.left := tank.left + 2;
                 snarNapr := 4;
               end;
  end;
  iVrez := 0;
  end;

if not(pauseBool) then
begin
  if racingBool then racing.Play;
  if not(racingBool) then racing.Stop;
//������
  j := 0;
  while j < iSnar do
    begin
      j := j + 1;
    //����� ������
      case snar[j].napravl of
        1 : snar[j].im.Top := snar[j].im.Top - 4;
        2 : snar[j].im.Top := snar[j].im.Top + 4;
        3 : snar[j].im.left := snar[j].im.left - 4;
        4 : snar[j].im.left := snar[j].im.left + 4
      end;
    //������������ ������ �� ��������� ����
      if (snar[j].im.Top < 20) or (snar[j].im.Top > field.height - 106)
         or (snar[j].im.Left < 20) or (snar[j].im.Left > field.Width - 66)
         then begin
                miss.Play;
                snar[j].im.Destroy;
                for j1 := j to isnar - 1 do
                  snar[j1] := snar[j1 + 1];
                iSnar := iSnar - 1;
              end;
    end;
  for j2 := 1 to iVragSnar do
//����� ��������� ������
      case VragSnar[j2].napr of
        1 : VragSnar[j2].im.top := VragSnar[j2].im.Top + 4;
        2 : VragSnar[j2].im.top := VragSnar[j2].im.Top - 4;
        3 : VragSnar[j2].im.left := VragSnar[j2].im.left + 4;
        4 : VragSnar[j2].im.left := VragSnar[j2].im.left - 4;
      end;
//�������� ��������� ������ �� ��������� ����
  j2 := 0;
  while j2 < iVragSnar  do
    begin
      j2 := j2 + 1;
      if (VragSnar[j2].im.top < 20) or (VragSnar[j2].im.top > field.Height - 106) or
         (VragSnar[j2].im.left < 20) or (VragSnar[j2].im.left > field.Width - 66)
         then begin
                VragSnar[j2].im.destroy;
                iVragSnar := iVragSnar - 1;
                for j1 := j2 to iVragSnar do
                  VragSnar[j1] := VragSnar[j1 + 1];
                j2 := j2 - 1;
              end;
    end;
//����������� ��������� � ���� ������ ����� ������������
  j2 := 1;
  while j2 <= iVragSnar do
    begin
      for j1 := 1 to iSnar do
        if (snar[j1].im.top + 5 > VragSnar[j2].im.top) and
           (snar[j1].im.top - 5 < VragSnar[j2].im.top) and
           (snar[j1].im.left + 5 > VragSnar[j2].im.left) and
           (snar[j1].im.left - 5 < VragSnar[j2].im.left)
           then begin
                  snar[j1].im.Destroy;
                  VragSnar[j2].im.Destroy;
                  iSnar := iSnar - 1;
                  for j3 := j1 to iSnar do
                    snar[j3] := snar[j3 + 1];
                  iVragSnar := iVragSnar - 1;
                  for j3 := j2 to iVragSnar do
                    VragSnar[j3] := VragSnar[j3 + 1];
                end;
      j2 := j2 + 1
    end;
//����������� ��������� ������, �������� � ������� �����
  if level < 5 then
  for j := 1 to iVrag do
    begin
      j2 := 0;
      while j2 < iVragSnar  do
        begin
          j2 := j2 + 1;
          if VragSnar[j2].vipustVrag <> j then
            if((VragSnar[j2].im.Top <= vrag[j].im.top + 43) and
               (VragSnar[j2].im.Top >= vrag[j].im.Top - 13)) and
               ((VragSnar[j2].im.Left <= vrag[j].im.left + 43) and
               (VragSnar[j2].im.Left >= vrag[j].im.Left - 7))
              then begin
                     VragSnar[j2].im.destroy;
                     iVragSnar := iVragSnar - 1;
                     for j1 := j2 to iVragSnar do
                       VragSnar[j1] := VragSnar[j1 + 1];
                     j2 := j2 - 1
                   end;

        end;
  end;
//�������� �������� �����
  j2 := 1;
  while j2 <= iVrag do
    begin
      if ((Vrag[j2].im.Top <= gerb.top + 35) and
          (Vrag[j2].im.Top >= gerb.Top - 35)) and
         ((Vrag[j2].im.Left <= gerb.left + 40) and
          (Vrag[j2].im.Left >= gerb.Left - 35)) and not(boolEnd) and
          (iHealth > 50) then begin
                                health := 0;
                                bang.Play;
                              end;

      j2 := j2 + 1;
    end;
//�������� �������� �������
  if (health < 1) then
    begin
      boolEnd := true;
      racingBool := false;
      healthLabel.caption := '';
      label1.Caption := '�� ���������';
      level := 0;
      speed := 0;
    end;
  j2 := 1;
  while j2 <= iVragSnar do
    begin
      if ((VragSnar[j2].im.Top <= tank.top + 43) and
           (VragSnar[j2].im.Top >= tank.Top - 7)) and
           ((VragSnar[j2].im.Left <= tank.left + 43) and
           (VragSnar[j2].im.Left >= tank.Left - 5)) and
           (iHealth > 50)
           then begin
                  bang.Play;
                  VragSnar[j2].im.destroy;
                  iVragSnar := iVragSnar - 1;
                  for j1 := j2 to iVragSnar do
                    VragSnar[j1] := VragSnar[j1 + 1];
                  health := health - 1;
                  if (health > 0) then
                  if fDopVrag(100, 100)
                    then begin
                           tank.top := 50;
                           tank.left := 50;
                         end
                    else
                      if fDopVrag(450, 450)
                        then begin
                               tank.top := 400;
                               tank.left := 400;
                             end
                        else
                          if fDopVrag(100, 450)
                            then begin
                                   tank.top := 50;
                                   tank.left := 400;
                                 end
                            else
                              if fDopVrag(450, 100)
                                then begin
                                       tank.top := 50;
                                       tank.left := 50;
                                     end;
                  iHealth := 0;
                end;
      j2 := j2 + 1;
    end;

  j := 1;
  while j <= iSnar do
    begin
//����������� ����� � �������� � ���� ������
      for j1 := 1 to iVrag do
        if ((snar[j].im.Top <= vrag[j1].im.top + 41) and
           (snar[j].im.Top >= vrag[j1].im.Top - 5)) and
           ((snar[j].im.Left <= vrag[j1].im.left + 43) and
           (snar[j].im.Left >= vrag[j1].im.Left - 5))
           then begin
                  bang.Play;
                  snar[j].im.destroy;
                  iSnar := iSnar - 1;
                  for j2 := j to iSnar do
                    snar[j] := snar[j + 1];
                  dopVrag := dopVrag + 1;
//��������� ����� ��������� ��������
                  if dopVrag <= maxDopVrag then
                    begin
                        if fDopVrag(100, 100)
                          then begin
                                 vrag[j1].im.top := 50;
                                 vrag[j1].im.left := 50;
                                 vrag[j1].bool := true;
                                 vrag[j1].x := random(83) * 5 + 20;
                                 vrag[j1].y := random(83) * 5 + 20;
                               end
                          else
                            if fDopVrag(450, 450)
                              then begin
                                     vrag[iVrag].bool := true;
                                     vrag[j1].x := random(83) * 5 + 20;
                                     vrag[j1].y := random(83) * 5 + 20;
                                     vrag[j1].im.top := 400;
                                     vrag[j1].im.left := 400;
                               end
                              else
                                if fDopVrag(100, 450)
                                  then begin
                                         vrag[j1].bool := true;
                                         vrag[j1].x := random(83) * 5 + 20;
                                         vrag[j1].y := random(83) * 5 + 20;
                                         vrag[j1].im.top := 50;
                                         vrag[j1].im.left := 400;
                                       end
                                  else
                                    if fDopVrag(450, 100)
                                      then begin
                                             vrag[iVrag].bool := true;
                                             vrag[j1].x := random(83) * 5 + 20;
                                             vrag[j1].y := random(83) * 5 + 20;
                                             vrag[j1].im.top := 400;
                                             vrag[j1].im.left := 50;
                                           end
                                      else
                                         begin
                                             vrag[iVrag].bool := true;
                                             vrag[j1].x := random(83) * 5 + 20;
                                             vrag[j1].y := random(83) * 5 + 20;
                                             vrag[j1].im.top := 200;
                                             vrag[j1].im.left := 200;
                                           end

                    end
                  else begin
                         vrag[j1].im.Destroy;
                         iVrag := iVrag - 1;
                         for j2 := j1 to iVrag do
                           vrag[j2] := vrag[j2 + 1];
                       end
                end;
      j := j + 1;
    end;
end;
end;

end.
