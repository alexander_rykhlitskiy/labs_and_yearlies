unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ExtCtrls, StdCtrls;

type
  RecSnar = record
              napravl : byte;
              im : TImage
            end;
  RecVragSnar = record
                  im : TImage;
                  napr : byte
                end;
  RecVrag = record
              x, y : integer;
              bool : boolean;
              im : TImage;
              napr : integer
            end;


  TField = class(TForm)
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    Image1: TImage;
    Timer1: TTimer;
    Label1: TLabel;
    procedure N2Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  boolTank, boolVrag : boolean;   {�������� ������� ������� ��� ������� ����� ����}
  speed, snarNapr, isnar : integer;    {snarNapr, isnar - ������� � ����� ������� � �������}
  iVrag, perezar, iVragSnar : integer;                      {iVrag - ����� ����� � �������}
  tank : TImage;
  vragSnar : array[1..100] of RecVragSnar;
  snar : array[1..10] of RecSnar;
  vrag : array[1..10] of RecVrag;
  boolEnd : boolean;
  Field: TField;

implementation

{$R *.dfm}

procedure TField.N2Click(Sender: TObject);
begin
  close;
end;

procedure TField.N1Click(Sender: TObject);
var j : byte;
begin
  boolEnd := false;
  for j := 1 to iSnar do
    snar[j].im.Destroy;
  for j := 1 to iVragSnar do
    VragSnar[j].im.Destroy;
  perezar := 10;
  speed := 5;
  label1.caption := '';
//�������� �������
  if boolTank then tank.Destroy;
  boolTank := true;
  tank := TImage.Create(Field);
  tank.Parent := field;
  tank.Picture.LoadFromFile('tank.bmp');
  tank.top := 210;
  tank.left := 250;
  SnarNapr := 1;
  iSnar :=0;
  iVragSnar := 0;
//�������� ������
  if boolVrag
    then for j := 1 to iVrag do
           vrag[j].im.Destroy;
  boolVrag := true;
  for j := 1 to 4 do
    with vrag[j] do
      begin
        im := TImage.Create(field);
        im.Picture.LoadFromFile('tankvrag.bmp');
        im.Parent := field;
      end;
  vrag[1].im.top := 50;
  vrag[1].im.left := 50;
  vrag[2].im.top := 400;
  vrag[2].im.left := 50;
  vrag[3].im.top := 50;
  vrag[3].im.left := 400;
  vrag[4].im.top := 400;
  vrag[4].im.left := 400;
  iVrag := 4;
//���� ������ ����������
  for j := 1 to 4 do
    begin
      vrag[j].bool := true;
      vrag[j].x := random(90) * 5;
      vrag[j].y := random(90) * 5
    end;
end;

procedure TField.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var j, iVrez : integer;
begin
if not(boolEnd) then
begin
//����������� ����� � �������
  for j := 1 to iVrag do
    begin
      if (tank.Top < vrag[j].im.Top + 50) and
         (tank.top > vrag[j].im.Top + 10) and
         (tank.left < vrag[j].im.left + 45) and
         (tank.Left > vrag[j].im.left - 45)
         then iVrez := 1;
      if (tank.Top < vrag[j].im.Top + 10) and
         (tank.top > vrag[j].im.Top - 50) and
         (tank.left < vrag[j].im.left + 45) and
         (tank.Left > vrag[j].im.left - 45)
         then iVrez := 2;
      if (tank.top < vrag[j].im.Top + 45) and
         (tank.top > vrag[j].im.Top - 45) and
         (tank.left < vrag[j].im.left + 50) and
         (tank.Left > vrag[j].im.left + 10)
         then iVrez := 3;
      if (tank.top < vrag[j].im.Top + 45) and
         (tank.top > vrag[j].im.Top - 45) and
         (tank.left < vrag[j].im.left + 10) and
         (tank.Left > vrag[j].im.left - 50)
         then iVrez := 4;
    end;
//��������
  if boolTank then
  case key of
    vk_space : if perezar > 10 then
               begin
                 perezar := 0;
                 iSnar := iSnar + 1;
                 with snar[iSnar] do
                   begin
                     im := TImage.Create(field);
                     napravl := snarNapr;
                     im.parent := field;
                     case snarNapr of
                       1 : im.Picture.loadFromFile('snar.bmp');
                       2 : im.Picture.loadFromFile('snarDown.bmp');
                       3 : im.Picture.loadFromFile('snarLeft.bmp');
                       4 : im.Picture.loadFromFile('snarRight.bmp');
                     end;
                     im.Top := tank.Top + 18;
                     im.Left := tank.Left + 20
                   end
               end;
    vk_up : begin
              tank.Picture.LoadFromFile('tank.bmp');
              if iVrez <> 1 then
                if tank.Top > 0
                  then tank.Top := tank.Top - speed;
              snarNapr := 1;
            end;
    vk_down : begin
                tank.Picture.LoadFromFile('tankDown.bmp');
                if iVrez <> 2 then
                  if tank.Top < field.height - 105
                    then tank.Top := tank.Top + speed;
                snarNapr := 2;
              end;
    vk_left : begin
                tank.Picture.LoadFromFile('tankLeft.bmp');
                if iVrez <> 3 then
                  if tank.Left > 0
                    then tank.left := tank.left - speed;
                snarNapr := 3;
              end;
    vk_right : begin
                 tank.Picture.LoadFromFile('tankRight.bmp');
                 if iVrez <> 4 then
                   if tank.left < field.width - 65
                     then tank.left := tank.left + speed;
                 snarNapr := 4;
               end;
  end;
  iVrez := 0;
end;
end;

procedure TField.FormActivate(Sender: TObject);
begin
  randomize;
  speed := 5;
  boolTank := false;
end;

procedure TField.Timer1Timer(Sender: TObject);
var
  j, j1, j2 : integer;
begin
  perezar := perezar + 1;
//�������� ������
  if (iVrag = 0) and (boolTank) and (not(boolEnd))
    then begin
           label1.caption := '�� ��������';
           speed := 0
         end;
//������
  for j := 1 to iSnar do
    begin
    //����� ������
      case snar[j].napravl of
        1 : snar[j].im.Top := snar[j].im.Top - 15;
        2 : snar[j].im.Top := snar[j].im.Top + 15;
        3 : snar[j].im.left := snar[j].im.left - 15;
        4 : snar[j].im.left := snar[j].im.left + 15
      end;

   //����������� ����� � �������� � ���� ������
      for j1 := 1 to iVrag do
        if ((snar[j].im.Top <= vrag[j1].im.top + 35) and
           (snar[j].im.Top >= vrag[j1].im.Top - 13)) and
           ((snar[j].im.Left <= vrag[j1].im.left + 40) and
           (snar[j].im.Left >= vrag[j1].im.Left - 7))
           then begin
                  snar[j].im.destroy;
                  iSnar := iSnar - 1;
                  for j2 := j to iSnar do
                    snar[j] := snar[j + 1];
                  vrag[j1].im.Destroy;
                  iVrag := iVrag - 1;
                  for j2 := j1 to iVrag do
                    vrag[j2] := vrag[j2 + 1];
                end;
    //������������ ������ �� ��������� ����
      if (snar[j].im.Top < 0) or (snar[j].im.Top > field.height)
         or (snar[j].im.Left < 0) or (snar[j].im.Left > field.Width)
         then begin
                snar[j].im.Destroy;
                for j1 := j to isnar - 1 do
                  snar[j1] := snar[j1 + 1];
                iSnar := iSnar - 1;
              end;
    end;
if not(boolEnd) then
//���� ��������� ������
  for j := 1 to iVrag do
    begin
//�������� ��������� � ����������� ���� �����
      if vrag[j].bool
        then begin
               if vrag[j].im.top = vrag[j].y
                 then begin
                        vrag[j].y := random(90) * 5;
                        vrag[j].bool := false
                      end;
               if vrag[j].im.top < vrag[j].y
                 then begin
                        vrag[j].im.Picture.LoadFromFile('tankvragdown.bmp');
                        vrag[j].im.top := vrag[j].im.top + speed;
                        vrag[j].napr := 1;
                      end;
               if vrag[j].im.top > vrag[j].y
                 then begin
                        vrag[j].im.Picture.LoadFromFile('tankvrag.bmp');
                        vrag[j].im.top := vrag[j].im.top - speed;
                        vrag[j].napr := 2;
                      end
             end

        else begin
               if vrag[j].im.Left = vrag[j].x
                 then begin
                        vrag[j].x := random(90) * 5;
                        vrag[j].bool := true
                      end;
               if vrag[j].im.Left < vrag[j].x
                 then begin
                        vrag[j].im.Picture.LoadFromFile('tankvragright.bmp');
                        vrag[j].im.Left := vrag[j].im.Left + speed;
                        vrag[j].napr := 3;
                      end;
               if vrag[j].im.Left > vrag[j].x
                 then begin
                        vrag[j].im.Picture.LoadFromFile('tankvragleft.bmp');
                        vrag[j].im.Left := vrag[j].im.Left - speed;
                        vrag[j].napr := 4;
                      end
             end;
//��������� ������� � ����� ��� ���������� ������
      if (vrag[j].im.Top < tank.Top + 45) and
         (vrag[j].im.Top > tank.Top - 45) and
         (vrag[j].im.left > tank.left - 45) and
         (vrag[j].im.left < tank.left + 45)
         then begin
                vrag[j].bool := not(vrag[j].bool);
                case vrag[j].napr of
                  1 : vrag[j].im.top := vrag[j].im.top - speed;
                  2 : vrag[j].im.top := vrag[j].im.top + speed;
                  3 : vrag[j].im.left := vrag[j].im.left - speed;
                  4 : vrag[j].im.left := vrag[j].im.left + speed
                end;
              end;
//��������� ��������� �������� ��� ���������� ������
      for j2 := j + 1 to iVrag do
        if (vrag[j].im.Top < vrag[j2].im.Top + 50) and
           (vrag[j].im.Top > vrag[j2].im.Top - 50) and
           (vrag[j].im.left > vrag[j2].im.left - 50) and
           (vrag[j].im.left < vrag[j2].im.left + 50)
          then begin
                 vrag[j2].bool := not(vrag[j2].bool);
                 vrag[j].bool := not(vrag[j].bool);
                 vrag[j].x := random(90) * 5;
                 vrag[j].y := random(90) * 5;
                 vrag[j2].x := random(90) * 5;
                 vrag[j2].y := random(90) * 5;
                 case vrag[j2].napr of
                   1 : vrag[j2].im.top := vrag[j2].im.top - speed;
                   2 : vrag[j2].im.top := vrag[j2].im.top + speed;
                   3 : vrag[j2].im.left := vrag[j2].im.left - speed;
                   4 : vrag[j2].im.left := vrag[j2].im.left + speed
                 end;
                 case vrag[j].napr of
                   1 : vrag[j].im.top := vrag[j].im.top - speed;
                   2 : vrag[j].im.top := vrag[j].im.top + speed;
                   3 : vrag[j].im.left := vrag[j].im.left - speed;
                   4 : vrag[j].im.left := vrag[j].im.left + speed
                 end
               end;
//��������� �������

//1 - ��� ������ ���������� ����
//2 - ������ ���������� �����
//3 - ������ ���������� ������
//4 - ������ ���������� �����
//����� �� ��������� ��� ���������� �������

//������ ���������� �������
      if random(20) = 3 then
      case vrag[j].napr of
        1 : begin
              iVragSnar := iVragSnar + 1;
              VragSnar[iVragSnar].im := TImage.Create(Field);
              VragSnar[iVragSnar].im.Parent := field;
              VragSnar[iVragSnar].im.Picture.LoadFromFile('snarDown.bmp');
              VragSnar[iVragSnar].im.top := vrag[j].im.top + 18;
              VragSnar[iVragSnar].im.left := vrag[j].im.left + 20;
              VragSnar[iVragSnar].napr := vrag[j].napr
            end;
        2 : begin
              iVragSnar := iVragSnar + 1;
              VragSnar[iVragSnar].im := TImage.Create(Field);
              VragSnar[iVragSnar].im.Parent := field;
              VragSnar[iVragSnar].im.Picture.LoadFromFile('snar.bmp');
              VragSnar[iVragSnar].im.top := vrag[j].im.top + 18;
              VragSnar[iVragSnar].im.left := vrag[j].im.left + 20;
              VragSnar[iVragSnar].napr := vrag[j].napr
            end;
        3 : begin
              iVragSnar := iVragSnar + 1;
              VragSnar[iVragSnar].im := TImage.Create(Field);
              VragSnar[iVragSnar].im.Parent := field;
              VragSnar[iVragSnar].im.Picture.LoadFromFile('snarRight.bmp');
              VragSnar[iVragSnar].im.top := vrag[j].im.top + 18;
              VragSnar[iVragSnar].im.left := vrag[j].im.left + 20;
              VragSnar[iVragSnar].napr := vrag[j].napr
            end;
        4 : begin
              iVragSnar := iVragSnar + 1;
              VragSnar[iVragSnar].im := TImage.Create(Field);
              VragSnar[iVragSnar].im.Parent := field;
              VragSnar[iVragSnar].im.Picture.LoadFromFile('snarLeft.bmp');
              VragSnar[iVragSnar].im.top := vrag[j].im.top + 18;
              VragSnar[iVragSnar].im.left := vrag[j].im.left + 20;
              VragSnar[iVragSnar].napr := vrag[j].napr
            end;
      end;
    end;
  for j2 := 1 to iVragSnar do
    begin
//����� ��������� ������
      case VragSnar[j2].napr of
        1 : VragSnar[j2].im.top := VragSnar[j2].im.Top + 15;
        2 : VragSnar[j2].im.top := VragSnar[j2].im.Top - 15;
        3 : VragSnar[j2].im.left := VragSnar[j2].im.left + 15;
        4 : VragSnar[j2].im.left := VragSnar[j2].im.left - 15;
      end;
//�������� ��������� ������ �� ��������� ����
      if (VragSnar[j2].im.top < 0) or (VragSnar[j2].im.top > field.Height) or
         (VragSnar[j2].im.left < 0) or (VragSnar[j2].im.left > field.Width)
         then begin
                VragSnar[j2].im.destroy;
                iVragSnar := iVragSnar - 1;
                for j1 := j2 to iVragSnar do
                  VragSnar[j1] := VragSnar[j1 + 1];
              end;
      if ((VragSnar[j2].im.Top <= tank.top + 35) and
           (VragSnar[j2].im.Top >= tank.Top - 13)) and
           ((VragSnar[j2].im.Left <= tank.left + 40) and
           (VragSnar[j2].im.Left >= tank.Left - 7))
           then begin
                  VragSnar[j2].im.destroy;
                  iVragSnar := iVragSnar - 1;
                  for j1 := j2 to iVragSnar do
                    VragSnar[j1] := VragSnar[j1 + 1];
                         {?????????????????????}
                  speed := 0;
                  boolEnd := true;
                  label1.Caption := '�� ���������';
                end;
    end;

end;


end.

