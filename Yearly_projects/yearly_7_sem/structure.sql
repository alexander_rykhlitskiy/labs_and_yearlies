SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;
CREATE TABLE ad_types (
    id integer NOT NULL,
    name character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);
CREATE SEQUENCE ad_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE ad_types_id_seq OWNED BY ad_types.id;
CREATE TABLE ads (
    id integer NOT NULL,
    content text,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    state character varying(255),
    ad_type_id integer,
    published_at timestamp without time zone,
    place_id integer,
    subsection_id integer,
    price integer,
    currency_id integer
);
CREATE SEQUENCE ads_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE ads_id_seq OWNED BY ads.id;
CREATE TABLE ads_keywords (
    ad_id integer,
    keyword_id integer
);
CREATE TABLE announcements (
    id integer NOT NULL,
    content text,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    ad_id integer
);
CREATE SEQUENCE announcements_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE announcements_id_seq OWNED BY announcements.id;
CREATE TABLE comments (
    id integer NOT NULL,
    content character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    ad_id integer,
    user_id integer
);
CREATE SEQUENCE comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE comments_id_seq OWNED BY comments.id;
CREATE TABLE currencies (
    id integer NOT NULL,
    name character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);
CREATE SEQUENCE currencies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE currencies_id_seq OWNED BY currencies.id;
CREATE TABLE keywords (
    id integer NOT NULL,
    name character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);
CREATE SEQUENCE keywords_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE keywords_id_seq OWNED BY keywords.id;
CREATE TABLE payments (
    id integer NOT NULL,
    wallet_id integer,
    amount integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);
CREATE SEQUENCE payments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE payments_id_seq OWNED BY payments.id;
CREATE TABLE photos (
    id integer NOT NULL,
    ad_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    name character varying(255),
    file_file_name character varying(255),
    file_content_type character varying(255),
    file_file_size integer,
    file_updated_at timestamp without time zone
);
CREATE SEQUENCE photos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE photos_id_seq OWNED BY photos.id;
CREATE TABLE places (
    id integer NOT NULL,
    name character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);
CREATE SEQUENCE places_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE places_id_seq OWNED BY places.id;
CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);
CREATE TABLE sections (
    id integer NOT NULL,
    name character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);
CREATE SEQUENCE sections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE sections_id_seq OWNED BY sections.id;
CREATE TABLE subsections (
    id integer NOT NULL,
    name character varying(255),
    section_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);
CREATE SEQUENCE subsections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE subsections_id_seq OWNED BY subsections.id;
CREATE TABLE users (
    id integer NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    role character varying(255),
    provider character varying(255),
    url character varying(255),
    first_name character varying(255),
    last_name character varying(255)
);
CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE users_id_seq OWNED BY users.id;
CREATE TABLE wallets (
    id integer NOT NULL,
    balance integer,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);
CREATE SEQUENCE wallets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE wallets_id_seq OWNED BY wallets.id;
ALTER TABLE ONLY ad_types ALTER COLUMN id SET DEFAULT nextval('ad_types_id_seq'::regclass);
ALTER TABLE ONLY ads ALTER COLUMN id SET DEFAULT nextval('ads_id_seq'::regclass);
ALTER TABLE ONLY announcements ALTER COLUMN id SET DEFAULT nextval('announcements_id_seq'::regclass);
ALTER TABLE ONLY comments ALTER COLUMN id SET DEFAULT nextval('comments_id_seq'::regclass);
ALTER TABLE ONLY currencies ALTER COLUMN id SET DEFAULT nextval('currencies_id_seq'::regclass);
ALTER TABLE ONLY keywords ALTER COLUMN id SET DEFAULT nextval('keywords_id_seq'::regclass);
ALTER TABLE ONLY payments ALTER COLUMN id SET DEFAULT nextval('payments_id_seq'::regclass);
ALTER TABLE ONLY photos ALTER COLUMN id SET DEFAULT nextval('photos_id_seq'::regclass);
ALTER TABLE ONLY places ALTER COLUMN id SET DEFAULT nextval('places_id_seq'::regclass);
ALTER TABLE ONLY sections ALTER COLUMN id SET DEFAULT nextval('sections_id_seq'::regclass);
ALTER TABLE ONLY subsections ALTER COLUMN id SET DEFAULT nextval('subsections_id_seq'::regclass);
ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);
ALTER TABLE ONLY wallets ALTER COLUMN id SET DEFAULT nextval('wallets_id_seq'::regclass);
ALTER TABLE ONLY ad_types
    ADD CONSTRAINT ad_types_pkey PRIMARY KEY (id);
ALTER TABLE ONLY ads
    ADD CONSTRAINT ads_pkey PRIMARY KEY (id);
ALTER TABLE ONLY announcements
    ADD CONSTRAINT announcements_pkey PRIMARY KEY (id);
ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);
ALTER TABLE ONLY currencies
    ADD CONSTRAINT currencies_pkey PRIMARY KEY (id);
ALTER TABLE ONLY keywords
    ADD CONSTRAINT keywords_pkey PRIMARY KEY (id);
ALTER TABLE ONLY payments
    ADD CONSTRAINT payments_pkey PRIMARY KEY (id);
ALTER TABLE ONLY photos
    ADD CONSTRAINT photos_pkey PRIMARY KEY (id);
ALTER TABLE ONLY places
    ADD CONSTRAINT places_pkey PRIMARY KEY (id);
ALTER TABLE ONLY sections
    ADD CONSTRAINT sections_pkey PRIMARY KEY (id);
ALTER TABLE ONLY subsections
    ADD CONSTRAINT subsections_pkey PRIMARY KEY (id);
ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
ALTER TABLE ONLY wallets
    ADD CONSTRAINT wallets_pkey PRIMARY KEY (id);
CREATE INDEX index_ads_on_user_id_and_created_at ON ads USING btree (user_id, created_at);
CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);
CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);
CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);
INSERT INTO schema_migrations (version) VALUES ('20131001125838');
INSERT INTO schema_migrations (version) VALUES ('20131002163513');
INSERT INTO schema_migrations (version) VALUES ('20131003201908');
INSERT INTO schema_migrations (version) VALUES ('20131010110159');
INSERT INTO schema_migrations (version) VALUES ('20131014163800');
INSERT INTO schema_migrations (version) VALUES ('20131015102451');
INSERT INTO schema_migrations (version) VALUES ('20131020114528');
INSERT INTO schema_migrations (version) VALUES ('20131022155909');
INSERT INTO schema_migrations (version) VALUES ('20131022163545');
INSERT INTO schema_migrations (version) VALUES ('20131030115017');
INSERT INTO schema_migrations (version) VALUES ('20131105094310');
INSERT INTO schema_migrations (version) VALUES ('20131105114048');
INSERT INTO schema_migrations (version) VALUES ('20131106112218');
INSERT INTO schema_migrations (version) VALUES ('20131118162045');
INSERT INTO schema_migrations (version) VALUES ('20131118162801');
INSERT INTO schema_migrations (version) VALUES ('20131120184535');
INSERT INTO schema_migrations (version) VALUES ('20131121152832');
INSERT INTO schema_migrations (version) VALUES ('20131121153234');
INSERT INTO schema_migrations (version) VALUES ('20131121183124');
INSERT INTO schema_migrations (version) VALUES ('20131121192243');
INSERT INTO schema_migrations (version) VALUES ('20131121192452');
INSERT INTO schema_migrations (version) VALUES ('20131125162625');
INSERT INTO schema_migrations (version) VALUES ('20131125164527');
INSERT INTO schema_migrations (version) VALUES ('20131126174218');
INSERT INTO schema_migrations (version) VALUES ('20131127083907');
INSERT INTO schema_migrations (version) VALUES ('20131127110327');
INSERT INTO schema_migrations (version) VALUES ('20131127184022');
INSERT INTO schema_migrations (version) VALUES ('20131205093752');
INSERT INTO schema_migrations (version) VALUES ('20131205100023');
INSERT INTO schema_migrations (version) VALUES ('20131210143538');
INSERT INTO schema_migrations (version) VALUES ('20131210150148');
INSERT INTO schema_migrations (version) VALUES ('20131210153823');
